<%@ Page Language="VB" CodeBehind="frm11_AdjDetail.aspx.vb" AutoEventWireup="false"
    Inherits="frm11_AdjDetail" MasterPageFile="~/Master/Inventory.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       
   
<%--     <script type="text/javascript">
         // test auto-ready logic - call corner before DOM is ready
         $('#ContentPlaceHolder1_pnlSearchCriteria').corner();
    </script>--%>

     <asp:Panel ID="pnBody" Style="z-index: 105; left: 45px; top: 130px; font-family: 'Microsoft Sans Serif';
        font-size: 8pt; position: relative;" runat="server" Width="870" Height="350" >
        <asp:Panel ID="pndetail" runat="server" Style="z-index: 105; left: 0px; top: 0px; font-family: 'Microsoft Sans Serif';
        font-size: 8pt; position: relative; width: 870px; height: 226px;" 
             SkinID="panelSearch">

        <table>
        
        <tr>
        <td>
        <asp:Label ID="Label10"  runat="server" Width="65px" Height="13px" 
                Font-Bold="True" >Category :</asp:Label>
        </td>
        <td>
        <asp:Label ID="txtCategory" runat="server" Width="218px" Height="18px"    
                TabIndex="52"></asp:Label>
        </td>
        <td>
        <asp:Label ID="Label2"  runat="server" Width="91px" Height="13px" Font-Bold="True" >Sub Category :</asp:Label>
        </td>
        <td>
        <asp:Label ID="txtSubCategory"  runat="server" Width="242px"    
                Height="18px" TabIndex="46"></asp:Label>
        </td>
        </tr>
        
        <tr style="margin-top: 5px; height:20px;">
        <td>
        <asp:Label ID="Label6"  runat="server" Width="86px"  Height="13px" Font-Bold="True">Unit Of Issue :</asp:Label>
        </td>
        <td>
        <asp:Label ID="txtUnitOfRec"  runat="server"   Width="130" Height="18px" TabIndex="79"></asp:Label>
        </td>
        <td>
        <asp:Label ID="Label8"   runat="server" 
            Width="50px" Font-Bold="True">Adj By :</asp:Label>
        </td>
        <td>
        <asp:Label ID="txtAdjBy"  runat="server" Width="130"  Height="18px" ></asp:Label>
        </td>
        </tr>
        <tr style="margin-top: 5px; height:20px;">
        <td>
        <asp:Label ID="Label1"  runat="server" Width="62px"  Height="13px" Font-Bold="True">Adj Date :</asp:Label>
        </td>
        <td>
        <asp:Label ID="txtAdjDate"  runat="server"    Width="130" Height="18px" TabIndex="102"></asp:Label>
        </td>
        <td>
        <asp:Label ID="Label7"  runat="server" Width="55px"  Height="21px" Font-Bold="True">Qty Adj :</asp:Label>
        </td>
        <td>
        <asp:Label ID="txtAdjQty"  runat="server"     Width="130" Height="18px" TabIndex="44"></asp:Label>
        </td>
         </tr>
         <tr style="margin-top: 5px; height:20px;">
         <td>
         <asp:Label ID="Label9"  runat="server" Width="67px"  Height="13px" Font-Bold="True">Value Adj :</asp:Label>
         </td>
         <td>
         <asp:Label ID="txtAdjValue"  runat="server"  Width="130" Height="18px"   TabIndex="92"></asp:Label>
         </td>
         <td>
        <asp:Label ID="Label14" runat="server"   Width="64px" Height="13px" Font-Bold="True">Unit Cost :</asp:Label>
        </td>
        <td>
        <asp:Label ID="txtUnitCost" runat="server"  Width="130" Height="18px"   TabIndex="48"></asp:Label>
        </td>
         </tr>
        </table>
        <table style="margin-top: 5px;">
        <tr >
        <td>
        <asp:Label ID="Label11" runat="server" Width="71px" 
                Height="13px" Font-Bold="True">Item Desc :</asp:Label>
        </td>
        <td>
        <asp:Label ID="txtItem"  runat="server" Width="560px"   
                Height="18px" TabIndex="47"></asp:Label>
        </td>
                
        </tr>
        <tr style="margin-top: 5px; height:20px;">
        <td>
        <asp:Label ID="Label16"  runat="server"  Width="85px" Height="13px" 
                Font-Bold="True">Adj Reas :</asp:Label>
        </td>
        <td>
        <asp:Label ID="txtAdjReas"  runat="server" Width="356"  Height="18px"   TabIndex="99"></asp:Label>
        </td>
        
        </tr>
        <tr style="margin-top: 5px; height:20px;">
        <td>
        <asp:Label ID="Label13"  runat="server" Width="74px"   Height="13px" 
                Font-Bold="True">Comments :</asp:Label>
        </td>
        <td>
        <asp:Label ID="txtComments"  runat="server"    Width="356"  Height="48" TabIndex="84"></asp:Label>
        </td>
        </tr>
        </table>
       
         
            <asp:ImageButton ID="btnClose" Style="z-index: 134; left: 9px;
            top:194px; position: absolute; " runat="server" 
             Text="Cancel" ImageUrl="~/Images/cancel_static.png"
                        CausesValidation="False"></asp:ImageButton>
     </asp:Panel>
        
       
 </asp:Panel>

</asp:Content>
