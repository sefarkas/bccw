Option Strict Off
Option Explicit On
Imports System.Data.SqlClient
Imports System
Imports DataAccess

Partial Class frm52_SearchAdjustments
    Inherits InventoryBase
    Protected designerPlaceholderDeclaration As Object
    Private ODDataSet As New DataSet()
    Private strSQLCriteria As String
    Public Sub RefreshDataGridView()
        PopulateDataGridView(dgvSearchResult, strSQLCriteria, ODDataSet)
        If ODDataSet.Tables(0).Rows.Count = 0 Then
            cutValidator.ErrorMessage = My.Resources.ErrorMessage.SEARCH_NORECORD
            cutValidator.IsValid = False
        End If
    End Sub

    Public Sub RefreshSearchForm()
        ' Populate the combo boxes to enable searching
        PopulateSearchCriteria(pnlSearchCriteria, "vwAdjustments", False)
        RefreshDataGridView()
    End Sub


    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        Me.InitializeComponent()
        MyBase.OnInit(e)
    End Sub
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        SetPageTitle("Inventory Adjustments - Search Screen")

        If Not Page.IsPostBack = True Then

            lblAdjDate2.Visible = False
            imgBtnRceiveDateTo.Visible = False
            txtToAdjDate.Visible = False
            populdatecontrols()
            RetainValue()

            SearchResult()
            If GetUserRole() = "View" Then
                btnAddNew.Enabled = False
                btnAuditList.Enabled = False
            Else
                If GetUserRole() = "Admin" Then
                    btnAddNew.Enabled = True
                    btnAuditList.Enabled = True
                Else
                    btnAddNew.Enabled = False
                    btnAuditList.Enabled = False
                End If
            End If

        End If


    End Sub
    Private Sub InitializeComponent()
        '  AddHandler Load, AddressOf Me.Page_Load
    End Sub
    Private Sub populdatecontrols()
        Dim Mode As String
        Mode = "Edit"

        LoadCatSubItem(cboCategory, cboSubCategory, cboItemName, GetCurrentPageName(), Mode)

        Dim strSql As SqlDataReader
        Dim adj As New AdjustmentDAO

        strSql = adj.GetAdjBy()
        PopulateComboBox(cboAdjBy, strSql)

        strSql = adj.GetAdjReason()
        PopulateComboBox(cboAdjReas, strSql)
    End Sub

    Protected Sub cboDateSearchAdjDate_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboDateSearchAdjDate.SelectedIndexChanged

        If cboDateSearchAdjDate.SelectedItem.Text = "Between" Then
            txtToAdjDate.Visible = True
            lblAdjDate2.Visible = True
            lblAdjDate2.Visible = True
            imgBtnRceiveDateTo.Visible = True
            txtToAdjDate.Text = Date.Now().Date
        Else
            '  cboDateSearchAdjDate.SelectedItem.Text = ""
            txtToAdjDate.Visible = False
            lblAdjDate2.Visible = False
            imgBtnRceiveDateTo.Visible = False
            lblAdjDate2.Visible = False
        End If
    End Sub


    Private Sub dgvSearchResult_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgvSearchResult.RowCommand
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If

      

        If e.CommandName = "btnSelect" Then
            RedirectTo("frm11_AdjDetail.aspx?ID=" + e.CommandArgument)
        End If
    End Sub

    Protected Sub dgvSearchResult_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvSearchResult.RowDataBound
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If
        If Not e.Row.RowIndex = -1 Then
            CType(e.Row.FindControl("btnSelect"), ImageButton).CommandArgument = e.Row.Cells(1).Text
        End If
    End Sub

    Protected Sub cutValidator_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cutValidator.ServerValidate
        args.IsValid = True
        If txtAdjDate.Text <> "" Then
            If Not IsDate(txtAdjDate.Text) Then
                cutValidator.ErrorMessage = My.Resources.ErrorMessage.SEARCH_VALIDDATE
                args.IsValid = False
                Exit Sub
            End If
        End If

        If txtToAdjDate.Text <> "" Then
            If Not IsDate(txtToAdjDate.Text) Then
                cutValidator.ErrorMessage = My.Resources.ErrorMessage.SEARCH_VALIDDATE
                args.IsValid = False
                Exit Sub
            End If
        End If
    End Sub
    Private Sub SearchResult()
        GetSortableData()
    End Sub

    Private Function GetSortableData() As DataTable
        Dim strsql As String
        Dim ODDataTable As New DataTable()

        strsql = " [AdjID],[Item],[Category],[SubCategory],[Adj Qty],[Adj Value],[Adj Date],[Adj By],[Adj Reas]"
        strSQLCriteria = GetSearchCriteria1(pnlSearchCriteria, "vw52AdjustmentSearch", strsql)

        If txtAdjDate.Text <> "" Then AddSqlForDateSearchforAdj("Adj Date", cboDateSearchAdjDate.SelectedItem.Text, txtAdjDate, txtToAdjDate, strSQLCriteria)
        If rbtAll.Checked = False Then
            If InStr(strSQLCriteria, "where") Then
                strSQLCriteria += " and "
            Else
                strSQLCriteria += " where "
            End If
            If Me.rbtPosAdj.Checked = True Then
                strSQLCriteria += "[Adj Value] > 0"
            Else
                strSQLCriteria += "[Adj Value] < 0"
            End If
        End If
        strSQLCriteria += " order by [Adj Date] desc, Shop, [Part No]"
        ODDataSet = PopulateDataGridViewRetDataSet(dgvSearchResult, strSQLCriteria, ODDataSet)

        With ODDataSet ' by Farkas 8Feb11
            If .Tables.Count = 0 OrElse .Tables(0).Rows.Count = 0 Then
                cutValidator.ErrorMessage = My.Resources.ErrorMessage.SEARCH_NORECORD
                cutValidator.IsValid = False
            Else
                dgvSearchResult.DataSource = .Tables(0)
                dgvSearchResult.DataBind()
                ODDataTable = ODDataSet.Tables(0)
            End If

            Return ODDataTable ' may be an empty object when no rows are found

        End With

      


    End Function

    Protected Sub dgvSearchResult_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles dgvSearchResult.PageIndexChanging
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If
        dgvSearchResult.DataSource = SortDataTable(GetSortableData(), True)
        dgvSearchResult.PageIndex = e.NewPageIndex
        dgvSearchResult.DataBind()
    End Sub

    Protected Sub dgvSearchResult_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgvSearchResult.Sorting
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If
        GridViewSortExpression = e.SortExpression
        Dim pageIndex As Integer = dgvSearchResult.PageIndex
        dgvSearchResult.DataSource = SortDataTable(GetSortableData(), False)
        dgvSearchResult.DataBind()
        dgvSearchResult.PageIndex = pageIndex
    End Sub


    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFind.Click
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If

        '*****************added to enter date search validations02/10/2001***************

        If (cboDateSearchAdjDate.SelectedItem.ToString().Trim <> "") Then
            If (cboDateSearchAdjDate.SelectedItem.ToString().Trim = "Between") Then
                If (String.IsNullOrEmpty(txtAdjDate.Text.Trim) Or String.IsNullOrEmpty(txtToAdjDate.Text.Trim)) Then
                    cutValidator.ErrorMessage = My.Resources.ErrorMessage.ADJ_SEARCHDT
                    cutValidator.IsValid = False
                End If

                If (Not (String.IsNullOrEmpty(txtAdjDate.Text.Trim)) And Not (String.IsNullOrEmpty(txtToAdjDate.Text.Trim))) Then
                    If (isFutureDate(txtAdjDate.Text.Trim) Or isFutureDate(txtToAdjDate.Text.Trim)) Then
                        cutValidator.ErrorMessage = My.Resources.ErrorMessage.ADJUST_FUTUREDATE
                        cutValidator.IsValid = False
                    ElseIf (DateRangeCheck(txtAdjDate.Text.Trim, txtToAdjDate.Text.Trim)) Then
                        cutValidator.ErrorMessage = My.Resources.ErrorMessage.ADJUST_DTRANGECHECK
                        cutValidator.IsValid = False

                    End If

                End If
            Else
                If (String.IsNullOrEmpty(txtAdjDate.Text.Trim)) Then
                    cutValidator.ErrorMessage = My.Resources.ErrorMessage.ADJUST_DTEMPTY
                    cutValidator.IsValid = False
                End If
                If (isFutureDate(txtAdjDate.Text.Trim)) Then
                    cutValidator.ErrorMessage = My.Resources.ErrorMessage.FUTURE_DT
                    cutValidator.IsValid = False

                End If
            End If
        End If
        If Page.IsValid Then
            SearchResult()

            Dim arrAdjustment As New ArrayList
            With arrAdjustment
                .Add(cboAdjBy.SelectedItem.Text.Trim)
                .Add(cboSubCategory.SelectedItem.Text.Trim)
                .Add(cboItemName.SelectedItem.Text.Trim)
                .Add(cboAdjReas.SelectedItem.Text.Trim)
                .Add(cboCategory.SelectedItem.Text.Trim)
                .Add(txtAdjDate.Text)
                .Add(cboDateSearchAdjDate.SelectedItem.Text)
            End With
    
            If (Session("arrAdjustment") Is Nothing) Then
                Session.Add("arrAdjustment", arrAdjustment)
            Else
                Session("arrAdjustment") = arrAdjustment
            End If
        End If

    End Sub


    Private Shared Function isFutureDate(ByVal candidate As String) As Boolean
        Dim future As DateTime
        If (DateTime.TryParse(candidate, future)) Then
            Return future > DateTime.Now
        Else
            Return False
        End If
    End Function

    Private Shared Function DateRangeCheck(ByVal FromDt As String, ByVal ToDt As String) As Boolean
        Return (Date.Compare(Date.Parse(FromDt), Date.Parse(ToDt)) > 0)
    End Function

    Private Sub btnAddNew_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddNew.Click
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If
        RedirectTo("frm4_AddInvAdj.aspx")
    End Sub

    Private Sub btnAuditList_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAuditList.Click
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If
        RedirectTo("../frmAuditListRep.aspx")
    End Sub

    Protected Sub ddlCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboCategory.SelectedIndexChanged

        Dim mode1 As String
        mode1 = "Edit"
        cboSubCategory.Items.Clear()
        cboItemName.Items.Clear()
        LoadCatSubItem(cboCategory, cboSubCategory, cboItemName, GetCurrentPageName(), mode1)
    End Sub

    Protected Sub ddlSubCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboSubCategory.SelectedIndexChanged
        Dim mode1 As String
        mode1 = "Edit"
        cboItemName.Items.Clear()
        LoadCatSubItem(cboCategory, cboSubCategory, cboItemName, GetCurrentPageName(), mode1)
    End Sub

    Private Sub cboItemName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboItemName.SelectedIndexChanged
        Dim mode1 As String
        mode1 = "Edit"
        cboCategory.Items.Clear()
        cboSubCategory.Items.Clear()
        LoadCatSubItem(cboCategory, cboSubCategory, cboItemName, GetCurrentPageName(), mode1)
    End Sub
    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnReset.Click
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If
        Dim Mode As String
        Mode = "Edit"
        cboCategory.Items.Clear()
        cboSubCategory.Items.Clear()
        cboItemName.Items.Clear()
        LoadCatSubItem(cboCategory, cboSubCategory, cboItemName, GetCurrentPageName(), Mode)

        Dim strSql As SqlDataReader
        Dim adj As New AdjustmentDAO

        strSql = adj.GetAdjBy()
        PopulateComboBox(cboAdjBy, strSql)

        strSql = adj.GetAdjReason()
        PopulateComboBox(cboAdjReas, strSql)
        cboDateSearchAdjDate.SelectedIndex = 0
        txtAdjDate.Text = ""
        txtToAdjDate.Text = ""

        rbtAll.Checked = True
        rbtNegAdj.Checked = False
        rbtPosAdj.Checked = False

        Session("arrAdjustment") = Nothing
        SearchResult()

    End Sub
    Private Sub RetainValue()
        Dim arrAdjustment As New ArrayList
        arrAdjustment = Session("arrAdjustment")
        If Not Session("arrAdjustment") Is Nothing Then
            cboAdjBy.SelectedIndex = cboAdjBy.Items.IndexOf(cboAdjBy.Items.FindByText(arrAdjustment(0)))
            cboSubCategory.SelectedIndex = cboSubCategory.Items.IndexOf(cboSubCategory.Items.FindByText(arrAdjustment(1)))
            cboItemName.SelectedIndex = cboItemName.Items.IndexOf(cboItemName.Items.FindByText(arrAdjustment(2)))
            cboAdjReas.SelectedIndex = cboAdjReas.Items.IndexOf(cboAdjReas.Items.FindByText(arrAdjustment(3)))
            cboCategory.SelectedIndex = cboCategory.Items.IndexOf(cboCategory.Items.FindByText(arrAdjustment(4)))
            txtAdjDate.Text = arrAdjustment(5)
            cboDateSearchAdjDate.SelectedIndex = cboCategory.Items.IndexOf(cboDateSearchAdjDate.Items.FindByText(arrAdjustment(6)))
        End If
    End Sub
End Class

