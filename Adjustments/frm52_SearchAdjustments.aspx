<%@ Page Language="VB" Theme="InvSkin" CodeBehind="frm52_SearchAdjustments.aspx.vb"
    AutoEventWireup="false" Inherits="frm52_SearchAdjustments" MasterPageFile="~/Master/Inventory.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<%--    <script type="text/javascript">
        // test auto-ready logic - call corner before DOM is ready
        $('#ContentPlaceHolder1_pnlSearchCriteria').corner();
    </script>--%>
    <asp:Panel ID="pnBody" Style="z-index: 105; left: 45px; top: 150px; font-family: 'Microsoft Sans Serif';
        font-size: 8pt; position: relative;" runat="server" Width="950" Height="550"
        DefaultButton="btnFind">
        <asp:Panel ID="Panel3" Style="z-index: 105; left: 0px; top: 110px; font-family: 'Microsoft Sans Serif';
            font-size: 8pt; position: absolute; width: 870px; height: 390px;" runat="server"
            ScrollBars="Auto">
            <asp:GridView ID="dgvSearchResult" SkinID="gridviewSkin" Style="z-index: 135; left: 8px;
                top: 110px; font-family: 'Microsoft Sans Serif'; font-size: 8pt;" runat="server"
                Width="864px" AllowSorting="true" AllowPaging="true" OnSorting="dgvSearchResult_Sorting"
                OnPageIndexChanging="dgvSearchResult_PageIndexChanging" TabIndex="17">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:ImageButton ID="btnSelect" TabIndex="18" runat="server" Text="View" SkinID="View"
                                CommandName="btnSelect" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:Panel>
        <asp:Panel ID="pnlSearchCriteria" Style="z-index: 108; left: 0px; top: 2px; font-family: 'Microsoft Sans Serif';
            font-size: 8pt; position: absolute; height: 104px; width: 870px;" runat="server"
            SkinID="panelSearch">
            <asp:Label ID="Label6" Style="z-index: 109; left: 452px; top: 2px; font-family: 'Verdana';
                font-size: 8pt; position: absolute; height: 13px;" runat="server" Width="63px"
                Font-Bold="True" Visible="False">Adj Type:</asp:Label>
            <asp:Panel ID="pnlAdjType" Style="z-index: 110; left: 518px; top: -6px; font-family: 'Microsoft Sans Serif';
                font-size: 8pt; position: absolute; width: 340px;" runat="server" Height="26">
                <asp:RadioButton ID="rbtAll" GroupName="radAdj" Checked="True" Style="z-index: 111;
                    left: 281px; top: 6px; font-family: 'Verdana'; font-size: 8pt; position: absolute;"
                    runat="server" Width="39" Height="17" Text="All" Font-Bold="True" TabIndex="6"
                    Visible="False"></asp:RadioButton>
                <asp:RadioButton ID="rbtPosAdj" GroupName="radAdj" Style="z-index: 112; left: 4px;
                    top: 6px; font-family: 'Verdana'; font-size: 8pt; position: absolute; width: 115px;"
                    runat="server" Height="17" Text="Receiving Adj" Font-Bold="True" TabIndex="4"
                    Visible="False"></asp:RadioButton>
                <asp:RadioButton ID="rbtNegAdj" GroupName="radAdj" Style="z-index: 113; left: 148px;
                    top: 6px; font-family: 'Verdana'; font-size: 8pt; position: absolute; width: 130px;"
                    runat="server" Height="17" Text="Dispensing Adj" Font-Bold="True" TabIndex="5"
                    Visible="False"></asp:RadioButton>
            </asp:Panel>
            <asp:DropDownList ID="cboDateSearchAdjDate" Style="z-index: 117; left: 301px; top: 50px;
                font-family: 'Verdana'; font-size: 8pt; position: absolute; height: 21px; width: 147px;"
                runat="server" AutoPostBack="True" TabIndex="9">
                <asp:ListItem Value=""></asp:ListItem>
                <asp:ListItem Value="0">Equal To</asp:ListItem>
                <asp:ListItem Value="1">Before</asp:ListItem>
                <asp:ListItem Value="2">After</asp:ListItem>
                <asp:ListItem Value="3">Between</asp:ListItem>
            </asp:DropDownList>
            <asp:TextBox ID="txtToAdjDate" Style="z-index: 115; left: 672px; top: 51px; font-family: 'Verdana';
                font-size: 8pt; position: absolute; width: 107px; height: 13px;" runat="server"
                TabIndex="12"></asp:TextBox>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="None"
                ErrorMessage="Invalid To Date." ControlToValidate="txtToAdjDate" ValidationExpression="((^(10|12|0?[13578])([/])(3[01]|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(11|0?[469])([/])(30|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(2[0-8]|1[0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(29)([/])([2468][048]00)$)|(^(0?2)([/])(29)([/])([3579][26]00)$)|(^(0?2)([/])(29)([/])([1][89][0][48])$)|(^(0?2)([/])(29)([/])([2-9][0-9][0][48])$)|(^(0?2)([/])(29)([/])([1][89][2468][048])$)|(^(0?2)([/])(29)([/])([2-9][0-9][2468][048])$)|(^(0?2)([/])(29)([/])([1][89][13579][26])$)|(^(0?2)([/])(29)([/])([2-9][0-9][13579][26])$))">
            </asp:RegularExpressionValidator>
            <asp:ImageButton ID="imgBtnRceiveDateTo" runat="server" SkinID="Calender" Style="left: 789px;
                top: 53px; font-family: 'Verdana'; font-size: 8pt; position: absolute; height: 19px;
                width: 18px;" TabIndex="13" />
            <cc1:CalendarExtender ID="calLotDate1" runat="server" PopupButtonID="imgBtnRceiveDateTo"
                TargetControlID="txtToAdjDate" />
            <asp:TextBox ID="txtAdjDate" Style="z-index: 119; left: 455px; top: 51px; font-family: 'Verdana';
                font-size: 8pt; position: absolute; right: 308px; height: 13px; width: 107px;"
                runat="server" TabIndex="10"></asp:TextBox>
            <asp:RegularExpressionValidator ID="ToDateRegExp" runat="server" Display="None" ErrorMessage="Invalid  Date."
                ControlToValidate="txtAdjDate" ValidationExpression="((^(10|12|0?[13578])([/])(3[01]|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(11|0?[469])([/])(30|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(2[0-8]|1[0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(29)([/])([2468][048]00)$)|(^(0?2)([/])(29)([/])([3579][26]00)$)|(^(0?2)([/])(29)([/])([1][89][0][48])$)|(^(0?2)([/])(29)([/])([2-9][0-9][0][48])$)|(^(0?2)([/])(29)([/])([1][89][2468][048])$)|(^(0?2)([/])(29)([/])([2-9][0-9][2468][048])$)|(^(0?2)([/])(29)([/])([1][89][13579][26])$)|(^(0?2)([/])(29)([/])([2-9][0-9][13579][26])$))">
            </asp:RegularExpressionValidator>
            <asp:ImageButton ID="imgBtnRceiveDateFrom" runat="server" SkinID="Calender" Style="left: 578px;
                top: 52px; font-family: 'Verdana'; font-size: 8pt; position: absolute; width: 18px;
                height: 19px;" TabIndex="11" />
            <cc1:CalendarExtender ID="calLotDate" runat="server" PopupButtonID="imgBtnRceiveDateFrom"
                TargetControlID="txtAdjDate" />
            <asp:Label ID="Label2" Style="z-index: 120; left: 2px; top: 53px; font-family: 'Verdana';
                font-size: 8pt; position: absolute; width: 73px;" runat="server" Height="13px"
                Font-Bold="True">Adj By:</asp:Label>
            <asp:DropDownList ID="cboAdjBy" Style="z-index: 121; left: 74px; top: 51px; font-family: 'Verdana';
                font-size: 8pt; position: absolute; height: 21px; width: 132px;" runat="server"
                TabIndex="8">
            </asp:DropDownList>
            <asp:Label ID="Label10" Style="z-index: 122; left: 213px; top: 53px; font-family: 'Verdana';
                font-size: 8pt; position: absolute; width: 86px;" runat="server" Height="13px"
                Font-Bold="True">Adj Date:</asp:Label>
            <%--<asp:DropDownList ID="cboShopName" Style="z-index: 123; left: 73px; top: 64px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="140" Height="21">
            </asp:DropDownList>--%>
            <asp:Label ID="Label12" Style="z-index: 124; left: 213px; top: 6px; font-family: 'Verdana';
                font-size: 8pt; position: absolute; width: 92px; right: 565px;" runat="server"
                Height="13px" Font-Bold="True">SubCategory:</asp:Label>
            <asp:DropDownList ID="cboSubCategory" AutoPostBack="true" Style="z-index: 125; left: 301px;
                top: 2px; font-family: 'Verdana'; font-size: 8pt; position: absolute; height: 21px;
                width: 147px;" runat="server" TabIndex="2">
            </asp:DropDownList>
            <asp:Label ID="Label11" Style="z-index: 126; left: 2px; top: 28px; font-family: 'Verdana';
                font-size: 8pt; position: absolute; width: 71px; height: 12px;" runat="server"
                Font-Bold="True">Item Desc:</asp:Label>
            <asp:DropDownList ID="cboItemName" AutoPostBack="true" Style="z-index: 127; left: 74px;
                top: 27px; font-family: 'Verdana'; font-size: 8pt; position: absolute; width: 373px;
                height: 21px;" runat="server" TabIndex="3">
            </asp:DropDownList>
            <asp:Label ID="Label5" Style="z-index: 115; left: 452px; top: 27px; font-family: 'Verdana';
                font-size: 8pt; position: absolute; width: 63px;" runat="server" Height="13px"
                Font-Bold="True">Adj Reas:</asp:Label>
            <asp:DropDownList ID="cboAdjReas" Style="z-index: 114; left: 518px; top: 27px; font-family: 'Verdana';
                font-size: 8pt; position: absolute; height: 21px; width: 316px;" runat="server"
                TabIndex="7">
            </asp:DropDownList>
            <%--<asp:Label ID="Label4" Style="z-index: 128; left: 12px; top: 67px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="41px" 
                Height="13px">Location:</asp:Label>--%>
            <asp:Label ID="Label3" runat="server" Height="13px" Style="z-index: 130; left: 2px;
                top: 6px; font-family: 'Verdana'; font-size: 8pt; position: absolute; width: 61px;"
                Font-Bold="True">Category:</asp:Label>
            <asp:DropDownList ID="cboCategory" AutoPostBack="true" Style="z-index: 129; left: 74px;
                top: 3px; font-family: 'Verdana'; font-size: 8pt; position: absolute; height: 21px;
                width: 132px;" runat="server" TabIndex="1">
            </asp:DropDownList>
            <asp:Label ID="lblAdjDate2" runat="server" Height="13px" Style="z-index: 116; left: 619px;
                top: 54px; font-family: 'Verdana'; font-size: 8pt; position: absolute;" Width="28px"
                Font-Bold="True">and</asp:Label>
            <asp:ImageButton ID="btnFind" runat="server" Style="position: absolute; top: 85px;
                left: 3px;" SkinID="findButton" TabIndex="14" />
            <asp:ImageButton ID="btnReset" runat="server" SkinID="resetButton" Style="position: absolute;
                top: 85px; left: 54px; right: 802px;" TabIndex="15" />
            <asp:ImageButton ID="btnAddNew" runat="server" SkinID="AddNewButton" Style="position: absolute;
                top: 85px; left: 157px;" TabIndex="16" />
            <asp:ImageButton ID="btnAuditList" runat="server" ImageUrl="~/Images/Audit_static.png"
                Style="position: absolute; top: 112px; left: 254px;" Visible="False" />
        </asp:Panel>
        <asp:CustomValidator ID="cutValidator" runat="server" Display="None"></asp:CustomValidator>
    </asp:Panel>
</asp:Content>
