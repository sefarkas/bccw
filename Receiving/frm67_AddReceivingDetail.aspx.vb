﻿Imports System.Data.SqlClient
Imports DataAccess
Imports DataAccess.ReceivingDAO
Imports Entities

Public Class frmAddReceivingDetail
    Inherits InventoryBase


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim RecTransID As String
        Dim ShopName As String
        Dim PartNo As String


        If Not Page.IsPostBack = True Then
            txtDateRec.Text = Date.Now().ToShortDateString()
            PopulateDropdowns()
            If Not Request("ID") Is Nothing AndAlso Request("ID").Length > 0 Then
                RecTransID = Request.QueryString("ID").Substring(0, (Request.QueryString("ID").IndexOf(",")))
                ShopName = Request.QueryString("ID").Substring(Request.QueryString("ID").IndexOf(",") + 1, ((Request.QueryString("ID").LastIndexOf(",")) - Request.QueryString("ID").IndexOf(",")) - 1)
                PartNo = Request.QueryString("ID").Substring(Request.QueryString("ID").LastIndexOf(",") + 1)

                hdnRecTransId.Value = RecTransID
                hdnShopName.Value = ShopName
                hdnPartNoEdit.Value = PartNo

                GetReceiveItem(RecTransID, ShopName, PartNo)
                cboCategory.Enabled = False
                cboSubCategory.Enabled = False
                cboDescription.Enabled = False
                txtPONo.Enabled = False
                txtComments.Enabled = False

                btnReset.Visible = False
                SetPageTitle("Receiving - Edit")

            Else
                SetPageTitle("Receiving - Add New")
                btnReset.Visible = True

            End If

            Randomize()


        End If


    End Sub
    Private Sub GetReceiveItem(ByVal ReceiveItemID As Integer, ByVal ShopName As String, ByVal PartNo As String)
        Dim ReceiveItem As ReceivingItem
        Dim ReceiveDAO As New ReceivingDAO
        ReceiveItem = ReceiveDAO.GetREceivedItemByReceiveItemID(ReceiveItemID, ShopName, PartNo)
        If Not ReceiveItem Is Nothing Then
            cboCategory.SelectedIndex = cboCategory.Items.IndexOf(cboCategory.Items.FindByText(ReceiveItem.Category.ToString.Trim()))
            cboSubCategory.SelectedIndex = cboSubCategory.Items.IndexOf(cboSubCategory.Items.FindByText(ReceiveItem.SubCategory.ToString.Trim()))
            cboDescription.SelectedIndex = cboDescription.Items.IndexOf(cboDescription.Items.FindByText(ReceiveItem.ItemName.ToString.Trim()))
            txtQty.Text = ReceiveItem.CQOH
            txtUnitSize.Text = ReceiveItem.UnitofRec
            txtComments.Text = ReceiveItem.Comments
            txtPartNo.Text = ReceiveItem.StockNo
            txtDateRec.Text = ReceiveItem.DateRec
            txtQtyRec.Text = ReceiveItem.QtyRec
            txtUnitCost.Text = Math.Round(CDec(ReceiveItem.UnitCost), 2).ToString()
            txtPONo.Text = ReceiveItem.PONO
        End If
    End Sub
    Private Sub PopulateDropdowns()

        Dim Mode As String
        If Not Request("ID") Is Nothing AndAlso Request("ID").Length > 0 Then
            Mode = "Edit"
        Else
            Mode = "Add"
        End If


        LoadCatSubItem(cboCategory, cboSubCategory, cboDescription, GetCurrentPageName(), Mode)

    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Response.Redirect("~\Receiving\frm57_SearchReceiving.aspx?Mode=edit")
    End Sub

    Protected Sub cboCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboCategory.SelectedIndexChanged
        Dim Mode As String
        If Not Request("ID") Is Nothing AndAlso Request("ID").Length > 0 Then
            Mode = "Edit"
        Else
            Mode = "Add"
        End If
        cboSubCategory.Items.Clear()
        cboDescription.Items.Clear()
        LoadCatSubItem(cboCategory, cboSubCategory, cboDescription, GetCurrentPageName(), Mode)

    End Sub

    Protected Sub cboSubCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboSubCategory.SelectedIndexChanged
        Dim Mode As String
        If Not Request("ID") Is Nothing AndAlso Request("ID").Length > 0 Then
            Mode = "Edit"
        Else
            Mode = "Add"
        End If
        cboDescription.Items.Clear()

        LoadCatSubItem(cboCategory, cboSubCategory, cboDescription, GetCurrentPageName(), Mode)

    End Sub
    Private Sub ValidateForm()

     
        If (txtDateRec.Text.ToString().Trim() = "") Then
            cutValidator.ErrorMessage = "Enter a Date" 'My.Resources.ErrorMessage.RECEIVE_DATERECEIVE
            cutValidator.IsValid = False
            Exit Sub
        ElseIf IsDate(txtDateRec.Text.Trim) = False Then
            cutValidator.ErrorMessage = "Please enter valid date." 'My.Resources.ErrorMessage.RECEIVE_DATERECEIVE
            cutValidator.IsValid = False
            Exit Sub
        ElseIf (IsDate(txtDateRec.Text.Trim) And isFutureDate(txtDateRec.Text.Trim)) Then
            cutValidator.ErrorMessage = "Receive date can not be future date."
            cutValidator.IsValid = False
            Exit Sub
        ElseIf IsTransactionPresent() = True Then
            Exit Sub
        End If
    End Sub
    Private Function IsTransactionPresent()

        If CDate(Convert.ToDateTime(txtDateRec.Text).ToShortDateString()) < CDate("07-01-2011") Then
            cutValidator.ErrorMessage = "Please select receiving date greater then or equal to 07-01-2011"
            cutValidator.IsValid = False
            Return True
        Else
            cutValidator.IsValid = True
            Return True
        End If

    

    End Function

  
    Private Shared Function isFutureDate(ByVal candidate As String) As Boolean
        Dim future As DateTime

        DateTime.TryParse(candidate, future)

        Return future > DateTime.Now
    End Function

    Protected Sub cboDescription_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboDescription.SelectedIndexChanged

        Dim Mode As String
        If Not Request("ID") Is Nothing AndAlso Not Request("ID") = "" Then
            Mode = "Edit"
        Else
            Mode = "Add"
        End If

        cboCategory.Items.Clear()
        cboSubCategory.Items.Clear()

        LoadCatSubItem(cboCategory, cboSubCategory, cboDescription, GetCurrentPageName(), Mode)

        Dim description As String
        Dim strSQL As SqlDataReader
        Dim recDAO As New ReceivingDAO

        description = cboDescription.Text.Trim()

        strSQL = recDAO.GetPartNoByDesc(description)

        While strSQL.Read()
            If Not strSQL.Item(0) Is System.DBNull.Value Then
                hdnPartNo.Value = strSQL.Item(0)
            End If
            If Not strSQL.Item(1) Is System.DBNull.Value Then
                txtPartNo.Text = strSQL.Item(1)
            End If
            If Not strSQL.Item(2) Is System.DBNull.Value Then
                txtUnitCost.Text = Math.Round(CDec(strSQL.Item(2)), 2).ToString()

            End If
            If Not strSQL.Item(3) Is System.DBNull.Value Then
                txtUnitSize.Text = strSQL.Item(3)
            End If
            If Not strSQL.Item(4) Is System.DBNull.Value Then
                txtQty.Text = strSQL.Item(4)
            End If
        End While
    End Sub
   

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        ValidateForm()

        If Page.IsValid = True Then

            Dim dbAccess As New ReceivingDAO

            If (txtQtyRec.Text.ToString().Trim() = "") Then
                cutValidator.ErrorMessage = My.Resources.ErrorMessage.RECEIVE_QTYREC
                cutValidator.IsValid = False
                Exit Sub
            ElseIf Not IsNumeric(txtQtyRec.Text.Trim()) Then
                cutValidator.ErrorMessage = My.Resources.ErrorMessage.RECEIVE_QTY
                cutValidator.IsValid = False
                Exit Sub
            ElseIf Not IsSmallint(txtQtyRec.Text.Trim()) Then
                cutValidator.ErrorMessage = My.Resources.ErrorMessage.Qtyvalue
                cutValidator.IsValid = False
                Exit Sub
            ElseIf CInt(txtQtyRec.Text.Trim() <= 0) Then
                cutValidator.ErrorMessage = My.Resources.ErrorMessage.RECEIVE__POSITIVEQTY
                cutValidator.IsValid = False
                Exit Sub
            ElseIf txtUnitCost.Text.Trim.Length = 0 Then
                cutValidator.ErrorMessage = "Please enter valid unit price greater then zero."
                cutValidator.IsValid = False
                Exit Sub
            ElseIf Not IsNumeric(txtUnitCost.Text) Then
                cutValidator.ErrorMessage = "Please enter valid unit price greater then zero."
                cutValidator.IsValid = False
                Exit Sub
            ElseIf txtUnitCost.Text <= 0 Then
                cutValidator.ErrorMessage = "Please enter valid unit price greater then zero."
                cutValidator.IsValid = False
                Exit Sub
            End If
            If Not Request("ID") Is Nothing AndAlso Not Request("ID") = "" Then
                'Updating the data
                Dim intResult As Integer
                intResult = dbAccess.UpdateReceivingItem(hdnRecTransId.Value, hdnShopName.Value, hdnPartNoEdit.Value, txtQtyRec.Text.Trim(), txtDateRec.Text, txtUnitCost.Text)
                If (intResult > 0) Then
                    cutValidator.ErrorMessage = My.Resources.ErrorMessage.COMMON_RECORDSAVED
                    cutValidator.IsValid = False
                Else
                    cutValidator.ErrorMessage = My.Resources.ErrorMessage.RECEIVE__POSITIVEQTYPOS
                    cutValidator.IsValid = False
                End If


            Else

                If (cboDescription.SelectedItem.ToString().Trim() = "") Then
                    cutValidator.ErrorMessage = My.Resources.ErrorMessage.RECEIVE_DESCRIPTION
                    cutValidator.IsValid = False
                    Exit Sub
                ElseIf (txtDateRec.Text.ToString().Trim() = "") Then
                    cutValidator.ErrorMessage = My.Resources.ErrorMessage.RECEIVE_DATERECEIVE
                    cutValidator.IsValid = False
                    Exit Sub


                ElseIf (txtPONo.Text.ToString().Trim() = "") Then
                    cutValidator.ErrorMessage = My.Resources.ErrorMessage.RECEIVE_PONo
                    cutValidator.IsValid = False
                    Exit Sub


                Else

                    Dim randObj As New Random
                    Dim RandomNumber As Integer
                    RandomNumber = randObj.Next()


                    dbAccess.AddNewReceivingItem(txtQtyRec.Text.Trim(), txtDateRec.Text.Trim(), txtPONo.Text.Trim(), GetUserName(), txtComments.Text.Trim(), RandomNumber, hdnPartNo.Value, txtUnitCost.Text)

                    cutValidator.ErrorMessage = My.Resources.ErrorMessage.COMMON_RECORDSAVED
                    cutValidator.IsValid = False
                    ClearFields()

                End If
            End If
        End If
    End Sub


    Protected Sub btnAddNew_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddNew.Click
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If
        Session("Mode") = "Add"
        RedirectTo("../Admin/frm07_AddNewPart.aspx")
    End Sub

    Private Sub btnReset_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnReset.Click
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If

        ClearFields()

    End Sub

    Private Sub ClearFields()

        cboCategory.Items.Clear()
        cboSubCategory.Items.Clear()
        cboDescription.Items.Clear()
        PopulateDropdowns()
        txtQty.Text = ""
        txtUnitSize.Text = ""
        txtComments.Text = ""
        txtPartNo.Text = ""
        txtQtyRec.Text = ""
        txtUnitCost.Text = ""
        txtPONo.Text = ""
    End Sub
    Private Function IsSmallint(ByVal obj As Object) As Boolean
        Dim bl As Boolean

        Dim i As Integer
        Try
            i = Convert.ToInt16(obj)
            bl = True

        Catch
            bl = False
        End Try

        Return bl
    End Function

    Protected Sub btnSave_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs)

    End Sub
End Class