<%@ Page Language="VB" CodeBehind="frm38_ReceivingDetail.aspx.vb" AutoEventWireup="false"
    Inherits="frmReceivingDetail" MasterPageFile="~/Master/Inventory.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<%--    <script type="text/javascript">
        // test auto-ready logic - call corner before DOM is ready
        $('#ContentPlaceHolder1_pnlDetail').corner();
    </script>--%>
    <asp:Panel ID="pnBody" Style="z-index: 105; left: 45px; top: 15px; font-family: 'Microsoft Sans Serif';
        font-size: 8pt; position: relative; width: 876px;" runat="server" 
        Height="300">
        <asp:Panel ID="pnlDetail" Style="z-index: 102; left: 0px; font-family: 'Microsoft Sans Serif';
            font-size: 8pt; position: absolute; height: 182px; top: 0px;width: 870px;" 
            runat="server" SkinID="panelSearch">
            <table style="height: 163px">
                <tr>
                    <td>
                        <asp:Label ID="Label10" Font-Bold="true" runat="server" >Category:</asp:Label>
                    </td>
                    <td>
                        <%--  <asp:TextBox ID="txtCategory" ReadOnly="true" runat="server"></asp:TextBox>--%>
                        <asp:Label ID="lblCategory" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="Label2" runat="server" Font-Bold="true" >Sub Category:</asp:Label>
                    </td>
                    <td>
                        <%--<asp:TextBox ID="txtSubCategory" ReadOnly="true" runat="server"></asp:TextBox>--%>
                        <asp:Label ID="lblSubCategory" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label25" Font-Bold="true" runat="server" >Item Desc:</asp:Label>
                    </td>
                    <td>
                        <%--   <asp:TextBox ID="txtDescription" ReadOnly="true" runat="server"> </asp:TextBox>--%>
                        <asp:Label ID="lblDescription" runat="server" Width="200px" ></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="Label24"  Font-Bold="true" runat="server" >Stock No:</asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblfldStockNo" runat="server"></asp:Label>
                        <%--<asp:TextBox ID="txtStockNo" ReadOnly="true" runat="server"></asp:TextBox>--%>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label9" Font-Bold="true" runat="server" >Qty On Hand:</asp:Label>
                    </td>
                    <td>
                        <%-- <asp:TextBox ID="txtUnitOfRec" ReadOnly="true" runat="server"></asp:TextBox>--%>
                        <asp:Label ID="lblCQOH" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="labelUnitSize" Font-Bold="true" runat="server" >Unit Size:</asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblUnitofRec" runat="server"></asp:Label>
                        <%--  <asp:TextBox ID="txtUnitSize" ReadOnly="true" runat="server"></asp:TextBox>--%>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label5" Font-Bold="true" runat="server" >Item Description:</asp:Label>
                    </td>
                    <td colspan="3">
                        <%--    <asp:TextBox ID="txtComments" ReadOnly="true" Width="300px" runat="server"> </asp:TextBox>--%>
                        <asp:Label ID="lblComments" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label8" Font-Bold="true" runat="server" >Date:</asp:Label>
                    </td>
                    <td>
                        <%--  <asp:TextBox ID="txtDateRec" ReadOnly="true" runat="server"></asp:TextBox>--%>
                        <asp:Label ID="lblDateRec" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="Label4" Font-Bold="true" runat="server" >Qty Receive:</asp:Label>
                    </td>
                    <td>
                        <%-- <asp:TextBox ID="txtQtyRec" ReadOnly="true" runat="server"></asp:TextBox>--%>
                        <asp:Label ID="lblQtyRec" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label14" Font-Bold="true" runat="server" >Unit Price:</asp:Label>
                    </td>
                    <td>
                        <%-- <asp:TextBox ID="txtUnitCost" ReadOnly="true" runat="server"></asp:TextBox>--%>
                        <asp:Label ID="lblUnitCost" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="Label6" runat="server"  Font-Bold="true">PO No:</asp:Label>
                    </td>
                    <td>
                        <%--   <asp:TextBox ID="txtPONo" ReadOnly="true" runat="server"></asp:TextBox>--%>
                        <asp:Label ID="lblPONo" runat="server"></asp:Label>
                    </td>
                </tr>
               
            </table>
             <asp:ImageButton ID="btnClose" runat="server" SkinID="CancelButton" />
            <asp:TextBox ID="txtItemTotal" Style="z-index: 102; left: 309px; top: 321px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="20"
                Visible="False"></asp:TextBox>
            <asp:Label ID="Label23" Style="z-index: 103; left: 240px; top: 324px; position: absolute;"
                runat="server" Width="71px" Height="13px" Visible="False">Item Total:</asp:Label>
            <asp:TextBox ID="txtQOH" Style="z-index: 104; left: 309px; top: 386px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="23"
                Visible="False"></asp:TextBox>
            <asp:TextBox ID="txtAddDate" Style="z-index: 105; left: 85px; top: 386px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="127" 
                Height="21" TabIndex="22" Visible="False"></asp:TextBox>
            <asp:Label ID="Label20" Style="z-index: 106; left: 273px; top: 389px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="38px" Height="13px"
                Visible="False">QOH:</asp:Label>
            <asp:TextBox ID="txtDateOrd" Style="z-index: 107; left: 85px; top: 267px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="15"
                Visible="False"></asp:TextBox>
            <asp:TextBox ID="txtReqNumber" Style="z-index: 109; left: 85px; top: 37px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="2"
                Visible="False"></asp:TextBox>
            <asp:TextBox ID="txtVendorName" Style="z-index: 110; left: 85px; top: 240px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="354" Height="21" TabIndex="14"
                Visible="False"></asp:TextBox>
            <asp:TextBox ID="txtQtyOrd" Style="z-index: 111; left: 309px; top: 267px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="16"
                Visible="False"></asp:TextBox>
            <asp:Label ID="Label16" Style="z-index: 112; left: 4px; top: 40px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="83px" Height="13px"
                Visible="False">Req Number:</asp:Label>
            <asp:Label ID="Label17" Style="z-index: 114; left: 228px; top: 270px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="83px" Height="13px"
                Visible="False">Qty Ordered:</asp:Label>
            <asp:Label ID="Label19" Style="z-index: 116; left: 11px; top: 270px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="76px" Height="13px"
                Visible="False">Order Date:</asp:Label>
            <asp:TextBox ID="txtRecBy" Style="z-index: 117; left: 309px; top: 10px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="1"
                Visible="False">JLomonaco</asp:TextBox>
            <asp:Label ID="Label7" Style="z-index: 118; left: 259px; top: 13px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="52px" Height="13px"
                Visible="False">Rec By:</asp:Label>
            <asp:TextBox ID="txtSINo" Style="z-index: 119; left: 309px; top: 37px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="3"
                Visible="False"></asp:TextBox>
            <asp:Panel ID="pnlPartInfo" Style="z-index: 122; left: 7px; top: 64px; font-family: 'Microsoft Sans Serif';
                font-size: 8pt; position: absolute;" runat="server" Width="442" Height="170">
                <asp:TextBox ID="txtCQOH" Style="z-index: 123; left: 301px; top: 141px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="13"
                    Visible="False"></asp:TextBox>
                <asp:Label ID="Label22" Style="z-index: 124; left: 256px; top: 144px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute;" runat="server" Width="47" Height="13" Visible="false">CQOH:</asp:Label>
                <asp:TextBox ID="txtPartNo" Style="z-index: 125; left: 301px; top: 6px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" Visible="false"
                    TabIndex="6"></asp:TextBox>
                <asp:TextBox ID="txtShopName" Style="z-index: 126; left: 77px; top: 6px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="5"
                    Visible="False"></asp:TextBox>
                <asp:TextBox ID="txtItemType" Style="z-index: 128; left: 77px; top: 60px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute; right: 235px;" runat="server" 
                    Width="130" Height="21" TabIndex="9"
                    Visible="False"></asp:TextBox>
                <asp:TextBox ID="txtSubItemType" Style="z-index: 131; left: 301px; top: 60px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="10"
                    Visible="False"></asp:TextBox>
                <asp:Label ID="Label1" Style="z-index: 132; left: 38px; top: 9px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute;" runat="server" Width="41px" Height="13px"
                    Visible="False">Shop:</asp:Label>
                <asp:Label ID="Label11" Style="z-index: 134; left: 8px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute;" runat="server" Width="71px" Height="13px"
                    Visible="False">Item Type:</asp:Label>
                <asp:Label ID="Label3" Style="z-index: 136; left: 249px; top: 9px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute;" runat="server" Width="54" Height="13" Visible="false">Part No:</asp:Label>
                <asp:Label ID="Label12" Style="z-index: 139; left: 206px; top: 53px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute; width: 108px;" runat="server" Height="13px"
                    Visible="False">Sub Item Type:</asp:Label>
            </asp:Panel>
            <asp:Label ID="Label18" Style="z-index: 141; left: 34px; top: 243px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="53px" Height="13px"
                Visible="False">Vendor:</asp:Label>
            <asp:Label ID="Label15" Style="z-index: 142; left: 267px; top: 40px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="44px" Height="13px"
                Visible="False">SI No:</asp:Label>
            <asp:Label ID="Label21" Style="z-index: 148; left: 0px; top: 389px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="87px" 
                Height="13px" Visible="False">Date Entered:</asp:Label>
            <%--   <asp:Label ID="lblRecNo" Style="z-index: 149; left: 176px; top: 511px; font-family: 'Verdana';
            font-size: 8pt; position: absolute; height: 16px;" runat="server" Width="39px"
            Visible="False"></asp:Label>--%>
            <asp:Button ID="btnFirst" runat="server" Text="<<" Style="z-index: 134; left: 142px;
                top: 477px; position: absolute; width: 56px;" Height="21" Visible="False" />
            <asp:Button ID="btnPrevious" runat="server" Text="<" Style="z-index: 134; left: 63px;
                top: 478px; position: absolute; width: 79px;" Height="21" Visible="False" />
            <asp:Button ID="btnNext" runat="server" Text=">" Style="z-index: 134; left: 225px;
                top: 477px; position: absolute; width: 62px;" Height="21" Visible="False" />
            <asp:Button ID="btnLast" runat="server" Text=">>" Style="z-index: 134; left: 289px;
                top: 477px; position: absolute; width: 62px;" Height="21" Visible="False" />
        </asp:Panel>
    </asp:Panel>
</asp:Content>
