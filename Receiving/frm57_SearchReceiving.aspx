<%@ Page Language="VB" Theme="InvSkin" CodeBehind="frm57_SearchReceiving.aspx.vb"
    AutoEventWireup="false" Inherits="frm57_SearchReceiving" MasterPageFile="~/Master/Inventory.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style2
        {
            width: 68px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<%--    <script type="text/javascript">
        // test auto-ready logic - call corner before DOM is ready
        $('#ContentPlaceHolder1_pnlSearchCriteria').corner();
    </script>--%>
    <asp:Panel ID="pnBody" Style="z-index: 105; left: 45px; top: 130px; font-family: 'Microsoft Sans Serif';
        font-size: 8pt; position: relative; height: 500px;" runat="server" Width="890" DefaultButton="btnFind">
        
        <asp:Panel ID="Panel3" Style="z-index: 137; left: 0px; top: 140px; font-family: 'Verdana';
            font-size: 8pt; position: absolute; height: 350px;" runat="server" Width="871"
            ScrollBars="Auto">
            <asp:GridView ID="dgvSearchResult" SkinID="gridviewSkin" Style="z-index: 135; left: 6px;
                top: 2px; font-family: 'Microsoft Sans Serif'; font-size: 8pt; position: absolute;"
                AllowSorting="true" AllowPaging="true" OnSorting="dgvSearchResult_Sorting" runat="server"
                OnPageIndexChanging="dgvSearchResult_PageIndexChanging" TabIndex="13">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:ImageButton ID="btnSelect" runat="server" Text="View" SkinID="View" CommandName="Select"
                                TabIndex="14" CommandArgument='<%# Eval("RecTransID").ToString()+ "," +Eval("Shop Name").ToString()+","+Eval("Part No").ToString()   %>' />
                            <%--     <asp:ImageButton ID="btnSelect" runat="server" Text="View" SkinID="View" CommandName="Select"
                                CommandArgument='<%# Eval("RecTransID").ToString() %>' />--%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="btnEdit" runat="server" Text="Edit" ImageUrl="~/Images/edit_static.png"
                                TabIndex="15" CommandName="Edit" CommandArgument='<%# Eval("RecTransID").ToString()+ "," +Eval("Shop Name").ToString()+","+Eval("Part No").ToString()   %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%--   <asp:TemplateField>
                        <ItemTemplate>
                            <asp:ImageButton ID="btnRecInv" runat="server" Text="Receive" ImageUrl="~/Images/Receive_static.png"
                                CommandName="RecInv" />
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                    <%--  <asp:TemplateField Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lblRecTRansID" runat="server" Text='<%# Eval("RecTransID") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                        <asp:BoundField DataField="PO No" SortExpression="PO No"   HeaderText="PO No" />
                        <asp:BoundField DataField="Date Rec" SortExpression="Date Rec"   HeaderText="PO No"  />
                                                <asp:BoundField DataField="Rec By" SortExpression="Rec By"   HeaderText="Received By"  />
                    --%>
                </Columns>
            </asp:GridView>
        </asp:Panel>
        <asp:Panel ID="Panel1" Style="z-index: 108; left: 0px; top: 25px; font-family: 'Microsoft Sans Serif';
            font-size: 8pt; position: absolute; height: 104px; width: 870px;" runat="server"
            SkinID="panelSearch">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="pnlSearchCriteria" SkinID="panelSearch" runat="server"
                            style="width: 871px; height: 113px; ">
                            <table border="0px">
                                <tr>
                                    <td class="style2">
                                        <asp:Label ID="Label6" Font-Bold="true" runat="server">PO No:</asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="cboPONo" runat="server" Height="21px" Width="170px" TabIndex="1">
                                        </asp:DropDownList>
                                    </td>
                                    <%--    <asp:Label ID="Label4" runat="server" Width="41" Height="13">Location:</asp:Label>--%>
                                    <%--<asp:DropDownList ID="cboLocation" runat="server" AutoPostBack="True">
                                </asp:DropDownList>--%>
                                    <%--   <asp:DropDownList ID="cboLocation" runat="server" AutoPostBack="True">
                                </asp:DropDownList>--%>
                                    <td>
                                        <asp:Label ID="Label9" Font-Bold="true" runat="server">Received By:</asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="cboRecBy" runat="server" Width="128px" Height="22px" TabIndex="2">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:Label ID="Label15" Font-Bold="true" runat="server">Receive Date:</asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtReceiveDate" Width="70px" runat="server" Height="16px" TabIndex="3"></asp:TextBox>
                                        <asp:ImageButton ID="imgBtnRceiveDateFrom" runat="server" SkinID="Calender" TabIndex="4" />
                                        <cc1:CalendarExtender ID="calLotDate" runat="server" PopupButtonID="imgBtnRceiveDateFrom"
                                            TargetControlID="txtReceiveDate" PopupPosition="TopRight" />
                                        <asp:RegularExpressionValidator ID="RegExpReceiveDt" runat="server" Display="None"
                                            ErrorMessage="Invalid From Date." ControlToValidate="txtReceiveDate" ValidationExpression="((^(10|12|0?[13578])([/])(3[01]|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(11|0?[469])([/])(30|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(2[0-8]|1[0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(29)([/])([2468][048]00)$)|(^(0?2)([/])(29)([/])([3579][26]00)$)|(^(0?2)([/])(29)([/])([1][89][0][48])$)|(^(0?2)([/])(29)([/])([2-9][0-9][0][48])$)|(^(0?2)([/])(29)([/])([1][89][2468][048])$)|(^(0?2)([/])(29)([/])([2-9][0-9][2468][048])$)|(^(0?2)([/])(29)([/])([1][89][13579][26])$)|(^(0?2)([/])(29)([/])([2-9][0-9][13579][26])$))">
                                        </asp:RegularExpressionValidator>
                                        <asp:Label ID="lblReceiveDate2" runat="server" Font-Bold="true">and</asp:Label>
                                        <asp:TextBox ID="txtToReceiveDate" Width="70px" runat="server" Height="16px" TabIndex="5"></asp:TextBox>
                                        <asp:ImageButton ID="imgBtnRceiveDateTo" runat="server" SkinID="Calender" TabIndex="6" />
                                        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" PopupButtonID="imgBtnRceiveDateTo"
                                            TargetControlID="txtToReceiveDate" PopupPosition="TopRight" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="None"
                                            ErrorMessage="Invalid To Date." ControlToValidate="txtToReceiveDate" ValidationExpression="((^(10|12|0?[13578])([/])(3[01]|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(11|0?[469])([/])(30|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(2[0-8]|1[0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(29)([/])([2468][048]00)$)|(^(0?2)([/])(29)([/])([3579][26]00)$)|(^(0?2)([/])(29)([/])([1][89][0][48])$)|(^(0?2)([/])(29)([/])([2-9][0-9][0][48])$)|(^(0?2)([/])(29)([/])([1][89][2468][048])$)|(^(0?2)([/])(29)([/])([2-9][0-9][2468][048])$)|(^(0?2)([/])(29)([/])([1][89][13579][26])$)|(^(0?2)([/])(29)([/])([2-9][0-9][13579][26])$))">
                                        </asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style2">
                                        <asp:Label ID="lblCategory" Font-Bold="true" runat="server">Category:</asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="cboCategory" Width="170px" Height="22px" runat="server" AutoPostBack="True"
                                            TabIndex="7">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblSubCategory" Font-Bold="true" runat="server">Sub Category:</asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="cboSubCategory" Width="128px" runat="server" AutoPostBack="True"
                                            Height="22px" TabIndex="8">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                            <table border="0px">
                                <tr>
                                    <td>
                                        <asp:Label ID="Label13" Font-Bold="true" runat="server">Item Desc:</asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:DropDownList ID="txtDescription" Width="384px" runat="server" AutoPostBack="true"
                                            Height="22px" Style="margin-left: 7px" TabIndex="9">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                            <asp:Label ID="Label5" Style="z-index: 104; left: 282px; top: 69px; font-family: 'Verdana';
                                font-size: 8pt; position: absolute;" runat="server" Width="44px" Height="13px"
                                Visible="False">SI No:</asp:Label>
                            <asp:DropDownList ID="cboItemType" Style="z-index: 105; left: 88px; top: 39px; font-family: 'Verdana';
                                font-size: 8pt; position: absolute;" runat="server" Width="140" Height="21" AutoPostBack="True"
                                Visible="False">
                            </asp:DropDownList>
                            <asp:DropDownList ID="cboSINo" Style="z-index: 106; left: 359px; top: 84px; font-family: 'Verdana';
                                font-size: 8pt; position: absolute;" runat="server" Width="140" Height="21" Visible="False">
                            </asp:DropDownList>
                            <asp:Label ID="Label2" Style="z-index: 108; left: 4px; top: 104px; font-family: 'Verdana';
                                font-size: 8pt; position: absolute; width: 65px;" runat="server" Height="13px"
                                Visible="False">Req Number:</asp:Label>
                            <asp:DropDownList ID="cboReqNumber" Style="z-index: 110; left: 88px; top: 86px; font-family: 'Verdana';
                                font-size: 8pt; position: absolute;" runat="server" Width="140" Height="21" AutoPostBack="True"
                                Visible="False">
                            </asp:DropDownList>
                            <asp:DropDownList ID="cboSubItemType" Style="z-index: 112; left: 328px; top: 39px;
                                font-family: 'Verdana'; font-size: 8pt; position: absolute;" runat="server" Width="140"
                                Height="21" Visible="False">
                            </asp:DropDownList>
                            <asp:DropDownList ID="cboDateSearchReceiveDate" Style="z-index: 113; left: 328px;
                                top: 119px; font-family: 'Verdana'; font-size: 8pt; position: absolute; right: 482px;"
                                runat="server" Width="140" Height="21" AutoPostBack="True" Visible="false">
                                <asp:ListItem Value="0">Equal To</asp:ListItem>
                                <asp:ListItem Value="1">Before</asp:ListItem>
                                <asp:ListItem Value="2">After</asp:ListItem>
                                <asp:ListItem Value="3">Between</asp:ListItem>
                            </asp:DropDownList>
                            <asp:Label ID="Label11" Style="z-index: 114; left: 230px; top: 41px; font-family: 'Verdana';
                                font-size: 8pt; position: absolute;" runat="server" Width="97px" Height="13px"
                                Visible="False">Sub Item Type:</asp:Label>
                            <asp:Label ID="Label12" Style="z-index: 116; left: 17px; top: 41px; font-family: 'Verdana';
                                font-size: 8pt; position: absolute; height: 13px;" runat="server" Width="71px"
                                Visible="False">Item Type:</asp:Label>
                            <asp:Label ID="lblOrderDate2" Style="z-index: 117; left: 712px; top: 95px; font-family: 'Verdana';
                                font-size: 8pt; position: absolute;" runat="server" Width="28px" Height="13px"
                                Visible="False">and</asp:Label>
                            <%--<asp:TextBox id="txtDescription" style="z-index:118; left:541px; top:39px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="370" Height="21" TabIndex="8"></asp:TextBox>--%>
                            <%--  <asp:DropDownList ID="cboVendorName" Style="z-index: 120; left: 541px; top: 12px;
                        font-family: 'Verdana'; font-size: 8pt; position: absolute;" runat="server" Width="370"
                        Height="21" Visible="False">
                    </asp:DropDownList>--%>
                            <asp:DropDownList ID="cboDelivery" Style="z-index: 121; left: 88px; top: 91px; font-family: 'Verdana';
                                font-size: 8pt; position: absolute;" runat="server" Width="140" Height="21" Visible="False">
                            </asp:DropDownList>
                            <asp:Label ID="Label10" Style="z-index: 122; left: 487px; top: 15px; font-family: 'Verdana';
                                font-size: 8pt; position: absolute;" runat="server" Width="53px" Height="13px"
                                Visible="False">Vendor:</asp:Label>
                            <asp:DropDownList ID="cboDateSearchOrderDate" Style="z-index: 123; left: 328px; top: 92px;
                                font-family: 'Verdana'; font-size: 8pt; position: absolute;" runat="server" Width="140"
                                Height="21" AutoPostBack="True" Visible="False">
                                <asp:ListItem Value="0">Equal To</asp:ListItem>
                                <asp:ListItem Value="1">Before</asp:ListItem>
                                <asp:ListItem Value="2">After</asp:ListItem>
                                <asp:ListItem Value="3">Between</asp:ListItem>
                            </asp:DropDownList>
                            <asp:Label ID="Label7" Style="z-index: 124; left: 27px; top: 106px; font-family: 'Verdana';
                                font-size: 8pt; position: absolute;" runat="server" Width="60px" Height="13px"
                                Visible="False">Delivery:</asp:Label>
                            <asp:Label ID="Label8" Style="z-index: 126; left: 250px; top: 95px; font-family: 'Verdana';
                                font-size: 8pt; position: absolute;" runat="server" Width="76px" Height="13px"
                                Visible="False">Order Date:</asp:Label>
                            <asp:TextBox ID="txtOrderDate" Style="z-index: 128; left: 541px; top: 92px; font-family: 'Verdana';
                                font-size: 8pt; position: absolute;" runat="server" Width="135" Height="21" TabIndex="14"
                                Visible="False"></asp:TextBox>
                            <asp:TextBox ID="txtToOrderDate" Style="z-index: 129; left: 776px; top: 92px; font-family: 'Verdana';
                                font-size: 8pt; position: absolute;" runat="server" Width="135" Height="21" TabIndex="15"
                                Visible="False"></asp:TextBox>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:CustomValidator ID="cutValidator" runat="server" Display="None"></asp:CustomValidator>
                <asp:ImageButton ID="btnFind" runat="server" SkinID="findButton" Style="position: absolute;
                    top: 91px; left: 3px;" TabIndex="10" />
                <asp:ImageButton ID="btnReset" runat="server" SkinID="resetButton" Style="position: absolute;
                    top: 91px; left: 55px;" TabIndex="11" />
                <asp:ImageButton ID="AddnewPO" Style="position: relative; top: -23px; left: 156px;
                    top: -22px;" SkinID="AddNewReceive" runat="server" TabIndex="12"></asp:ImageButton>

        </asp:Panel>

    </asp:Panel>
</asp:Content>
