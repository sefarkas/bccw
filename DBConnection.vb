﻿Imports System.Configuration



''' <summary>
''' This class provides functionality to fetches the connection string from the web.config file and 
''' establishes a connection to the database
''' </summary>
Public Class DBConnection
    ''' <summary>
    ''' This static method fetches the connection string from the web.config file and 
    ''' establishes a connection to the database
    ''' </summary>
    ''' <returns></returns>
    Public Shared Function GetConnection() As String
        Try

            Dim connections As ConnectionStringSettingsCollection = ConfigurationManager.ConnectionStrings
            Dim dsnyConn As [String] = String.Empty
            For Each connection As ConnectionStringSettings In connections
                If connection.Name = "DSNYCPConnectionString" Then
                    dsnyConn = connection.ConnectionString
                End If
            Next
            Return dsnyConn
        Catch ex As Exception
            Throw ' ex -- by Farkas 8Feb11
        End Try

    End Function

End Class

