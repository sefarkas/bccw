<%@ Page Language="VB" CodeBehind="frmReportMenu.aspx.vb" AutoEventWireup="false"
    Inherits="frmReportMenu" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Inventory Reports</title>
    <meta name="GENERATOR" content="form.suite4.net">
    <meta name="CODE_LANGUAGE" content="VB">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
</head>
<body ms_positioning="GridLayout">
    <form id="frmReportMenu" method="post" runat="server">
  <%--  <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>--%>
    <table>
        <tr>
            <td>
                <asp:Panel ID="Panel2" Style="z-index: 101; left: 12px; top: 12px; font-family: 'Microsoft Sans Serif';
                    font-size: 8pt; position: absolute;" runat="server" Width="495" Height="54">
                    <asp:Label ID="Label1" Style="z-index: 102; left: 7px; top: 11px; font-family: 'Verdana';
                        font-size: 15pt; position: absolute;" runat="server" Width="340" Height="25">Inventory Reports - Main Menu</asp:Label>
                    <asp:Button ID="btnItemSnapshot" Style="z-index: 149; left: 26px; top: 118px; font-family: 'Verdana';
                        font-size: 8pt; position: absolute; width: 177px; height: 27px; right: 292px;"
                        runat="server" Text="Item Snapshot" />
                    <asp:Button ID="btnPartListing" Style="z-index: 149; left: 19px; top: 159px; font-family: 'Verdana';
                        font-size: 8pt; position: absolute; height: 24px; width: 187px;" runat="server"
                        Text="Part Listing" />
                    <asp:Button ID="Button1" Style="z-index: 149; left: 270px; top: 156px; font-family: 'Verdana';
                        font-size: 8pt; position: absolute; width: 146px; height: 25px;" runat="server"
                        Text="Orders" />
                    <asp:Button ID="btnInactiveParts" Style="z-index: 149; left: 19px; top: 197px; font-family: 'Verdana';
                        font-size: 8pt; position: absolute; width: 190px; height: 30px;" runat="server"
                        Text="Flagged Parts by Status" />
                    <asp:Button ID="btnReceiving" Style="z-index: 149; left: 266px; top: 197px; font-family: 'Verdana';
                        font-size: 8pt; position: absolute; width: 147px; height: 26px;" runat="server"
                        Text="Receiving Report" />
                    <asp:Button ID="btnDispensing" Style="z-index: 149; left: 21px; top: 242px; font-family: 'Verdana';
                        font-size: 8pt; position: absolute; width: 195px; height: 28px;" runat="server"
                        Text="Dispensing Report" />
                  <%--  <asp:Button ID="btnReOrdReviewRep" Style="z-index: 149; left: 272px; top: 240px;
                        font-family: 'Verdana'; font-size: 8pt; position: absolute; height: 26px; width: 149px;"
                        runat="server" Text="Re-Ord Review Report" />--%>
                    <asp:Button ID="btnDispWithoutWONo" Style="z-index: 149; left: 19px; top: 283px;
                        font-family: 'Verdana'; font-size: 8pt; position: absolute; height: 28px; width: 193px;"
                        runat="server" Text="Dispensed Parts without WO No" />
                    <asp:Button ID="btnAdjustments" Style="z-index: 149; left: 276px; top: 285px; font-family: 'Verdana';
                        font-size: 8pt; position: absolute; width: 147px; height: 25px;" runat="server"
                        Text="Adjustments" />
                   <%-- <asp:Button ID="btnVioDispensing" Style="z-index: 149; left: 22px; top: 328px; font-family: 'Verdana';
                        font-size: 8pt; position: absolute; width: 183px; height: 29px;" runat="server"
                        Text="Violation Related Dispensing" />--%>
                    <asp:Button ID="btnSalvage" Style="z-index: 149; left: 272px; top: 331px; font-family: 'Verdana';
                        font-size: 8pt; position: absolute; width: 152px; height: 29px;" runat="server"
                        Text="Salvage" />
                    <asp:Button ID="btnOrders" Style="z-index: 149; left: 268px; top: 114px; font-family: 'Verdana';
                        font-size: 8pt; position: absolute; width: 152px; height: 29px;" runat="server"
                        Text="Order Details" />
                </asp:Panel>
            </td>
        </tr>
        <%--<tr>
            <td>
                <rsweb:ReportViewer ID="ReportViewer1" runat="server" 
                    style="margin-right: 40px" Width="449px">
                </rsweb:ReportViewer>
            </td>
        </tr>--%>
    </table>
    </form>
</body>
</html>
