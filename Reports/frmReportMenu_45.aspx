<%@ Page Language="VB" CodeBehind="frmReportMenu_45.aspx.vb" AutoEventWireup="false"
    Inherits="frmReportMenu" MasterPageFile="~/Master/Inventory.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<%--    <script type="text/javascript">
        // test auto-ready logic - call corner before DOM is ready
        $('#ContentPlaceHolder1_pnBody').corner();
    </script>--%>

    <asp:Panel ID="pnBody" runat="server" Style="z-index: 102; left: 250px; font-size: 8pt;
        position: relative; height: 300px; top: 0px; width: 300px;" >
        <table width="50%" style="text-align: center" cellpadding="7" cellspacing="15">
            <tr>
                <td>
                    <asp:ImageButton ID="btn451_Issued" runat="server" SkinID="ImgBtnIssued" TabIndex="1" />
                </td>
                <td>
                    <asp:ImageButton ID="btn452_Receive" runat="server" SkinID="ImgBtnReceived" TabIndex="2" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:ImageButton ID="btn453_ItemQuan" runat="server" SkinID="ImgBtnItemQty" TabIndex="3" />
                </td>
                <td>
                    <asp:ImageButton ID="btn454_Location" SkinID="RptCQOHLocation" runat="server" TabIndex="4" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:ImageButton ID="btn455_MonthlyRpt" runat="server" SkinID="BtnMonthlyRpt" TabIndex="5" />
                    <%--<asp:Button ID="btnMonthlyRpt" runat="server" Text="Monthly" />--%>
                </td>
                <td>
                    <%--<asp:Button ID="btnFiscal" runat="server" Text="Fiscal" />--%>
                    <asp:ImageButton ID="btnJanDec" runat="server" TabIndex="5" SkinID="Rptfiscal_112" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:ImageButton ID="btnJunJUn" runat="server" TabIndex="5" SkinID="Rptfiscal_76" />
                </td>
                <td>
                    <asp:ImageButton ID="btnIssuedSlip" runat="server" TabIndex="5" SkinID="RptIssuedSlip" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:ImageButton ID="btnQTOHMInMax" runat="server" TabIndex="5" SkinID="RptqtyHandLess" />
                </td>
                <td>
                    <asp:ImageButton ID="btnAnnualAudit" runat="server" TabIndex="5" SkinID="RptAnnualAudit" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:ImageButton ID="btnItemSnapshot" Visible="false" ImageUrl="~/Images/Item_Snapshot_static.png"
                        runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:ImageButton ID="btnPartListing" Visible="false" ImageUrl="~/Images/Part_Listing_static.png"
                        runat="server" />
                </td>
                <td>
                    <asp:ImageButton ID="btnInactiveParts" Visible="false" ImageUrl="~/Images/Flagged_parts_static.png"
                        runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:ImageButton ID="btnReceiving" Visible="false" ImageUrl="~/Images/receiving_static.png"
                        runat="server" />
                </td>
                <td>
                    <asp:ImageButton ID="btnDispensing" Visible="false" ImageUrl="~/Images/Dispensing-Report_static.png"
                        runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:ImageButton ID="btnDispWithoutWONo" Visible="false" ImageUrl="~/Images/Dispensed-Parts-without-WO-No_static.png"
                        runat="server" />
                </td>
                <td>
                    <asp:ImageButton ID="btnAdjustments" Visible="false" ImageUrl="~/Images/adjustments_static.png"
                        runat="server" />
                </td>
            </tr>
        </table>
        <asp:Panel ID="Panel1" runat="server" Style="z-index: 105; left: 40px; top: 1px;
            font-family: 'Microsoft Sans Serif'; font-size: 8pt; position: relative;" Width="880"
            Height="370">

        </asp:Panel>

    </asp:Panel>

</asp:Content>
