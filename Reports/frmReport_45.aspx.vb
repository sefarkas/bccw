﻿Public Class frmReport_45
    Inherits InventoryBase
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.QueryString(0) = "/BCC_Warehouse_Reports/rpt451_BCCISSUED" Then
            SetPageTitle("Report : Issued")
        ElseIf Request.QueryString(0) = "/BCC_Warehouse_Reports/rpt452_BCCRECEIVED" Then
            SetPageTitle("Report : Received")
        ElseIf Request.QueryString(0) = "/BCC_Warehouse_Reports/rpt453_BCCITEM_QUAN" Then
            SetPageTitle("Report : Item Qty")
        ElseIf Request.QueryString(0) = "/BCC_Warehouse_Reports/rpt454_CQOHLocationMapping" Then
            SetPageTitle("Report : Location")
        ElseIf Request.QueryString(0) = "/BCC_Warehouse_Reports/rpt455_MonthlyReport" Then
            SetPageTitle("Report : Montly")
        ElseIf Request.QueryString(0) = "/BCC_Warehouse_Reports/rpt456_FiscalReport_JanDec" Then
            SetPageTitle("Report : Fiscal Jan-Dec")
        ElseIf Request.QueryString(0) = "/BCC_Warehouse_Reports/rpt456_FiscalReport_Junjun" Then
            SetPageTitle("Report : Fiscal July-Jun")
        ElseIf Request.QueryString(0) = "/BCC_Warehouse_Reports/rpt801_BCCISSUEDBySlip" Then
            SetPageTitle("Report : Issued Slip")
        ElseIf Request.QueryString(0) = "/BCC_Warehouse_Reports/rpt600_BCCQTYLESS" Then
            SetPageTitle("Report : QTOH(Quantity on hand)")
        ElseIf Request.QueryString(0) = "/BCC_Warehouse_Reports/AuditReport" Then
            SetPageTitle("Report : Audit Report")
        End If

        If Not Page.IsPostBack = True Then
            ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote
            ReportViewer1.ServerReport.ReportServerUrl = New Uri(Global.My.MySettings.Default.REPORTPATH)
            'ReportViewer1.ServerReport.ReportPath = "/BCC_Warehouse_Reports/rpt12Adjustments"
            ReportViewer1.ServerReport.ReportPath = Request.QueryString(0)
            ReportViewer1.ShowPageNavigationControls = True
            ReportViewer1.ShowParameterPrompts = True
            ReportViewer1.ShowPrintButton = True
            'ReportViewer1.ServerReport.Refresh()
        End If
    End Sub
End Class