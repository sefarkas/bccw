﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Security
Imports System.Web.Security
Imports System.Configuration
Imports System.Xml




Public Class SecurityUtility

    Public Sub New()
    End Sub
    ''' <summary>
    ''' This method redirects the form login page based on userName, Roles, PersistentCookie, Cookie
    ''' </summary>
    ''' <param name="userName"></param>
    ''' <param name="commaSeperatedRoles"></param>
    ''' <param name="createPersistentCookie"></param>
    ''' <param name="strCookiePath"></param>
    Private Shared Sub RedirectFromLoginPageMain(ByVal userName As String, ByVal commaSeperatedRoles As String, ByVal createPersistentCookie As Boolean, ByVal strCookiePath As String)
        SetAuthCookieMain(userName, commaSeperatedRoles, createPersistentCookie, strCookiePath)
        HttpContext.Current.Response.Redirect(FormsAuthentication.GetRedirectUrl(userName, createPersistentCookie))
    End Sub

    Public Shared Sub RedirectFromLoginPage(ByVal userName As String, ByVal commaSeparatedRoles As String, ByVal createPersistentCookie As Boolean, ByVal stringCookiePath As String)
        RedirectFromLoginPageMain(userName, commaSeparatedRoles, createPersistentCookie, stringCookiePath)
    End Sub
   
    Public Shared Sub RedirectFromLoginPage(ByVal userName As String, ByVal commaSeparatedRoles As String, ByVal createPersistentCookie As Boolean)
        RedirectFromLoginPageMain(userName, commaSeparatedRoles, createPersistentCookie, Nothing)
    End Sub
    ''' <summary>
    ''' This method set the form authentication cookie based on userName, Roles, PersistentCookie, Cookie 
    ''' </summary>
    ''' <param name="userName"></param>
    ''' <param name="commaSeperatedRoles"></param>
    ''' <param name="createPersistentCookie"></param>
    ''' <param name="strCookiePath"></param>
    ''' <returns></returns>
    Private Shared Function CreateAuthenticationTicket(ByVal userName As String, ByVal commaSeperatedRoles As String, ByVal createPersistentCookie As Boolean, ByVal strCookiePath As String) As FormsAuthenticationTicket
        Dim cookiePath As String = If(strCookiePath Is Nothing, FormsAuthentication.FormsCookiePath, strCookiePath)

        Dim expirationMinutes As Integer = GetCookieTimeoutValue()


        Dim ticket As New FormsAuthenticationTicket(1, userName, DateTime.Now, DateTime.Now.AddMinutes(expirationMinutes), createPersistentCookie, commaSeperatedRoles, _
         cookiePath)

        Return ticket
    End Function

    ''' <summary>
    ''' This method returns the Cookie time out value
    ''' The default is 30 minutes
    ''' </summary>
    ''' <returns></returns>
    Private Shared Function GetCookieTimeoutValue() As Integer
        Dim timeout As Integer = 30
        Dim webConfig As New XmlDocument()
        webConfig.Load(HttpContext.Current.Server.MapPath("~\web.config"))
        Dim node As XmlNode = webConfig.SelectSingleNode("/configuration/system.web/authentication/forms")
        If node IsNot Nothing AndAlso node.Attributes("timeout") IsNot Nothing Then
            timeout = Integer.Parse(node.Attributes("timeout").Value)
        End If

        Return timeout
    End Function

    Public Shared Sub SetAuthCookie(ByVal userName As String, ByVal commaSeparatedRoles As String, ByVal createPersistentCookie As Boolean)
        SetAuthCookieMain(userName, commaSeparatedRoles, createPersistentCookie, Nothing)
    End Sub

    Public Shared Sub SetAuthCookie(ByVal userName As String, ByVal commaSeparatedRoles As String, ByVal createPersistentCookie As Boolean, ByVal stringCookiePath As String)
        SetAuthCookieMain(userName, commaSeparatedRoles, createPersistentCookie, stringCookiePath)
    End Sub
    ''' <summary>
    ''' This method returns form authentication ticket with based on userName, Roles, PersistentCookie, Cookie 
    ''' </summary>
    ''' <param name="userName"></param>
    ''' <param name="commaSeperatedRoles"></param>
    ''' <param name="createPersistentCookie"></param>
    ''' <param name="strCookiePath"></param>
    Private Shared Sub SetAuthCookieMain(ByVal userName As String, ByVal commaSeperatedRoles As String, ByVal createPersistentCookie As Boolean, ByVal strCookiePath As String)
        Dim ticket As FormsAuthenticationTicket = CreateAuthenticationTicket(userName, commaSeperatedRoles, createPersistentCookie, strCookiePath)
        Dim encrypetedTicket As String = FormsAuthentication.Encrypt(ticket)

        If Not FormsAuthentication.CookiesSupported Then
            FormsAuthentication.SetAuthCookie(encrypetedTicket, createPersistentCookie)
        Else
            Dim authCookie As New HttpCookie(FormsAuthentication.FormsCookieName, encrypetedTicket)

            If ticket.IsPersistent Then
                authCookie.Expires = ticket.Expiration
            End If
            HttpContext.Current.Response.Cookies.Add(authCookie)
        End If
    End Sub

End Class
