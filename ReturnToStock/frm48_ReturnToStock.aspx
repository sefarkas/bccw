<%@ Page language="VB" Codebehind="frm48_ReturnToStock.aspx.vb" AutoEventWireup="false" Inherits="frmReturnToStock" MasterPageFile="~/Master/Inventory.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       <asp:Panel ID="pnBody" Style="z-index: 105; left: 40px; top: 1px; font-family: 'Microsoft Sans Serif';
        font-size: 8pt; position: relative; width: 877px;" runat="server" 
           Height="350"> 

<asp:Panel id="pnlDetail" style="z-index:101; left:0px; top:106px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:absolute;" runat="server" Width="773" Height="57"><asp:DropDownList id="cboReturnTo" style="z-index:102; left:12px; top:26px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="180" Height="21"></asp:DropDownList>
<asp:TextBox id="txtReturnDate" style="z-index:103; left:230px; top:26px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="140" Height="21" TabIndex="55"></asp:TextBox>
<asp:Label id="Label5" style="z-index:104; left:616px; top:10px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="68" Height="13">Disp Date:</asp:Label>
<asp:Label id="Label4" style="z-index:105; left:9px; top:10px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="68" Height="13">Return To:</asp:Label>
<asp:Label id="Label8" style="z-index:106; left:227px; top:10px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="81" Height="13">Return Date:</asp:Label>
<asp:TextBox id="txtWONo" style="z-index:107; left:403px; top:26px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="180" Height="21" TabIndex="43"></asp:TextBox>
<asp:Label id="Label2" style="z-index:108; left:400px; top:10px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="51" Height="13">WO No:</asp:Label>
<asp:TextBox id="txtDateDisp" style="z-index:109; left:619px; top:26px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="140" Height="21" TabIndex="38"></asp:TextBox>
</asp:Panel>
<asp:Panel id="Panel2" style="z-index:110; left:0px; top:169px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:absolute;" runat="server" Width="773" Height="296"><asp:Label id="lblRecCount" style="z-index:111; left:39px; top:250px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="241" Height="13">There are no items that can be returned.</asp:Label>
<asp:Label id="Label11" style="z-index:112; left:7px; top:7px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="120" Height="13">Items Dispensed:</asp:Label>
<asp:GridView id="dgvDispItems" style="z-index:113; left:10px; top:23px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:absolute;" runat="server" Width="750" Height="217"></asp:GridView>
</asp:Panel>
<asp:Panel id="pnlHeader" style="z-index:114; left:0px; top:12px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:absolute;" runat="server" Width="772" Height="88"><asp:Panel id="pnlAdjType" style="z-index:115; left:7px; top:5px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:absolute;" runat="server" Width="111" Height="77"><asp:RadioButton id="rbtByVehicleID" style="z-index:116; left:5px; top:50px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="103" Height="17"></asp:RadioButton>
<asp:RadioButton id="rbtByWONo" style="z-index:117; left:5px; top:5px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="79" Height="17"></asp:RadioButton>
<asp:RadioButton id="rbtByWorker" style="z-index:118; left:5px; top:27px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="86" Height="17"></asp:RadioButton>
</asp:Panel>
<asp:DropDownList id="cboWONo" style="z-index:119; left:128px; top:36px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="130" Height="21"></asp:DropDownList>
<asp:Label id="Label1" style="z-index:120; left:125px; top:20px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="53" Height="13">WO No:</asp:Label>
<asp:Label id="Label3" style="z-index:121; left:269px; top:20px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="142" Height="13">Disp Date (optional):</asp:Label>
<asp:DropDownList id="cboDateDisp" style="z-index:122; left:272px; top:36px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="130" Height="21"></asp:DropDownList>
<asp:Button ID="btnGo" Text="Go" 
        style="z-index:123; left:438px; top:36px; font-family:'Verdana'; font-size:8pt; position:absolute; width: 47px; right: 287px;" 
        runat="server" Height="21" />
        <asp:Button ID="btnProcess" Text="Process" 
        style="z-index:124; left:519px; top:36px; font-family:'Verdana'; font-size:8pt; position:absolute; width: 56px; right: 197px;" 
        runat="server" Height="21" />
        <asp:Button ID="btnCancel" Text="Cancel" 
        style="z-index:125; left:591px; top:41px; font-family:'Verdana'; font-size:8pt; position:absolute; width: 56px; right: 125px;" 
        runat="server" Height="21" />
        <asp:Label ID="lblMsg" Visible="false"  style="z-index:126; left:163px; top:63px; font-family:'Verdana'; font-size:8pt; position:absolute; width: 56px; right: 553px;" 
        runat="server" Height="21"></asp:Label>
</asp:Panel>
</asp:Panel>
</asp:Content>