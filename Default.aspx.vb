﻿Public Class cDefault
    Inherits InventoryBase

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim path As String = "~\inventory\" ' System.Web.HttpContext.Current.Server.MapPath("~")

        If Session("UserRole") = "Chief" Then
            Response.Redirect("~\Reports\" & "frmReportMenu_45.aspx", True)

        Else
            Response.Redirect(path & "frm54_SearchInventory.aspx", True)
        End If

        OnLoad(e)

    End Sub

End Class