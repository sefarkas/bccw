﻿Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Text

Public Class LocationDTO
    Dim IDlocation, ParentIDlocation As Int32
    Dim Namelocation, Codelocation, Namefieldlocation As String
       

    '/// <summary>
    '/// Public property
    '/// </summary>
    Public Property LocationID() As Integer
        Get
            Return IDlocation
        End Get
        Set(ByVal value As Integer)
            IDlocation = value
        End Set
    End Property

    Public Property LocationName() As String
        Get
            Return Namelocation
        End Get
        Set(ByVal value As String)
            Namelocation = value
        End Set
    End Property
     
    Public Property LocationCode() As String
        Get
            Return Codelocation
        End Get
        Set(ByVal value As String)
            Codelocation = value
        End Set
    End Property
    Public Property LocationParentID() As Integer
        Get
            Return ParentIDlocation
        End Get
        Set(ByVal value As Integer)
            ParentIDlocation = value
        End Set
    End Property

    

    Public Property FieldLocationName() As String
        Get
            Return Namefieldlocation
        End Get
        Set(ByVal value As String)
            Namefieldlocation = value
        End Set
    End Property

    
End Class
