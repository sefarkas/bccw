﻿Imports System.Security.Principal


Public Class Inventor
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

       
        Dim user As String

        If Context IsNot Nothing AndAlso Context.Session IsNot Nothing Then
            If Not IsNothing(Session("IsAuthenticated")) Then

                If Not IsNothing(Session("User")) Then
                    user = Session("User")
                Else
                    user = My.Resources.ErrorMessage.DEFAULT_USER
                End If
                ' **---***
                'by Farkas 30Nov10
                If Len(Trim(user.ToString & "")) > 0 Then _
                lblUser.Text = "Welcome " & user
                ' **---***
            Else
                FormsAuthentication.RedirectToLoginPage()
            End If
        Else
            FormsAuthentication.RedirectToLoginPage()
        End If

        Dim connection As String
        Dim server As String
        Dim database As String


        Dim connconnectionArray As String()
        connection = DBConnection.GetConnection()

        connconnectionArray = connection.Split(";")

        If connconnectionArray.Length > 0 Then

            Dim servername As String()
            Dim DatabaseName As String()
            servername = connconnectionArray(0).Split("=")
            server = servername(1)

            DatabaseName = connconnectionArray(1).Split("=")
            database = DatabaseName(1)
        Else
            server = ""
            database = ""
        End If

        Dim version As String
        version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString


        Me.lblMachineAndVersion.Text = System.Environment.MachineName.ToString.Trim() & " using BUILD NUMBER " & version & " and  " & server & "-" & database


        AddAttributesMouseActionForButton()
        SetCurrentPageImage()

        RoleAccess()
    End Sub

    Private Sub RoleAccess()
        If IsNothing(Session("UserRole")) Then
            btnadjustments.Visible = False
            btndispensing.Visible = False
            btnInventory.Visible = False
            btnReceiving.Visible = False
            btnReorder.Visible = False
            btnreports.Visible = False
            btnsalvage.Visible = False
            btnLocation.Visible = False
        Else
            If Session("UserRole") = "Chief" Then
                btnadjustments.Visible = False
                btndispensing.Visible = False
                btnInventory.Visible = False
                btnReceiving.Visible = False
                btnReorder.Visible = False
                btnreports.Visible = True
                btnsalvage.Visible = False
                btnLocation.Visible = False
            Else
                btnadjustments.Visible = True
                btndispensing.Visible = True
                btnInventory.Visible = True
                btnReceiving.Visible = True
                btnReorder.Visible = False
                btnreports.Visible = True
                btnsalvage.Visible = False
                btnLocation.Visible = True
            End If

        End If


    End Sub
    Public Function GetCurrentPageName() As String
        Dim sPath As String = System.Web.HttpContext.Current.Request.Url.AbsolutePath
        Dim oInfo As New System.IO.FileInfo(sPath)
        Dim sRet As String = oInfo.Name
        Return sRet
    End Function
    Private Sub SetCurrentPageImage()
        Dim pageName As String = GetCurrentPageName()
        If pageName = "frm4_AddInvAdj.aspx" OrElse pageName = "frm11_AdjDetail.aspx" OrElse pageName = "frm52_SearchAdjustments.aspx" Then
            btnadjustments.ImageUrl = "../Images/adjustments_selected.png"
            btnadjustments.Attributes.Remove("onmouseover")
            btnadjustments.Attributes.Remove("onmouseout")
        ElseIf pageName = "frm53_SearchDispensing.aspx" OrElse pageName = "frm20_Dispensing.aspx" OrElse pageName = "frm19_DispenseDetail.aspx" OrElse pageName = "frm18_Dispense.aspx" OrElse pageName = "frm03_AddDispItems.aspx" Then
            btndispensing.ImageUrl = "../Images/dispensing_selected.png"
            btndispensing.Attributes.Remove("onmouseover")
            btndispensing.Attributes.Remove("onmouseout")
        ElseIf pageName = "frm60_SelectAddMode.aspx" OrElse pageName = "frm54_SearchInventory.aspx" OrElse pageName = "frm28_MfrInfo.aspx" OrElse pageName = "frm26_Inventory.aspx" OrElse pageName = "frm14_AdminFunctions.aspx" Then
            btnInventory.ImageUrl = "../Images/Inventory_selected.png"
            btnInventory.Attributes.Remove("onmouseover")
            btnInventory.Attributes.Remove("onmouseout")
        ElseIf pageName = "frm37_Receive.aspx" OrElse pageName = "frm38_ReceivingDetail.aspx" OrElse pageName = "frm57_SearchReceiving.aspx" Then
            btnReceiving.ImageUrl = "../Images/receiving_selected.png"
            btnReceiving.Attributes.Remove("onmouseover")
            btnReceiving.Attributes.Remove("onmouseout")
        ElseIf pageName = "frmReOrderReview.aspx" OrElse pageName = "frmReOrdItem_09.aspx" OrElse pageName = "frmReOrdReviewDetail.aspx" Then
            btnReorder.ImageUrl = "../Images/reorder_selected.png"
            btnReorder.Attributes.Remove("onmouseover")
            btnReorder.Attributes.Remove("onmouseout")
        ElseIf pageName = "frmReportMenu_45.aspx" Then
            btnreports.ImageUrl = "../Images/reports_selected.png"
            btnreports.Attributes.Remove("onmouseover")
            btnreports.Attributes.Remove("onmouseout")
        ElseIf pageName = "frm49_Salvage.aspx" OrElse pageName = "frm50_SalvageDetail" OrElse pageName = "frm58_SearchSalvage.aspx" Then
            btnsalvage.ImageUrl = "../Images/salvage_selected.png"
            btnsalvage.Attributes.Remove("onmouseover")
            btnsalvage.Attributes.Remove("onmouseout")
        ElseIf pageName = "frm64_ItemLocationSearch.aspx" Then  ' by Farkas 30Nov10
            btnLocation.ImageUrl = "../Images/location_selected_btn.png"
            btnLocation.Attributes.Remove("onmouseover")
            btnLocation.Attributes.Remove("onmouseout")
        End If
    End Sub
    Private Sub AddAttributesMouseActionForButton()
      

        btnLocation.Attributes.Add("onmouseover", "this.src='../Images/location_over_btn.png'")
        btnLocation.Attributes.Add("onmouseout", "this.src='../Images/location_static_btn.png'")
        btnLocation.Attributes.Add("onclick", "this.src='../Images/location_selected_btn.png'")

        btnadjustments.Attributes.Add("onmouseover", "this.src='../Images/adjustments_rollover.png'")
        btnadjustments.Attributes.Add("onmouseout", "this.src='../Images/adjustments_static.png'")
        btnadjustments.Attributes.Add("onclick", "this.src='../Images/adjustments_selected.png'")

        btndispensing.Attributes.Add("onmouseover", "this.src='../Images/dispensing_rollover.png'")
        btndispensing.Attributes.Add("onmouseout", "this.src='../Images/dispensing_static.png'")
        btndispensing.Attributes.Add("onclick", "this.src='../Images/dispensing_selected.png'")

        btnsalvage.Attributes.Add("onmouseover", "this.src='../Images/salvage_rollover.png'")
        btnsalvage.Attributes.Add("onmouseout", "this.src='../Images/salvage_static.png'")
        btnsalvage.Attributes.Add("onclick", "this.src='../Images/salvage_selected.png'")

        btnReorder.Attributes.Add("onmouseover", "this.src='../Images/reorder_rollover.png'")
        btnReorder.Attributes.Add("onmouseout", "this.src='../Images/reorder_static.png'")
        btnReorder.Attributes.Add("onclick", "this.src='../Images/reorder_selected.png'")


        btnreports.Attributes.Add("onmouseover", "this.src='../Images/reports_rollover.png'")
        btnreports.Attributes.Add("onmouseout", "this.src='../Images/reports_static.png'")
        btnreports.Attributes.Add("onclick", "this.src='../Images/reports_selected.png'")

        btnReceiving.Attributes.Add("onmouseover", "this.src='../Images/receiving_rollover.png'")
        btnReceiving.Attributes.Add("onmouseout", "this.src='../Images/receiving_static.png'")
        btnReceiving.Attributes.Add("onclick", "this.src='../Images/receiving_selected.png'")


        btnInventory.Attributes.Add("onmouseover", "this.src='../Images/Inventory_rollover.png'")
        btnInventory.Attributes.Add("onmouseout", "this.src='../Images/Inventory_static.png'")
        btnInventory.Attributes.Add("onclick", "this.src='../Images/Inventory_selected.png'")


      
    End Sub

End Class