<%@ Page Language="VB" CodeBehind="frm10_AddSalvItems.aspx.vb" AutoEventWireup="false"
    Inherits="frmAddSalvItems" MasterPageFile="~/Master/Inventory.Master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       <asp:Panel ID="pnBody" Style="z-index: 105; left: 40px; top: 1px; font-family: 'Microsoft Sans Serif';
        font-size: 8pt; position: relative; width: 876px;" runat="server" 
           Height="350"> 

    <asp:Label ID="Label3" Style="z-index: 101; left: 9px; top: 13px; font-family: 'Verdana';
        font-size: 8pt; position: absolute;" runat="server" Width="248" Height="13">Select an Inventory Item to Salvage:</asp:Label>
    <asp:Panel ID="pnlDetail" Style="z-index: 102; left: 0px; top: 73px; font-family: 'Microsoft Sans Serif';
        font-size: 8pt; position: absolute; height: 291px;" runat="server" Width="469">
        <asp:TextBox ID="txtItemTotal" Style="z-index: 103; left: 329px; top: 214px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="92"></asp:TextBox>
        <asp:Label ID="Label9" Style="z-index: 104; left: 259px; top: 217px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="71" Height="13">Item Total:</asp:Label>
        <asp:DropDownList ID="cboPartNo" Style="z-index: 105; left: 319px; top: 6px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21">
        </asp:DropDownList>
        <asp:TextBox ID="txtCQOH" Style="z-index: 106; left: 319px; top: 33px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="90"></asp:TextBox>
        <asp:TextBox ID="txtQty" Style="z-index: 107; left: 93px; top: 33px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="131" Height="21" TabIndex="3"></asp:TextBox>
        <asp:Label ID="Label4" Style="z-index: 108; left: 267px; top: 9px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="54" Height="13">Part No:</asp:Label>
        <asp:Label ID="Label7" Style="z-index: 109; left: 62px; top: 36px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="32" Height="13">Qty:</asp:Label>
        <asp:Panel ID="pnlPartDetail" Style="z-index: 110; left: 7px; top: 60px; font-family: 'Microsoft Sans Serif';
            font-size: 8pt; position: absolute;" runat="server" Width="452" Height="146">
            <asp:TextBox ID="txtDescription2" Style="z-index: 111; left: 85px; top: 115px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="356" Height="21" TabIndex="6"></asp:TextBox>
            <asp:TextBox ID="txtDescription1" Style="z-index: 112; left: 85px; top: 88px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="356" Height="21" TabIndex="5"></asp:TextBox>
            <asp:TextBox ID="txtUnitCost" Style="z-index: 113; left: 85px; top: 61px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="48"></asp:TextBox>
            <asp:TextBox ID="txtSubCategory" Style="z-index: 114; left: 311px; top: 7px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="46"></asp:TextBox>
            <asp:TextBox ID="txtItemType" Style="z-index: 115; left: 85px; top: 34px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="47"></asp:TextBox>
            <asp:TextBox ID="txtCategory" Style="z-index: 116; left: 85px; top: 7px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="52"></asp:TextBox>
            <asp:TextBox ID="txtUnitOfRec" Style="z-index: 117; left: 311px; top: 61px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="79"></asp:TextBox>
            <asp:Label ID="Label14" Style="z-index: 118; left: 22px; top: 64px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="64" Height="13">Unit Cost:</asp:Label>
            <asp:TextBox ID="txtSubItemType" Style="z-index: 119; left: 311px; top: 34px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="51"></asp:TextBox>
            <asp:Label ID="Label10" Style="z-index: 120; left: 21px; top: 10px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="65" Height="13">Category:</asp:Label>
            <asp:Label ID="Label11" Style="z-index: 121; left: 15px; top: 37px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="71" Height="13">Item Type:</asp:Label>
            <asp:Label ID="Label5" Style="z-index: 122; left: 10px; top: 91px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="76" Height="13">Description:</asp:Label>
            <asp:Label ID="Label2" Style="z-index: 123; left: 222px; top: 10px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="91" Height="13">Sub Category:</asp:Label>
            <asp:Label ID="Label12" Style="z-index: 124; left: 216px; top: 37px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="97" Height="13">Sub Item Type:</asp:Label>
            <asp:Label ID="Label6" Style="z-index: 125; left: 227px; top: 64px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="86" Height="13">Unit Of Issue:</asp:Label>
        </asp:Panel>
        <asp:DropDownList ID="cboShopName" Style="z-index: 126; left: 93px; top: 6px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21">
        </asp:DropDownList>
        <asp:Label ID="Label8" Style="z-index: 127; left: 53px; top: 9px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="41" Height="13">Shop:</asp:Label>
        <asp:Label ID="Label1" Style="z-index: 128; left: 274px; top: 36px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="47" Height="13">CQOH:</asp:Label>
        <asp:Button ID="btnDone" runat="server" Text="Done" Style="z-index: 134; left: 185px;
            top: 257px; position: absolute; width: 62px;" runat="server" Height="21" />
        <asp:Button ID="btnNext" runat="server" Text="Next" Style="z-index: 134; left: 52px;
            top: 256px; position: absolute; width: 62px;" runat="server" Height="21" />
        <asp:Button ID="btnCancel" runat="server" Height="21" Style="z-index: 134; left: 327px;
            top: 255px; position: absolute; width: 62px;" Text="Cancel" />
            <asp:CustomValidator ID="cutValidator" runat="server" Display="None"></asp:CustomValidator>
    </asp:Panel>
</asp:Panel>
</asp:Content>