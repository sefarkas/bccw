<%@ Page Language="VB" Theme="InvSkin" CodeBehind="frm58_SearchSalvage.aspx.vb" AutoEventWireup="false"
    Inherits="frm58_SearchSalvage" MasterPageFile="~/Master/Inventory.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        // test auto-ready logic - call corner before DOM is ready
        $('#ContentPlaceHolder1_pnlSearchCriteria').corner();
    </script>
    <asp:Panel ID="pnBody" Style="z-index: 105; left: 40px; top: 1px; font-family: 'Microsoft Sans Serif';
        font-size: 8pt; position: relative; width: 932px;" runat="server" 
        Height="370">
        <asp:Panel ID="Panel3" Style="z-index: 105; left: 0px; top: 137px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="925" Height="220"
            ScrollBars="Auto">
            
            <asp:GridView ID="dgvSearchResult" SkinID="gridviewSkin" Style="z-index: 135; left: 6px;
                top: 1px; font-family: 'Microsoft Sans Serif'; font-size: 8pt; position: absolute;"
                runat="server" Width="893px" Height="284px">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:ImageButton ID="btnEdit" runat="server" Text="Edit" ImageUrl="~/Images/edit_static.png"
                                CommandName="btnEdit" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate >
                            <asp:ImageButton ID="btnsalvage" runat="server" Visible="false"  Text="Edit" ImageUrl="~/Images/salvage_static.png"
                                CommandName="btnsalvage" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <asp:Label ID="lblRecCount" Style="z-index: 107; left: 39px; top: 308px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="91" Height="13">Record 1 of 17</asp:Label>
        </asp:Panel>
        <asp:Panel ID="pnlSearchCriteria" SkinID="panelSearch" Style="z-index: 108; left: 0px;
            top: 3px; font-family: 'Verdana'; font-size: 8pt; position: absolute;" runat="server"
            Width="925" Height="98">
            <asp:Label ID="lblSalvageDate2" Style="z-index: 109; left: 704px; top: 41px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="28" Height="13">and</asp:Label>
            <asp:DropDownList ID="cboDateSearchSalvageDate" Style="z-index: 110; left: 324px;
                top: 37px; font-family: 'Verdana'; font-size: 8pt; position: absolute;" runat="server"
                Width="131" Height="21">
                <asp:ListItem Value="0">Equals To</asp:ListItem>
                <asp:ListItem Value="1">Before</asp:ListItem>
                <asp:ListItem Value="2">After</asp:ListItem>
                <asp:ListItem Value="3">Between</asp:ListItem>
            </asp:DropDownList>
            <asp:TextBox ID="txtToSalvageDate" Style="z-index: 111; left: 769px; top: 37px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="140" Height="21" TabIndex="65"></asp:TextBox>
            <asp:TextBox ID="txtSalvageDate" Style="z-index: 112; left: 530px; top: 37px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="140" Height="21" TabIndex="64"></asp:TextBox>
            <asp:Label ID="Label7" Style="z-index: 113; left: 235px; top: 40px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="89" Height="13">Salvage Date:</asp:Label>
            <asp:DropDownList ID="cboSalvageReas" Style="z-index: 114; left: 93px; top: 37px;
                font-family: 'Verdana'; font-size: 8pt; position: absolute;" runat="server" Width="140"
                Height="21">
            </asp:DropDownList>
            <asp:Label ID="Label5" Style="z-index: 115; left: 4px; top: 41px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="90" Height="13">Salvage Reas:</asp:Label>
            <asp:Label ID="lblReqstDate2" Style="z-index: 116; left: 704px; top: 14px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="28" Height="13">and</asp:Label>
            <asp:DropDownList ID="cboDateSearchReqstDate" Style="z-index: 117; left: 324px; top: 10px;
                font-family: 'Verdana'; font-size: 8pt; position: absolute;" runat="server" Width="131"
                Height="21" AutoPostBack="True">
                <asp:ListItem Value="0">Equals To</asp:ListItem>
                <asp:ListItem Value="1">Before</asp:ListItem>
                <asp:ListItem Value="2">After</asp:ListItem>
                <asp:ListItem Value="3">Between</asp:ListItem>
            </asp:DropDownList>
            <asp:TextBox ID="txtToReqstDate" Style="z-index: 118; left: 769px; top: 10px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="140" Height="21" TabIndex="58"></asp:TextBox>
            <asp:TextBox ID="txtReqstDate" Style="z-index: 119; left: 530px; top: 10px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="140" Height="21" TabIndex="57"></asp:TextBox>
            <asp:Label ID="Label10" Style="z-index: 120; left: 249px; top: 13px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="75" Height="13">Reqst Date:</asp:Label>
            <asp:DropDownList ID="cboShopName" Style="z-index: 121; left: 93px; top: 64px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="140" Height="21" AutoPostBack="True">
            </asp:DropDownList>
            <asp:Label ID="Label12" Style="z-index: 122; left: 459px; top: 67px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="71" Height="13">Item Type:</asp:Label>
            <asp:DropDownList ID="cboItemType" Style="z-index: 123; left: 530px; top: 64px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="140" Height="21" AutoPostBack="True">
            </asp:DropDownList>
            <asp:Label ID="Label11" Style="z-index: 124; left: 671px; top: 67px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="97" Height="13">Sub Item Type:</asp:Label>
            <asp:DropDownList ID="cboSubItemType" Style="z-index: 125; left: 769px; top: 64px;
                font-family: 'Verdana'; font-size: 8pt; position: absolute;" runat="server" Width="140"
                Height="21">
            </asp:DropDownList>
            <asp:Label ID="Label4" Style="z-index: 126; left: 53px; top: 67px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="41" Height="13">Shop:</asp:Label>
            <asp:DropDownList ID="cboPartNo" Style="z-index: 127; left: 324px; top: 64px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="131" Height="21">
            </asp:DropDownList>
            <asp:Label ID="Label3" Style="z-index: 128; left: 270px; top: 67px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="54" Height="13">Part No:</asp:Label>
            <asp:Panel ID="Panel2" Style="z-index: 129; left: 93px; top: 10px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="149" Height="21">
                <asp:DropDownList ID="cboReqstByName" Style="z-index: 130; left: 0px; top: 0px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute;" runat="server" Width="140" Height="21">
                </asp:DropDownList>
            </asp:Panel>
            <asp:Label ID="Label2" Style="z-index: 131; left: 31px; top: 13px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="63" Height="13">Reqst By:</asp:Label>
        </asp:Panel>
        <asp:CustomValidator ID="cutValidator" runat="server" Display="None"></asp:CustomValidator>
        <asp:ImageButton ID="btnReset" runat="server" SkinID="resetButton" Style="position: absolute;
            top: 110px; left: 72px;" />
        <asp:ImageButton ID="btnFind" runat="server" SkinID="findButton" Style="position: absolute;
            top: 110px; left: 12px;" />
        <asp:ImageButton ID="btnsalvage" runat="server"  Text="Edit" ImageUrl="~/Images/salvage_static.png"
                                Style="position: absolute;
            top: 110px; left: 182px; width: 100px;" />
    </asp:Panel>
</asp:Content>
