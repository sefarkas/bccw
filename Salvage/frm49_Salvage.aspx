<%@ Page language="VB" Codebehind="frm49_Salvage.aspx.vb" AutoEventWireup="false" Inherits="frmSalvage" MasterPageFile="~/Master/Inventory.Master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       <asp:Panel ID="pnBody" Style="z-index: 105; left: 40px; top: 1px; font-family: 'Microsoft Sans Serif';
        font-size: 8pt; position: relative; width: 875px;" runat="server" 
           Height="350"> 

<asp:Panel id="pnlSalvageItems" style="z-index:101; left:0px; top:141px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:absolute;" runat="server" Width="773" Height="282"><asp:Label id="lblRecCount" style="z-index:102; left:21px; top:251px;  position:absolute;" runat="server" Width="0" Height="13"></asp:Label>
<asp:Label id="Label11" style="z-index:103; left:7px; top:4px;  position:absolute;" runat="server" Width="122" Height="13">Items to Salvage:</asp:Label>
<asp:GridView id="dgvSalvageItems" style="z-index:104; left:10px; top:20px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:absolute;" runat="server" Width="750" Height="217"></asp:GridView>
<asp:TextBox id="txtSalvageTotal" style="z-index:105; left:620px; top:248px;  position:absolute;" runat="server" Width="140" Height="21" TabIndex="77">$18.66</asp:TextBox>
<asp:Label id="Label21" style="z-index:106; left:530px; top:251px;  position:absolute;" runat="server" Width="90" Height="13">Salvage Total:</asp:Label>
</asp:Panel>
<asp:Panel id="pnlDetail" style="z-index:107; left:0px; top:12px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:absolute;" runat="server" Width="773" Height="123"><asp:DropDownList id="cboBureau" style="z-index:108; left:591px; top:38px;  position:absolute;" runat="server" Width="169" Height="21"></asp:DropDownList>
<asp:TextBox id="txtRelinqNo" style="z-index:109; left:94px; top:65px;  position:absolute;" runat="server" Width="140" Height="21" TabIndex="87"></asp:TextBox>
<asp:Label id="Label1" style="z-index:110; left:30px; top:68px;  position:absolute;" runat="server" Width="66" Height="13">Relinq No:</asp:Label>
<asp:DropDownList id="cboAgency" style="z-index:111; left:591px; top:38px;  position:absolute;" runat="server" Width="169" Height="21"></asp:DropDownList>
<asp:Label id="Label20" style="z-index:112; left:539px; top:41px;  position:absolute;" runat="server" Width="54" Height="13">Agency:</asp:Label>
<asp:DropDownList id="cboActionWitnName" style="z-index:113; left:94px; top:92px;  position:absolute;" runat="server" Width="140" Height="21"></asp:DropDownList>
<asp:Label id="Label19" style="z-index:114; left:40px; top:95px;  position:absolute;" runat="server" Width="56" Height="13">Witn By:</asp:Label>
<asp:DropDownList id="cboAction" style="z-index:115; left:308px; top:38px;  position:absolute;" runat="server" Width="215" Height="21"></asp:DropDownList>
<asp:Label id="Label18" style="z-index:116; left:263px; top:41px;  position:absolute;" runat="server" Width="47" Height="13">Action:</asp:Label>
<asp:TextBox id="txtSalvageDate" style="z-index:117; left:94px; top:38px;  position:absolute;" runat="server" Width="140" Height="21" TabIndex="79"></asp:TextBox>
<asp:Label id="Label17" style="z-index:118; left:7px; top:41px;  position:absolute;" runat="server" Width="89" Height="13">Salvage Date:</asp:Label>
<asp:DropDownList id="cboSalvageReas" style="z-index:119; left:308px; top:11px;  position:absolute;" runat="server" Width="215" Height="21"></asp:DropDownList>
<asp:Label id="Label5" style="z-index:120; left:257px; top:14px;  position:absolute;" runat="server" Width="54" Height="13">Reason:</asp:Label>
<asp:TextBox id="txtReqstDate" style="z-index:121; left:94px; top:11px;  position:absolute;" runat="server" Width="140" Height="21" TabIndex="65"></asp:TextBox>
<asp:DropDownList id="cboReqstByName" style="z-index:122; left:591px; top:11px;  position:absolute;" runat="server" Width="169" Height="21"></asp:DropDownList>
<asp:Label id="Label10" style="z-index:123; left:21px; top:14px;  position:absolute;" runat="server" Width="75" Height="13">Reqst Date:</asp:Label>
<asp:Label id="Label2" style="z-index:124; left:530px; top:14px;  position:absolute;" runat="server" Width="63" Height="13">Reqst By:</asp:Label>
<asp:TextBox id="txtComments" style="z-index:125; left:308px; top:65px;  position:absolute;" runat="server" Width="452" Height="48" TabIndex="8"></asp:TextBox>
<asp:Label id="Label15" 
        style="z-index:126; left:242px; top:84px;  position:absolute; height: 28px;" 
        runat="server" Width="74">Comments:</asp:Label>
        <asp:Button ID="btnProcess" Text="Process" 
        
        
        style="z-index:127; left:230px; top:404px;  position:absolute; height: 22px;" runat="server" 
        Width="74" />
        <asp:Button ID="btnSave" Text="Save"         
        
        style="z-index:128; left:329px; top:405px;  position:absolute; height: 22px;" runat="server" 
        Width="74" />
        <asp:Button ID="btnDeleteItem" Text="Delete Item"         
        
        
        style="z-index:129; left:435px; top:409px;  position:absolute; height: 22px;" runat="server" 
        Width="74" />
         <asp:Button ID="btnAddItem" Text="Add Item"         
        
        
        style="z-index:130; left:525px; top:404px;  position:absolute; height: 22px;" runat="server" 
        Width="74" />
</asp:Panel>
<p>

</p>
         <asp:Button ID="btnCancel" Text="Cancel"         
        
        
        
    style="z-index:131; left:615px; top:416px;  position:absolute; height: 22px;" runat="server" 
        Width="74" />
        <p>
        <asp:Label ID="lblMsg" runat="server" Visible="false" 
                style="z-index:132; left:64px; top:452px;  position:absolute; height: 22px; width: 643px;"></asp:Label>
</p>
</asp:Panel>
</asp:Content>