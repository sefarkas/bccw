﻿
Public Class Global_asax
    Inherits System.Web.HttpApplication

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application is started
        Debug.Print("anything")
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session is started
        Debug.Print("anything")
    End Sub

    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires at the beginning of each request
        Debug.Print("anything")
    End Sub

    Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires upon attempting to authenticate the use
        Debug.Print("anything")
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)

        Dim err As New Entities.AppError
        Dim ex As Exception = Server.GetLastError()
        Dim exTemp As Exception = ex
        Dim levCnt As Integer = 1
        While exTemp IsNot Nothing
            err.ErrorMessage += (" Level " & levCnt.ToString() & "==> ") + exTemp.Message

            err.StackTrace += (" Level " & levCnt.ToString() & "==> ") + exTemp.StackTrace
            levCnt += 1

            exTemp = exTemp.InnerException
        End While

        err.RequestType = Request.RequestType

        err.Url = Request.RawUrl
        err.UserID = User.Identity.Name

        Dim elogerror As New ErrorUtility
        elogerror.LogError(err)

    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session ends
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application ends
    End Sub

End Class