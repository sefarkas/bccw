﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frm18_Dispense.aspx.vb" Inherits="frm18_Dispense" MasterPageFile="~/Master/Inventory.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

       <asp:Panel ID="pnBody" Style="z-index: 105; left: 0px; top: 1px; font-family: 'Microsoft Sans Serif';
        font-size: 8pt; position: relative;" runat="server" Width="950" Height="350"> 
    <asp:Panel ID="Panel2" runat="server" Height="281" 
        style="z-index:101; left:40px; top:166px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:absolute;" 
        Width="773">
        <asp:TextBox ID="txtDispTotal" runat="server" Height="21" 
            style="z-index:102; left:620px; top:250px; font-family:'Verdana'; font-size:8pt; position:absolute;" 
            TabIndex="46" Width="140">$0.00</asp:TextBox>
        <asp:Label ID="Label12" runat="server" Height="13" 
            style="z-index:103; left:525px; top:253px; font-family:'Verdana'; font-size:8pt; position:absolute;" 
            Width="96">Dispense Total:</asp:Label>
        <asp:Label ID="lblRecCount" runat="server" Height="13" 
            style="z-index:104; left:23px; top:253px; font-family:'Verdana'; font-size:8pt; position:absolute;" 
            Width="0"></asp:Label>
        <asp:Label ID="Label11" runat="server" Height="13" 
            style="z-index:105; left:2px; top:-12px; font-family:'Verdana'; font-size:8pt; position:absolute;" 
            Width="132">Items To Dispense:</asp:Label>
        <asp:GridView ID="dgvDispItems" runat="server" Height="217" 
            style="z-index:106; left:10px; top:23px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:absolute;" 
            Width="750">
        </asp:GridView>
    </asp:Panel>
    <asp:Panel ID="pnlDetail" runat="server" Height="151" 
        style="z-index:107; left:40px; top:9px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:absolute;" 
        Width="773">
        <asp:CheckBox ID="chkViolationAvoid" runat="server" Height="17" 
            style="z-index:108; left:116px; top:40px; font-family:'Verdana'; font-size:8pt; position:absolute;" 
            Width="138" />
        <asp:CheckBox ID="chkViolation" runat="server" Height="17" 
            style="z-index:109; left:26px; top:40px; font-family:'Verdana'; font-size:8pt; position:absolute;" 
            Width="75" />
        <asp:TextBox ID="txtWONo" runat="server" Height="21" 
            style="z-index:110; left:87px; top:11px; font-family:'Verdana'; font-size:8pt; position:absolute;" 
            TabIndex="0" Width="167"></asp:TextBox>
        <asp:DropDownList ID="cboVehicleID" runat="server" Height="21" 
            style="z-index:111; left:580px; top:65px; font-family:'Verdana'; font-size:8pt; position:absolute;" 
            Width="180">
        </asp:DropDownList>
        <asp:CheckBox ID="chkDispToTruck" runat="server" Height="17" 
            style="z-index:112; left:580px; top:40px; font-family:'Verdana'; font-size:8pt; position:absolute;" 
            Width="139" />
        <asp:TextBox ID="txtDateDisp" runat="server" Height="21" 
            style="z-index:113; left:580px; top:11px; font-family:'Verdana'; font-size:8pt; position:absolute;" 
            TabIndex="5" Width="180">10/22/2010</asp:TextBox>
        <asp:DropDownList ID="cboDispLoc" runat="server" Height="21" 
            style="z-index:114; left:87px; top:65px; font-family:'Verdana'; font-size:8pt; position:absolute;" 
            Width="167">
        </asp:DropDownList>
        <asp:DropDownList ID="cboDispBy" runat="server" Height="21" 
            style="z-index:115; left:327px; top:11px; font-family:'Verdana'; font-size:8pt; position:absolute;" 
            Width="172">
        </asp:DropDownList>
        <asp:Label ID="Label2" runat="server" Height="13" 
            style="z-index:116; left:37px; top:13px; font-family:'Verdana'; font-size:8pt; position:absolute;" 
            Width="51">WO No:</asp:Label>
        <asp:Label ID="Label7" runat="server" Height="13" 
            style="z-index:117; left:272px; top:15px; font-family:'Verdana'; font-size:8pt; position:absolute;" 
            Width="56">Disp By:</asp:Label>
        <asp:Label ID="Label8" runat="server" Height="13" 
            style="z-index:118; left:513px; top:15px; font-family:'Verdana'; font-size:8pt; position:absolute;" 
            Width="68">Disp Date:</asp:Label>
        <asp:DropDownList ID="cboDispTo" runat="server" Height="21" 
            style="z-index:119; left:327px; top:38px; font-family:'Verdana'; font-size:8pt; position:absolute;" 
            Width="172">
        </asp:DropDownList>
        <asp:DropDownList ID="cboBureau" runat="server" Height="21" 
            style="z-index:120; left:327px; top:65px; font-family:'Verdana'; font-size:8pt; position:absolute;" 
            Width="172">
        </asp:DropDownList>
        <asp:Label ID="Label10" runat="server" 
            style="z-index:121; left:0px; top:73px; font-family:'Verdana'; font-size:8pt; position:absolute; height: 8px;" 
            Width="88">Disp Location:</asp:Label>
        <asp:Label ID="Label6" runat="server" Height="13" 
            style="z-index:122; left:276px; top:68px; font-family:'Verdana'; font-size:8pt; position:absolute;" 
            Width="53">Bureau:</asp:Label>
        <asp:Label ID="Label9" runat="server" Height="13" 
            style="z-index:123; left:511px; top:68px; font-family:'Verdana'; font-size:8pt; position:absolute;" 
            Width="71">Vehicle ID:</asp:Label>
        <asp:Label ID="Label5" runat="server" Height="13" 
            style="z-index:124; left:273px; top:41px; font-family:'Verdana'; font-size:8pt; position:absolute;" 
            Width="55">Disp To:</asp:Label>
        <asp:TextBox ID="txtComments" runat="server" Height="48" 
            style="z-index:125; left:87px; top:92px; font-family:'Verdana'; font-size:8pt; position:absolute;" 
            TabIndex="8" Width="673"></asp:TextBox>
        <asp:Label ID="Label15" runat="server" Height="13" 
            style="z-index:126; left:14px; top:95px; font-family:'Verdana'; font-size:8pt; position:absolute;" 
            Width="74">Comments:</asp:Label>
  <asp:Button ID="btnProcess" Text="Process"  runat="server"   style="z-index:127; left:121px; top:444px; font-family:'Verdana'; font-size:8pt; position:absolute;" 
            Width="74" />
<asp:Button ID="btnAddItem" Text="Add Item"  runat="server"   style="z-index:128; left:230px; top:441px; font-family:'Verdana'; font-size:8pt; position:absolute;" 
            Width="74" />
            <asp:Button ID="btnDeleteItem" Text="Delete Item"  runat="server"   style="z-index:129; left:346px; top:441px; font-family:'Verdana'; font-size:8pt; position:absolute;" 
            Width="74" />
            <asp:Button ID="btnCancel" Text="Cancel"  runat="server"   style="z-index:130; left:443px; top:441px; font-family:'Verdana'; font-size:8pt; position:absolute;" 
            Width="74" />
    </asp:Panel>
    </asp:Panel>
    </asp:Content>
