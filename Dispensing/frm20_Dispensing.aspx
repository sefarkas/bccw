<%@ Page language="VB" Codebehind="frm20_Dispensing.aspx.vb" AutoEventWireup="false" Inherits="frm20_Dispensing" MasterPageFile="~/Master/Inventory.Master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:Panel ID="pnBody" Style="z-index: 105; left: 0px; top: 1px; font-family: 'Microsoft Sans Serif';
        font-size: 8pt; position: relative;" runat="server" Width="950" Height="350"> 
<asp:Panel id="pnlDetail" style="z-index:101; left:40px; top:106px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:absolute;" runat="server" Width="773" Height="146"><asp:CheckBox id="chkViolationAvoid" style="z-index:102; left:120px; top:35px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="138" Height="17"></asp:CheckBox>
<asp:CheckBox id="chkViolation" style="z-index:103; left:31px; top:35px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="75" Height="17"></asp:CheckBox>
<asp:TextBox id="txtDispByName" style="z-index:104; left:338px; top:6px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="177" Height="21" TabIndex="12"></asp:TextBox>
<asp:TextBox id="txtWONo" style="z-index:105; left:92px; top:6px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="166" Height="21" TabIndex="11"></asp:TextBox>
<asp:DropDownList id="cboVehicleID" style="z-index:106; left:586px; top:60px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="172" Height="21"></asp:DropDownList>
<asp:CheckBox id="chkDispToTruck" style="z-index:107; left:586px; top:35px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="139" Height="17"></asp:CheckBox>
<asp:TextBox id="txtDateDisp" style="z-index:108; left:586px; top:6px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="173" Height="21" TabIndex="13"></asp:TextBox>
<asp:DropDownList id="cboDispLoc" style="z-index:109; left:92px; top:60px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="166" Height="21"></asp:DropDownList>
<asp:Label id="Label2" style="z-index:110; left:42px; top:9px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="51" Height="13">WO No:</asp:Label>
<asp:Label id="Label7" style="z-index:111; left:284px; top:9px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="56" Height="13">Disp By:</asp:Label>
<asp:Label id="Label8" style="z-index:112; left:519px; top:9px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="68" Height="13">Disp Date:</asp:Label>
<asp:TextBox id="txtComments" style="z-index:113; left:92px; top:87px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="667" Height="48" TabIndex="21"></asp:TextBox>
<asp:DropDownList id="cboDispToName" style="z-index:114; left:338px; top:33px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="177" Height="21"></asp:DropDownList>
<asp:Label id="Label15" style="z-index:115; left:17px; top:90px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="74" Height="13">Comments:</asp:Label>
<asp:DropDownList id="cboBureau" style="z-index:116; left:338px; top:60px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="177" Height="21"></asp:DropDownList>
<asp:Label id="Label10" style="z-index:117; left:5px; top:63px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="88" Height="13">Disp Location:</asp:Label>
<asp:Label id="Label6" style="z-index:118; left:286px; top:63px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="53" Height="13">Bureau:</asp:Label>
<asp:Label id="Label9" style="z-index:119; left:516px; top:63px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="71" Height="13">Vehicle ID:</asp:Label>
<asp:Label id="Label5" style="z-index:120; left:284px; top:35px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="55" Height="13">Disp To:</asp:Label>
<asp:TextBox id="txtDispTransID" style="z-index:121; left:680px; top:28px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="69" Height="21" TabIndex="62">461</asp:TextBox>
</asp:Panel>
<asp:Panel id="Panel2" style="z-index:122; left:40px; top:258px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:absolute;" runat="server" Width="773" Height="281"><asp:TextBox id="txtDispTotal" style="z-index:123; left:620px; top:250px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="140" Height="21" TabIndex="46">$44.50</asp:TextBox>
<asp:Label id="Label12" style="z-index:124; left:525px; top:253px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="96" Height="13">Dispense Total:</asp:Label>
<asp:Label id="lblRecCount" style="z-index:125; left:23px; top:253px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="84" Height="13">Record 1 of 5</asp:Label>
<asp:Label id="Label11" style="z-index:126; left:7px; top:7px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="120" Height="13">Items Dispensed:</asp:Label>
<asp:GridView id="dgvDispItems" style="z-index:127; left:10px; top:23px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:absolute;" runat="server" Width="750" Height="217"></asp:GridView>
</asp:Panel>
<asp:Panel id="pnlHeader" style="z-index:128; left:40px; top:12px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:absolute;" runat="server" Width="772" Height="88"><asp:Panel id="pnlDispBy" style="z-index:129; left:7px; top:6px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:absolute;" runat="server" Width="111" Height="77"><asp:RadioButton id="rbtByVehicleID" style="z-index:130; left:5px; top:50px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="103" Height="17"></asp:RadioButton>
<asp:RadioButton id="rbtByWONo" style="z-index:131; left:5px; top:5px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="79" Height="17"></asp:RadioButton>
<asp:RadioButton id="rbtByWorker" style="z-index:132; left:5px; top:27px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="86" Height="17"></asp:RadioButton>
</asp:Panel>
<asp:DropDownList id="cboWoNo" style="z-index:133; left:128px; top:36px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="130" Height="21"></asp:DropDownList>
<asp:Label id="Label1" style="z-index:134; left:125px; top:20px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="53" Height="13">WO No:</asp:Label>
<asp:Label id="Label3" style="z-index:135; left:269px; top:20px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="73" Height="13">Disp Date:</asp:Label>
<asp:DropDownList id="cboDateDisp" style="z-index:136; left:272px; top:36px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="130" Height="21"></asp:DropDownList>
<asp:Button ID="btnSave" Text="Save" 
        style="z-index:137; left:535px; top:36px; font-family:'Verdana'; font-size:8pt; position:absolute;" 
        runat="server" Width="40" Height="21" />
        <asp:Button ID="btnCancel" Text="Cancel" 
        style="z-index:138; left:591px; top:36px; font-family:'Verdana'; font-size:8pt; position:absolute; width: 51px;" 
        runat="server" Height="21" />
        <asp:Button ID="btnClose" Text="Close" 
        style="z-index:139; left:666px; top:36px; font-family:'Verdana'; font-size:8pt; position:absolute;" 
        runat="server" Width="40" Height="21" />
        <asp:Button ID="btnGo" Text="Go" 
        style="z-index:140; left:486px; top:37px; font-family:'Verdana'; font-size:8pt; position:absolute;" 
        runat="server" Width="40" Height="21" />
        <asp:Label ID="lblMsg"  Visible="false" style="z-index:141; left:256px; top:62px; font-family:'Verdana'; font-size:8pt; position:absolute; width: 489px;" 
        runat="server" Height="21"></asp:Label>
        <asp:Button ID="btnPrevious" Text="<" 
        style="z-index:142; left:400px; top:37px; font-family:'Verdana'; font-size:8pt; position:absolute;" 
        runat="server" Width="40" Height="21" />

    <asp:Button ID="btnNext" runat="server" Height="21" 
        style="z-index:141; left:442px; top:37px; font-family:'Verdana'; font-size:8pt; position:absolute;" 
        Text="&gt;" Width="40" />

</asp:Panel>
    </asp:Panel>
    </asp:Content>