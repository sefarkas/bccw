﻿Imports System
Imports System.Data.SqlClient
Imports DataAccess.clsQueriesAction

Public Class frm18_Dispense

    Inherits InventoryBase
    Private DSDetail As New DataSet()
    Friend DSDispItems As New DataSet()
    Friend intTempDispTransID As Integer
    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        Me.InitializeComponent()
        MyBase.OnInit(e)
    End Sub
    Private Sub InitializeComponent()
        AddHandler Load, AddressOf Me.Page_Load
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim strSQL As String

        ClearData(Me)
        intTempDispTransID = 0
        txtDateDisp.Text = FormatDateTime(Now, DateFormat.ShortDate)
        chkDispToTruck.Checked = False
        chkViolation.Checked = False
        chkViolationAvoid.Checked = False
        ' Populate data detail combo boxes:
        strSQL = "select LoginID as DispTo, Name as DispToName from vwEmployee order by Name"
        cboDispTo.DataSource = PopulateTable(strSQL)
        'cboDispTo.ValueMember = "DispTo"
        'cboDispTo.DisplayMember = "DispToName"
        cboDispTo.SelectedIndex = -1
        strSQL = "select distinct p.LoginID, e.Name from vwShopPersonnel p inner join vwEmployee e on p.LoginID = e.LoginID order by e.Name"
        cboDispBy.DataSource = PopulateTable(strSQL)
        'cboDispBy.ValueMember = "LoginID"
        'cboDispBy.DisplayMember = "Name"
        cboDispBy.SelectedValue = GetUserName()
        ' strSQL = "select AddressID, (case when [Address] is not null then (District + ' - ' + [Address] + ', ' + isnull(City, '') + ', ' + isnull([State], '') + ' ' + isnull(Zip, '')) else District end) as Location from vwLocations  order by District, [Address]"
        'cboDispLocID.DataSource = PopulateTable(strSQL)
        'cboDispLocID.ValueMember = "AddressID"
        'cboDispLocID.DisplayMember = "Location"
        'cboDispLocID.SelectedIndex = -1
        PopulateComboBox(cboVehicleID, "select Vehicle from vwBBMVehicles order by Vehicle", True)
        PopulateComboBox(cboBureau, "select Bureau from vwBureau order by Bureau", True)
        'PopulateComboBox(cboDispLoc, "select distinct District from vwLocations order by District", True)

        txtDispTotal.Text = Format(0, "currency")
        cboVehicleID.Enabled = False
        btnProcess.Enabled = False
        btnDeleteItem.Enabled = False
        ' Initialize the data grid view that will display the items to be dispensed:
        strSQL = "select DispItemID, SysPartNo, Shop, [Part No], [Issue Out], [Qty Disp], [Unit Cost], [Value Disp], Description, [CQOH], [Item Comments] from vwDispItems where DispTransID = " & intTempDispTransID
        PopulateDataGridView(dgvDispItems, strSQL, DSDispItems)
        InitializeDataGridView(dgvDispItems)
        ' dgvDispItems.Sort(dgvDispItems.Columns("DispItemID"), System.ComponentModel.ListSortDirection.Ascending)

    End Sub

    Protected Sub btnProcess_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnProcess.Click

    End Sub

    Protected Sub btnAddItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddItem.Click
        Dim DataValues(,) As String = New String(11, 1) {{"WONo", txtWONo.Text.ToString()}, {"DispToTruck", chkDispToTruck.Checked.ToString()}, _
                           {"VehicleID", cboVehicleID.Text.ToString()}, {"DateDisp", txtDateDisp.Text.ToString()}, _
                           {"Violation", chkViolation.Checked.ToString()}, {"ViolationAvoid", chkViolationAvoid.Checked.ToString()}, _
                           {"Bureau", cboBureau.Text.ToString()}, {"DispTo", cboDispTo.SelectedValue.ToString()}, _
                           {"DispLoc", cboDispLoc.Text.ToString()}, {"DispBy", cboDispBy.SelectedValue.ToString()}, _
                           {"AddCPU", My.Computer.Name}, {"Comments", txtComments.Text.ToString()}}
        intTempDispTransID = ExecuteSP(DataValues, "SPAddDispTrans")
        If intTempDispTransID <> 0 Then
            PopulateDataSet("select * from vwDispensingTemp where DispTransID = " & intTempDispTransID, DSDetail)
        End If
    End Sub

    Protected Sub btnDeleteItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnDeleteItem.Click
        'DeleteItem(dgvDispItems.CurrentRow.Cells("DispItemID").Value, "Dispense")
    End Sub
End Class