<%@ Page Language="VB" CodeBehind="frm47_ReturnItems.aspx.vb" AutoEventWireup="false"
    Inherits="frmReturnItems" MasterPageFile="~/Master/Inventory.Master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:Panel ID="pnBody" Style="z-index: 105; left: 40px; top: 1px; font-family: 'Microsoft Sans Serif';
        font-size: 8pt; position: relative;" runat="server" Width="950" Height="350"> 

    <asp:Label ID="lblRecNo" Style="z-index: 101; left: 175px; top: 451px; font-family: 'Verdana';
        font-size: 8pt; position: absolute;" runat="server" Width="31" Height="13">4</asp:Label>
    <asp:TextBox ID="txtTotalQtyRet" Style="z-index: 102; left: 382px; top: 10px; font-family: 'Verdana';
        font-size: 8pt; position: absolute;" runat="server" Width="80" Height="21" TabIndex="106">0</asp:TextBox>
    <asp:Label ID="Label15" Style="z-index: 103; left: 297px; top: 13px; font-family: 'Verdana';
        font-size: 8pt; position: absolute;" runat="server" Width="87" Height="13">Total Qty Ret:</asp:Label>
    <asp:TextBox ID="txtQtyRet" Style="z-index: 104; left: 156px; top: 10px; font-family: 'Verdana';
        font-size: 8pt; position: absolute;" runat="server" Width="80" Height="21" TabIndex="1"></asp:TextBox>
    <asp:Label ID="Label3" Style="z-index: 105; left: 101px; top: 13px; font-family: 'Verdana';
        font-size: 8pt; position: absolute;" runat="server" Width="55" Height="13">Qty Ret:</asp:Label>
    <asp:CheckBox ID="chkReturn" Style="z-index: 106; left: 12px; top: 12px; font-family: 'Verdana';
        font-size: 8pt; position: absolute;" runat="server" Width="64" Height="17"></asp:CheckBox>
    <asp:Panel ID="pnlDetail" Style="z-index: 107; left: 12px; top: 37px; font-family: 'Microsoft Sans Serif';
        font-size: 8pt; position: absolute;" runat="server" Width="469" Height="388">
        <asp:TextBox ID="txtQtyDisp" Style="z-index: 108; left: 319px; top: 304px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="28">4</asp:TextBox>
        <asp:TextBox ID="txtComments" Style="z-index: 109; left: 93px; top: 331px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="356" Height="48" TabIndex="2"></asp:TextBox>
        <asp:Label ID="Label1" Style="z-index: 110; left: 20px; top: 334px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="74" Height="13">Comments:</asp:Label>
        <asp:Panel ID="pnlDispInfo" Style="z-index: 111; left: 7px; top: 183px; font-family: 'Microsoft Sans Serif';
            font-size: 8pt; position: absolute;" runat="server" Width="452" Height="116">
            <asp:TextBox ID="txtVehicleID" Style="z-index: 112; left: 311px; top: 61px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="24"></asp:TextBox>
            <asp:TextBox ID="txtBureau" Style="z-index: 113; left: 311px; top: 33px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="22">BBM</asp:TextBox>
            <asp:Label ID="Label21" Style="z-index: 114; left: 242px; top: 64px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="71" Height="13">Vehicle ID:</asp:Label>
            <asp:TextBox ID="txtDispByName" Style="z-index: 115; left: 311px; top: 88px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="26"></asp:TextBox>
            <asp:CheckBox ID="chkDispToTruck" Style="z-index: 116; left: 85px; top: 63px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="139" Height="17">
            </asp:CheckBox>
            <asp:TextBox ID="txtDispLoc" Style="z-index: 117; left: 85px; top: 33px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="21">BBM</asp:TextBox>
            <asp:TextBox ID="txtDispToName" Style="z-index: 118; left: 311px; top: 6px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="20"></asp:TextBox>
            <asp:TextBox ID="txtDateDisp" Style="z-index: 119; left: 85px; top: 88px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="25">7/6/2009</asp:TextBox>
            <asp:Label ID="Label17" Style="z-index: 120; left: 257px; top: 91px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="56" Height="13">Disp By:</asp:Label>
            <asp:TextBox ID="txtWONo" Style="z-index: 121; left: 85px; top: 6px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="19">1234</asp:TextBox>
            <asp:Label ID="Label18" Style="z-index: 122; left: 18px; top: 91px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="68" Height="13">Disp Date:</asp:Label>
            <asp:Label ID="Label22" Style="z-index: 123; left: 258px; top: 9px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="55" Height="13">Disp To:</asp:Label>
            <asp:Label ID="Label19" Style="z-index: 124; left: -2px; top: 36px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="88" Height="13">Disp Location:</asp:Label>
            <asp:Label ID="Label20" Style="z-index: 125; left: 260px; top: 36px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="53" Height="13">Bureau:</asp:Label>
            <asp:Label ID="Label13" Style="z-index: 126; left: 35px; top: 9px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="51" Height="13">WO No:</asp:Label>
        </asp:Panel>
        <asp:Panel ID="pnlPartInfo" Style="z-index: 127; left: 7px; top: 7px; font-family: 'Microsoft Sans Serif';
            font-size: 8pt; position: absolute;" runat="server" Width="452" Height="170">
            <asp:TextBox ID="txtUnitCost" Style="z-index: 128; left: 85px; top: 87px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="16">$1.16</asp:TextBox>
            <asp:TextBox ID="txtSubCategory" Style="z-index: 129; left: 311px; top: 33px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="13"></asp:TextBox>
            <asp:TextBox ID="txtItemType" Style="z-index: 130; left: 85px; top: 60px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="14">CEILING</asp:TextBox>
            <asp:TextBox ID="txtPartNo" Style="z-index: 131; left: 311px; top: 6px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="11">0324</asp:TextBox>
            <asp:TextBox ID="txtCategory" Style="z-index: 132; left: 85px; top: 33px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="12"></asp:TextBox>
            <asp:TextBox ID="txtShopName" Style="z-index: 133; left: 85px; top: 6px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="10">CARPENTRY</asp:TextBox>
            <asp:TextBox ID="txtUnitOfRec" Style="z-index: 134; left: 311px; top: 87px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="17">EACH</asp:TextBox>
            <asp:Label ID="Label14" Style="z-index: 135; left: 22px; top: 90px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="64" Height="13">Unit Cost:</asp:Label>
            <asp:TextBox ID="txtDescription" Style="z-index: 136; left: 85px; top: 114px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="356" Height="48" TabIndex="18">CROSS TEE 4'X 1" ( WHITE )   75 PCS,BOX </asp:TextBox>
            <asp:TextBox ID="txtSubItemType" Style="z-index: 137; left: 311px; top: 60px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="15">CROSS TEE</asp:TextBox>
            <asp:Label ID="Label8" Style="z-index: 138; left: 45px; top: 9px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="41" Height="13">Shop:</asp:Label>
            <asp:Label ID="Label10" Style="z-index: 139; left: 21px; top: 36px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="65" Height="13">Category:</asp:Label>
            <asp:Label ID="Label11" Style="z-index: 140; left: 15px; top: 63px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="71" Height="13">Item Type:</asp:Label>
            <asp:Label ID="Label5" Style="z-index: 141; left: 10px; top: 117px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="76" Height="13">Description:</asp:Label>
            <asp:Label ID="Label2" Style="z-index: 142; left: 222px; top: 36px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="91" Height="13">Sub Category:</asp:Label>
            <asp:Label ID="Label12" Style="z-index: 143; left: 216px; top: 63px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="97" Height="13">Sub Item Type:</asp:Label>
            <asp:Label ID="Label6" Style="z-index: 144; left: 237px; top: 90px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="76" Height="13">Unit Of Rec:</asp:Label>
            <asp:Label ID="Label4" Style="z-index: 145; left: 259px; top: 9px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="54" Height="13">Part No:</asp:Label>
        </asp:Panel>
        <asp:CheckBox ID="chkIssueOut" Style="z-index: 146; left: 93px; top: 306px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="81" Height="17"></asp:CheckBox>
        <asp:Label ID="Label7" Style="z-index: 147; left: 260px; top: 307px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="61" Height="13">Qty Disp:</asp:Label>
        <asp:Button ID="btnFirst" runat="server" Text="<<" Style="z-index: 134; left: 82px;
            top: 407px; position: absolute; width: 56px;" runat="server" Height="21" />
        <asp:Button ID="btnPrevious" runat="server" Text="<" Style="z-index: 134; left: 3px;
            top: 408px; position: absolute; width: 79px;" runat="server" Height="21" />
        <asp:Button ID="btnNext" runat="server" Text=">" Style="z-index: 134; left: 195px;
            top: 407px; position: absolute; width: 62px;" runat="server" Height="21" />
        <asp:Button ID="btnLast" runat="server" Text=">>" Style="z-index: 134; left: 259px;
            top: 407px; position: absolute; width: 62px;" runat="server" Height="21" />
        <asp:Button ID="btnClose" runat="server" Text="Close" Style="z-index: 134; left: 389px;
            top: 407px; position: absolute; width: 62px;" runat="server" Height="21" />
    </asp:Panel>
    </asp:Panel>
    </asp:Content>