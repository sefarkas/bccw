<%@ Page Language="VB" CodeBehind="frm19_DispenseDetail.aspx.vb" AutoEventWireup="false"
    Inherits="frm19_DispenseDetail" MasterPageFile="~/Master/Inventory.Master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

       <asp:Panel ID="pnBody" Style="z-index: 105; left: 0px; top: 1px; font-family: 'Microsoft Sans Serif';
        font-size: 8pt; position: relative;" runat="server" Width="830" Height="350"> 
    <asp:Label ID="lblRecNo" Style="z-index: 101; left: 219px; top: 348px; font-family: 'Verdana';
        font-size: 8pt; position: absolute;" runat="server" Width="31" Height="13">1</asp:Label>
    <asp:Panel ID="pnlDetail" Style="z-index: 102; left:40px; top: 10px; font-family: 'Microsoft Sans Serif';
        font-size: 8pt; position: absolute; height: 350px;" runat="server" 
        Width="469">
        <asp:Label ID="Label15" Style="z-index: 103; left: 0px; top: 199px; font-family: 'Verdana';
            font-size: 6pt; position: absolute;" runat="server" Width="87" Height="10">(On Date Of Disp)</asp:Label>
        <asp:TextBox ID="txtCQOH" Style="z-index: 104; left: 323px; top: 156px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="8"></asp:TextBox>
        <asp:Label ID="Label4" Style="z-index: 105; left: 278px; top: 159px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="47" Height="13">CQOH:</asp:Label>
        <asp:TextBox ID="txtItemComments" Style="z-index: 106; left: 86px; top: 237px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="367" Height="48" TabIndex="13"></asp:TextBox>
        <asp:Label ID="Label13" Style="z-index: 107; left: 13px; top: 240px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="74" Height="13">Comments:</asp:Label>
        <asp:TextBox ID="txtUnitofRec" Style="z-index: 108; left: 323px; top: 183px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="10"></asp:TextBox>
        <asp:Panel ID="pnlPartInfo" Style="z-index: 109; left: 7px; top: 7px; font-family: 'Microsoft Sans Serif';
            font-size: 8pt; position: absolute;" runat="server" Width="452" Height="143">
            <asp:TextBox ID="txtShopName" Style="z-index: 110; left: 78px; top: 6px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="43"></asp:TextBox>
            <asp:TextBox ID="txtPartNo" Style="z-index: 111; left: 315px; top: 6px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="38"></asp:TextBox>
            <asp:TextBox ID="txtSubCategory" Style="z-index: 112; left: 315px; top: 33px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="3"></asp:TextBox>
            <asp:TextBox ID="txtItemType" Style="z-index: 113; left: 78px; top: 60px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="4"></asp:TextBox>
            <asp:TextBox ID="txtCategory" Style="z-index: 114; left: 78px; top: 33px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="2"></asp:TextBox>
            <asp:TextBox ID="txtDescription" Style="z-index: 115; left: 78px; top: 87px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="367" Height="48" TabIndex="6">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </asp:TextBox>
            <asp:TextBox ID="txtSubItemType" Style="z-index: 116; left: 315px; top: 60px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="5"></asp:TextBox>
            <asp:Label ID="Label1" Style="z-index: 117; left: 39px; top: 9px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="41" Height="13">Shop:</asp:Label>
            <asp:Label ID="Label10" Style="z-index: 118; left: 15px; top: 36px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="65" Height="13">Category:</asp:Label>
            <asp:Label ID="Label11" Style="z-index: 119; left: 9px; top: 63px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="71" Height="13">Item Type:</asp:Label>
            <asp:Label ID="Label5" Style="z-index: 120; left: 4px; top: 90px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="76" Height="13">Description:</asp:Label>
            <asp:Label ID="Label3" Style="z-index: 121; left: 263px; top: 9px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="54" Height="13">Part No:</asp:Label>
            <asp:Label ID="Label2" Style="z-index: 122; left: 226px; top: 36px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="91" Height="13">Sub Category:</asp:Label>
            <asp:Label ID="Label12" Style="z-index: 123; left: 220px; top: 63px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="97" Height="13">Sub Item Type:</asp:Label>
        </asp:Panel>
        <asp:TextBox ID="txtQtyDisp" Style="z-index: 124; left: 86px; top: 156px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="7"></asp:TextBox>
        <asp:TextBox ID="txtReturnQty" Style="z-index: 125; left: 323px; top: 210px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="12"></asp:TextBox>
        <asp:Label ID="Label7" Style="z-index: 126; left: 27px; top: 159px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="61" Height="13">Qty Disp:</asp:Label>
        <asp:Label ID="Label9" Style="z-index: 127; left: 239px; top: 186px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="86" Height="13">Unit Of Issue:</asp:Label>
        <asp:TextBox ID="txtTotalValueDisp" Style="z-index: 128; left: 323px; top: 291px;
            font-family: 'Verdana'; font-size: 8pt; position: absolute;" runat="server" Width="130"
            Height="21" TabIndex="14"></asp:TextBox>
        <asp:Label ID="Label21" Style="z-index: 129; left: 237px; top: 213px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="88" Height="13">Qty Returned:</asp:Label>
        <asp:CheckBox ID="chkIssueOut" Style="z-index: 130; left: 86px; top: 212px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="88" Height="17" Text="Issued Out">
        </asp:CheckBox>
        <asp:Label ID="Label6" Style="z-index: 131; left: 252px; top: 294px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="73" Height="13">Value Disp:</asp:Label>
        <asp:TextBox ID="txtUnitCost" Style="z-index: 132; left: 86px; top: 183px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="9"></asp:TextBox>
        <asp:Label ID="Label14" Style="z-index: 133; left: 23px; top: 186px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="64" Height="13">Unit Cost:</asp:Label>
        <asp:Button ID="btnFirst" runat="server" Text="<<" Style="z-index: 134; left: 142px;
            top: 327px; position: absolute; width: 56px;" runat="server" Height="21" />
        <asp:Button ID="btnPrevious" runat="server" Text="<" Style="z-index: 134; left: 62px;
            top: 328px; position: absolute; width: 79px;" runat="server" Height="21" />
        <asp:Button ID="btnNext" runat="server" Text=">" Style="z-index: 134; left: 225px;
            top: 327px; position: absolute; width: 62px;" runat="server" Height="21" />
        <asp:Button ID="btnLast" runat="server" Text=">>" Style="z-index: 134; left: 289px;
            top: 327px; position: absolute; width: 62px;" runat="server" Height="21" />
            <asp:Button ID="btnClose" runat="server" Text="Close" Style="z-index: 134; left: 389px;
            top: 327px; position: absolute; width: 62px;" runat="server" Height="21" />

    </asp:Panel>
    </asp:Panel>
    </asp:Content>