Option Strict Off
Option Explicit On
Imports System
Imports DataAccess
Imports DataAccess.clsQueriesAction
Imports System.Data.SqlClient
Partial Class frm07_AddNewPart
    Inherits InventoryBase
    Protected designerPlaceholderDeclaration As Object
    Private DSMfrs As New DataSet()
    Private DSDetail As New DataSet()
    Dim Mode As String
    Dim SysPartID As Integer = 0
    Dim Item As String
    Private Sub cboCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCategory.SelectedIndexChanged
        cboSubCategory.Items.Clear()
        LoadCatSubItem(cboCategory, cboSubCategory, GetCurrentPageName(), Mode)
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Not IsNothing(Session("Mode")) Then
            Mode = Session("Mode").ToString()
            If (Mode = "Edit") Then
                SetPageTitle("Edit Inventory Item")
            ElseIf (Mode = "Add") Then
                SetPageTitle("Add Inventory Item")
            End If
            If Not Page.IsPostBack = True Then
                If Mode = "Edit" Then
                    Populatedetails()

                    If Not Request.QueryString(0) Is Nothing Then
                        Item = Request.QueryString(0)
                        GetCurrentRecord(Item)
                    End If

                    chckIsDeleted.Visible = True
                    lbldeleted.Visible = True
                    lblCQOH.Visible = True
                ElseIf Mode = "Add" Then

                    Dim InvDAO As New InventoryDAO
                    DSMfrs = InvDAO.GetSearchMfrforEdit()
                    PopulateDataGridViewDataSet(gdvMfrs, DSMfrs)

                    gdvMfrs.Columns(1).Visible = False
                    Populatedetails()

                    chckIsDeleted.Visible = False
                    lbldeleted.Visible = False
                    lblCQOH.Visible = False
                End If
            End If
        End If
    End Sub

    Private Sub Populatedetails()
        cboCategory.Items.Clear()
        cboSubCategory.Items.Clear()
        LoadCatSubItem(cboCategory, cboSubCategory, GetCurrentPageName(), Mode)

        Dim InvDAO As New InventoryDAO
        Dim strSql As SqlDataReader
        strSql = InvDAO.GetUnit()
        PopulateComboBox(cboUnitOfRec, strSql)

    End Sub

    Private Sub GetCurrentRecord(ByVal strQuery As String)
        'Get the record from the database and populate controls on the form

        Dim InvDAO As New InventoryDAO
        DSDetail = InvDAO.GetInventoryDetail(strQuery)

        txtDescription1.Text = DSDetail.Tables(0).Rows(0).Item("Description1").ToString()
        'txtDescription2.Text = DSDetail.Tables(0).Rows(0).Item("Description2").ToString()
        txtUnitCost.Text = Math.Round(CDec(DSDetail.Tables(0).Rows(0).Item("UnitCost").ToString()), 2)

        txtQtyPerUnit.Text = DSDetail.Tables(0).Rows(0).Item("QtyPerUnit").ToString()
        txtSize.Text = DSDetail.Tables(0).Rows(0).Item("Size").ToString()
        txtSystempartNo.Text = DSDetail.Tables(0).Rows(0).Item("fldStockNo").ToString()

        cboCategory.SelectedItem.Text = DSDetail.Tables(0).Rows(0).Item("Category").ToString()

        cboSubCategory.SelectedItem.Text = DSDetail.Tables(0).Rows(0).Item("SubCategory").ToString()
        cboUnitOfRec.SelectedItem.Text = DSDetail.Tables(0).Rows(0).Item("UnitofRec").ToString()

        If DSDetail.Tables(0).Rows(0).Item("NotActive") <> Nothing Then
            If (CType(DSDetail.Tables(0).Rows(0).Item("NotActive"), Boolean) = True) Then
                chckIsDeleted.Checked = True
            Else
                chckIsDeleted.Checked = False
            End If
        Else
            chckIsDeleted.Checked = False
        End If

        If Not DSDetail.Tables(0).Rows(0).Item("MaxQty") Is Nothing Then
            If Not IsDBNull(DSDetail.Tables(0).Rows(0).Item("MaxQty")) Then
                txtMax.Text = DSDetail.Tables(0).Rows(0).Item("MaxQty").ToString()
            End If

        End If



        If Not DSDetail.Tables(0).Rows(0).Item("MinQty") Is Nothing Then
            If Not IsDBNull(DSDetail.Tables(0).Rows(0).Item("MinQty")) Then
                txtMin.Text = DSDetail.Tables(0).Rows(0).Item("MinQty").ToString()
            End If
        End If


        If Not DSDetail.Tables(0).Rows(0).Item("CQOH") Is Nothing Then
            If Not IsDBNull(DSDetail.Tables(0).Rows(0).Item("CQOH")) Then
                lblCQOHValue.Text = DSDetail.Tables(0).Rows(0).Item("CQOH").ToString()
            Else
                lblCQOHValue.Text = 0
            End If
        Else
            lblCQOHValue.Text = 0
        End If

        fillMfrgdv()
        cboCategory.Enabled = False
        cboSubCategory.Enabled = False
    End Sub

    Private Sub fillMfrgdv()
        Dim mfrDataset As New DataSet()
        Dim InvDAO As New InventoryDAO
        Try '
            DSMfrs = InvDAO.GetSearchMfrforEdit()
            PopulateDataGridViewDataSet(gdvMfrs, DSMfrs)
            If Mode = "Edit" Then

                Item = Request.QueryString(0)
                SysPartID = InvDAO.GetItemId(Item)
                mfrDataset = InvDAO.GetMfrForItem(SysPartID)

                If mfrDataset.Tables.Count = 1 AndAlso mfrDataset.Tables(0).Rows.Count() > 0 Then

                    For i = 0 To gdvMfrs.Rows.Count - 1

                        Dim chk As CheckBox = CType(gdvMfrs.Rows(i).Cells(0).FindControl("chkItem"), CheckBox)
                        For j = 0 To (mfrDataset.Tables(0).Rows.Count() - 1)

                            If (gdvMfrs.Rows(i).Cells(3).Text.ToString() = mfrDataset.Tables(0).Rows(j).Item("MfrId").ToString()) Then

                                chk.Checked = True

                            End If

                        Next

                    Next
                End If
            End If
        Catch ex As Exception
            Throw
        Finally
            InvDAO = Nothing
            mfrDataset.Dispose()
            mfrDataset = Nothing

        End Try


    End Sub
    Protected Sub cutValidator_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cutValidator.ServerValidate

        args.IsValid = True
        With cutValidator
            ' Validate data: shop name, item type, unit of rec, and description are required
            If cboCategory.Items.Count <= 0 Then
                .ErrorMessage = My.Resources.ErrorMessage.SelectCategory
                args.IsValid = False
                Exit Sub
            ElseIf cboCategory.SelectedItem.Text = String.Empty Then
                .ErrorMessage = My.Resources.ErrorMessage.SelectCategory
                args.IsValid = False
                Exit Sub
            ElseIf cboSubCategory.Items.Count <= 0 Then
                .ErrorMessage = My.Resources.ErrorMessage.SelectSubCategory
                args.IsValid = False
                Exit Sub
            ElseIf cboSubCategory.SelectedItem.Text = String.Empty Then
                .ErrorMessage = My.Resources.ErrorMessage.SelectSubCategory
                args.IsValid = False
                Exit Sub
            ElseIf txtMin.Text = "0" Then
                .ErrorMessage = "Please enter valid min value greater then zero."
                args.IsValid = False
                Exit Sub
            ElseIf txtMax.Text = "0" Then
                .ErrorMessage = "Please enter valid max value greater then zero."
                args.IsValid = False
                Exit Sub
            ElseIf txtMin.Text.Trim.Length > 0 AndAlso txtMax.Text.Trim.Length > 0 AndAlso CInt(txtMin.Text) > CInt(txtMax.Text) Then
                .ErrorMessage = "Min value can not be greater then max value."
                args.IsValid = False
                Exit Sub
            ElseIf txtDescription1.Text = "" Then
                .ErrorMessage = My.Resources.ErrorMessage.NEWPART_UNITDESC
                args.IsValid = False
                Exit Sub
            End If
            If IsNumeric(txtUnitCost.Text) Then
                If txtUnitCost.Text >= 0 Then
                    txtUnitCost.Text = Format(txtUnitCost.Text, "currency")
                Else
                    .ErrorMessage = My.Resources.ErrorMessage.NEWPART_UNITCOST
                    args.IsValid = False
                    Exit Sub
                End If
            ElseIf txtUnitCost.Text <> "" Then
                .ErrorMessage = My.Resources.ErrorMessage.NEWPART_UNITCOST
                args.IsValid = False
                Exit Sub
            End If
            If IsNumeric(txtCQOH.Text) Then
                If txtCQOH.Text < 0 Or InStr(txtCQOH.Text, ".") <> 0 Then
                    .ErrorMessage = My.Resources.ErrorMessage.NEWPART_POSITIVEINT
                    args.IsValid = False
                    Exit Sub
                End If
            ElseIf txtCQOH.Text.Trim().Length > 0 Then
                .ErrorMessage = My.Resources.ErrorMessage.NEWPART_POSITIVEINT2
                args.IsValid = False
                Exit Sub
            End If
            If IsNumeric(txtQtyPerUnit.Text) AndAlso Isint(txtQtyPerUnit.Text) = True Then
                If txtQtyPerUnit.Text < 0 Or InStr(txtQtyPerUnit.Text, ".") <> 0 Then
                    .ErrorMessage = My.Resources.ErrorMessage.NEWPART_POSITIVEINT
                    args.IsValid = False
                    Exit Sub
                End If
            ElseIf txtQtyPerUnit.Text.Trim().Length > 0 Then
                .ErrorMessage = My.Resources.ErrorMessage.NEWPART_POSITIVEINT2
                args.IsValid = False
                Exit Sub
            End If
            If IsNumeric(txtMaxCapcity.Text) Then
                If txtMaxCapcity.Text < 1 Or InStr(txtMaxCapcity.Text, ".") <> 0 Then
                    .ErrorMessage = My.Resources.ErrorMessage.NEWPART_POSITIVEINT2
                    args.IsValid = False
                    Exit Sub
                End If
            ElseIf txtMaxCapcity.Text <> "" Then
                .ErrorMessage = My.Resources.ErrorMessage.NEWPART_POSITIVEINT2
                args.IsValid = False
                Exit Sub
            End If
            If chckIsDeleted.Checked Then
                If CInt(lblCQOHValue.Text) > 0 Then
                    .ErrorMessage = "Item can not be marked as dead iteam.CQOH is greater then zero."
                    args.IsValid = False
                    Exit Sub
                End If
            End If
        End With
    End Sub

    Private Function Isint(ByVal obj As Object) As Boolean
        Dim bl As Boolean

        Dim i As Integer
        Try
            i = Convert.ToInt32(obj)
            bl = True

        Catch ex As System.Exception
            bl = False
        End Try

        Return bl
    End Function

    Private Sub btnMfrs_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMfrs.Click
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If
        RedirectTo("frmMfrInfo.aspx")
    End Sub
    Private Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnRefresh.Click
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If
        txtUnitCost.Text = ""
        txtDescription1.Text = ""
        cboUnitOfRec.SelectedItem.Text = ""
        txtSize.Text = ""
        txtQtyPerUnit.Text = ""
        chkShowSelect.Checked = False
        txtSystempartNo.Text = ""

        If Mode = "Add" Then
            cboCategory.Items.Clear()
            cboSubCategory.Items.Clear()
            Populatedetails()
            fillMfrgdv()
        Else
            Item = Request.QueryString(0)
            Populatedetails()
            GetCurrentRecord(Item)
        End If

    End Sub

    Private Function IsSmallint(ByVal obj As Object) As Boolean
        Dim bl As Boolean

        Dim i As Integer '
        Try
            i = Convert.ToInt16(obj)
            bl = True

        Catch ex As System.Exception '
            bl = False
        End Try

        Return bl
    End Function
    Private Sub btnProcess_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnProcess.Click

        Dim NotActive As Integer
        If (chckIsDeleted.Checked) Then
            NotActive = 1
        Else
            NotActive = 0
        End If

        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If
        Mode = Session("Mode").ToString()

        Dim SysPartId As Integer
        Dim InvDAO As New InventoryDAO
        With cutValidator
            If (txtQtyPerUnit.Text.Trim() = "") Then
                .ErrorMessage = My.Resources.ErrorMessage.INV_QtyPerUnitEmpty
                .IsValid = False
                Exit Sub

            ElseIf Not IsSmallint(txtQtyPerUnit.Text.Trim()) Then
                .ErrorMessage = My.Resources.ErrorMessage.INV_QTTINT
                .IsValid = False
                Exit Sub
            ElseIf Not IsNumeric(txtQtyPerUnit.Text.Trim()) Then
                .ErrorMessage = My.Resources.ErrorMessage.INV_QTTINT
                .IsValid = False
                Exit Sub
            ElseIf (CInt(txtQtyPerUnit.Text.Trim()) <= 0 And txtQtyPerUnit.Text.Trim() <> "") Then
                .ErrorMessage = My.Resources.ErrorMessage.INV_QTYPerUnitPositive
                .IsValid = False
                Exit Sub
            ElseIf (txtUnitCost.Text.Trim = "") Then
                .ErrorMessage = My.Resources.ErrorMessage.UnitCost
                .IsValid = False
                Exit Sub
            ElseIf Not IsNumeric(txtUnitCost.Text.Trim()) Then
                .ErrorMessage = My.Resources.ErrorMessage.INV_UnitCostNumeric
                .IsValid = False
                Exit Sub
            ElseIf (CInt(txtUnitCost.Text.Trim() <= 0) And txtUnitCost.Text.Trim() <> "") Then
                .ErrorMessage = My.Resources.ErrorMessage.INV_UnitCostPositive
                .IsValid = False
                Exit Sub
            End If



            If Page.IsValid = True Then
                If Not cboCategory.SelectedItem.ToString() = "" Then
                    If Not txtUnitCost.Text.ToString() = "" Then
                        If Not cboUnitOfRec.SelectedItem.ToString() = "" Then
                            If Not txtDescription1.Text.ToString() = "" Then
                                If Not txtSystempartNo.Text.ToString() = "" Then

                                    If Mode = "Add" Then

                                        Dim count As Integer = InvDAO.GetCountItemId(cboCategory.SelectedValue.ToString(), cboSubCategory.SelectedValue.ToString(), txtDescription1.Text.ToString())

                                        If count = 0 Then

                                            SysPartId = InvDAO.GetCountMaxItemId()
                                            InvDAO.InsertInventory(SysPartId, cboCategory.SelectedValue.ToString(), cboSubCategory.SelectedValue.ToString(), txtUnitCost.Text.ToString(), cboUnitOfRec.SelectedValue.ToString(), txtQtyPerUnit.Text.ToString(), txtDescription1.Text.ToString().Trim(), txtSize.Text.ToString(), txtSystempartNo.Text.ToString(), txtMin.Text, txtMax.Text)

                                            SysPartId = InvDAO.GetItemIdByDescription(txtDescription1.Text.ToString())

                                            For i = 0 To gdvMfrs.Rows.Count - 1

                                                Dim chk As CheckBox = CType(gdvMfrs.Rows(i).Cells(0).FindControl("chkItem"), CheckBox)

                                                If (chk.Checked = True) Then
                                                    Dim Id As String = CType(gdvMfrs.Rows(i).Cells(1).FindControl("lblMfrId"), Label).Text

                                                    InvDAO.AddMfr(Id.ToString(), SysPartId.ToString())
                                                End If
                                            Next
                                            txtUnitCost.Text = ""
                                            txtDescription1.Text = ""
                                            cboUnitOfRec.SelectedItem.Text = ""
                                            txtSize.Text = ""
                                            txtQtyPerUnit.Text = ""
                                            chkShowSelect.Checked = False
                                            txtSystempartNo.Text = ""
                                            txtMax.Text = ""
                                            txtMin.Text = ""
                                            fillMfrgdv()
                                            cutValidator.ErrorMessage = My.Resources.ErrorMessage.COMMON_RECORDSAVED
                                            cutValidator.IsValid = False
                                            Populatedetails()

                                        Else
                                            .ErrorMessage = My.Resources.ErrorMessage.ItemExist
                                            .IsValid = False
                                        End If
                                    ElseIf Mode = "Edit" Then
                                        Item = Request.QueryString(0)
                                        SysPartId = Item

                                        Dim PartData(,) As String
                                        Dim intResult As Integer
                                        PartData = New String(16, 1) {{"SysPartNo", SysPartId.ToString()}, {"Category", cboCategory.Text.ToString()}, _
                                                    {"SubCategory", cboSubCategory.Text.ToString()}, {"ItemType", cboItemType.Text.ToString()}, {"SubItemType", cboSubItemType.Text.ToString()}, _
                                                    {"Description1", txtDescription1.Text.ToString().Trim()}, {"Description2", ""}, {"UnitCost", Math.Round(CDec(txtUnitCost.Text.ToString()), 2).ToString()}, _
                                                    {"UnitofRec", cboUnitOfRec.Text.ToString()}, {"QtyPerUnit", txtQtyPerUnit.Text.ToString()}, {"Size", txtSize.Text.ToString()}, _
                                                    {"EditUser", GetUserName()}, {"EditCPU", My.Computer.Name}, {"StockNo", txtSystempartNo.Text.ToString().Trim()}, {"NotActive", NotActive}, {"MaxValue", txtMax.Text}, {"MinValue", txtMin.Text}}

                                        intResult = UpdateInvPart(True, PartData, False)

                                        If intResult = 1 Then ' by Farkas 8Feb11  UpdateInvPart() returns zero when the action query fails
                                            InvDAO.DeleteMfrforItem(SysPartId)
                                            For i = 0 To gdvMfrs.Rows.Count - 1

                                                Dim chk As CheckBox = CType(gdvMfrs.Rows(i).Cells(0).FindControl("chkItem"), CheckBox)

                                                If (chk.Checked = True) Then
                                                    Dim Id As String = CType(gdvMfrs.Rows(i).Cells(1).FindControl("lblMfrId"), Label).Text

                                                    InvDAO.AddMfr(Id.ToString(), SysPartId.ToString())

                                                End If
                                            Next
                                            .ErrorMessage = My.Resources.ErrorMessage.COMMON_RECORDSAVED
                                        Else
                                            .ErrorMessage = My.Resources.ErrorMessage.PART_NOupdate & SysPartId.ToString()

                                        End If
                                        .IsValid = False ' by Farkas 8Feb11 used to follow immediately after cutValidator.ErrorMessage 

                                    End If

                                ElseIf txtSystempartNo.Text.ToString() = "" Then
                                    .ErrorMessage = My.Resources.ErrorMessage.StockNo
                                    .IsValid = False
                                End If
                            ElseIf txtDescription1.Text.ToString() = "" Then
                                .ErrorMessage = My.Resources.ErrorMessage.Item
                                .IsValid = False

                            End If

                        ElseIf cboUnitOfRec.SelectedItem.ToString() = "" Then
                            .ErrorMessage = My.Resources.ErrorMessage.UnitOfRec
                            .IsValid = False
                        End If
                    ElseIf txtUnitCost.Text.ToString() = "" Then
                        .ErrorMessage = My.Resources.ErrorMessage.UNITCOSTPOSITIVE
                        .IsValid = False
                    End If
                ElseIf cboCategory.SelectedItem.ToString() = "" Then
                    .ErrorMessage = My.Resources.ErrorMessage.SelectCategory
                    .IsValid = False
                End If
            End If
        End With
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If
        Mode = ""
        Session("Mode") = ""
        Response.Redirect("../Inventory/frm54_SearchInventory.aspx?Mode=edit")

    End Sub

    Protected Sub chkShowSelect_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkShowSelect.CheckedChanged

        If chkShowSelect.Checked = True Then
            For i = 0 To gdvMfrs.Rows.Count - 1

                Dim chk As CheckBox = CType(gdvMfrs.Rows(i).Cells(0).FindControl("chkItem"), CheckBox)

                If (chk.Checked = False) Then

                    gdvMfrs.Rows(i).Visible = False

                End If
            Next
        ElseIf chkShowSelect.Checked = False Then
            For i = 0 To gdvMfrs.Rows.Count - 1

                Dim chk As CheckBox = CType(gdvMfrs.Rows(i).Cells(0).FindControl("chkItem"), CheckBox)

                If (chk.Checked = False) Then

                    gdvMfrs.Rows(i).Visible = True

                End If
            Next

        End If

    End Sub

    Private Sub cboSubCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSubCategory.SelectedIndexChanged
        LoadCatSubItem(cboCategory, cboSubCategory, GetCurrentPageName(), Mode)
    End Sub

End Class

