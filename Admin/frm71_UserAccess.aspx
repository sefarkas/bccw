﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frm71_UserAccess.aspx.vb"
    Inherits=".frm71_UserAccess" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>User Access Role</title>
</head>
<body>
    <style type="text/css">
        .style2
        {
            width: 79px;
        }
        .style3
        {
            width: 87px;
        }
    </style>
    <form id="form1" runat="server">
    <asp:Panel ID="pnBody" Style="z-index: 105; left: 45px; top: 130px; font-family: 'Microsoft Sans Serif';
        font-size: 8pt; position: relative; width: 872px; height: 450px;" runat="server">
        <table>
            <tr>
                <td class="style2">
                    <asp:Label ID="lblUser" runat="server" Text="Login ID: "></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtUser" runat="server" Width="156px"></asp:TextBox>
                </td>
                <td class="style3" align="center">
                    <asp:Label ID="lblRole" runat="server" Text="Role : "></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlrole" runat="server" Height="21px" Width="133px" DataTextField="LoginID">
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
        <asp:ImageButton ID="btnProcess" Style="z-index: 103; left: 180px; top: 47px; font-family: 'Trebuchet MS';
            font-size: 9pt; position: absolute;" runat="server" Text="Process" ImageUrl="~/Images/save_static.png">
        </asp:ImageButton>
        <asp:CustomValidator ID="cutValidator" runat="server" Display="None"></asp:CustomValidator>
        <asp:Panel ID="Panel3" runat="server" Width="870px" Height="251px" Style="top: 60;
            margin-top: 56px;" ScrollBars="Auto">
            <asp:GridView ID="dgvSearchResult" SkinID="gridviewSkin" runat="server" Width="571px"
                Height="251px" Style="margin-top: 6px;">
                <Columns>
                    <asp:TemplateField ItemStyle-Width="90px">
                        <ItemTemplate>
                            <asp:ImageButton ID="btnSelect" runat="server" ImageUrl="~/Images/delete_static.png"
                                CommandName="btnSelect" CommandArgument='<%# Eval("LoginID") %>' Width="90px" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:Panel>
    </asp:Panel>
    </form>
</body>
</html>
