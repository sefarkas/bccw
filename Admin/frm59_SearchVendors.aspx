<%@ Page language="VB"  Codebehind="frm59_SearchVendors.aspx.vb" AutoEventWireup="false" Inherits="frm59_SearchVendors" MasterPageFile="~/Master/Inventory.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:Panel id="pnBody" style="z-index:105; left:0px; top:1px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:relative;" 
    runat="server" Width="950" Height="370"  >

<asp:Panel id="Panel3" 
    style="z-index:104; left:0px; top:133px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:absolute;" 
    runat="server" Width="830" height="180" ScrollBars="Auto">
<asp:GridView id="dgvSearchResult" SkinID="gridviewSkin"
        style="z-index:135; left:40px; top:11px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:absolute;" 
        runat="server" Width="790px" Height="284px">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:ImageButton ID="btnSelect" runat="server" Text="Edit" CommandName="btnSelect" SkinID="EditButton" />
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
    </asp:GridView>
</asp:Panel>
<asp:Panel id="pnlSearchCriteria" SkinID="panelSearch"
        style="z-index:107; left:40px; top:10px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:absolute;" 
        runat="server" Width="780" Height="98"><asp:TextBox id="txtTypeOfWork" style="z-index:108; left:96px; top:62px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="235" Height="21" TabIndex="8"></asp:TextBox>
<asp:DropDownList id="cboTaxID" style="z-index:109; left:431px; top:35px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="235" Height="21"></asp:DropDownList>
<asp:Label id="Label4" style="z-index:110; left:382px; top:38px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="51" Height="13">Tax ID:</asp:Label>
<asp:DropDownList id="cboDCASNumber" style="z-index:111; left:431px; top:62px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="235" Height="21"></asp:DropDownList>
<asp:DropDownList id="cboVendorCode" style="z-index:112; left:96px; top:35px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="235" Height="21"></asp:DropDownList>
<asp:Label id="Label2" style="z-index:113; left:7px; top:65px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="91" Height="13">Type Of Work:</asp:Label>
<asp:Label id="Label9" style="z-index:114; left:373px; top:11px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="19" Height="13">or</asp:Label>
<asp:DropDownList id="cboVendorName" style="z-index:115; left:96px; top:8px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="235" Height="21"></asp:DropDownList>
<asp:Label id="Label6" style="z-index:116; left:7px; top:11px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="90" Height="13">Vendor Name:</asp:Label>
<asp:TextBox id="txtVendorName" style="z-index:117; left:431px; top:8px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="235" Height="21" TabIndex="5"></asp:TextBox>
<asp:Label id="Label3" style="z-index:118; left:10px; top:38px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="87" Height="13">Vendor Code:</asp:Label>
<asp:Label id="Label5" style="z-index:119; left:338px; top:65px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="95" Height="13">DCAS Number:</asp:Label>
</asp:Panel>
    <asp:CustomValidator ID="cutValidator" runat="server" Display="None"></asp:CustomValidator>
    <asp:ImageButton ID="btnFind" runat="server" skinid="findButton" 
        style="position:absolute; top: 110px; left: 87px;" />
    <asp:ImageButton ID="btnAddNew" runat="server" skinid="AddNewButton" 
        style="position:absolute; top: 110px; left: 282px;" />
    <asp:ImageButton ID="btnReset" runat="server"  skinid="resetButton"
        style="position:absolute; top: 110px; left: 181px;" />
</asp:Panel>
</asp:Content>