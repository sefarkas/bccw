<%@ Page language="VB" Codebehind="frmVendors_61.aspx.vb" AutoEventWireup="false" Inherits="frmVendors" MasterPageFile="~/Master/Inventory.Master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:Panel id="pnBody" style="z-index:105; left:8px; top:1px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:relative;" 
    runat="server" Width="950" Height="370"  >

<asp:Panel ID="PnlMessage" runat="server" SkinID="panelskin" Width="773px">
        <asp:Label ID="lblVendorDetail" runat="server" Text="Vendor Detail" ></asp:Label>
    </asp:Panel>
    
  <asp:Panel id="pnlDetail" 
    style="z-index:101; left:40px; top:10px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:absolute; height: 217px; width: 776px;" 
    runat="server">
    <table>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server">Vendor Code:</asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtVendorCode" runat="server" TabIndex="8"></asp:TextBox>
            </td>
            <td>
                <asp:Label ID="Label5" runat="server">Vendor Name:</asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtVendorName" runat="server" TabIndex="7"></asp:TextBox>
            </td>
            <td>
                <asp:Label ID="Label6" runat="server">Tax ID:</asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtTaxID" runat="server" TabIndex="9"></asp:TextBox>
            </td>
        </tr>
        <tr>
        <td>
        <asp:Label id="Label1"  runat="server" >DCAS Number:</asp:Label>
        </td>
        <td>
      <asp:TextBox id="txtDCASNumber"  runat="server" TabIndex="10"></asp:TextBox>  
        </td>
        <td>
        <asp:Label id="Label9"  runat="server" >Type Of Work:</asp:Label>
        </td>
        <td>
        <asp:TextBox id="txtTypeOfWork"  runat="server"  TabIndex="11"></asp:TextBox>
        </td>
        </tr>
        <tr>
        <td><asp:Label id="Label14"  runat="server" >Address:</asp:Label></td>
        <td></td>
        <td>
        
        </td>
        <td></td>
        <td><asp:Label id="Label29" runat="server">Contact:</asp:Label></td>
        <td></td>
        </tr>
        <tr>
        <td><asp:Label id="Label25"  runat="server" >Street:</asp:Label>
        </td>
        <td><asp:TextBox id="txtStreet"  runat="server"  TabIndex="12"></asp:TextBox></td>
        <td>
        <asp:Label id="Label24"  runat="server" >City:</asp:Label>
        </td>
        <td><asp:TextBox id="txtCity"  runat="server"  TabIndex="13"></asp:TextBox></td>
        <td><asp:Label id="Label7"  runat="server" >First Name:</asp:Label></td>
        <td><asp:TextBox id="txtFirstName"  runat="server" TabIndex="18"></asp:TextBox></td>
        </tr>
        <tr>
        <td><asp:Label id="Label12"  runat="server" >State:</asp:Label></td>
        <td><asp:TextBox id="txtState"  runat="server"  TabIndex="14"></asp:TextBox></td>
        <td><asp:Label id="Label11"  runat="server" Width="30" Height="13">Zip:</asp:Label></td>
        </tr>
    </table>
    


  
  <asp:TextBox id="txtZip" style="z-index:102; left:304px; top:149px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="65" Height="21" TabIndex="15"></asp:TextBox>
<asp:Label id="Label15" style="z-index:103; left:480px; top:179px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="32" Height="13">Fax:</asp:Label>


<asp:TextBox id="txtTitle" style="z-index:106; left:510px; top:149px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="173" Height="21" TabIndex="20"></asp:TextBox>
<asp:TextBox id="txtPhoneExt" style="z-index:107; left:304px; top:176px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="65" Height="21" TabIndex="17"></asp:TextBox>

<asp:TextBox id="txtLastName" style="z-index:109; left:510px; top:122px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="173" Height="21" TabIndex="19"></asp:TextBox>


<asp:Label id="Label13" 
          style="z-index:112; left:46px; top:179px; font-family:'Verdana'; font-size:8pt; position:absolute; right: 593px;" 
          runat="server" Width="47" Height="13">Phone:</asp:Label>












<asp:Label id="Label4" style="z-index:129; left:476px; top:152px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="36" Height="13">Title:</asp:Label>
<asp:Label id="Label3" style="z-index:130; left:440px; top:125px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="72" Height="13">Last Name:</asp:Label>
<asp:Label id="Label16" style="z-index:131; left:275px; top:179px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="30" Height="13">Ext:</asp:Label>
</asp:Panel>
<asp:Panel id="pnlHeader" 
    style="z-index:132; left:8px; top:38px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:absolute; height: 80px; width: 778px;" 
    runat="server">

<table class="Table">
    <tr>
       <td class="style10" ><asp:Label ID="Label8"  runat="server" >Vendor Code:</asp:Label></td>
       <td class="style8"><asp:DropDownList id="cboVendorCode" runat="server" 
               Height="16px" Width="123px" ></asp:DropDownList></td>
       <td class="style11"><asp:Label id="Label10"  runat="server" >Vendor Name:</asp:Label></td>
  <td><asp:DropDownList id="cboVendorName"  runat="server" Height="16px" Width="151px" 
           ></asp:DropDownList></td>
  <td><asp:Button ID="btnGo" runat="server" Text="Go"  /></td>
  <td> <asp:Button ID="btnSave" runat="server" Text="Save" 
           /></td>
  <td   > 
      <asp:Button ID="btnCancel" runat="server" Text="Cancel" 
           /></td>
  <td  >   
      <asp:Button ID="btnClose" runat="server" Text="Close" 
         /></td>
    </tr>
  </table>
    <table class="Table" >
    <tr>
    <td  class="ErrorMsg">  
        <asp:Label ID="lblMsg" Visible="False"  runat="server" Height="21px" 
            Width="666px"></asp:Label></td>
          <td>   <asp:Button ID="btnConfirm" Visible="false" Text="Go Anyway"  
        runat="server"/></td>
    </tr>
    </table>
         
</asp:Panel>
</asp:Panel>
</asp:Content>