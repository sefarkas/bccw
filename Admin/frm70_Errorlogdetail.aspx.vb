﻿Imports DataAccess.clsQueriesSelect
Imports DataAccess.clsQueriesAction
Imports DataAccess.AdminDAO
Imports Entities
Imports System
Imports System.Data.SqlClient
Imports DataAccess

Imports System.Web.UI.WebControls
Public Class frm70_Errorlogdetail
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim errorId As String
        errorId = Request.QueryString(0)

        Dim errorlog As Errorlog
        Dim adminDAO As New AdminDAO
        errorlog = adminDAO.GetErrorlogById(errorId)


        If Not errorlog Is Nothing Then
            txtUser.Text = errorlog.UserID.ToString.Trim()
            txtDate.Text = errorlog.EntryDate.ToString.Trim()
            txtErrormsg.Text = errorlog.ErrorMessage.ToString.Trim()
            txturl.Text = errorlog.Url.ToString.Trim()
            txtStacktrace.Text = errorlog.StackTrace.ToString.Trim()

        End If



    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If
        Response.Redirect("frm69_errorlog.aspx")
    End Sub

   
End Class