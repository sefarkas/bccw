﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Web
Imports System.Data
Imports System.Collections
Imports Security
''' <summary>
''' This method provides functionality to 
''' loads the users based on the personal ID,method returns true for authenticated users
''' Gets the list of users based on personal ID,assembles the list of Users from the DTO
'''  returns the new users and calls Createmembership method,method to returns the 
''' new member created,  method to returns the Membership based on the group name(DS249, Advocate and BCAD)
''' saves the User based on the boolean value, method to resets the password, method to deletes the user
''' method to generate the new user ID, method to validate the user from the group(DS249, Advocate and BCAD)
''' and method to return the string array by asigin respective roles to there members.
''' </summary>

Public Class User
    'Private usrMembership As Membership
    Public Sub New()
        m_personalId = -1
        m_userId = [String].Empty
        m_password = [String].Empty
        m_emailId = [String].Empty
        m_lastLoginDateTime = DateTime.MinValue
        m_lastLogin = DateTime.MinValue

    End Sub

#Region "Private Variables"
    ''' <summary>
    ''' Private Variables
    ''' </summary>
    Private m_personalId As Int64
    Private m_userId As [String]
    Private m_password As [String]
    Private m_emailId As [String]
    Private m_isTempPassword As [Boolean]
    Private m_isAdmin As [Boolean]
    Private m_lastLoginDateTime As DateTime
    Private m_lastLogin As DateTime

#End Region

#Region "Public Property"

    ''' <summary>
    ''' Public Property
    ''' </summary>
    Public Property PersonalId() As Int64
        Get
            Return m_personalId
        End Get
        Set(ByVal value As Int64)
            m_personalId = value
        End Set
    End Property

    Public Property UserId() As [String]
        Get
            Return m_userId
        End Get
        Set(ByVal value As [String])
            m_userId = value
        End Set
    End Property

    Public Property Password() As [String]
        Get
            Return m_password
        End Get
        Set(ByVal value As [String])
            m_password = value
        End Set
    End Property

    Public Property EmailId() As [String]
        Get
            Return m_emailId
        End Get
        Set(ByVal value As [String])
            m_emailId = value
        End Set
    End Property

    Public Property IsTempPassword() As [Boolean]
        Get
            Return m_isTempPassword
        End Get
        Set(ByVal value As [Boolean])
            m_isTempPassword = value
        End Set
    End Property

    Public Property IsAdmin() As [Boolean]
        Get
            Return m_isAdmin
        End Get
        Set(ByVal value As [Boolean])
            m_isAdmin = value
        End Set
    End Property


    Public Property LastLoginDateTime() As DateTime
        Get
            Return m_lastLoginDateTime
        End Get
        Set(ByVal value As DateTime)
            m_lastLoginDateTime = value
        End Set
    End Property
    Public Property LastLogin() As DateTime
        Get
            Return m_lastLogin
        End Get
        Set(ByVal value As DateTime)
            m_lastLogin = value
        End Set
    End Property


#End Region

#Region "Public Method"
    ''' <summary>
    ''' This method returns true for authenticated users
    ''' </summary>
    ''' <returns></returns>
    Public Function AuthenticateUser() As UserDTO

        Try
            Dim proxy As New Security.Security
            Dim listUser As UserDTO()



            listUser = proxy.AuthenticateUser(Me.m_userId, Me.m_password)
            If listUser IsNot Nothing Then
                If listUser.Count > 0 Then
                    Return listUser(0)
                Else
                    Return Nothing
                End If
            Else
                Return Nothing
            End If
        Catch ex As Exception
            'Throw New Exception("Web Service down")
            ' MessageBox(ex.Message)
            'ASPNET_MsgBox(ex.Message)
            Dim listUser1 As New UserDTO
            Dim errormsg As String
            errormsg = ""
            If ex.GetType.Name = "WebException" Then
                errormsg = "Unable to connect authentication service.Please contact administrator."
            End If
            listUser1.ErrorMessage = errormsg

            Return listUser1


        End Try
    End Function

 

#End Region
End Class
