﻿Imports DataAccess.clsQueriesSelect
Imports DataAccess
Imports System.Data.SqlClient
Public Class frm71_UserAccess
    Inherits System.Web.UI.Page
    '  Inherits InventoryBase

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            PopulateRoleddl()
            Populategridview()
        End If

    End Sub
    Private Sub PopulateRoleddl()
        Dim ReaderObj As SqlDataReader
        Dim adminDAO As New AdminDAO
        ReaderObj = adminDAO.populateRoles()
        PopulateComboBox(ddlrole, ReaderObj)
    End Sub

    Public Sub PopulateComboBox(ByVal ComboBoxObj As DropDownList, ByVal Results As SqlDataReader)


        ComboBoxObj.Items.Clear()

        While Results.Read()
            If Results.FieldCount = 1 Then
                If Not Results.Item(0) Is System.DBNull.Value Then
                    ComboBoxObj.Items.Add((Results.Item(0)))
                End If
            End If
        End While
        If ComboBoxObj.Items.Count > 0 Then
            ComboBoxObj.Items.Insert(0, "")
        End If
        Results.Close()

    End Sub

    Protected Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnProcess.Click
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If
        Dim adminDAO As New AdminDAO

        If (txtUser.Text.ToString() = "") Then
            cutValidator.ErrorMessage = My.Resources.ErrorMessage.Userlogin
            cutValidator.IsValid = False
        Else
            If (ddlrole.Text.ToString() = "") Then
                cutValidator.ErrorMessage = My.Resources.ErrorMessage.UserRole
                cutValidator.IsValid = False
            Else
                Dim count As Integer
                count = adminDAO.GetUserCount(txtUser.Text.ToString())
                If (count = 0) Then
                    adminDAO.AddUser(txtUser.Text.ToString(), ddlrole.SelectedValue.ToString())
                    cutValidator.ErrorMessage = My.Resources.ErrorMessage.COMMON_RECORDSAVED
                    cutValidator.IsValid = False
                    txtUser.Text = ""
                    PopulateRoleddl()
                    Populategridview()
                Else
                    cutValidator.ErrorMessage = My.Resources.ErrorMessage.UserExist
                    cutValidator.IsValid = False
                End If

            End If

        End If


    End Sub
    Protected Sub Populategridview()
        Dim sqlreader As DataSet
        Dim adminDAO As New AdminDAO
        sqlreader = adminDAO.GetSearchresults()
        PopulateDataGridViewDataSet(dgvSearchResult, sqlreader)
    End Sub
    Protected Sub dgvSearchResult_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles dgvSearchResult.PageIndexChanging
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If
        dgvSearchResult.PageIndex = e.NewPageIndex
        Populategridview()
    End Sub

    Protected Sub dgvSearchResult_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgvSearchResult.RowCommand
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If
        If e.CommandName = "btnSelect" Then
            Dim loginId As String
            loginId = e.CommandArgument()
            Dim adminDAO As New AdminDAO
            adminDAO.DeleteUser(loginId)
            Populategridview()
            cutValidator.ErrorMessage = My.Resources.ErrorMessage.Recorddeleted
            cutValidator.IsValid = False
        End If
    End Sub
    Public Sub PopulateDataGridViewDataSet(ByRef dgvObj As GridView, ByRef dsObj As DataSet)
        dgvObj.DataSource = dsObj.Tables(0)
        dgvObj.DataBind()

    End Sub
End Class