﻿
Imports System
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.IO
Imports DataAccess.clsQueriesSelect
Imports DataAccess
Public Class frm66_LocationMapping
    Inherits InventoryBase
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        SetPageTitle("Add New Location")
        If Not (Page.IsPostBack) Then
            makeInvisible()
        End If
    End Sub
    Protected Sub cutValidator_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cutValidator.ServerValidate


        args.IsValid = True

        ' Validate reqired fields
        If txtLocation.Text = "" Then
            cutValidator.ErrorMessage = My.Resources.BuildingName
            Exit Sub
        End If



    End Sub


    Protected Sub radBuilding_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radBuilding.CheckedChanged
        makeInvisible()
        makeInvisiblerad()

        lblAdd.Visible = True
        lblAdd.Text = "Add New Building"
        radAdd.Visible = True
        radAdd.Checked = False
        lblEdit.Visible = True
        lblEdit.Text = "Change Exist Building Name"
        radEdit.Visible = True
        radEdit.Checked = False
        radDelete.Visible = True
        radDelete.Checked = False
        lblDelete.Visible = True
        lblDelete.Text = "Delete Exist Building"



    End Sub
    Protected Sub radAdd_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radAdd.CheckedChanged

        ddlBuilding.Visible = False
        ddlAisle.Visible = False
        ddlTier.Visible = False
        ddlLevel.Visible = False
        lblBuildingdropdown.Visible = False
        lblAisleddl.Visible = False
        lblTierddl.Visible = False
        lblLevelddl.Visible = False
        btnDelete.Visible = False
        ddlAisle.Items.Clear()

        ddlTier.Items.Clear()
        ddlLevel.Items.Clear()
        If radBuilding.Checked = True Then
            lblLocation.Visible = True
            lblLocation.Text = "Enter Building Name:"
            txtLocation.Visible = True
            txtLocation.Text = ""
            btnSave.Visible = True

        ElseIf radAisle.Checked = True Then
            lblBuildingdropdown.Visible = True
            ddlBuilding.Visible = True
            PopulateBuilding()
            lblLocation.Visible = True
            lblLocation.Text = "Enter Aisle Name:"
            txtLocation.Text = ""
            txtLocation.Visible = True
            btnSave.Visible = True
        ElseIf radTier.Checked = True Then
            lblBuildingdropdown.Visible = True
            ddlBuilding.Visible = True
            PopulateBuilding()
            ddlAisle.Items.Clear()
            ddlAisle.Visible = True
            lblAisleddl.Visible = True
            lblLocation.Visible = True
            lblLocation.Text = "Enter Tier Name:"
            txtLocation.Text = ""
            txtLocation.Visible = True
            btnSave.Visible = True
        ElseIf radLevel.Checked = True Then
            lblBuildingdropdown.Visible = True
            ddlBuilding.Visible = True
            PopulateBuilding()
            ddlAisle.Items.Clear()
            ddlAisle.Visible = True
            lblAisleddl.Visible = True
            lblTierddl.Visible = True
            ddlTier.Items.Clear()
            ddlTier.Visible = True
            lblLocation.Visible = True
            lblLocation.Text = "Enter Level Name:"
            txtLocation.Text = ""
            txtLocation.Visible = True
            btnSave.Visible = True
        End If

    End Sub

    Protected Sub PopulateBuilding()
        Dim ReaderObj As DataSet
        Dim locDAO As New LocationDAO

        ReaderObj = locDAO.GetBuilding()
        PopulateComboBoxWithTwoFields(ddlBuilding, ReaderObj)

    End Sub




    Protected Sub radEdit_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radEdit.CheckedChanged

        ddlBuilding.Visible = False
        ddlAisle.Visible = False
        ddlTier.Visible = False
        ddlLevel.Visible = False
        lblBuildingdropdown.Visible = False
        lblAisleddl.Visible = False
        lblTierddl.Visible = False
        lblLevelddl.Visible = False
        btnDelete.Visible = False
        ddlAisle.Items.Clear()
        ddlTier.Items.Clear()
        ddlLevel.Items.Clear()
        If radBuilding.Checked = True Then
            lblBuildingdropdown.Visible = True
            lblBuildingdropdown.Text = "Select Building:"
            ddlBuilding.Visible = True
            PopulateBuilding()
            lblLocation.Visible = True
            lblLocation.Text = "Enter Building Name:"
            txtLocation.Visible = True
            txtLocation.Text = ""
            btnSave.Visible = True
        ElseIf radAisle.Checked = True Then
            lblBuildingdropdown.Visible = True
            lblBuildingdropdown.Text = "Select Building Name:"
            ddlBuilding.Visible = True
            PopulateBuilding()
            lblAisleddl.Visible = True
            lblAisleddl.Text = "Select Aisle:"
            ddlAisle.Visible = True
            lblLocation.Visible = True
            lblLocation.Text = "Enter Aisle Name:"
            txtLocation.Text = ""
            txtLocation.Visible = True
            btnSave.Visible = True
        ElseIf radTier.Checked = True Then
            lblBuildingdropdown.Visible = True
            ddlBuilding.Visible = True
            PopulateBuilding()
            ddlAisle.Items.Clear()
            lblAisleddl.Visible = True
            lblAisleddl.Text = "Select Aisle:"
            ddlAisle.Visible = True
            lblTierddl.Visible = True
            lblTierddl.Text = "Select Tier:"
            ddlTier.Items.Clear()
            ddlTier.Visible = True
            lblLocation.Visible = True
            lblLocation.Text = "Enter Tier Name:"
            txtLocation.Text = ""
            txtLocation.Visible = True
            btnSave.Visible = True
        ElseIf radLevel.Checked = True Then
            lblBuildingdropdown.Visible = True
            ddlBuilding.Visible = True
            PopulateBuilding()
            ddlAisle.Items.Clear()
            ddlAisle.Visible = True
            lblAisleddl.Visible = True
            lblAisleddl.Text = "Select Aisle:"
            lblTierddl.Visible = True
            lblTierddl.Text = "Select Tier:"
            ddlTier.Items.Clear()
            ddlTier.Visible = True
            lblLevelddl.Visible = True
            lblLevelddl.Text = "Select Level:"
            ddlLevel.Items.Clear()
            ddlLevel.Visible = True
            lblLocation.Visible = True
            lblLocation.Text = "Enter Level Name:"
            txtLocation.Text = ""
            txtLocation.Visible = True
            btnSave.Visible = True
        End If

    End Sub
    Protected Sub radDelete_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radDelete.CheckedChanged

        ddlBuilding.Visible = False
        ddlAisle.Visible = False
        ddlTier.Visible = False
        ddlLevel.Visible = False
        lblBuildingdropdown.Visible = False
        lblAisleddl.Visible = False
        lblTierddl.Visible = False
        lblLevelddl.Visible = False
        btnDelete.Visible = True
        'btnDelete.Text = "Delete"
        btnSave.Visible = False
        lblLocation.Visible = False
        txtLocation.Visible = False
        ddlAisle.Items.Clear()

        ddlTier.Items.Clear()
        ddlLevel.Items.Clear()
        If radBuilding.Checked = True Then

            lblBuildingdropdown.Visible = True
            lblBuildingdropdown.Text = "Select Building:"
            ddlBuilding.Visible = True
            PopulateBuilding()
           
        ElseIf radAisle.Checked = True Then
            lblBuildingdropdown.Visible = True
            lblBuildingdropdown.Text = "Select Building Name:"
            ddlBuilding.Visible = True
           
            PopulateBuilding()
            lblAisleddl.Visible = True
            lblAisleddl.Text = "Select Aisle:"
            ddlAisle.Visible = True
        ElseIf radTier.Checked = True Then
            lblBuildingdropdown.Visible = True
            ddlBuilding.Visible = True
           
            PopulateBuilding()
            ddlAisle.Items.Clear()
            lblAisleddl.Visible = True
            lblAisleddl.Text = "Select Aisle:"
            ddlAisle.Visible = True
            lblTierddl.Visible = True
            lblTierddl.Text = "Select Tier:"
            ddlTier.Items.Clear()
            ddlTier.Visible = True
        ElseIf radLevel.Checked = True Then
            lblBuildingdropdown.Visible = True
            ddlBuilding.Visible = True
           
            PopulateBuilding()
            ddlAisle.Items.Clear()
            ddlAisle.Visible = True
            lblAisleddl.Visible = True
            lblAisleddl.Text = "Select Aisle:"
            lblTierddl.Visible = True
            lblTierddl.Text = "Select Tier:"
            ddlTier.Items.Clear()
            ddlTier.Visible = True
            lblLevelddl.Visible = True
            lblLevelddl.Text = "Select Level:"
            ddlLevel.Items.Clear()
            ddlLevel.Visible = True
        End If
    End Sub

    Protected Sub radAisle_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radAisle.CheckedChanged

        makeInvisible()
        makeInvisiblerad()

        lblAdd.Visible = True
        lblAdd.Text = "Add New Aisle"
        radAdd.Visible = True
        radAdd.Checked = False
        lblEdit.Visible = True
        ddlAisle.Items.Clear()
        lblEdit.Text = "Change Exist Aisle Name"
        radEdit.Visible = True
        radEdit.Checked = False
        radDelete.Visible = True
        lblDelete.Visible = True
        radDelete.Checked = False
        lblDelete.Text = "Delete Exist Aisle"
    End Sub
    Protected Sub radTier_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radTier.CheckedChanged

        makeInvisible()
        makeInvisiblerad()
        lblAdd.Visible = True
        lblAdd.Text = "Add New Tier"
        radAdd.Visible = True
        radAdd.Checked = False
        lblEdit.Visible = True
        lblEdit.Text = "Change Exist Tier Name"
        radEdit.Visible = True
        radEdit.Checked = False
        radDelete.Visible = True
        lblDelete.Visible = True
        radDelete.Checked = False
        lblDelete.Text = "Delete Exist Tier"
        ddlAisle.Items.Clear()
        ddlTier.Items.Clear()

    End Sub
    Protected Sub radLevel_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radLevel.CheckedChanged

        makeInvisible()
        makeInvisiblerad()
        lblAdd.Visible = True
        lblAdd.Text = "Add New Level"
        radAdd.Visible = True
        radAdd.Checked = False
        lblEdit.Visible = True
        lblEdit.Text = "Change Exist Level Name"
        radEdit.Visible = True
        radEdit.Checked = False
        radDelete.Visible = True
        lblDelete.Visible = True
        radDelete.Checked = False
        lblDelete.Text = "Delete Exist Level"
        ddlAisle.Items.Clear()

        ddlTier.Items.Clear()
        ddlLevel.Items.Clear()

    End Sub
    Protected Sub ddlBuilding_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlBuilding.SelectedIndexChanged
        ddlAisle.Items.Clear()
        ddlTier.Items.Clear()
        ddlLevel.Items.Clear()
        Dim ReaderObj As DataSet
        Dim locDAO As New LocationDAO
        If Not ddlBuilding.SelectedValue() = "" Then
            ReaderObj = locDAO.GetAisle(ddlBuilding.SelectedValue())
            PopulateComboBoxWithTwoFields(ddlAisle, ReaderObj)
        End If

    End Sub
    Protected Sub ddlAisle_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlAisle.SelectedIndexChanged

        ddlTier.Items.Clear()
        ddlLevel.Items.Clear()
        Dim ReaderObj As DataSet
        Dim locDAO As New LocationDAO
        If Not ddlAisle.SelectedValue() = "" Then
            ReaderObj = locDAO.GetTier(ddlBuilding.SelectedValue(), ddlAisle.SelectedValue())
            PopulateComboBoxWithTwoFields(ddlTier, ReaderObj)
        End If
    End Sub
    Protected Sub ddlTier_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlTier.SelectedIndexChanged
        Dim ReaderObj As DataSet
        Dim locDAO As New LocationDAO

        ddlLevel.Items.Clear()
        If Not ddlTier.SelectedValue() = "" Then
            ReaderObj = locDAO.GetLevel(ddlBuilding.SelectedValue(), ddlAisle.SelectedValue(), ddlTier.SelectedValue())
            PopulateComboBoxWithTwoFields(ddlLevel, ReaderObj)
        End If

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click


        If radBuilding.Checked = True Then
            If radAdd.Checked = True Then
                If Not (txtLocation.Text.ToString() = "") Then

                    Dim locDAO As New LocationDAO
                    Dim count As Integer
                  
                    count = locDAO.GetBuildingCount(txtLocation.Text.ToString())
                    If (count = 0) Then
                       
                        locDAO.AddBuilding(txtLocation.Text.ToString())
                        cutValidator.ErrorMessage = My.Resources.ErrorMessage.COMMON_RECORDSAVED
                        cutValidator.IsValid = False
                        txtLocation.Text = ""
                        Clearddl()
                    Else
                        cutValidator.ErrorMessage = My.Resources.BuildingNameExist
                        cutValidator.IsValid = False
                    End If

                Else
                    cutValidator.ErrorMessage = My.Resources.BuildingName
                    cutValidator.IsValid = False


                End If
            ElseIf radEdit.Checked = True Then
                If Not ddlBuilding.SelectedItem.ToString() = "" Then
                    If Not (txtLocation.Text.ToString() = "") Then
                        Dim locDAO As New LocationDAO
                        Dim count As Integer
                        count = locDAO.GetBuildingCount(txtLocation.Text.ToString())
                        If (count = 0) Then
                            locDAO.UpdateBuilding(txtLocation.Text.ToString(), ddlBuilding.SelectedValue.ToString())
                            cutValidator.ErrorMessage = My.Resources.RecordUpdated
                            cutValidator.IsValid = False
                            Clearddl()
                            txtLocation.Text = ""
                        Else
                            cutValidator.ErrorMessage = My.Resources.BuildingNameExist
                            cutValidator.IsValid = False
                        End If
                    Else
                        cutValidator.ErrorMessage = My.Resources.BuildingName
                        cutValidator.IsValid = False
                    End If



                Else
                    cutValidator.ErrorMessage = My.Resources.SelectBuilding
                    cutValidator.IsValid = False
                End If

            End If
        ElseIf (radAisle.Checked = True) Then
            If radAdd.Checked = True Then

                If Not (ddlBuilding.SelectedItem.ToString() = "") Then

                    If Not txtLocation.Text.ToString() = "" Then

                        Dim count1 As Integer
                        Dim locDAO As New LocationDAO
                        count1 = locDAO.GetAisleCount(txtLocation.Text.ToString())
                        If (count1 = 0) Then
                    
                            locDAO.AddAisle(txtLocation.Text.ToString())
                        
                            Dim count As Integer
                            count = locDAO.GetLocationMappingCount(ddlBuilding.SelectedValue.ToString(), DBNull.Value.ToString(), DBNull.Value.ToString(), DBNull.Value.ToString())
                            If count = 1 Then

                                locDAO.UpdateLocationMappingAisle(ddlBuilding.SelectedValue.ToString(), txtLocation.Text.ToString())
                            Else
            
                                locDAO.LocationMappingAsileAdd(ddlBuilding.SelectedValue.ToString(), txtLocation.Text.ToString())

                            End If
                            cutValidator.ErrorMessage = My.Resources.ErrorMessage.COMMON_RECORDSAVED
                            cutValidator.IsValid = False
                            txtLocation.Text = ""
                            Clearddl()
                        ElseIf count1 > 0 Then
                            Dim count2 As Integer
                            count2 = locDAO.GetAisleBuildingCount(ddlBuilding.SelectedValue.ToString(), txtLocation.Text.ToString())
                            If count2 = 0 Then
                                Dim count As Integer
                                count = locDAO.GetLocationMappingCount(ddlBuilding.SelectedValue.ToString(), DBNull.Value.ToString(), DBNull.Value.ToString(), DBNull.Value.ToString())
                                If count = 1 Then

                                    locDAO.UpdateLocationMappingAisle(ddlBuilding.SelectedValue.ToString(), txtLocation.Text.ToString())
                                Else

                                    locDAO.LocationMappingAsileAdd(ddlBuilding.SelectedValue.ToString(), txtLocation.Text.ToString())

                                End If
                                cutValidator.ErrorMessage = My.Resources.ErrorMessage.COMMON_RECORDSAVED
                                cutValidator.IsValid = False
                                txtLocation.Text = ""
                                Clearddl()

                            Else

                                cutValidator.ErrorMessage = My.Resources.AisleNameExist
                                cutValidator.IsValid = False
                                txtLocation.Text = ""
                            End If
                        End If

                    Else
                        cutValidator.ErrorMessage = My.Resources.AisleName
                        cutValidator.IsValid = False

                    End If



                Else
                    cutValidator.ErrorMessage = My.Resources.SelectBuilding
                    cutValidator.IsValid = False


                End If

            ElseIf radEdit.Checked = True Then
                If Not (ddlBuilding.SelectedItem.ToString() = "") Then
                    If Not (ddlAisle.SelectedItem.ToString() = "") Then

                        If Not txtLocation.Text.ToString() = "" Then
                         
                            Dim count1 As Integer
                            Dim locDAO As New LocationDAO
                            count1 = locDAO.GetAisleCount(txtLocation.Text.ToString())
                            If (count1 = 0) Then
                                Dim count2 As Integer
                                count2 = locDAO.GetBuildingCountByAisle(ddlAisle.SelectedValue.ToString())
                                If count2 = 1 Then
                                    locDAO.UpdateAisle(txtLocation.Text.ToString(), ddlAisle.SelectedValue.ToString())
                                ElseIf count2 > 1 Then
                                    locDAO.AddAisle(txtLocation.Text.ToString())
                                    locDAO.UpdateLocationMappingByAisle(ddlBuilding.SelectedValue.ToString(), txtLocation.Text.ToString())
                                End If
                                cutValidator.ErrorMessage = My.Resources.RecordUpdated
                                cutValidator.IsValid = False
                                Clearddl()
                                txtLocation.Text = ""
                            ElseIf count1 > 0 Then
                                Dim count2 As Integer
                                count2 = locDAO.GetForUpdateBuildingCountByAisle(ddlBuilding.SelectedValue.ToString(), txtLocation.Text.ToString())
                                If count2 = 0 Then
                                    locDAO.UpdateLocationMappingByAisle(ddlBuilding.SelectedValue.ToString(), txtLocation.Text.ToString())
                                    cutValidator.ErrorMessage = My.Resources.RecordUpdated
                                    cutValidator.IsValid = False
                                    Clearddl()
                                    txtLocation.Text = ""
                                Else

                                    cutValidator.ErrorMessage = My.Resources.AisleNameExist
                                    cutValidator.IsValid = False

                                End If

                               
                            End If
                        Else
                            cutValidator.ErrorMessage = My.Resources.AisleName
                            cutValidator.IsValid = False
                        End If
                    Else
                        cutValidator.ErrorMessage = My.Resources.SelectAisle
                        cutValidator.IsValid = False
                    End If

                Else
                    cutValidator.ErrorMessage = My.Resources.SelectBuilding
                    cutValidator.IsValid = False
                End If

            End If
        ElseIf (radTier.Checked = True) Then
            If radAdd.Checked = True Then
                If Not (ddlBuilding.SelectedItem.ToString() = "") Then
                    If Not (ddlAisle.SelectedItem.ToString() = "") Then


                        If Not txtLocation.Text.ToString() = "" Then
                            Dim locDAO As New LocationDAO
                     
                            Dim count1 As Integer
                            count1 = locDAO.GetTierCount(txtLocation.Text.ToString())
                            If count1 = 0 Then
                           
                                locDAO.AddTier(txtLocation.Text.ToString())

                          
                                Dim count As Integer
                                count = locDAO.GetLocationMappingCount(ddlBuilding.SelectedValue.ToString(), ddlAisle.SelectedValue.ToString(), DBNull.Value.ToString(), DBNull.Value.ToString())
                                If count = 1 Then
                     
                                    locDAO.UpdateLocationMappingTier(ddlBuilding.SelectedValue.ToString(), ddlAisle.SelectedValue.ToString(), txtLocation.Text.ToString())

                                Else
                       
                                    locDAO.LocationMappingTierAdd(ddlBuilding.SelectedValue.ToString(), ddlAisle.SelectedValue.ToString(), txtLocation.Text.ToString())
                                End If
                                cutValidator.ErrorMessage = My.Resources.ErrorMessage.COMMON_RECORDSAVED
                                cutValidator.IsValid = False
                                txtLocation.Text = ""
                                Clearddl()

                            ElseIf count1 > 0 Then
                                Dim count2 As Integer
                                count2 = locDAO.GetTierAisleBuildingCount(ddlBuilding.SelectedValue.ToString(), ddlAisle.SelectedValue.ToString(), txtLocation.Text.ToString())
                                If count2 = 0 Then
                                    Dim count As Integer
                                    count = locDAO.GetLocationMappingCount(ddlBuilding.SelectedValue.ToString(), ddlAisle.SelectedValue.ToString(), DBNull.Value.ToString(), DBNull.Value.ToString())
                                    If count = 1 Then

                                        locDAO.UpdateLocationMappingTier(ddlBuilding.SelectedValue.ToString(), ddlAisle.SelectedValue.ToString(), txtLocation.Text.ToString())

                                    Else

                                        locDAO.LocationMappingTierAdd(ddlBuilding.SelectedValue.ToString(), ddlAisle.SelectedValue.ToString(), txtLocation.Text.ToString())
                                    End If
                                    cutValidator.ErrorMessage = My.Resources.ErrorMessage.COMMON_RECORDSAVED
                                    cutValidator.IsValid = False
                                    txtLocation.Text = ""
                                    Clearddl()
                                Else
                                    cutValidator.ErrorMessage = My.Resources.TierNameExist
                                    cutValidator.IsValid = False
                                    txtLocation.Text = ""
                                    Clearddl()
                                End If
                               
                            End If
                        Else
                            cutValidator.ErrorMessage = My.Resources.TierName
                            cutValidator.IsValid = False

                        End If
                        txtLocation.Text = ""
                    Else
                        cutValidator.ErrorMessage = My.Resources.SelectAisle
                        cutValidator.IsValid = False

                    End If
                Else
                    cutValidator.ErrorMessage = My.Resources.SelectBuilding
                    cutValidator.IsValid = False

                    txtLocation.Text = ""

                End If

            ElseIf radEdit.Checked = True Then
                If Not (ddlBuilding.SelectedItem.ToString() = "") Then
                    If Not (ddlAisle.SelectedItem.ToString() = "") Then
                        If Not (ddlTier.SelectedItem.ToString() = "") Then

                            If Not txtLocation.Text.ToString() = "" Then

                                Dim locDAO As New LocationDAO
                                Dim count1 As Integer = locDAO.GetTierCount(txtLocation.Text.ToString())
                                If count1 = 0 Then
                                    Dim count2 As Integer
                                    count2 = locDAO.GetBuildingCountByTier(ddlTier.SelectedValue.ToString())
                                    If count2 = 1 Then
                                        locDAO.UpdateTier(txtLocation.Text.ToString(), ddlTier.SelectedValue.ToString())
                                        txtLocation.Text = ""
                                        Clearddl()
                                        cutValidator.ErrorMessage = My.Resources.RecordUpdated
                                        cutValidator.IsValid = False
                                    ElseIf count2 > 1 Then
                                        locDAO.AddTier(txtLocation.Text.ToString())
                                        locDAO.UpdateLocationMappingByTier(ddlBuilding.SelectedValue.ToString(), ddlAisle.SelectedValue.ToString(), txtLocation.Text.ToString())
                                        txtLocation.Text = ""
                                        Clearddl()
                                        cutValidator.ErrorMessage = My.Resources.RecordUpdated
                                        cutValidator.IsValid = False
                                    End If

                                ElseIf count1 > 0 Then
                                    Dim count2 As Integer
                                    count2 = locDAO.GetForUpdateBuildingCountByTier(ddlBuilding.SelectedValue.ToString(), ddlAisle.SelectedValue.ToString(), txtLocation.Text.ToString())
                                    If count2 = 0 Then
                                        locDAO.UpdateLocationMappingByTier(ddlBuilding.SelectedValue.ToString(), ddlAisle.SelectedValue.ToString(), txtLocation.Text.ToString())
                                        txtLocation.Text = ""
                                        Clearddl()
                                        cutValidator.ErrorMessage = My.Resources.RecordUpdated
                                        cutValidator.IsValid = False
                                    Else
                                        cutValidator.ErrorMessage = My.Resources.TierNameExist
                                        cutValidator.IsValid = False
                                    End If

                                End If

                            Else
                                cutValidator.ErrorMessage = My.Resources.TierName
                                cutValidator.IsValid = False

                            End If
                        Else
                            cutValidator.ErrorMessage = My.Resources.SelectTier
                            cutValidator.IsValid = False
                        End If
                    Else
                        cutValidator.ErrorMessage = My.Resources.SelectAisle
                        cutValidator.IsValid = False
                    End If
                Else
                    cutValidator.ErrorMessage = My.Resources.SelectBuilding
                    cutValidator.IsValid = False

                End If
            End If

        ElseIf radLevel.Checked = True Then
            If radAdd.Checked = True Then
                If Not (ddlBuilding.SelectedItem.ToString() = "") Then
                    If Not (ddlAisle.SelectedItem.ToString() = "") Then
                        If Not (ddlTier.SelectedItem.ToString() = "") Then

                            If Not txtLocation.Text.ToString() = "" Then
                                Dim locDAO As New LocationDAO

                                Dim count1 As Integer = locDAO.GetLevelCount(txtLocation.Text.ToString())
                                If (count1 = 0) Then
                                   
                                    locDAO.AddLevel(txtLocation.Text.ToString())


                                    Dim count As Integer = locDAO.GetLocationMappingCount(ddlBuilding.SelectedValue.ToString(), ddlAisle.SelectedValue.ToString(), ddlTier.SelectedValue.ToString(), DBNull.Value.ToString())

                                    If count = 1 Then
                                      
                                        locDAO.UpdateLocationMappingLevel(ddlBuilding.SelectedValue.ToString(), ddlAisle.SelectedValue.ToString(), ddlTier.SelectedValue.ToString(), txtLocation.Text.ToString())
                                    Else
                                     
                                        locDAO.LocationMappingLevelAdd(ddlBuilding.SelectedValue.ToString(), ddlAisle.SelectedValue.ToString(), ddlTier.SelectedValue.ToString(), txtLocation.Text.ToString())

                                    End If
                                    cutValidator.ErrorMessage = My.Resources.ErrorMessage.COMMON_RECORDSAVED
                                    cutValidator.IsValid = False
                                    txtLocation.Text = ""
                                    Clearddl()
                                ElseIf count1 > 0 Then
                                    Dim count2 As Integer
                                    count2 = locDAO.GetLevelTierAisleBuildingCount(ddlBuilding.SelectedValue.ToString(), ddlAisle.SelectedValue.ToString(), ddlTier.SelectedValue.ToString(), txtLocation.Text.ToString())
                                    If count2 = 0 Then
                                        Dim count As Integer = locDAO.GetLocationMappingCount(ddlBuilding.SelectedValue.ToString(), ddlAisle.SelectedValue.ToString(), ddlTier.SelectedValue.ToString(), DBNull.Value.ToString())

                                        If count = 1 Then

                                            locDAO.UpdateLocationMappingLevel(ddlBuilding.SelectedValue.ToString(), ddlAisle.SelectedValue.ToString(), ddlTier.SelectedValue.ToString(), txtLocation.Text.ToString())
                                        Else

                                            locDAO.LocationMappingLevelAdd(ddlBuilding.SelectedValue.ToString(), ddlAisle.SelectedValue.ToString(), ddlTier.SelectedValue.ToString(), txtLocation.Text.ToString())

                                        End If
                                        cutValidator.ErrorMessage = My.Resources.ErrorMessage.COMMON_RECORDSAVED
                                        cutValidator.IsValid = False
                                        txtLocation.Text = ""
                                        Clearddl()
                                    Else
                                        cutValidator.ErrorMessage = My.Resources.LevelNameExist
                                        cutValidator.IsValid = False


                                    End If
                                  

                                End If


                            Else
                                cutValidator.ErrorMessage = My.Resources.LevelName
                                cutValidator.IsValid = False

                            End If
                        Else
                            cutValidator.ErrorMessage = My.Resources.SelectTier
                            cutValidator.IsValid = False
                            txtLocation.Text = ""

                        End If
                    Else
                        cutValidator.ErrorMessage = My.Resources.SelectAisle
                        cutValidator.IsValid = False
                        txtLocation.Text = ""


                    End If
                Else
                    cutValidator.ErrorMessage = My.Resources.SelectBuilding
                    cutValidator.IsValid = False

                    txtLocation.Text = ""

                End If

            ElseIf radEdit.Checked = True Then
                If Not (ddlBuilding.SelectedItem.ToString() = "") Then
                    If Not (ddlAisle.SelectedItem.ToString() = "") Then
                        If Not (ddlTier.SelectedItem.ToString() = "") Then
                            If Not (ddlLevel.SelectedItem.ToString() = "") Then
                                Dim locDAO As New LocationDAO

                                If Not txtLocation.Text.ToString() = "" Then

                                    Dim count1 As Integer = locDAO.GetLevelCount(txtLocation.Text.ToString())
                                    If (count1 = 0) Then
                                        Dim count2 As Integer
                                        count2 = locDAO.GetBuildingCountByLevel(ddlLevel.SelectedValue.ToString())
                                        If count2 = 1 Then
                                            locDAO.UpdateLevel(txtLocation.Text.ToString(), ddlLevel.SelectedValue.ToString())
                                            txtLocation.Text = ""
                                            Clearddl()
                                            cutValidator.ErrorMessage = My.Resources.RecordUpdated
                                            cutValidator.IsValid = False
                                        ElseIf count2 > 1 Then
                                            locDAO.AddLevel(txtLocation.Text.ToString())
                                            locDAO.UpdateLocationMappingByLevel(ddlBuilding.SelectedValue.ToString(), ddlAisle.SelectedValue.ToString(), ddlTier.SelectedValue.ToString(), txtLocation.Text.ToString())
                                            txtLocation.Text = ""
                                            Clearddl()
                                            cutValidator.ErrorMessage = My.Resources.RecordUpdated
                                            cutValidator.IsValid = False
                                        End If
                                       
                                       
                                    ElseIf count1 > 0 Then
                                        Dim count2 As Integer
                                        count2 = locDAO.GetForUpdateBuildingCountByLevel(ddlBuilding.SelectedValue.ToString(), ddlAisle.SelectedValue.ToString(), ddlTier.SelectedValue.ToString(), txtLocation.Text.ToString())
                                        If count2 = 0 Then
                                            locDAO.UpdateLocationMappingByLevel(ddlBuilding.SelectedValue.ToString(), ddlAisle.SelectedValue.ToString(), ddlTier.SelectedValue.ToString(), txtLocation.Text.ToString())
                                            txtLocation.Text = ""
                                            Clearddl()
                                            cutValidator.ErrorMessage = My.Resources.RecordUpdated
                                            cutValidator.IsValid = False
                                        Else
                                            cutValidator.ErrorMessage = My.Resources.LevelNameExist
                                            cutValidator.IsValid = False
                                        End If
                                       

                                    End If

                                Else
                                    cutValidator.ErrorMessage = My.Resources.LevelName
                                    cutValidator.IsValid = False
                                End If
                            Else
                                cutValidator.ErrorMessage = My.Resources.SelectLevel
                                cutValidator.IsValid = False
                            End If
                        Else
                            cutValidator.ErrorMessage = My.Resources.SelectTier
                            cutValidator.IsValid = False
                        End If
                    Else
                        cutValidator.ErrorMessage = My.Resources.SelectAisle
                        cutValidator.IsValid = False
                    End If
                Else
                    cutValidator.ErrorMessage = My.Resources.SelectBuilding
                    cutValidator.IsValid = False
                End If
            End If

        End If


    End Sub
    Private Sub Clearddl()
       
        PopulateBuilding()
        ddlAisle.Items.Clear()
        ddlTier.Items.Clear()
        ddlLevel.Items.Clear()
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnDelete.Click

        Dim locDAO As New LocationDAO
        If radBuilding.Checked = True Then
            If radDelete.Checked = True Then
                If Not ddlBuilding.SelectedItem.ToString() = "" Then

                    locDAO.DeleteBuilding(ddlBuilding.SelectedValue.ToString())
                    cutValidator.ErrorMessage = My.Resources.ErrorMessage.Recorddeleted
                    cutValidator.IsValid = False
                    ddlBuilding.Items.Clear()
                Else
                    cutValidator.ErrorMessage = My.Resources.SelectBuilding
                    cutValidator.IsValid = False
                End If
            End If


        ElseIf radAisle.Checked = True Then
            If radDelete.Checked = True Then
                If Not (ddlBuilding.SelectedItem.ToString() = "") Then
                    If Not (ddlAisle.SelectedItem.ToString() = "") Then
                        Dim count As Integer
                        count = locDAO.GetBuildingCountByAisle(ddlAisle.SelectedValue.ToString())
                        If count = 1 Then

                            locDAO.DeleteAisle(ddlAisle.SelectedValue.ToString())
                            cutValidator.ErrorMessage = My.Resources.ErrorMessage.Recorddeleted
                            cutValidator.IsValid = False
                            ddlBuilding.Items.Clear()
                            ddlAisle.Items.Clear()
                        ElseIf count > 1 Then
                            locDAO.DeleteAisleByCount(ddlBuilding.SelectedValue.ToString(), ddlAisle.SelectedValue.ToString())
                            cutValidator.ErrorMessage = My.Resources.ErrorMessage.Recorddeleted
                            cutValidator.IsValid = False
                            ddlBuilding.Items.Clear()
                            ddlAisle.Items.Clear()
                        End If


                    Else
                        cutValidator.ErrorMessage = My.Resources.SelectAisle
                        cutValidator.IsValid = False

                    End If
                Else
                    cutValidator.ErrorMessage = My.Resources.SelectBuilding
                    cutValidator.IsValid = False
                End If
            End If
        ElseIf (radTier.Checked = True) Then
            If radDelete.Checked = True Then
                If Not (ddlBuilding.SelectedItem.ToString() = "") Then
                    If Not (ddlAisle.SelectedItem.ToString() = "") Then
                        If Not (ddlTier.SelectedItem.ToString() = "") Then
                            Dim count As Integer
                            count = locDAO.GetBuildingCountByTier(ddlTier.SelectedValue.ToString())
                            If count = 1 Then
                                locDAO.DeleteTier(ddlTier.SelectedValue.ToString())
                                cutValidator.ErrorMessage = My.Resources.ErrorMessage.Recorddeleted
                                cutValidator.IsValid = False
                                ddlBuilding.Items.Clear()
                                ddlAisle.Items.Clear()
                                ddlTier.Items.Clear()
                            ElseIf count > 1 Then
                                locDAO.DeleteTierByCount(ddlBuilding.SelectedValue.ToString(), ddlAisle.SelectedValue.ToString(), ddlTier.SelectedValue.ToString())
                                cutValidator.ErrorMessage = My.Resources.ErrorMessage.Recorddeleted
                                cutValidator.IsValid = False
                                ddlBuilding.Items.Clear()
                                ddlAisle.Items.Clear()
                                ddlTier.Items.Clear()
                            End If
                           
                        Else
                            cutValidator.ErrorMessage = My.Resources.SelectTier
                            cutValidator.IsValid = False
                        End If
                    Else
                        cutValidator.ErrorMessage = My.Resources.SelectAisle
                        cutValidator.IsValid = False
                    End If
                Else
                    cutValidator.ErrorMessage = My.Resources.SelectBuilding
                    cutValidator.IsValid = False
                End If
            End If
        ElseIf radLevel.Checked = True Then
            If radDelete.Checked = True Then
                If Not (ddlBuilding.SelectedItem.ToString() = "") Then
                    If Not (ddlAisle.SelectedItem.ToString() = "") Then
                        If Not (ddlTier.SelectedItem.ToString() = "") Then
                            If Not (ddlLevel.SelectedItem.ToString() = "") Then
                                Dim count As Integer
                                count = locDAO.GetBuildingCountByLevel(ddlLevel.SelectedValue.ToString())
                                If count = 1 Then
                                    locDAO.DeleteLevel(ddlLevel.SelectedValue.ToString())
                                    cutValidator.ErrorMessage = My.Resources.ErrorMessage.Recorddeleted
                                    cutValidator.IsValid = False
                                    ddlBuilding.Items.Clear()
                                    ddlAisle.Items.Clear()
                                    ddlTier.Items.Clear()
                                    ddlLevel.Items.Clear()
                                Else
                                    locDAO.DeleteLevelByCount(ddlBuilding.SelectedValue.ToString(), ddlAisle.SelectedValue.ToString(), ddlTier.SelectedValue.ToString(), ddlLevel.SelectedValue.ToString())
                                    cutValidator.ErrorMessage = My.Resources.ErrorMessage.Recorddeleted
                                    cutValidator.IsValid = False
                                    ddlBuilding.Items.Clear()
                                    ddlAisle.Items.Clear()
                                    ddlTier.Items.Clear()
                                    ddlLevel.Items.Clear()
                                End If
                               
                            Else
                                cutValidator.ErrorMessage = My.Resources.SelectLevel
                                cutValidator.IsValid = False
                            End If
                        Else
                            cutValidator.ErrorMessage = My.Resources.SelectTier
                            cutValidator.IsValid = False
                        End If
                    Else
                        cutValidator.ErrorMessage = My.Resources.SelectAisle
                        cutValidator.IsValid = False
                    End If
                Else
                    cutValidator.ErrorMessage = My.Resources.SelectBuilding
                    cutValidator.IsValid = False
                End If
            End If
        End If

        PopulateBuilding()
        ddlAisle.Items.Clear()
        ddlTier.Items.Clear()
        ddlLevel.Items.Clear()
    End Sub
    Protected Sub makeInvisiblerad()
        lblAdd.Visible = False
        lblAdd.Text = ""
        radAdd.Visible = False
        lblEdit.Visible = False
        lblEdit.Text = ""
        radEdit.Visible = False
        radDelete.Visible = False
        lblDelete.Visible = False
        lblDelete.Text = " "
    End Sub

    Protected Sub makeInvisible()
        lblLocation.Visible = False
        lblLocation.Text = ""
        txtLocation.Visible = False
        btnSave.Visible = False
        lblBuildingdropdown.Visible = False
        ddlBuilding.Visible = False
        lblAisleddl.Visible = False
        ddlAisle.Visible = False
        lblTierddl.Visible = False
        ddlTier.Visible = False
        txtLocation.Text = ""
        lblEBuilding.Visible = False
        txtEBuilding.Visible = False
        lblEAisle.Visible = False
        txtEAisle.Visible = False
        lblETier.Visible = False
        txtETier.Visible = False
        lblELevel.Visible = False
        txtELevel.Visible = False
        lblLevelddl.Visible = False
        lblLevelddl.Text = ""
        ddlLevel.Visible = False
        btnDelete.Visible = False
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If
        RedirectToPrevious()
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnReset.Click
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If

        makeInvisible()
        makeInvisiblerad()
        radBuilding.Checked = False
        radAisle.Checked = False
        radTier.Checked = False
        radLevel.Checked = False
      
        PopulateBuilding()
        ddlAisle.Items.Clear()
        ddlTier.Items.Clear()
        ddlLevel.Items.Clear()
    End Sub
End Class