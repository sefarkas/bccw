﻿Imports Security
Imports System.Data.SqlClient
Imports DataAccess.AdminDAO
Public Class Login1
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        

        Master.FindControl("Nevigation").Visible = False

        If Not IsPostBack Then
            If Session("IsAuthenticated") = "true" Then
                Session("IsAuthenticated") = "false"
                Session.RemoveAll()
                FormsAuthentication.SignOut()
            End If

            Session("IsAuthenticated") = "false"

            txtUserName.Focus()
            If Request.QueryString("Session") = "False" Then
                lMessage.Text = "Session Expired! Please Login Again"
                lMessage.ForeColor = System.Drawing.Color.Red
            End If
            Page.Form.DefaultButton = btnLogin.UniqueID


            lblVersion.Text = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString
        End If
    End Sub

    Protected Sub btnLogin_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLogin.Click
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If
        Dim userList As UserDTO
        Dim user As New User()
        user.UserId = txtUserName.Text.Trim()
        user.Password = txtPassword.Text.Trim()

        userList = user.AuthenticateUser()

        If Not IsNothing(userList.ErrorMessage) Then
            If Not String.IsNullOrEmpty(userList.ErrorMessage) Then
                txtUserName.Text = [String].Empty
                lMessage.Text = userList.ErrorMessage
                lMessage.ForeColor = System.Drawing.Color.Red
               
            End If
        ElseIf userList.PersonalID <> -1 Then
            Session("IsAuthenticated") = "true"
            Session("UserEmpID") = userList.PersonalID
            Session("User") = txtUserName.Text
            Session("clsUser") = userList
            Session("UserId") = userList.UserID
            SetRole(txtUserName.Text)
            SecurityUtility.RedirectFromLoginPage(txtUserName.Text.Trim(), "Dummy", True)
        Else

            txtUserName.Text = [String].Empty
            lMessage.Text = "Login Failed. The UserName/Password Entered is Incorrect."
            lMessage.ForeColor = System.Drawing.Color.Red
        End If


    End Sub

    Public Sub SetRole(ByVal strLoginID As String)
        Dim strResult As String
        Dim strUserRole As String

        Dim adminDAO As New DataAccess.AdminDAO
       
        strResult = adminDAO.GetRole(strLoginID)
        If strResult <> "" Then
            strUserRole = strResult
        Else
            strUserRole = "View"
        End If

        If IsNothing(Session("UserRole")) Then
            Session.Add("UserRole", strUserRole)
        Else
            Session("UserRole") = strUserRole
        End If

    End Sub
End Class