<%@ Page Language="VB" CodeBehind="frm07_AddNewPart.aspx.vb" AutoEventWireup="false"
    Inherits="frm07_AddNewPart" MasterPageFile="~/Master/Inventory.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<%--    <script type="text/javascript">
        // test auto-ready logic - call corner before DOM is ready
        $('#ContentPlaceHolder1_pnlDetail').corner();
    </script>--%>
    <asp:Panel ID="pnBody" Style="z-index: 105; left: 45px; top: 130px; font-family: 'Microsoft Sans Serif';
        font-size: 8pt; position: relative;" runat="server" Width="800" Height="390" DefaultButton="btnProcess">
        <%--     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <contenttemplate>--%>
        <asp:Panel ID="pnlDetail" Style="z-index: 105; left: 45px; top: -1px; font-family: 'Microsoft Sans Serif';
            font-size: 8pt; position: relative; height: 328px; width: 870px;" SkinID="panelSearch"
            runat="server" BorderStyle="None">
            <asp:ImageButton ID="btnCancel" Style="z-index: 102; left: 437px; top: 216px; font-family: 'Trebuchet MS';
                font-size: 9pt; position: absolute;" runat="server" Text="Cancel" ImageUrl="~/Images/cancel_static.png"
                CausesValidation="False" TabIndex="13"></asp:ImageButton>
            <asp:ImageButton ID="btnRefresh" Style="z-index: 102; left: 437px; top: 250px; font-family: 'Trebuchet MS';
                font-size: 9pt; position: absolute;" runat="server"  ImageUrl="~/Images/refresh_static.png"
                CausesValidation="False" TabIndex="14"></asp:ImageButton>
            <asp:ImageButton ID="btnProcess" Style="z-index: 103; left: 437px; top: 179px; font-family: 'Trebuchet MS';
                font-size: 9pt; position: absolute;" runat="server" Text="Process" 
                ImageUrl="~/Images/save_static.png" TabIndex="12" >
            </asp:ImageButton>
            <asp:ImageButton ID="btnMfrs" Style="z-index: 106; left: 437px; top: 131px; font-family: 'Trebuchet MS';
                font-size: 9pt; position: absolute;" runat="server" Text="Mfr Info" ImageUrl="~/Images/Mfr_Info_static.png"
                Visible="False" CausesValidation="False"></asp:ImageButton>
            <asp:Panel ID="pnlGeneralInfo" Style="z-index: 107; left: 0px; top: 0px; font-family: 'Microsoft Sans Serif';
                font-size: 8pt; position: absolute; height: 313px; width: 870px;" 
                runat="server">
                <asp:DropDownList ID="cboCategory" Style="z-index: 110; left: 77px; top: 12px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute; height: 21px; width: 200px;" 
                    runat="server" AutoPostBack="True" TabIndex="1">
                </asp:DropDownList>
                <asp:TextBox ID="txtQtyPerUnit" Style="z-index: 112; left: 669px; top: 41px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute; height: 16px; width: 136px;" runat="server"
                    TabIndex="5" MaxLength="4"></asp:TextBox>
                <asp:DropDownList ID="cboSubCategory" 
                    
                    
                    
                    Style="z-index: 113; left: 379px; top: 11px;
                    font-family: 'Verdana'; font-size: 8pt; position: absolute; height: 21px; width: 200px;" runat="server"
                    AutoPostBack="True" TabIndex="2">
                </asp:DropDownList>
                <asp:Label ID="Label9" Style="z-index: 114; left: 279px; top: 14px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute;font-weight:bold; width: 101px;" 
                    runat="server" Height="13">
                    <span style="color: Red; font-weight: bolder">*</span>Sub Category:</asp:Label>
                <asp:TextBox ID="txtDescription1" 
                    Style="z-index: 116; left: 77px; top: 39px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute; width: 496px; bottom: 258px; height: 16px;" 
                    runat="server" TabIndex="3" MaxLength="70"></asp:TextBox>
                <asp:Label ID="Label30" Style="z-index: 120; left: 0px; top: 13px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute; width: 76px; font-weight:bold;" runat="server" Height="13">
                    <span style="color: Red; font-weight: bolder">*</span>Category:</asp:Label>
                <asp:Label ID="Label17" 
                    Style="z-index: 121; left: 579px; top: 42px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute;font-weight:bold; height: 14px; width: 93px;" 
                    runat="server">
                    <span style="color: Red; font-weight: bolder">*</span>Qty Per Unit:</asp:Label>
                <asp:TextBox ID="txtUnitCost" Style="z-index: 122; left: 669px; top: 70px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute; height: 16px; bottom: 227px; width: 136px;"
                    runat="server" TabIndex="8" MaxLength="12"  ></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revUnitcost" runat="server"    
                    ControlToValidate="txtUnitCost"  ErrorMessage="Enter Valid Unit Cost with Two Decimals"   ForeColor="Red"  
                    ValidationExpression="^\$?[0-9]+(,[0-9]{3})*(\.[0-9]{2})?$" Display="None" ></asp:RegularExpressionValidator>
                <asp:CompareValidator ID="vld_CostShare" runat="server" ControlToValidate="txtUnitCost"
                    SetFocusOnError="true" Operator="DataTypeCheck" Type="Currency" ValidationGroup="vld"  
                    ForeColor="Red" Display="None" />
                <asp:TextBox ID="txtSize" Style="z-index: 123; left: 77px; top: 69px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute; height: 16px; width: 191px;" runat="server"
                    TabIndex="6" MaxLength="20"></asp:TextBox>
                <asp:TextBox ID="txtMin" Style="z-index: 149; left: 379px; top: 99px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute; height: 16px; width: 58px;" runat="server"
                    TabIndex="7" MaxLength="3"></asp:TextBox>
                <asp:Label ID="Label11" Style="z-index: 124; left: 579px; top: 14px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute; width: 84px; font-weight:bold;" 
                    runat="server" Height="13">
                    <span style="color: Red; font-weight: bolder">*</span>Unit Of Rec:</asp:Label>
                <asp:Label ID="Label12" Style="z-index: 125; left: 579px; top: 72px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute; width: 82px; font-weight:bold;" 
                    runat="server" Height="13">
                    <span style="color: Red; font-weight: bolder">*</span>Unit Cost: </asp:Label>
                <asp:Label ID="Label14" Style="z-index: 126; left: 0px; top: 40px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute;font-weight:bold; width: 86px;" 
                    runat="server" Height="13px">
                    <span style="color: Red; font-weight: bolder">*</span>Item Desc:</asp:Label>
                <asp:Label ID="Label16" Style="z-index: 127; left: 5px; top: 70px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute; height: 13px; width: 38px;font-weight:bold;" runat="server">
                     Size:</asp:Label>
                <asp:Label ID="lblSystemPartNo" 
                    Style="z-index: 150; left: 279px; top: 72px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute; height: 13px; width: 74px; font-weight:bold;" 
                    runat="server">
                    <span style="color: Red; font-weight: bolder">*</span>Stock No:</asp:Label>
                <asp:Label ID="lblShowSelect" Style="z-index: 149; left: 35px; top: 106px; font-family: 'Verdana'; font-weight:bold;
                    font-size: 8pt; position: absolute; width: 141px;" runat="server" Height="13">Show Selected Only</asp:Label>
                <asp:CheckBox ID="chkShowSelect" runat="server" AutoPostBack="true" Style="z-index: 150;
                    left: 10px; top: 101px; font-family: 'Verdana'; font-size: 8pt; position: absolute;
                    width: 23px; height: 21px;" TabIndex="9" />
                <asp:DropDownList ID="cboUnitOfRec" runat="server" 
                    Style="z-index: 118; left: 669px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute; height: 21px; width: 140px; top: 12px;" 
                    TabIndex="4">
                </asp:DropDownList>
                 <asp:CheckBox ID="chckIsDeleted" runat="server" Style="z-index: 150;
                    left: 665px; top: 101px; font-family: 'Verdana'; font-size: 8pt; position: absolute;
                    width: 23px; height: 21px;" TabIndex="9" ToolTip="Checked means item is dead."  />
                 <asp:Label ID="lbldeleted" Style="z-index: 125; left: 582px; top: 101px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute; width: 89px; font-weight:bold;" 
                    runat="server" Height="13">
                    <span style="color: Red; font-weight: bolder">*</span>Dead Item: </asp:Label>
                <asp:TextBox ID="txtSystempartNo" runat="server" MaxLength="20" Style="z-index: 149; left: 379px; top: 69px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute; height: 16px; width: 193px;" 
                    TabIndex="7"></asp:TextBox>
                <asp:Label ID="lblSystemPartNo0" runat="server" 
                    Style="z-index: 150; left: 459px; top: 102px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute; height: 13px; width: 45px; font-weight:bold;">Max :</asp:Label>
                <asp:Label ID="lblSystemPartNo1" runat="server" 
                    Style="z-index: 150; left: 319px; top: 102px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute; height: 13px; width: 45px; font-weight:bold;">Min :</asp:Label>
                <asp:TextBox ID="txtMax" runat="server" MaxLength="3" Style="z-index: 149; left: 509px; top: 99px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute; height: 16px; width: 58px;" 
                    TabIndex="7"></asp:TextBox>
                <asp:Label ID="lblCQOHValue" runat="server" Height="13px" 
                    Style="z-index: 125; left: 752px; top: 101px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute; width: 42px; font-weight:bold; right: 76px;"></asp:Label>
                <asp:Label ID="lblCQOH" runat="server" Height="13px" 
                    
                    Style="z-index: 125; left: 709px; top: 101px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute; width: 45px; font-weight:bold; right: 116px;">CQOH :</asp:Label>
            </asp:Panel>
            <asp:Panel ID="pnlMfr" runat="server" Style="z-index: 148; position: absolute; top: 137px;
                left: 0px; height: 149px; width: 312px;" ScrollBars="Vertical">
                <asp:GridView ID="gdvMfrs" runat="server" HeaderStyle-BackColor="Green" Style="z-index: 149;
                    left: 14px; top: 4px; font-family: 'Verdana'; font-size: 8pt; position: absolute;
                    height: 50px;" TabIndex="10">
                    <Columns>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkItem" runat="server" TabIndex="11"  />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblMfrId" runat="server" Text='<%# Eval("MfrID") %>' Visible="false" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="MfrID" Visible="false" />
                    </Columns>
                </asp:GridView>
            </asp:Panel>
            <asp:Panel ID="pnlShopInfo" Style="z-index: 128; left: 8px; top: 408px; font-family: 'Microsoft Sans Serif';
                font-size: 8pt; position: absolute; height: 108px;" runat="server" Width="684"
                Visible="False">
                <asp:Label ID="Label8" Style="z-index: 117; left: 5px; top: 38px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute;" runat="server" Width="71" Height="13">Item Type:</asp:Label>
                <asp:TextBox ID="txtComments" Style="z-index: 129; left: 75px; top: 62px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute;" runat="server" Width="375" Height="62" TabIndex="67"></asp:TextBox>
                <asp:DropDownList ID="cboShopName" Style="z-index: 130; left: 75px; top: 8px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute;" runat="server" Width="140" Height="21">
                </asp:DropDownList>
                <asp:Label ID="Label29" Style="z-index: 131; left: 460px; top: 11px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute;" runat="server" Width="66" Height="13">Location:</asp:Label>
                <asp:Panel ID="pnlLocation" Style="z-index: 132; left: 463px; top: 32px; font-family: 'Microsoft Sans Serif';
                    font-size: 8pt; position: absolute;" runat="server" Width="212" Height="92">
                    <asp:TextBox ID="txtSection" Style="z-index: 133; left: 61px; top: 10px; font-family: 'Verdana';
                        font-size: 8pt; position: absolute;" runat="server" Width="140" Height="21" TabIndex="17"></asp:TextBox>
                    <asp:TextBox ID="txtShelf" Style="z-index: 134; left: 61px; top: 36px; font-family: 'Verdana';
                        font-size: 8pt; position: absolute;" runat="server" Width="140" Height="21" TabIndex="18"></asp:TextBox>
                    <asp:TextBox ID="txtBin" Style="z-index: 135; left: 61px; top: 62px; font-family: 'Verdana';
                        font-size: 8pt; position: absolute;" runat="server" Width="140" Height="21" TabIndex="19"></asp:TextBox>
                    <asp:Label ID="Label23" Style="z-index: 136; left: 21px; top: 38px; font-family: 'Verdana';
                        font-size: 8pt; position: absolute;" runat="server" Width="41" Height="13">Shelf:</asp:Label>
                    <asp:Label ID="Label24" Style="z-index: 137; left: 32px; top: 65px; font-family: 'Verdana';
                        font-size: 8pt; position: absolute;" runat="server" Width="30" Height="13">Bin:</asp:Label>
                    <asp:Label ID="Label25" Style="z-index: 138; left: 8px; top: 11px; font-family: 'Verdana';
                        font-size: 8pt; position: absolute;" runat="server" Width="54" Height="13">Section:</asp:Label>
                </asp:Panel>
                <asp:TextBox ID="txtPartNo" Style="z-index: 139; left: 310px; top: 8px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute;" runat="server" Width="140" Height="21" TabIndex="11"></asp:TextBox>
                <asp:CheckBox ID="chkOrderAsNeeded" Style="z-index: 140; left: 326px; top: 37px;
                    font-family: 'Verdana'; font-size: 8pt; position: absolute;" runat="server" Width="124"
                    Height="17" Text="Order As Needed"></asp:CheckBox>
                <asp:Label ID="Label7" Style="z-index: 141; left: 257px; top: 11px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute;" runat="server" Width="54" Height="13">Part No:</asp:Label>
                <asp:TextBox ID="txtCQOH" Style="z-index: 142; left: 75px; top: 35px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute;" runat="server" Width="71" Height="21" TabIndex="12"></asp:TextBox>
                <asp:TextBox ID="txtMaxCapcity" Style="z-index: 143; left: 240px; top: 35px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute;" runat="server" Width="71" Height="21" TabIndex="13"></asp:TextBox>
                <asp:Label ID="Label1" Style="z-index: 144; left: 35px; top: 11px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute;" runat="server" Width="41" Height="13">Shop:</asp:Label>
                <asp:Label ID="Label15" Style="z-index: 145; left: 28px; top: 38px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute;" runat="server" Width="47" Height="13">CQOH:</asp:Label>
                <asp:Label ID="Label21" Style="z-index: 146; left: 151px; top: 38px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute;" runat="server" Width="89" Height="13">Max Capacity:</asp:Label>
                <asp:Label ID="Label13" Style="z-index: 147; left: 2px; top: 65px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute;" runat="server" Width="74" Height="13">Comments:</asp:Label>
                <asp:DropDownList ID="cboItemType" Style="z-index: 109; left: 75px; top: 35px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute;" runat="server" Width="140" Height="21" AutoPostBack="True">
                </asp:DropDownList>
                <asp:Label ID="Label10" Style="z-index: 119; left: 214px; top: 38px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute;" runat="server" Width="97" Height="13">Sub Item Type:</asp:Label>
                <asp:DropDownList ID="cboSubItemType" Style="z-index: 115; left: 310px; top: 35px;
                    font-family: 'Verdana'; font-size: 8pt; position: absolute;" runat="server" Width="140"
                    Height="21" AutoPostBack="True">
                </asp:DropDownList>
            </asp:Panel>
            <asp:CustomValidator ID="cutValidator" runat="server" Display="None"></asp:CustomValidator>
        </asp:Panel>
        <%--      </contenttemplate>
   </asp:UpdatePanel>--%>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
            ControlToValidate="txtMin" Display="None" 
            ErrorMessage="Please enter valid min value." ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
            ControlToValidate="txtMax" Display="None" 
            ErrorMessage="Please enter valid max value." ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
    </asp:Panel>
</asp:Content>
