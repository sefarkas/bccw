﻿Imports System
Imports DataAccess

Public Class cErrorlog
    Inherits System.Web.UI.Page
    Private ODDataSet As New DataSet()
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack() Then
            PopulateSearchresults()
        End If
        lblAdjDate2.Visible = False
        imgBtnRceiveDateTo.Visible = False
        txtToAdjDate.Visible = False

    End Sub

    Protected Sub CustomValidator1_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles CustomValidator1.ServerValidate


    End Sub

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFind.Click
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If


        ' If ODDataSet.Tables(0).Rows.Count = 0 Then

        PopulateSearchresults()

        'End If
    End Sub
    Public Function GetColumnName(ByVal strControlName As String) As String
        Dim strName, strTest As String
        Dim c, clast As Char
        clast = ""

        strName = Right(strControlName, Len(strControlName) - 3)

        For Each c In strName
            strTest = clast & c
            If Char.IsUpper(c) And strTest <> "PO" And strTest <> "SI" And strTest <> "WO" Then
                strName = Replace(strName, c, " " & c)
            End If
            clast = c
        Next

        GetColumnName = "[" & Trim(strName) & "]"

    End Function
    Public Sub AddSqlForDateSearchforAdj(ByVal strColName As String, ByVal strType As String,
 ByVal txtObj As Web.UI.WebControls.TextBox, ByVal txtToObj As Web.UI.WebControls.TextBox,
 ByRef strCriteria As String)
        If InStr(strCriteria, "where") Then
            strCriteria += " and "
        Else
            strCriteria += " where "
        End If
        Select Case strType
            Case "Equal To"

                strCriteria += "CONVERT(date,[" & strColName & "],101)  = '" & txtObj.Text & "'"
            Case "After"
                strCriteria += " CONVERT(date,[" & strColName & "],101) >= '" & txtObj.Text & "'"
            Case "Before"
                strCriteria += " CONVERT(date,[" & strColName & "],101) <= '" & txtObj.Text & "'"
            Case "Between"
                If txtToObj.Text = "" Then 'Default to "After"
                    strCriteria += " CONVERT(date,[" & strColName & "],101) >= '" & txtObj.Text & "'"
                Else
                    strCriteria += " CONVERT(date,[" & strColName & "],101) between '" & txtObj.Text & "' and '" & txtToObj.Text & "'"
                End If
        End Select
    End Sub
    Public Function GetSearchCriteria(ByVal pnlObj As Panel, ByVal strViewName As String, ByVal strColumns As String) As String
        Dim c As Web.UI.Control
        Dim dropdown As System.Web.UI.WebControls.DropDownList
        Dim textbox As System.Web.UI.WebControls.TextBox
        Dim strSQL As String

        strSQL = "select " & strColumns & " from " & strViewName & " where "
        For Each c In pnlObj.Controls
            If c.GetType Is GetType(Web.UI.WebControls.DropDownList) Then
                dropdown = CType(c, DropDownList)
                If dropdown.Text <> "" Then
                    If c.ID = "cboItemName" Then
                        strSQL += "Description like '%" & Replace(Replace(Replace(dropdown.SelectedItem.Text, "'", "''"), "[", "[[]"), "%", "[%]") & "%' and "
                    ElseIf c.ID = "cboCategory" Then
                        strSQL += "Category like '%" & Replace(Replace(Replace(dropdown.Text, "'", "''"), "[", "[[]"), "%", "[%]") & "%' and "
                    ElseIf c.ID = "cboSubCategory" Then
                        strSQL += "SubCategory like '%" & Replace(Replace(Replace(dropdown.Text, "'", "''"), "[", "[[]"), "%", "[%]") & "%' and "
                    ElseIf Not Right(c.ID, 4) = "Date" AndAlso Not c.ID.ToLower = "cbodatesearchdispdate" AndAlso Not c.ID = "cboCategory" AndAlso Not c.ID = "cboSubCategory" Then
                        strSQL += GetColumnName(c.ID) & "like '%" & Replace(Replace(Replace(dropdown.Text, "'", "''"), "[", "[[]"), "%", "[%]") & "%' and "
                    Else
                    End If
                End If
            End If
            If c.GetType Is GetType(Web.UI.WebControls.TextBox) Then
                textbox = CType(c, TextBox)
                If textbox.Text <> "" Then
                    If c.ID = "txtDescription" Then
                        strSQL += "Description like '%" & Replace(Replace(Replace(textbox.Text, "'", "''"), "[", "[[]"), "%", "[%]") & "%' and "
                    ElseIf c.ID = "txtStockNo" Then
                        strSQL += "StockNo like '%" & Replace(Replace(Replace(textbox.Text, "'", "''"), "[", "[[]"), "%", "[%]") & "%' and "
                    ElseIf Not (Right(c.ID, 4) = "Date") Then
                        strSQL += GetColumnName(c.ID) & " like '%" & textbox.Text & "%' and "
                    End If
                End If
            End If
        Next
        If Right(strSQL, 5) = " and " Then
            strSQL = Left(strSQL, Len(strSQL) - 5)
        ElseIf Right(strSQL, 7) = " where " Then
            strSQL = Left(strSQL, Len(strSQL) - 7)
        End If

        GetSearchCriteria = strSQL
    End Function
    Private Sub PopulateSearchresults()
        Dim strSql As String
        Dim strSQLCriteria As String
        strSql = "[ErrorID],[EntryDate],[Url],[UserID],[ErrorMessage]"
        strSQLCriteria = GetSearchCriteria(pnBody, "vw_69AppErrorLog", strSql)
        If txtAdjDate.Text <> "" Then AddSqlForDateSearchforAdj("EntryDate", cboDateSearchAdjDate.SelectedItem.Text, txtAdjDate, txtToAdjDate, strSQLCriteria)
        strSQLCriteria += " order by [EntryDate] desc"
        PopulateDataGridView(dgvSearchResult, strSQLCriteria, ODDataSet)
    End Sub

    Public Sub PopulateDataGridView(ByRef dgvObj As GridView, ByVal strSQL As String, ByRef dsObj As DataSet)
        Dim qa As New clsQueriesAction
        qa.ClearDataSet(strSQL, dsObj)
        dgvObj.DataSource = dsObj.Tables(0)
        dgvObj.DataBind()

    End Sub

    ' Public Sub AddSqlForDateSearchforAdj(ByVal strColName As String, ByVal strType As String, _
    'ByRef txtObj As Web.UI.WebControls.TextBox, ByRef txtToObj As Web.UI.WebControls.TextBox, _
    'ByRef strCriteria As String)
    '     If InStr(strCriteria & "", "where") Then
    '         strCriteria += " and "
    '     Else
    '         strCriteria += " where "
    '     End If
    '     Select Case strType
    '         Case "Equal To"

    '             strCriteria += "CONVERT(date,[" & strColName & "],101)  = '" & txtObj.Text & "'"
    '         Case "After"
    '             strCriteria += " CONVERT(date,[" & strColName & "],101) >= '" & txtObj.Text & "'"
    '         Case "Before"
    '             strCriteria += " CONVERT(date,[" & strColName & "],101) <= '" & txtObj.Text & "'"
    '         Case "Between"
    '             If txtToObj.Text = "" Then 'Default to "After"
    '                 strCriteria += " CONVERT(date,[" & strColName & "],101) >= '" & txtObj.Text & "'"
    '             Else
    '                 strCriteria += " CONVERT(date,[" & strColName & "],101) between '" & txtObj.Text & "' and '" & txtToObj.Text & "'"
    '             End If
    '     End Select
    ' End Sub
    Protected Sub cboDateSearchAdjDate_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cboDateSearchAdjDate.SelectedIndexChanged

        If cboDateSearchAdjDate.SelectedItem.Text = "Between" Then
            txtToAdjDate.Visible = True
            lblAdjDate2.Visible = True
            lblAdjDate2.Visible = True

            imgBtnRceiveDateTo.Visible = True


        ElseIf cboDateSearchAdjDate.SelectedItem.Text = "" Then
            cboDateSearchAdjDate.SelectedItem.Text = ""
            txtToAdjDate.Visible = False
            lblAdjDate2.Visible = False
            imgBtnRceiveDateTo.Visible = False
            lblAdjDate2.Visible = False
        End If
    End Sub

    Private Sub populatedgv()
        Dim strSql As String
        Dim strSQLCriteria As String
        strSql = "[ErrorID],[EntryDate],[Url],[UserID],[ErrorMessage]"
        strSQLCriteria = GetSearchCriteria(pnBody, "vw_69AppErrorLog", strSql)
        strSQLCriteria += " order by [ErrorID] desc"
        PopulateDataGridView(dgvSearchResult, strSQLCriteria, ODDataSet)
    End Sub



    Protected Sub dgvSearchResult_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles dgvSearchResult.PageIndexChanging
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If
        dgvSearchResult.PageIndex = e.NewPageIndex
        PopulateSearchresults()
    End Sub

    Protected Sub dgvSearchResult_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgvSearchResult.RowCommand
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If
        If e.CommandName = "btnSelect" Then
            RedirectTo("frm70_Errorlogdetail.aspx?ID=" + e.CommandArgument)
        End If
    End Sub
    Public Sub RedirectTo(ByVal strPage As String)
        Response.Redirect(strPage)
    End Sub
End Class