Option Strict Off
Option Explicit On

Imports System
Imports DataAccess.clsQueriesAction
Imports DataAccess
Imports System.Data.SqlClient
Imports DataAccess.InventoryDAO


Partial Class frm55_SearchMfrs
    Inherits InventoryBase
    Protected designerPlaceholderDeclaration As Object
    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        Me.InitializeComponent()
        MyBase.OnInit(e)
    End Sub
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        SetPageTitle("Manufacturers- Search Screen")
        If Not Page.IsPostBack = True Then
            InitializeForm()
            FillGrid()
        End If
    End Sub
    Private Sub InitializeComponent()
        'AddHandler Load, AddressOf Me.Page_Load
    End Sub

    'Private DSMfrs As New DataSet()
    Public strSQLCriteria As String
    ReadOnly InventoryDAO As New InventoryDAO
    Dim MfrName1 As String
    Dim Mfrname2 As String

    Friend Sub InitializeForm()

        SetPageTitle("Manufacturers- Search Screen")
        Dim dsMfr As DataSet = New DataSet()


        ' Format the data grid view control
        InitializeDataGridView(dgvSearchResult)
        'Dim ds As DataSet
        If Not IsNothing(ViewState("dgvSearchResult")) Then
            '  ds = CType(ViewState("dgvSearchResult"), DataSet)
        Else
            ViewState.Add("dgvSearchResult", dsMfr)
        End If

        ' Populate Mfr related combo boxes:

        dsMfr = InventoryDAO.GetSearchMfr()
        PopulateComboBoxWithTwoFields(cboMfrName, dsMfr)

    End Sub

    Protected Sub dgvSearchResult_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles dgvSearchResult.PageIndexChanging
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If
        dgvSearchResult.DataSource = SortDataTable(GetSortableData(MfrName1, Mfrname2), True)
        dgvSearchResult.PageIndex = e.NewPageIndex
        dgvSearchResult.DataBind()
    End Sub
    Protected Sub dgvSearchResult_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles dgvSearchResult.Sorting
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If

        GridViewSortExpression = e.SortExpression
        Dim pageIndex As Integer = dgvSearchResult.PageIndex

        dgvSearchResult.DataSource = SortDataTable(GetSortableData(MfrName1, Mfrname2), False)
        dgvSearchResult.DataBind()
        dgvSearchResult.PageIndex = pageIndex
    End Sub

    Public Function GetSortableData(ByVal MfrName1 As String, ByVal MfrName2 As String) As DataTable
        Dim dt As DataTable = New DataTable()
        Dim dsMfr As DataSet = New DataSet()

        'If InStr(1, txtDescription.SelectedItem.Text.Trim, "'") > 0 Then
        '    description = Replace(txtDescription.SelectedItem.Text.Trim, "'", "''")
        'Else
        '    description = txtDescription.SelectedItem.Text.Trim
        'End If
        'rdrSQL = ReceiveDAO.GetSearchResult(cboPONo.SelectedItem.Text.Trim, cboCategory.SelectedItem.Text.Trim, cboSubCategory.SelectedItem.Text.Trim, description, cboRecBy.SelectedItem.Text.Trim, txtReceiveDate.Text.Trim, txtToReceiveDate.Text.Trim)

        dsMfr = InventoryDAO.GetMfrSearchResult("", "")

        dgvSearchResult.DataSource = dsMfr
        dgvSearchResult.DataBind()

        dt = dsMfr.Tables(0)

        Return dt
    End Function


    Private Sub dgvSearchResult_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgvSearchResult.RowCommand
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If
        If e.CommandName = "btnSelect" Then
            RedirectTo("frm05_AddMfr.aspx?strFormMode=Edit&Id=" + e.CommandArgument)
        End If
    End Sub
    Protected Sub dgvSearchResult_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvSearchResult.RowDataBound
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If
        If Not e.Row.RowIndex = -1 Then
            CType(e.Row.FindControl("btnSelect"), ImageButton).CommandArgument = e.Row.Cells(1).Text + "&MfrName=" + e.Row.Cells(2).Text
        End If
    End Sub
    Protected Sub cutValidator_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cutValidator.ServerValidate

    End Sub

    Protected Sub btnFind_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFind.Click
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If
        FillGrid()
    End Sub
    Private Sub FillGrid()
        'Dim rdrSQL As SqlDataReader
        Dim DSMfrs As DataSet = New DataSet()

        MfrName1 = cboMfrName.SelectedValue
        Mfrname2 = txtMfrName.Text
        DSMfrs = InventoryDAO.GetMfrSearchResult(MfrName1, Mfrname2)

        PopulateDataGridViewDataSet(dgvSearchResult, DSMfrs)

        'Dim ds As DataSet
        If Not IsNothing(ViewState("dgvSearchResult")) Then
            '  ds = CType(ViewState("dgvSearchResult"), DataSet)
        Else
            ViewState.Add("dgvSearchResult", DSMfrs)
        End If

        If DSMfrs.Tables(0).Rows.Count = 0 Then
            cutValidator.ErrorMessage = My.Resources.ErrorMessage.SEARCH_NORECORD
            cutValidator.IsValid = False
        End If
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If
        txtMfrName.Text = String.Empty
        InitializeForm()
        FillGrid()
    End Sub

    Private Sub btnAddNew_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddNew.Click
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If
        RedirectTo("frm05_AddMfr.aspx?strFormMode=Add")
    End Sub
End Class
