Option Strict Off
Option Explicit On
Imports System
Imports DataAccess
Imports DataAccess.clsQueriesAction
Imports DataAccess.InventoryDAO
Partial Class frm05_AddMfr
    Inherits InventoryBase
    Protected Button1 As System.Web.UI.WebControls.Button
    Protected designerPlaceholderDeclaration As Object

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim strFormMode As String
        SetPageTitle("Add Manufacturer")

        If Not IsNothing(Request.QueryString("strFormMode")) Then
            strFormMode = Request.QueryString("strFormMode")
            If strFormMode = "Edit" Then
                SetPageTitle("Edit Manufacturer")
            Else
                SetPageTitle("Add Manufacturer")
            End If
        End If
        If Not IsPostBack Then
            If Not IsNothing(Request.QueryString("MfrName")) Then
                txtMfrName.Text = Request.QueryString("MfrName")
            End If
        End If
    End Sub

    Protected Sub cutValidator_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cutValidator.ServerValidate
        args.IsValid = True
        ' Validate reqired fields
        If txtMfrName.Text = "" Then
            cutValidator.ErrorMessage = My.Resources.ErrorMessage.MFG_NAME
            args.IsValid = False
            Exit Sub
        End If
    End Sub

    Protected Sub btnDone_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDone.Click
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If
        Dim intResult, intCount As Integer

        Dim strFormMode As String
        Dim MfrID As Integer
        Dim inventoryDAO As New InventoryDAO

        If Not IsNothing(Request.QueryString("strFormMode")) Then
            strFormMode = Request.QueryString("strFormMode")
        Else
            strFormMode = "Add"
        End If

        If Not IsNothing(Request.QueryString("Id")) Then
            MfrID = Request.QueryString("Id")
        End If

        If Page.IsValid Then
            intCount = inventoryDAO.GetCountMfrName(txtMfrName.Text)
            If intCount > 0 Then
                cutValidator.ErrorMessage = My.Resources.ErrorMessage.MFG_EXIST
                cutValidator.IsValid = False
                Exit Sub
            End If

            If strFormMode = "Add" Then
                intResult = inventoryDAO.AddMfr(txtMfrName.Text.Trim, GetUserName(), My.Computer.Name)
            Else
                intResult = inventoryDAO.EditMfr(MfrID.ToString(), txtMfrName.Text.Trim, GetUserName(), My.Computer.Name)
            End If
            If intResult > 0 Then
                cutValidator.ErrorMessage = My.Resources.ErrorMessage.COMMON_RECORDSAVED
                cutValidator.IsValid = False
            Else
                cutValidator.ErrorMessage = My.Resources.ErrorMessage.SAVE_PROBLEM
                cutValidator.IsValid = False
            End If
        End If
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If
        RedirectToPrevious()
    End Sub
End Class

