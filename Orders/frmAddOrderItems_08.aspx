<%@ Page language="VB" Codebehind="frmAddOrderItems_08.aspx.vb" AutoEventWireup="false" Inherits="frmAddOrderItems_08" MasterPageFile="~/Master/Inventory.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       <asp:Panel ID="pnBody" Style="z-index: 105; left: 8px; top: 1px; font-family: 'Microsoft Sans Serif';
        font-size: 8pt; position: relative;" runat="server" Width="950" Height="350"> 

<asp:Label id="Label8" style="z-index:101; left:8px; top:8px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="233" Height="13">Select an Inventory Item to Order:</asp:Label>
<asp:Panel id="pnlDetail" style="z-index:102; left:40px; top:10px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:absolute;" runat="server" Width="469" Height="311"><asp:DropDownList id="cboPartNo" style="z-index:103; left:315px; top:10px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="130" Height="21"></asp:DropDownList>
<asp:DropDownList id="cboShopName" style="z-index:104; left:91px; top:10px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="130" Height="21"></asp:DropDownList>
<asp:TextBox id="txtItemTotal" style="z-index:105; left:324px; top:281px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="130" Height="21" TabIndex="88"></asp:TextBox>
<asp:Label id="Label4" style="z-index:106; left:254px; top:284px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="71" Height="13">Item Total:</asp:Label>
<asp:TextBox id="txtComments" style="z-index:107; left:12px; top:227px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="442" Height="48" TabIndex="4"></asp:TextBox>
<asp:TextBox id="txtQtyOrd" style="z-index:108; left:91px; top:37px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="130" Height="21" TabIndex="2"></asp:TextBox>
<asp:Label id="Label17" style="z-index:109; left:9px; top:40px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="83" Height="13">Qty Ordered:</asp:Label>
<asp:Label id="Label1" style="z-index:110; left:52px; top:13px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="41" Height="13">Shop:</asp:Label>
<asp:Label id="Label3" style="z-index:111; left:263px; top:13px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="54" Height="13">Part No:</asp:Label>
<asp:TextBox id="txtUnitCost" style="z-index:112; left:315px; top:37px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="130" Height="21" TabIndex="3"></asp:TextBox>
<asp:Label id="Label14" style="z-index:113; left:253px; top:40px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="64" Height="13">Unit Cost:</asp:Label>
<asp:Panel id="pnlPartDetail" style="z-index:114; left:12px; top:64px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:absolute;" runat="server" Width="442" Height="144"><asp:TextBox id="txtLastUnitCost" style="z-index:115; left:360px; top:114px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="72" Height="21" TabIndex="82"></asp:TextBox>
<asp:Label id="Label7" style="z-index:116; left:272px; top:117px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="91" Height="13">Last Unit Cost:</asp:Label>
<asp:TextBox id="txtDescription2" style="z-index:117; left:78px; top:87px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="354" Height="21" TabIndex="81"></asp:TextBox>
<asp:TextBox id="txtSubCategory" style="z-index:118; left:302px; top:6px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="130" Height="21" TabIndex="5"></asp:TextBox>
<asp:TextBox id="txtItemType" style="z-index:119; left:78px; top:33px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="130" Height="21" TabIndex="6"></asp:TextBox>
<asp:TextBox id="txtCategory" style="z-index:120; left:78px; top:6px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="130" Height="21" TabIndex="4"></asp:TextBox>
<asp:TextBox id="txtDescription1" style="z-index:121; left:78px; top:60px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="354" Height="21" TabIndex="8"></asp:TextBox>
<asp:TextBox id="txtCQOH" style="z-index:122; left:197px; top:114px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="72" Height="21" TabIndex="15"></asp:TextBox>
<asp:TextBox id="txtSubItemType" style="z-index:123; left:302px; top:33px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="130" Height="21" TabIndex="7"></asp:TextBox>
<asp:TextBox id="txtUnitOfRec" style="z-index:124; left:78px; top:114px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="72" Height="21" TabIndex="11"></asp:TextBox>
<asp:Label id="Label10" style="z-index:125; left:15px; top:9px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="65" Height="13">Category:</asp:Label>
<asp:Label id="Label11" style="z-index:126; left:9px; top:36px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="71" Height="13">Item Type:</asp:Label>
<asp:Label id="Label20" style="z-index:127; left:153px; top:117px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="47" Height="13">CQOH:</asp:Label>
<asp:Label id="Label5" style="z-index:128; left:4px; top:63px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="76" Height="13">Description:</asp:Label>
<asp:Label id="Label2" style="z-index:129; left:212px; top:9px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="91" Height="13">Sub Category:</asp:Label>
<asp:Label id="Label12" style="z-index:130; left:206px; top:36px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="97" Height="13">Sub Item Type:</asp:Label>
<asp:Label id="Label9" style="z-index:131; left:4px; top:117px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="76" Height="13">Unit Of Rec:</asp:Label>
</asp:Panel>
<asp:Label id="Label6" style="z-index:132; left:246px; top:190px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="71" Height="13">Item Total:</asp:Label>
<asp:Label id="Label13" style="z-index:133; left:9px; top:211px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="74" Height="13">Comments:</asp:Label>
</asp:Panel>
    </asp:Panel>
</asp:Content>
