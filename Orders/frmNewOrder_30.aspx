<%@ Page language="VB" Codebehind="frmNewOrder_30.aspx.vb" AutoEventWireup="false" Inherits="frmNewOrder_30" MasterPageFile="~/Master/Inventory.Master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       <asp:Panel ID="pnBody" Style="z-index: 105; left: 0px; top: 1px; font-family: 'Microsoft Sans Serif';
        font-size: 8pt; position: relative;" runat="server" Width="915" Height="350"> 

<asp:Panel id="Panel2" style="z-index:101; left:40px; top:165px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:absolute;" runat="server" Width="773" Height="281"><asp:TextBox id="txtOrderTotal" style="z-index:102; left:620px; top:250px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="140" Height="21" TabIndex="10">$0.00</asp:TextBox>
<asp:Label id="Label12" style="z-index:103; left:543px; top:253px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="77" Height="13">Order Total:</asp:Label>
<asp:Label id="lblRecCount" style="z-index:104; left:23px; top:253px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="340" Height="13"></asp:Label>
<asp:Label id="Label11" style="z-index:105; left:7px; top:7px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="90" Height="13">Order Items:</asp:Label>
<asp:GridView id="dgvOrderItems" style="z-index:106; left:10px; top:23px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:absolute;" runat="server" Width="750" Height="217"></asp:GridView>
</asp:Panel>
<asp:Panel id="pnlDetail" style="z-index:107; left:40px; top:7px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:absolute;" runat="server" Width="773" Height="152"><asp:TextBox id="txtSINo" style="z-index:108; left:339px; top:10px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="140" Height="21" TabIndex="1"></asp:TextBox>
<asp:TextBox id="txtReqNumber" style="z-index:109; left:97px; top:11px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="140" Height="21" TabIndex="0">201140</asp:TextBox>
<asp:Label id="Label1" style="z-index:110; left:14px; top:13px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="83" Height="13">Req Number:</asp:Label>
<asp:TextBox id="txtOrderAddBy" style="z-index:111; left:620px; top:10px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="140" Height="21" TabIndex="2">dchauhan</asp:TextBox>
<asp:Label id="Label4" style="z-index:112; left:545px; top:13px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="75" Height="13">Entered By:</asp:Label>
<asp:TextBox id="txtOrderAddDate" style="z-index:113; left:620px; top:38px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="140" Height="21" TabIndex="5">10/15/2010</asp:TextBox>
<asp:Label id="Label3" style="z-index:114; left:592px; top:41px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="28" Height="13">On:</asp:Label>
<asp:TextBox id="txtPONo" style="z-index:115; left:97px; top:38px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="140" Height="21" TabIndex="3"></asp:TextBox>
<asp:TextBox id="txtComments" style="z-index:116; left:97px; top:92px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="663" Height="51" TabIndex="8"></asp:TextBox>
<asp:TextBox id="txtDateOrd" style="z-index:117; left:97px; top:65px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="140" Height="21" TabIndex="6"></asp:TextBox>
<asp:Label id="Label13" style="z-index:118; left:23px; top:95px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="74" Height="13">Comments:</asp:Label>
<asp:DropDownList id="cboOrderType" style="z-index:119; left:339px; top:37px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="140" Height="21"></asp:DropDownList>
<asp:Label id="Label9" style="z-index:120; left:262px; top:40px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="77" Height="13">Order Type:</asp:Label>
<asp:DropDownList id="cboVendorName" style="z-index:121; left:339px; top:64px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="422" Height="21"></asp:DropDownList>
<asp:Label id="Label8" style="z-index:122; left:21px; top:68px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="76" Height="13">Order Date:</asp:Label>
<asp:Label id="Label10" style="z-index:123; left:286px; top:67px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="53" Height="13">Vendor:</asp:Label>
<asp:Label id="Label6" style="z-index:124; left:50px; top:41px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="47" Height="13">PO No:</asp:Label>
<asp:Label id="Label7" style="z-index:125; left:295px; top:12px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="44" Height="13">SI No:</asp:Label>
</asp:Panel>
    </asp:Panel>
</asp:Content>
