<%@ Page Language="VB" CodeBehind="frmOrders_34.aspx.vb" AutoEventWireup="false"
    Inherits="frmOrders" MasterPageFile="~/Master/Inventory.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       <asp:Panel ID="pnBody" Style="z-index: 105; left: 40px; top: 1px; font-family: 'Microsoft Sans Serif';
        font-size: 8pt; position: relative; width: 873px;" runat="server" 
           Height="350"> 

    <asp:Panel ID="PnlMessage" runat="server" SkinID="panelskin">
        <asp:Label ID="lblOrderDetail" runat="server" Text="Order Detail" ></asp:Label>
    </asp:Panel>
    <asp:Panel ID="pnlHeader" Style="z-index: 101;" runat="server" >
        <table style="width: 754px">
            <tr>
                <td class="style6">
                    <asp:Label ID="Label2" Style="z-index: 103;" runat="server" Width="90" Height="13">Req Number:</asp:Label>
                </td>
                <td class="style7">
                    <asp:DropDownList ID="cboReqNumber" Style="z-index: 102;" runat="server" Height="20px"
                        Width="136px">
                    </asp:DropDownList>
                </td>
                <td class="style8">
                    &nbsp;<asp:Label ID="Label5" Style="z-index: 104;" runat="server" Height="13px" Width="94px">SI No:</asp:Label>
                </td>
                <td class="style9">
                    <asp:DropDownList ID="cboSINo" runat="server" Height="22px" Style="z-index: 105;"
                        Width="127px">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:Button ID="btnGo" Style="z-index: 131;" runat="server" Text="Go" Width="42px" />
                </td>
                <td>
                    <asp:Button ID="btnSave" Style="z-index: 132;" runat="server" Text="Save" Width="49px" />
                </td>
                <td>
                    <asp:Button ID="btnCancel" Style="z-index: 133;" runat="server" Text="Cancel" />
                </td>
                <td>
                    <asp:Button ID="btnClose" Style="z-index: 134;" runat="server" Text="Close" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlDetail" Style="z-index: 106; left: 0px; top: 73px; position: absolute;
        width: 756px;" runat="server">
        <table>
            <tr>
                <td class="style15">
                    <asp:Label ID="Label1" Style="z-index: 109;" runat="server">Req Number:</asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtReqNumber" Style="z-index: 108; margin-left: 12px;" runat="server"
                        TabIndex="6" ReadOnly="true" Width="137px"></asp:TextBox>
                </td>
                <td class="style16">
                    <asp:Label ID="Label7" Style="z-index: 124;" runat="server">SI No:</asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtSINo" Style="z-index: 107;" runat="server" TabIndex="7" 
                        ReadOnly="true" Width="133px"></asp:TextBox>
                </td>
                <td class="style11">
                    <asp:Label ID="Label4" Style="z-index: 111;" runat="server">Entered By:</asp:Label>
                </td>
                <td style="margin-left: 40px">
                    <asp:TextBox ID="txtOrderAddBy" Style="z-index: 110;" runat="server" TabIndex="8"
                        ReadOnly="true" Width="144px" Height="24px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style15">
                    <asp:Label ID="Label6" Style="z-index: 123;" runat="server">PO No:</asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtPONo" Style="z-index: 114; margin-left: 12px;" runat="server"
                        TabIndex="9" Width="135px"></asp:TextBox>
                </td>
                <td class="style16">
                    <asp:Label ID="Label9" Style="z-index: 119;" runat="server">Order Type:</asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="cboOrderType" Style="z-index: 118;" runat="server" Height="16px"
                        Width="133px">
                    </asp:DropDownList>
                </td>
                <td class="style11">
                    <asp:Label ID="Label3" Style="z-index: 113;" runat="server">On:</asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtOrderAddDate" Style="z-index: 112;" runat="server" TabIndex="11"
                        ReadOnly="true" Width="144px"></asp:TextBox>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td class="style18">
                    <asp:Label ID="Label8" Style="z-index: 121;" runat="server">Order Date:</asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtDateOrd" Style="z-index: 116; margin-left: 12px;" runat="server"
                        TabIndex="12" Width="133px"></asp:TextBox>
                </td>
                <td class="style19">
                    <asp:Label ID="Label10" Style="z-index: 122;" runat="server">Vendor:</asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="cboVendorName" Style="z-index: 120; " 
                        runat="server" Height="16px"
                        Width="257px">
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td>
                    <asp:Label ID="Label13" Style="z-index: 117;" runat="server" Height="13px" 
                        Width="86px">Comments:</asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtComments" Style="z-index: 115; left: 107px; top: 96px; position: absolute;
                        width: 640px;" runat="server" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="Panel2" Style="z-index: 125; left: 0px; top: 231px; font-family: 'Microsoft Sans Serif';
        font-size: 8pt; position: absolute;" runat="server" Width="773" Height="281">
        <asp:TextBox ID="txtOrderTotal" Style="z-index: 126; left: 620px; top: 250px; position: absolute;"
            runat="server" TabIndex="16"></asp:TextBox>
        <table >
            <tr  >
                <td>
                    <asp:Label ID="Label11" Style="z-index: 129; " SkinID="panelskin"  
                        runat="server" Width="700px" Height="13px">Order Items:</asp:Label>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label12" Style="z-index: 127; left: 513px; top: 253px; position: absolute; width: 109px;"
                        runat="server" Height="13">Order Total:</asp:Label>
                    <asp:Label ID="lblRecCount" Style="z-index: 128; left: 45px; top: 250px; position: absolute;
                        height: 13px; width: 115px;" runat="server">Record 1 of 2</asp:Label>
                    <asp:GridView ID="dgvOrderItems" SkinID="gridviewSkin" Style="z-index: 130;" runat="server">
                    </asp:GridView>
                   </td>
                  </tr>
                   <tr>
                   <td>
                    <asp:Button ID="btnDelete" 
                           Style="z-index: 135; left: 193px; top: 293px; position: absolute;" 
                           runat="server"  Text="Delete Item" />
                     <asp:Button ID="btnAddNewItem" 
                           Style="z-index: 137; left: 50px; top: 293px; position: absolute; " 
                           runat="server"  Text="Add New Item" />
                            <asp:Button ID="btnItemdetail" 
                           Style="z-index: 138; left: 314px; top: 293px; position: absolute;" 
                           runat="server"  Text="Item Detail" />
                           <asp:Button ID="btnApprovedItemList" 
                           Style="z-index: 139; left: 429px; top: 293px; position: absolute; " 
                           runat="server"  Text="Approved Item List" />
                           <asp:Button ID="btnRecevingHistory" 
                           Style="z-index: 140; left: 616px; top: 293px; position: absolute;" 
                           runat="server"  Text="Receving History" />
                    <asp:Label ID="lblMsg" Visible="false" Style="z-index: 136;" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
    </asp:Panel>
    </asp:Panel>
</asp:Content>
