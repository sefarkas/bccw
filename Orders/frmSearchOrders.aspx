<%@ Page language="VB" Theme="InvSkin" Codebehind="frmSearchOrders.aspx.vb" AutoEventWireup="false" Inherits="frmSearchOrders" MasterPageFile="~/Master/Inventory.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<script type="text/javascript">
    // test auto-ready logic - call corner before DOM is ready
    $('#ContentPlaceHolder1_pnlSearchCriteria').corner();
</script>
<asp:Panel id="pnBody" style="z-index:105; left:40px; top:1px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:relative;" 
    runat="server" Width="930" Height="500" >
<asp:Button id="btnReceive" 
    style="z-index:102; left:508px; top:551px; font-family:'Trebuchet MS'; font-size:9pt; position:absolute;" 
    runat="server" Width="147" Height="31" Text="Receive"></asp:Button>
<asp:Button id="btnVendors" 
    style="z-index:103; left:729px; top:551px; font-family:'Trebuchet MS'; font-size:9pt; position:absolute;" 
    runat="server" Width="147" Height="31" Text="Vendors"></asp:Button>
<asp:Button id="btnAddNew" 
    style="z-index:104; left:66px; top:551px; font-family:'Trebuchet MS'; font-size:9pt; position:absolute;" 
    runat="server" Width="147" Height="31" Text="Add New Order"></asp:Button>
<asp:Panel id="Panel3" 
    style="z-index:106; left:8px; top:201px; font-family:'Microsoft Sans Serif'; font-size:9pt; position:absolute;" 
    runat="server"  Width="925" Height="250" ScrollBars="Auto">
    
<asp:Label id="lblRecCount" style="z-index:108; left:39px; top:307px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="105" Height="13">Record 1 of 1548</asp:Label>
 
    <asp:GridView ID="dgvSearchResult" SkinID="gridviewSkin" runat="server" 
        style="z-index:135; left:16px; top:11px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:absolute;" 
        >
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Button ID="btnSelect" runat="server" Text="Edit" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
 
</asp:Panel>
<asp:Panel id="Panel1" style="z-index:109; left:8px; top:12px; font-family:'Microsoft Sans Serif'; font-size:9pt; position:absolute;" runat="server" Width="925" Height="55">
<asp:Button id="btnReset" 
        style="z-index:111; left:729px; top:13px; font-family:'Trebuchet MS'; font-size:9pt; position:absolute;" 
        runat="server" Width="79" Height="31" Text="Reset"></asp:Button>
<asp:Button id="btnFind" 
        style="z-index:112; left:628px; top:13px; font-family:'Trebuchet MS'; font-size:9pt; position:absolute;" 
        runat="server" Width="79" Height="31" Text="Find"></asp:Button>
<asp:Label id="Label1" style="z-index:113; left:5px; top:13px; font-family:'Verdana'; font-size:15pt; position:absolute;" runat="server" Width="369" Height="25">Inventory Orders - Search Screen</asp:Label>
</asp:Panel>
<asp:Panel id="pnlSearchCriteria" SkinID="panelSearch" style="z-index:114; left:8px; top:73px; font-family:'Microsoft Sans Serif'; font-size:9pt; position:absolute;" runat="server" Width="925" Height="122"><asp:Label id="lblOrderDate2" style="z-index:115; left:710px; top:41px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="28" Height="13">and</asp:Label>
<asp:DropDownList id="cboDateSearchOrderDate" style="z-index:116; left:335px; top:38px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="140" Height="21">
  <asp:ListItem Value="0">Equals To</asp:ListItem>
    <asp:ListItem Value="1">Before</asp:ListItem>
    <asp:ListItem Value="2">After</asp:ListItem>
    <asp:ListItem Value="3">Between</asp:ListItem>
</asp:DropDownList>
<asp:DropDownList id="cboItemType" style="z-index:117; left:88px; top:92px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="140" Height="21"></asp:DropDownList>
<asp:DropDownList id="cboSubItemType" style="z-index:118; left:335px; top:92px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="140" Height="21"></asp:DropDownList>
<asp:TextBox id="txtToOrderDate" style="z-index:119; left:771px; top:38px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="135" Height="21" TabIndex="11"></asp:TextBox>
<asp:Label id="Label11" style="z-index:120; left:237px; top:95px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="97" Height="13">Sub Item Type:</asp:Label>
<asp:Label id="Label12" style="z-index:121; left:17px; top:95px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="71" Height="13">Item Type:</asp:Label>
<asp:TextBox id="txtDescription" style="z-index:122; left:551px; top:92px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="355" Height="21" TabIndex="17"></asp:TextBox>
<asp:DropDownList id="cboVendorName" style="z-index:123; left:551px; top:65px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="355" Height="21"></asp:DropDownList>
<asp:Label id="Label10" style="z-index:124; left:497px; top:68px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="53" Height="13">Vendor:</asp:Label>
<asp:TextBox id="txtOrderDate" style="z-index:125; left:551px; top:38px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="135" Height="21" TabIndex="10"></asp:TextBox>
<asp:DropDownList id="cboOrderType" style="z-index:126; left:771px; top:11px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="135" Height="21"></asp:DropDownList>
<asp:Label id="Label9" style="z-index:127; left:694px; top:14px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="77" Height="13">Order Type:</asp:Label>
<asp:Label id="Label8" style="z-index:128; left:257px; top:41px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="76" Height="13">Order Date:</asp:Label>
<asp:DropDownList id="cboDelivery" style="z-index:129; left:88px; top:38px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="140" Height="21"></asp:DropDownList>
<asp:Label id="Label7" style="z-index:130; left:27px; top:41px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="60" Height="13">Delivery:</asp:Label>
<asp:DropDownList id="cboPONo" style="z-index:131; left:551px; top:11px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="135" Height="21"></asp:DropDownList>
<asp:Label id="Label6" style="z-index:132; left:503px; top:14px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="47" Height="13">PO No:</asp:Label>
<asp:DropDownList id="cboSINo" style="z-index:133; left:335px; top:11px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="140" Height="21"></asp:DropDownList>
<asp:Label id="Label5" style="z-index:134; left:289px; top:14px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="44" Height="13">SI No:</asp:Label>
<asp:DropDownList id="cboReqNumber" style="z-index:135; left:88px; top:11px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="140" Height="21"></asp:DropDownList>
<asp:Label id="Label4" style="z-index:136; left:47px; top:68px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="41" Height="13">Shop:</asp:Label>
<asp:DropDownList id="cboShopName" style="z-index:137; left:88px; top:65px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="140" Height="21"></asp:DropDownList>
<asp:Label id="Label2" style="z-index:138; left:4px; top:14px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="83" Height="13">Req Number:</asp:Label>
<asp:DropDownList id="cboPartNo" style="z-index:139; left:335px; top:65px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="140" Height="21"></asp:DropDownList>
<asp:Label id="Label3" style="z-index:140; left:280px; top:68px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="54" Height="13">Part No:</asp:Label>
<asp:Label id="Label13" style="z-index:141; left:479px; top:95px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="71" Height="13">Item Desc:</asp:Label>
</asp:Panel>
    <asp:CustomValidator ID="cutValidator" runat="server" Display="None"></asp:CustomValidator>
</asp:Panel>
</asp:Content>