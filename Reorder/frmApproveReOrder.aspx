<%@ Page language="VB" Codebehind="frmApproveReOrder.aspx.vb" AutoEventWireup="false" Inherits="frmApproveReOrder" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
<HEAD>
<title>Review & Approve Pending Items</title>
<meta name="GENERATOR" Content="form.suite4.net">
<meta name="CODE_LANGUAGE" Content="VB">
<meta name="vs_defaultClientScript" content="JavaScript">
<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
</HEAD>
<body MS_POSITIONING="GridLayout">
<form id="frmApproveReOrder" method="post" runat="server">
<asp:Panel id="pnlDetail" style="z-index:101; left:12px; top:73px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:absolute;" runat="server" Width="773" Height="57"><asp:TextBox id="txtRepType" style="z-index:102; left:208px; top:26px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="160" Height="21" TabIndex="57">1 YEAR</asp:TextBox>
<asp:TextBox id="txtRepDate" style="z-index:103; left:10px; top:26px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="160" Height="21" TabIndex="55">10/1/2010</asp:TextBox>
<asp:Label id="Label5" style="z-index:104; left:597px; top:10px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="70" Height="13">Appr Date:</asp:Label>
<asp:Label id="Label4" style="z-index:105; left:205px; top:10px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="82" Height="13">Report Type:</asp:Label>
<asp:Label id="Label8" style="z-index:106; left:7px; top:10px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="81" Height="13">Report Date:</asp:Label>
<asp:TextBox id="txtApprBy" style="z-index:107; left:404px; top:26px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="160" Height="21" TabIndex="43">dchauhan</asp:TextBox>
<asp:Label id="Label2" style="z-index:108; left:401px; top:10px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="58" Height="13">Appr By:</asp:Label>
<asp:TextBox id="txtApprDate" style="z-index:109; left:600px; top:26px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="160" Height="21" TabIndex="38">10/15/2010</asp:TextBox>
</asp:Panel>
<asp:Panel id="Panel2" style="z-index:110; left:12px; top:136px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:absolute;" runat="server" Width="773" Height="299"><asp:Label id="lblRecCount" style="z-index:111; left:39px; top:250px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="98" Height="13">Record 1 of 269</asp:Label>
<asp:Label id="Label11" style="z-index:112; left:7px; top:7px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="112" Height="13">Re-Order Items:</asp:Label>
<asp:GridView id="dgvItems" style="z-index:113; left:10px; top:23px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:absolute;" runat="server" Width="750" Height="217"></asp:GridView>
</asp:Panel>
<asp:Panel id="pnlHeader" style="z-index:114; left:12px; top:12px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:absolute;" runat="server" Width="772" Height="55"><asp:DropDownList id="cboShopName" style="z-index:115; left:122px; top:17px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="168" Height="21"></asp:DropDownList>
<asp:Label id="Label1" style="z-index:116; left:7px; top:20px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="113" Height="13">Shop (Optional):</asp:Label>
</asp:Panel>
</form>
</body>
</HTML>
