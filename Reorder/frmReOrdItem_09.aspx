<%@ Page Language="VB" CodeBehind="frmReOrdItem_09.aspx.vb" AutoEventWireup="false"
    Inherits="frmReOrdItem_09" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Approve Item</title>
    <meta name="GENERATOR" content="form.suite4.net">
    <meta name="CODE_LANGUAGE" content="VB">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
</head>
<body ms_positioning="GridLayout">
    <form id="frmReOrdItem" method="post" runat="server">
    <asp:Panel ID="pnlPartInfo" Style="z-index: 101; left: 12px; top: 40px; font-family: 'Microsoft Sans Serif';
        font-size: 8pt; position: absolute; height: 273px;" runat="server" Width="475">
        <asp:TextBox ID="txtReOrdQtyTotal" Style="z-index: 102; left: 85px; top: 168px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="50" Height="21" TabIndex="13"></asp:TextBox>
        <asp:TextBox ID="txtCQOH" Style="z-index: 103; left: 182px; top: 168px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="50" Height="21" TabIndex="14"></asp:TextBox>
        <asp:Label ID="Label13" Style="z-index: 104; left: 138px; top: 171px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="47" Height="13">CQOH:</asp:Label>
        <asp:TextBox ID="txtMinQty" Style="z-index: 105; left: 290px; top: 168px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="50" Height="21" TabIndex="15"></asp:TextBox>
        <asp:Label ID="Label9" Style="z-index: 106; left: 237px; top: 171px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="55" Height="13">Min Qty:</asp:Label>
        <asp:TextBox ID="txtMaxQty" Style="z-index: 107; left: 401px; top: 168px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="50" Height="21" TabIndex="16"></asp:TextBox>
        <asp:Label ID="Label7" Style="z-index: 108; left: 344px; top: 171px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="59" Height="13">Max Qty:</asp:Label>
        <asp:TextBox ID="txtUnitCost" Style="z-index: 109; left: 85px; top: 87px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="10"></asp:TextBox>
        <asp:TextBox ID="txtSubCategory" Style="z-index: 110; left: 321px; top: 33px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="7"></asp:TextBox>
        <asp:TextBox ID="txtItemType" Style="z-index: 111; left: 85px; top: 60px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="8"></asp:TextBox>
        <asp:TextBox ID="txtPartNo" Style="z-index: 112; left: 321px; top: 6px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="5"></asp:TextBox>
        <asp:TextBox ID="txtCategory" Style="z-index: 113; left: 85px; top: 33px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="6"></asp:TextBox>
        <asp:TextBox ID="txtShopName" Style="z-index: 114; left: 85px; top: 6px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="4"></asp:TextBox>
        <asp:TextBox ID="txtUnitOfRec" Style="z-index: 115; left: 321px; top: 87px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="11"></asp:TextBox>
        <asp:Label ID="Label14" Style="z-index: 116; left: 22px; top: 90px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="64" Height="13">Unit Cost:</asp:Label>
        <asp:TextBox ID="txtDescription" Style="z-index: 117; left: 85px; top: 114px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="366" Height="48" TabIndex="12"> </asp:TextBox>
        <asp:TextBox ID="txtSubItemType" Style="z-index: 118; left: 321px; top: 60px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="130" Height="21" TabIndex="9"></asp:TextBox>
        <asp:Label ID="Label8" Style="z-index: 119; left: 45px; top: 9px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="41" Height="13">Shop:</asp:Label>
        <asp:Label ID="Label10" Style="z-index: 120; left: 21px; top: 36px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="65" Height="13">Category:</asp:Label>
        <asp:Label ID="Label11" Style="z-index: 121; left: 15px; top: 63px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="71" Height="13">Item Type:</asp:Label>
        <asp:Label ID="Label5" Style="z-index: 122; left: 11px; top: 117px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="76" Height="13">Description:</asp:Label>
        <asp:Label ID="Label2" Style="z-index: 123; left: 232px; top: 36px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="91" Height="13">Sub Category:</asp:Label>
        <asp:Label ID="Label12" Style="z-index: 124; left: 226px; top: 63px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="97" Height="13">Sub Item Type:</asp:Label>
        <asp:Label ID="Label6" Style="z-index: 125; left: 247px; top: 90px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="76" Height="13">Unit Of Rec:</asp:Label>
        <asp:Label ID="Label4" Style="z-index: 126; left: 269px; top: 9px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="54" Height="13">Part No:</asp:Label>
        <asp:Label ID="Label1" Style="z-index: 127; left: 10px; top: 171px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="77" Height="13">Re-Ord Qty:</asp:Label>
    </asp:Panel>
    <asp:CheckBox ID="chkDelete" Style="z-index: 128; left: 314px; top: 14px; font-family: 'Verdana';
        font-size: 8pt; position: absolute;" runat="server" Width="166" Height="17" Text="Delete from Pending List">
    </asp:CheckBox>
    <asp:CheckBox ID="chkApprove" Text="Approve" Style="z-index: 132; left: 32px; top: 14px;
        font-family: 'Verdana'; font-size: 8pt; position: absolute;" 
        runat="server" Width="74"
        Height="17"></asp:CheckBox>
    <asp:Label ID="lblRecNo" Style="z-index: 129; left: 215px; top: 334px; font-family: 'Verdana';
        font-size: 8pt; position: absolute;" runat="server" Width="31" Height="13">1</asp:Label>
    <asp:TextBox ID="txtApprQty" Style="z-index: 130; left: 198px; top: 12px; font-family: 'Verdana';
        font-size: 8pt; position: absolute;" runat="server" Width="50" Height="21" 
        TabIndex="1"></asp:TextBox>
    <asp:Label ID="Label3" Style="z-index: 131; left: 127px; top: 15px; font-family: 'Verdana';
        font-size: 8pt; position: absolute;" runat="server" Width="63" Height="13">Appr Qty:</asp:Label>
    <asp:Button ID="btnFirst" runat="server" Text="<<" Style="z-index: 134; left: 142px;
        top: 327px; position: absolute; width: 56px;" runat="server" Height="21" />
    <asp:Button ID="btnPrevious" runat="server" Text="<" Style="z-index: 134; left: 62px;
        top: 328px; position: absolute; width: 79px;" runat="server" Height="21" />
    <asp:Button ID="btnNext" runat="server" Text=">" Style="z-index: 134; left: 225px;
        top: 327px; position: absolute; width: 62px;" runat="server" Height="21" />
    <asp:Button ID="btnLast" runat="server" Text=">>" Style="z-index: 134; left: 289px;
        top: 327px; position: absolute; width: 62px;" runat="server" Height="21" />
    <asp:Button ID="btnClose" runat="server" Text="Close" Style="z-index: 134; left: 389px;
        top: 327px; position: absolute; width: 62px;" runat="server" Height="21" />
    </form>
</body>
</html>
