<%@ Page language="VB" Codebehind="frmAddReOrdItems.aspx.vb" AutoEventWireup="false" Inherits="frmAddReOrdItems" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
<HEAD>
<title>Re-Order Items</title>
<meta name="GENERATOR" Content="form.suite4.net">
<meta name="CODE_LANGUAGE" Content="VB">
<meta name="vs_defaultClientScript" content="JavaScript">
<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
</HEAD>
<body MS_POSITIONING="GridLayout">
<form id="frmAddReOrdItems" method="post" runat="server">
<asp:Label id="lblRecNo" style="z-index:101; left:139px; top:362px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="31" Height="13">#</asp:Label>
<asp:Panel id="Panel1" style="z-index:102; left:12px; top:12px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:absolute;" runat="server" Width="495" Height="319"><asp:TextBox id="txtComments" style="z-index:103; left:13px; top:260px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="309" Height="48" TabIndex="145"></asp:TextBox>
<asp:TextBox id="txtQtyOnOrd" style="z-index:104; left:400px; top:287px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="81" Height="21" TabIndex="143"></asp:TextBox>
<asp:Label id="Label17" style="z-index:105; left:326px; top:290px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="77" Height="13">Qty On Ord:</asp:Label>
<asp:TextBox id="txtApprQty" style="z-index:106; left:400px; top:260px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="81" Height="21" TabIndex="13"></asp:TextBox>
<asp:CheckBox id="chkOrder" style="z-index:107; left:13px; top:10px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="59" Height="17"></asp:CheckBox>
<asp:Label id="Label16" style="z-index:108; left:10px; top:244px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="74" Height="13">Comments:</asp:Label>
<asp:TextBox id="txtUnitCost" style="z-index:109; left:335px; top:8px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="130" Height="21" TabIndex="139"></asp:TextBox>
<asp:TextBox id="txtOrdQty" style="z-index:110; left:148px; top:8px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="81" Height="21" TabIndex="129"></asp:TextBox>
<asp:Label id="Label15" style="z-index:111; left:273px; top:10px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="64" Height="13">Unit Cost:</asp:Label>
<asp:Panel id="pnlPartInfo" style="z-index:112; left:13px; top:33px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:absolute;" runat="server" Width="468" Height="204"><asp:TextBox id="txtCQOH" style="z-index:113; left:85px; top:168px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="81" Height="21" TabIndex="14"></asp:TextBox>
<asp:Label id="Label13" style="z-index:114; left:41px; top:171px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="47" Height="13">CQOH:</asp:Label>
<asp:TextBox id="txtMinQty" style="z-index:115; left:227px; top:168px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="81" Height="21" TabIndex="15"></asp:TextBox>
<asp:Label id="Label9" style="z-index:116; left:173px; top:171px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="55" Height="13">Min Qty:</asp:Label>
<asp:TextBox id="txtMaxQty" style="z-index:117; left:369px; top:168px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="81" Height="21" TabIndex="16"></asp:TextBox>
<asp:Label id="Label7" style="z-index:118; left:312px; top:171px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="59" Height="13">Max Qty:</asp:Label>
<asp:TextBox id="txtLastUnitCost" style="z-index:119; left:85px; top:87px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="130" Height="21" TabIndex="10"></asp:TextBox>
<asp:TextBox id="txtSubCategory" style="z-index:120; left:321px; top:33px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="130" Height="21" TabIndex="7"></asp:TextBox>
<asp:TextBox id="txtItemType" style="z-index:121; left:85px; top:60px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="130" Height="21" TabIndex="8"></asp:TextBox>
<asp:TextBox id="txtPartNo" style="z-index:122; left:321px; top:6px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="130" Height="21" TabIndex="5"></asp:TextBox>
<asp:TextBox id="txtCategory" style="z-index:123; left:85px; top:33px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="130" Height="21" TabIndex="6"></asp:TextBox>
<asp:TextBox id="txtShopName" style="z-index:124; left:85px; top:6px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="130" Height="21" TabIndex="4"></asp:TextBox>
<asp:TextBox id="txtUnitOfRec" style="z-index:125; left:321px; top:87px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="130" Height="21" TabIndex="11"></asp:TextBox>
<asp:Label id="Label14" style="z-index:126; left:22px; top:90px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="64" Height="13">Unit Cost:</asp:Label>
<asp:TextBox id="txtDescription" style="z-index:127; left:85px; top:114px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="366" Height="48" TabIndex="12"></asp:TextBox>
<asp:TextBox id="txtSubItemType" style="z-index:128; left:321px; top:60px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="130" Height="21" TabIndex="9"></asp:TextBox>
<asp:Label id="Label8" style="z-index:129; left:45px; top:9px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="41" Height="13">Shop:</asp:Label>
<asp:Label id="Label10" style="z-index:130; left:21px; top:36px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="65" Height="13">Category:</asp:Label>
<asp:Label id="Label11" style="z-index:131; left:15px; top:63px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="71" Height="13">Item Type:</asp:Label>
<asp:Label id="Label5" style="z-index:132; left:10px; top:117px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="76" Height="13">Description:</asp:Label>
<asp:Label id="Label2" style="z-index:133; left:232px; top:36px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="91" Height="13">Sub Category:</asp:Label>
<asp:Label id="Label12" style="z-index:134; left:226px; top:63px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="97" Height="13">Sub Item Type:</asp:Label>
<asp:Label id="Label6" style="z-index:135; left:247px; top:90px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="76" Height="13">Unit Of Rec:</asp:Label>
<asp:Label id="Label4" style="z-index:136; left:269px; top:9px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="54" Height="13">Part No:</asp:Label>
</asp:Panel>
<asp:Label id="Label3" style="z-index:137; left:92px; top:11px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="57" Height="13">Ord Qty:</asp:Label>
<asp:Label id="Label1" style="z-index:138; left:340px; top:263px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="63" Height="13">Appr Qty:</asp:Label>
</asp:Panel>
</form>
</body>
</HTML>
