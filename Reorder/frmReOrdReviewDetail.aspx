<%@ Page language="VB" Codebehind="frmReOrdReviewDetail.aspx.vb" AutoEventWireup="false" Inherits="frmReOrdReviewDetail" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
<HEAD>
<title>Re-Order Review Detail</title>
<meta name="GENERATOR" Content="form.suite4.net">
<meta name="CODE_LANGUAGE" Content="VB">
<meta name="vs_defaultClientScript" content="JavaScript">
<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
</HEAD>
<body MS_POSITIONING="GridLayout">
<form id="frmReOrdReviewDetail" method="post" runat="server">
<asp:Label id="lblRecNo" style="z-index:101; left:120px; top:365px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="39" Height="13">4</asp:Label>
<asp:Panel id="pnlDetail" style="z-index:102; left:10px; top:12px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:absolute;" runat="server" Width="472" Height="322"><asp:TextBox id="txtQtyOrd" style="z-index:103; left:380px; top:290px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="70" Height="21" TabIndex="144"></asp:TextBox>
<asp:Label id="Label21" style="z-index:104; left:325px; top:293px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="57" Height="13">Qty Ord:</asp:Label>
<asp:TextBox id="txtApprReOrdQty" style="z-index:105; left:234px; top:290px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="70" Height="21" TabIndex="135"></asp:TextBox>
<asp:TextBox id="txtReOrdQty" style="z-index:106; left:94px; top:290px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="70" Height="21" TabIndex="136">2</asp:TextBox>
<asp:Label id="Label17" style="z-index:107; left:173px; top:293px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="63" Height="13">Appr Qty:</asp:Label>
<asp:Label id="Label15" style="z-index:108; left:18px; top:293px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="77" Height="13">Re-Ord Qty:</asp:Label>
<asp:TextBox id="txtApprBy" style="z-index:109; left:320px; top:264px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="130" Height="21" TabIndex="106"></asp:TextBox>
<asp:TextBox id="txtApprDate" style="z-index:110; left:94px; top:264px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="130" Height="21" TabIndex="104"></asp:TextBox>
<asp:Label id="Label19" style="z-index:111; left:264px; top:267px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="58" Height="13">Appr By:</asp:Label>
<asp:Label id="Label20" style="z-index:112; left:25px; top:267px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="70" Height="13">Appr Date:</asp:Label>
<asp:TextBox id="txtRepDate" style="z-index:113; left:320px; top:8px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="130" Height="21" TabIndex="102">10/15/2010</asp:TextBox>
<asp:TextBox id="txtRunBy" style="z-index:114; left:320px; top:35px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="130" Height="21" TabIndex="92">fiat</asp:TextBox>
<asp:Label id="Label1" style="z-index:115; left:242px; top:11px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="81" Height="13">Report Date:</asp:Label>
<asp:TextBox id="txtRepTypeDesc" style="z-index:116; left:94px; top:35px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="130" Height="21" TabIndex="44">1 YEAR</asp:TextBox>
<asp:Panel id="pnlPartDetail" style="z-index:117; left:9px; top:61px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:absolute;" runat="server" Width="452" Height="197"><asp:TextBox id="txtMaxQty" style="z-index:118; left:370px; top:168px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="70" Height="21" TabIndex="139">59</asp:TextBox>
<asp:TextBox id="txtMinQty" style="z-index:119; left:224px; top:168px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="70" Height="21" TabIndex="138">30</asp:TextBox>
<asp:TextBox id="txtShopName" style="z-index:120; left:84px; top:6px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="130" Height="21" TabIndex="87">CARPENTRY</asp:TextBox>
<asp:TextBox id="txtPartNo" style="z-index:121; left:310px; top:6px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="130" Height="21" TabIndex="85">0651</asp:TextBox>
<asp:Label id="Label3" style="z-index:122; left:44px; top:9px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="41" Height="13">Shop:</asp:Label>
<asp:Label id="Label4" style="z-index:123; left:258px; top:9px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="54" Height="13">Part No:</asp:Label>
<asp:Label id="Label16" style="z-index:124; left:171px; top:171px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="55" Height="13">Min Qty:</asp:Label>
<asp:TextBox id="txtDescription" style="z-index:125; left:84px; top:114px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="356" Height="48" TabIndex="82">CYLINDER DEADLOCK 2"3/4 B/S </asp:TextBox>
<asp:TextBox id="txtUnitCost" style="z-index:126; left:84px; top:87px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="130" Height="21" TabIndex="48">$32.00</asp:TextBox>
<asp:TextBox id="txtSubCategory" style="z-index:127; left:310px; top:33px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="130" Height="21" TabIndex="46"></asp:TextBox>
<asp:TextBox id="txtCQOH" style="z-index:128; left:84px; top:168px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="70" Height="21" TabIndex="137">13</asp:TextBox>
<asp:TextBox id="txtItemType" style="z-index:129; left:84px; top:60px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="130" Height="21" TabIndex="47">DEADLOCK</asp:TextBox>
<asp:Label id="Label18" style="z-index:130; left:38px; top:171px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="47" Height="13">CQOH:</asp:Label>
<asp:TextBox id="txtCategory" style="z-index:131; left:84px; top:33px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="130" Height="21" TabIndex="52"></asp:TextBox>
<asp:TextBox id="txtUnitOfRec" style="z-index:132; left:310px; top:87px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="130" Height="21" TabIndex="79">EACH</asp:TextBox>
<asp:Label id="Label14" style="z-index:133; left:21px; top:90px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="64" Height="13">Unit Cost:</asp:Label>
<asp:TextBox id="txtSubItemType" style="z-index:134; left:310px; top:60px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="130" Height="21" TabIndex="51">DEAD BOLT ARROW</asp:TextBox>
<asp:Label id="Label10" style="z-index:135; left:20px; top:36px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="65" Height="13">Category:</asp:Label>
<asp:Label id="Label11" style="z-index:136; left:14px; top:63px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="71" Height="13">Item Type:</asp:Label>
<asp:Label id="Label5" style="z-index:137; left:9px; top:117px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="76" Height="13">Description:</asp:Label>
<asp:Label id="Label2" style="z-index:138; left:221px; top:36px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="91" Height="13">Sub Category:</asp:Label>
<asp:Label id="Label12" style="z-index:139; left:215px; top:63px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="97" Height="13">Sub Item Type:</asp:Label>
<asp:Label id="Label6" style="z-index:140; left:226px; top:90px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="86" Height="13">Unit Of Issue:</asp:Label>
<asp:Label id="Label13" style="z-index:141; left:313px; top:171px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="59" Height="13">Max Qty:</asp:Label>
</asp:Panel>
<asp:Label id="Label7" style="z-index:142; left:270px; top:38px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="53" Height="13">Run By:</asp:Label>
<asp:Label id="Label9" style="z-index:143; left:13px; top:38px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="82" Height="13">Report Type:</asp:Label>
<asp:Panel id="Panel1" style="z-index:144; left:92px; top:6px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:absolute;" runat="server" Width="143" Height="26"><asp:TextBox id="txtStatus" style="z-index:145; left:2px; top:2px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="130" Height="21" TabIndex="100">Pending</asp:TextBox>
</asp:Panel>
<asp:Label id="Label8" style="z-index:146; left:47px; top:11px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="48" Height="13">Status:</asp:Label>
</asp:Panel>
</form>
</body>
</HTML>
