<%@ Page language="VB" Theme="InvSkin" Codebehind="frmReOrderReview.aspx.vb" AutoEventWireup="false" Inherits="frmReOrderReview"  MasterPageFile="~/Master/Inventory.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<script type="text/javascript">
    // test auto-ready logic - call corner before DOM is ready
    $('#ContentPlaceHolder1_pnlSearchCriteria').corner();
</script>
<asp:Panel id="pnBody" style="z-index:105; left:8px; top:1px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:relative;" 
    runat="server" Width="950" Height="370" >

<asp:ImageButton id="btnPendingItems" 
    style="z-index:103; left:164px; top:129px; font-family:'Trebuchet MS'; font-size:9pt; position:absolute;" 
    runat="server" Text="Pending Item Review" 
        ImageUrl="~/Images/Preview_static.png" CausesValidation="False"></asp:ImageButton>
<asp:ImageButton id="btnMinMaxQuery" 
    style="z-index:104; left:262px; top:129px; font-family:'Trebuchet MS'; font-size:9pt; position:absolute;" 
    runat="server" Text="Run Min/Max Query" ImageUrl="~/Images/min_max_static.png"
        CausesValidation="False"></asp:ImageButton>
<asp:Panel id="Panel3" 
    style="z-index:105; left:8px; top:156px; font-family:'Verdana'; font-size:8pt; position:absolute;" 
    runat="server" Width="925" height="180" ScrollBars="Auto">
    <asp:GridView id="dgvSearchResult" SkinID="gridviewSkin"
        style="z-index:135; left:6px; top:1px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:absolute;" 
        runat="server" Width="893px" Height="284px">
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton ID="btnEdit" runat="server" Text="View" ImageUrl="~/Images/edit_static.png" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
<asp:Label id="lblRecCount" style="z-index:107; left:39px; top:308px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="98" Height="13">Record 1 of 269</asp:Label>
</asp:Panel>
<asp:Panel id="pnlSearchCriteria" SkinID="panelSearch"
        style="z-index:108; left:8px; top:3px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:absolute;" 
        runat="server" Width="925" Height="117">
    <asp:DropDownList id="cboCategory" 
        style="z-index:109; left:158px; top:33px; font-family:'Verdana'; font-size:8pt; position:absolute;" 
        runat="server" Width="125" Height="21" AutoPostBack="True"></asp:DropDownList>
<asp:DropDownList id="cboSubCategory" style="z-index:110; left:372px; top:33px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="125" Height="21"></asp:DropDownList>
<asp:Label id="Label6" style="z-index:111; left:95px; top:36px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="65" Height="13">Category:</asp:Label>
<asp:Label id="Label8" style="z-index:112; left:283px; top:36px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="91" Height="13">Sub Category:</asp:Label>
<asp:TextBox id="txtDescription" style="z-index:113; left:566px; top:6px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="348" Height="21" TabIndex="70"></asp:TextBox>
<asp:Label id="Label13" style="z-index:114; left:498px; top:9px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="71" Height="13">Item Desc:</asp:Label>
<asp:Label id="lblRepDate2" style="z-index:115; left:725px; top:90px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="28" Height="13">and</asp:Label>
<asp:DropDownList id="cboDateSearchRepDate" 
        style="z-index:116; left:372px; top:87px; font-family:'Verdana'; font-size:8pt; position:absolute;" 
        runat="server" Width="125" Height="21" AutoPostBack="True">
          <asp:ListItem Value="0">Equals To</asp:ListItem>
    <asp:ListItem Value="1">Before</asp:ListItem>
    <asp:ListItem Value="2">After</asp:ListItem>
    <asp:ListItem Value="3">Between</asp:ListItem>
        </asp:DropDownList>
<asp:Label id="lblApprDate2" style="z-index:117; left:725px; top:63px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="28" Height="13">and</asp:Label>
<asp:DropDownList id="cboRepTypeDesc" style="z-index:118; left:158px; top:87px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="125" Height="21"></asp:DropDownList>
<asp:TextBox id="txtToRepDate" style="z-index:119; left:789px; top:87px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="125" Height="21" TabIndex="65"></asp:TextBox>
<asp:DropDownList id="cboDateSearchApprDate" 
        style="z-index:120; left:372px; top:60px; font-family:'Verdana'; font-size:8pt; position:absolute;" 
        runat="server" Width="125" Height="21" AutoPostBack="True">
    <asp:ListItem Value="0">Equals To</asp:ListItem>
    <asp:ListItem Value="1">Before</asp:ListItem>
    <asp:ListItem Value="2">After</asp:ListItem>
    <asp:ListItem Value="3">Between</asp:ListItem>
    </asp:DropDownList>
<asp:TextBox id="txtToApprDate" style="z-index:121; left:789px; top:60px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="125" Height="21" TabIndex="58"></asp:TextBox>
<asp:TextBox id="txtRepDate" style="z-index:122; left:566px; top:87px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="125" Height="21" TabIndex="64">10/15/2010</asp:TextBox>
<asp:TextBox id="txtApprDate" style="z-index:123; left:566px; top:60px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="125" Height="21" TabIndex="57"></asp:TextBox>
<asp:Label id="Label7" style="z-index:124; left:309px; top:90px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="65" Height="13">Rep Date:</asp:Label>
<asp:DropDownList id="cboApprBy" style="z-index:125; left:158px; top:60px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="125" Height="21"></asp:DropDownList>
<asp:Label id="Label10" style="z-index:126; left:304px; top:63px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="70" Height="13">Appr Date:</asp:Label>
<asp:DropDownList id="cboShopName" 
        style="z-index:127; left:158px; top:6px; font-family:'Verdana'; font-size:8pt; position:absolute;" 
        runat="server" Width="125" Height="21" AutoPostBack="True"></asp:DropDownList>
<asp:DropDownList id="cboItemType" 
        style="z-index:128; left:566px; top:33px; font-family:'Verdana'; font-size:8pt; position:absolute;" 
        runat="server" Width="125" Height="21" AutoPostBack="True"></asp:DropDownList>
<asp:DropDownList id="cboSubItemType" style="z-index:129; left:789px; top:33px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="125" Height="21"></asp:DropDownList>
<asp:Label id="Label4" style="z-index:130; left:119px; top:9px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="41" Height="13">Shop:</asp:Label>
<asp:DropDownList id="cboPartNo" style="z-index:131; left:372px; top:6px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="125" Height="21"></asp:DropDownList>
<asp:Label id="Label3" style="z-index:132; left:320px; top:9px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="54" Height="13">Part No:</asp:Label>
<asp:Label id="Label2" style="z-index:133; left:102px; top:63px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="58" Height="13">Appr By:</asp:Label>
<asp:Label id="Label12" style="z-index:134; left:498px; top:36px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="71" Height="13">Item Type:</asp:Label>
<asp:Label id="Label11" style="z-index:135; left:694px; top:36px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="97" Height="13">Sub Item Type:</asp:Label>
<asp:Panel id="Panel2" style="z-index:136; left:8px; top:6px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:absolute;" runat="server" Width="85" Height="75">
    <asp:CheckBox id="chkOrdered" 
        style="z-index:137; left:4px; top:53px; font-family:'Verdana'; font-size:8pt; position:absolute;" 
        runat="server" Width="73" Height="17" AutoPostBack="True" Text="Ordered"></asp:CheckBox>
<asp:CheckBox id="chkPending" 
        style="z-index:138; left:4px; top:7px; font-family:'Verdana'; font-size:8pt; position:absolute;" 
        runat="server" Width="71" Height="17" AutoPostBack="True" Text="Pending"></asp:CheckBox>
<asp:CheckBox id="chkApproved" 
        style="z-index:139; left:4px; top:30px; font-family:'Verdana'; font-size:8pt; position:absolute;" 
        runat="server" Width="81" Height="17" AutoPostBack="True" Text="Approved"></asp:CheckBox>
</asp:Panel>
<asp:Label id="Label5" style="z-index:140; left:94px; top:90px; font-family:'Verdana'; font-size:8pt; position:absolute;" runat="server" Width="66" Height="13">Rep Type:</asp:Label>
</asp:Panel>
    <asp:CustomValidator ID="cutValidator" runat="server" Display="None"></asp:CustomValidator>
    <asp:ImageButton ID="btnFind" runat="server" SkinID="findButton" 
        style="position:absolute; top: 130px; left: 12px;"  />
    <asp:ImageButton ID="btnReset" runat="server"  SkinID ="resetButton" 
        style="position:absolute; top: 130px; left: 62px;" />
</asp:Panel>
</asp:Content>