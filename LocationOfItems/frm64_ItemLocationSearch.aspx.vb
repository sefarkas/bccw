﻿Imports System
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.IO
Imports DataAccess.clsQueriesSelect
Imports DataAccess


Public Class frm64_ItemLocationSearch
    Inherits InventoryBase
    'Dim ds As New DataSet
    'Dim ds1 As New DataSet
    'Dim da As New SqlDataAdapter
    Dim BuildingID As String
    Dim AisleID As String
    Dim TierID As String
    Dim LevelID As String
    Dim Category As String
    Dim SubCategory As String
    Dim Checked As String
    Const SYM_PLUS As String = "[+]"
    Const SYM_MINUS As String = "[-]"

    Public Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        lblMsg.Text = ""
        lblMsg.Visible = False
        BuildingID = DBNull.Value.ToString()
        AisleID = DBNull.Value.ToString()
        TierID = DBNull.Value.ToString()
        LevelID = DBNull.Value.ToString()

        SetPageTitle("Item Location Mapping")
        If Not (Page.IsPostBack) Then

            Dim Mode As String
            Mode = "Edit"

            LoadCatSubItem(ddlCategory, ddlSubCategory, GetCurrentPageName(), Mode)

            Dim ReaderObj As DataSet
            Dim locDAO As New LocationDAO

            ReaderObj = locDAO.GetBuilding()
            PopulateComboBoxWithTwoFields(ddlBuilding, ReaderObj)

            BindGridItem()
            InitializeDataGridView(gdvItem)
            gdvItem.Columns(2).Visible = False


            BindGrid(BuildingID, AisleID, TierID, LevelID)
            ddlAisle.Visible = False
            ddlLevel.Visible = False
            ddlTier.Visible = False
            lblAisle.Visible = False
            lblLevel.Visible = False
            lblTier.Visible = False

        End If



    End Sub

    Private Sub InitializeLoadCriteria()
        Dim ReaderObj As DataSet
        Dim locDAO As New LocationDAO

        ReaderObj = locDAO.GetBuilding()
        PopulateComboBoxWithTwoFields(ddlBuilding, ReaderObj)
        ddlAisle.Items.Clear()
        ddlTier.Items.Clear()
        ddlLevel.Items.Clear()
    End Sub
    Protected Sub BindGridItem()
        SearchResukts_gdvItem()
    End Sub

    Protected Sub BindGrid(ByVal BuildingID As String, ByVal AisleID As String, ByVal TierID As String, ByVal LevelID As String)
        'Dim ReaderObj As DataSet
        'Dim locDAO As New LocationDAO
        'ReaderObj = locDAO.GetLocations(BuildingID, AisleID, TierID, LevelID)
        'PopulateDataGridViewDataSet(gdvLocation, ReaderObj)
        GetSortableDataLocation(BuildingID, AisleID, TierID, LevelID)
    End Sub
    Protected Sub gdvItem_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gdvItem.PageIndexChanging
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If
        gdvItem.DataSource = SortDataTable(GetSortableData(), True)
        gdvItem.PageIndex = e.NewPageIndex
        gdvItem.DataBind()
    End Sub
    Protected Sub gdvItem_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gdvItem.Sorting
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If
        GridViewSortExpression = e.SortExpression
        Dim pageIndex As Integer = gdvItem.PageIndex
        gdvItem.DataSource = SortDataTable(GetSortableData(), False)
        gdvItem.DataBind()
        gdvItem.PageIndex = pageIndex
    End Sub
    Protected Sub gdvLocation_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gdvLocation.PageIndexChanging
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If
        gdvLocation.DataSource = SortDataTable(GetSortableDataLocation(BuildingID, AisleID, TierID, LevelID), True)
        gdvLocation.PageIndex = e.NewPageIndex
        gdvLocation.DataBind()
    End Sub
    Protected Sub gdvLocation_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gdvLocation.Sorting
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If
        GridViewSortExpression = e.SortExpression
        Dim pageIndex As Integer = gdvLocation.PageIndex
        gdvLocation.DataSource = SortDataTable(GetSortableDataLocation(BuildingID, AisleID, TierID, LevelID), False)
        gdvLocation.DataBind()
        gdvLocation.PageIndex = pageIndex
    End Sub

    Public Function GetSortableDataLocation(ByVal BuildingID As String, ByVal AisleID As String, ByVal TierID As String, ByVal LevelID As String) As DataTable
        Dim dt As DataTable = New DataTable()
        Dim ReaderObj As DataSet
        Dim locDAO As New LocationDAO
        ReaderObj = locDAO.GetLocations(BuildingID, AisleID, TierID, LevelID)
        'PopulateDataGridViewDataSet(gdvLocation, ReaderObj)

        dt = ReaderObj.Tables(0)
        gdvLocation.DataSource = ReaderObj.Tables(0)
        gdvLocation.DataBind()
        Return dt
    End Function
    Public Function GetSortableData() As DataTable
        Dim dt As DataTable = New DataTable()
        Dim ReaderObj As DataSet
        Dim locDAO As New LocationDAO

        'ReaderObj = locDAO.GetLocations(BuildingID, AisleID, TierID, LevelID)
        'PopulateDataGridViewRetDataSet(gdvLocation, ReaderObj)

        If Not ddlCategory.Text.ToString() = "" Then
            Category = ddlCategory.Text.ToString()
        Else
            Category = DBNull.Value.ToString()
        End If
        If Not ddlSubCategory.Text.ToString() = "" Then
            SubCategory = ddlSubCategory.Text.ToString()
        Else
            SubCategory = DBNull.Value.ToString()
        End If
        If chkItem.Checked = True Then
            Checked = True
        Else
            Checked = False
        End If
        ReaderObj = locDAO.GetItems(Category, SubCategory, Checked)
        'PopulateDataGridViewDataSet(gdvItem, ReaderObj)

        gdvItem.Columns(2).Visible = True

        gdvItem.Columns(2).Visible = False

        dt = ReaderObj.Tables(0)
        gdvItem.DataSource = ReaderObj.Tables(0)
        gdvItem.DataBind()
        Return dt
    End Function

    'Private ODDataSet As New DataSet()
    Protected Sub ddlCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlCategory.SelectedIndexChanged

        Dim mode1 As String
        mode1 = "Edit"
        ddlSubCategory.Items.Clear()

        LoadCatSubItem(ddlCategory, ddlSubCategory, GetCurrentPageName(), mode1)


    End Sub

    Protected Sub btnGo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ImgGo.Click
        SearchResukts_gdvItem()
    End Sub
    Private Sub SearchResukts_gdvItem()

        GetSortableData()
    End Sub


    Protected Sub btnfind_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnfind.Click
        If ddlBuilding.SelectedValue() <> "" Then
            BuildingID = ddlBuilding.SelectedValue()
        Else
            BuildingID = DBNull.Value.ToString()
        End If
        If ddlAisle.SelectedValue() <> "" Then
            AisleID = ddlAisle.SelectedValue()
        Else
            AisleID = DBNull.Value.ToString()
        End If
        If ddlTier.SelectedValue() <> "" Then
            TierID = ddlTier.SelectedValue()
        Else
            TierID = DBNull.Value.ToString()
        End If
        If ddlLevel.SelectedValue() <> "" Then
            LevelID = ddlLevel.SelectedValue()
        Else
            LevelID = DBNull.Value.ToString()
        End If


        BindGrid(BuildingID, AisleID, TierID, LevelID)


    End Sub

    Protected Sub btnAddtoLocation_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddtoLocation.Click

        Dim SelectFlagItem As String
        SelectFlagItem = "N"
        Dim SelectFlagLocation As String
        SelectFlagLocation = "N"


        For Each gvr As GridViewRow In gdvItem.Rows
            If (CType(gvr.FindControl("chkItem"), CheckBox)).Checked = True Then
                SelectFlagItem = "Y"
            End If
        Next gvr


        For Each gvr As GridViewRow In gdvLocation.Rows
            If (CType(gvr.FindControl("chkItem"), CheckBox)).Checked = True Then
                SelectFlagLocation = "Y"
            End If
        Next gvr


        If (SelectFlagItem = "N" And SelectFlagLocation = "N") Then
            lblMsg.Text = "* Please select the Item and Location *"
            lblMsg.Visible = True
        ElseIf (SelectFlagItem = "Y" And SelectFlagLocation = "N") Then
            lblMsg.Text = "* Please select the  Location *"
            lblMsg.Visible = True
        ElseIf (SelectFlagItem = "N" And SelectFlagLocation = "Y") Then
            lblMsg.Text = "* Please select the  Item *"
            lblMsg.Visible = True
        Else

            Dim loc As Integer


            '  BindGrid(BuildingID, AisleID, TierID, LevelID)


            For i = 0 To gdvItem.Rows.Count - 1
                Dim chk As CheckBox = CType(gdvItem.Rows(i).Cells(0).FindControl("chkItem"), CheckBox)
                If (chk.Checked = True) Then
                    Dim ItemId As String = CType(gdvItem.Rows(i).Cells(2).FindControl("lblItemid"), Label).Text()

                    For loc = 0 To gdvLocation.Rows.Count - 1
                        Dim chk1 As CheckBox = CType(gdvLocation.Rows(loc).Cells(1).FindControl("chkItem"), CheckBox)
                        Dim LMid As String = CType(gdvLocation.Rows(loc).Cells(2).FindControl("lblLMidSave"), Label).Text
                        If (chk1.Checked) = True Then
                            If LMid.Trim().Length > 0 Then


                                Dim count As Integer

                                Dim locDAO As New LocationDAO
                                count = locDAO.GetItemCount(ItemId, LMid)
                                If count <= 0 Then

                                    Dim LocationDAO As New LocationDAO
                                    LocationDAO.AddItemToLocation(Convert.ToInt16(ItemId), Convert.ToInt16(LMid))

                                    lblMsg.Text = "Item moved to the location."
                                    lblMsg.Visible = True

                                Else
                                    lblMsg.Text = "Item already exist in the location."
                                    lblMsg.Visible = True
                                    Exit Sub
                                End If

                            Else
                                lblMsg.Text = "Can't save record.Location mapping incomplete."
                                lblMsg.Visible = True
                                Exit Sub
                            End If
                        End If
                    Next
                End If
            Next


        End If

        BindGrid(BuildingID, AisleID, TierID, LevelID)
        SearchResukts_gdvItem()
        InitializeDataGridView(gdvItem)
        gdvItem.Columns(2).Visible = False
        InitializeLoadCriteria()
    End Sub

    Protected Sub gdvItem_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gdvItem.RowCommand
        If e Is Nothing Then
            Throw New ArgumentNullException("e") ' by Farkas 8Feb11 everywhere there is an e As System.Web.UI.
        End If

        If (e.CommandName = "btn") Then
            Dim ItemId As String = e.CommandArgument

            Dim gvChild As GridView = DirectCast((DirectCast(e.CommandSource, Control)).FindControl("gvChildOne"), GridView)

            Dim objDR As SqlDataReader
            Dim locDAO As New LocationDAO
            objDR = locDAO.GetLocationDR(ItemId)

            Call ChildGridOnOff(gvChild, objDR, e)


        End If
    End Sub
    Private Sub ChildGridOnOff(ByRef grv As GridView, ByRef objDR As SqlDataReader, ByRef e As GridViewCommandEventArgs)

        With grv
            .Visible = If(.Visible, False, True)
            If (.Visible) Then
                PopulateDataGridViewDataReader(grv, objDR)

                DirectCast(e.CommandSource, LinkButton).Text = SYM_MINUS ' "[-]"
            Else
                DirectCast(e.CommandSource, LinkButton).Text = SYM_PLUS  ' "[+]"

            End If
        End With

    End Sub

    Protected Sub gdvLocation_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gdvLocation.RowCommand
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If

        If Not (e.CommandName = "btn1" Or e.CommandName = "DeleteItem" Or e.CommandName = "Page" Or e.CommandName = "Sort") Then

            Dim LMId As String = e.CommandArgument
            Dim gvChild As GridView = DirectCast((DirectCast(e.CommandSource, Control)).FindControl("gvChild"), GridView)

            Dim ReaderObj As SqlDataReader
            Dim locDAO As New LocationDAO
            ReaderObj = locDAO.ItemsInLocationDR(LMId)

            Call ChildGridOnOff(gvChild, ReaderObj, e)



        End If
    End Sub




    Protected Sub gvChild_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If

        If (e.CommandName = "DeleteItem") Then

            'Dim index As Integer = Convert.ToInt64(e.CommandArgument)
            Dim Id As Integer = e.CommandArgument()
            Dim locDAO As New LocationDAO
            locDAO.DeleteItem(Id)
            BindGridItem()
            BindGrid(BuildingID, AisleID, TierID, LevelID)
            InitializeLoadCriteria()
        End If
    End Sub

    Protected Sub gdvItem_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gdvItem.RowDataBound
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If

        If (e.Row.RowType = DataControlRowType.DataRow) Then
            If (CType(e.Row.Cells(3).FindControl("lblLMId"), Label).Text() = 0) Then
                e.Row.Cells(0).Text = String.Empty
            Else
                CType(e.Row.Cells(0).FindControl("imgBtnRefresh"), LinkButton).Text() = SYM_PLUS
            End If
        End If

    End Sub

    Protected Sub gdvLocation_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gdvLocation.RowDataBound
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If


        If (e.Row.RowType = DataControlRowType.DataRow) Then

            If (CType(e.Row.Cells(2).FindControl("lblLMId"), Label).Text().Trim() = "") Then
                e.Row.Cells(0).Text = String.Empty
            Else

                CType(e.Row.Cells(0).FindControl("imgBtnDelete"), LinkButton).Text() = SYM_PLUS


            End If
        End If
    End Sub

    Protected Sub ddlBuilding_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlBuilding.SelectedIndexChanged
        Dim ReaderObj As DataSet
        Dim locDAO As New LocationDAO
        If Not ddlBuilding.SelectedValue() = "" Then
            ReaderObj = locDAO.GetAisle(ddlBuilding.SelectedValue())
            PopulateComboBoxWithTwoFields(ddlAisle, ReaderObj)
        Else
            ddlAisle.Items.Clear()
            ddlTier.Items.Clear()
            ddlLevel.Items.Clear()
        End If
        ddlAisle.Visible = True
        lblAisle.Visible = True
        ddlTier.Visible = False
        lblTier.Visible = False
        ddlLevel.Visible = False
        lblLevel.Visible = False


    End Sub

    Protected Sub ddlAisle_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlAisle.SelectedIndexChanged

        Dim ReaderObj As DataSet
        Dim locDAO As New LocationDAO
        If Not ddlAisle.SelectedValue() = "" Then
            ReaderObj = locDAO.GetTier(ddlBuilding.SelectedValue(), ddlAisle.SelectedValue())
            PopulateComboBoxWithTwoFields(ddlTier, ReaderObj)
        Else
            ddlTier.Items.Clear()
            ddlLevel.Items.Clear()
        End If

        ddlTier.Visible = True
        lblTier.Visible = True
        ddlLevel.Visible = False
        lblLevel.Visible = False

    End Sub

    Protected Sub ddlTier_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlTier.SelectedIndexChanged


        Dim ReaderObj As DataSet
        Dim locDAO As New LocationDAO
        If Not ddlTier.SelectedValue() = "" Then
            ReaderObj = locDAO.GetLevel(ddlBuilding.SelectedValue(), ddlAisle.SelectedValue(), ddlTier.SelectedValue())
            PopulateComboBoxWithTwoFields(ddlLevel, ReaderObj)

        Else
            ddlLevel.Items.Clear()
        End If

        ddlLevel.Visible = True
        lblLevel.Visible = True

    End Sub

    Protected Sub btnAddNewLocation_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnAddNewLocation.Click
        RedirectTo("../Admin/frm66_LocationMapping.aspx")
    End Sub


    Private Sub ddlSubCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSubCategory.SelectedIndexChanged
        Dim mode1 As String
        mode1 = "Edit"

        ddlCategory.Items.Clear()
        LoadCatSubItem(ddlCategory, ddlSubCategory, GetCurrentPageName(), mode1)
    End Sub
End Class