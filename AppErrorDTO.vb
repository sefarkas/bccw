﻿
<Serializable()> _
Public Class AppErrorDTO
    Public Property ErrorID() As Long
        Get
            Return m_ErrorID
        End Get
        Set(ByVal value As Long)
            m_ErrorID = value
        End Set
    End Property
    Private m_ErrorID As Long
    Public Property Url() As String
        Get
            Return m_Url
        End Get
        Set(ByVal value As String)
            m_Url = value
        End Set
    End Property
    Private m_Url As String
    Public Property RequestType() As String
        Get
            Return m_RequestType
        End Get
        Set(ByVal value As String)
            m_RequestType = value
        End Set
    End Property
    Private m_RequestType As String
    Public Property UserID() As String
        Get
            Return m_UserID
        End Get
        Set(ByVal value As String)
            m_UserID = value
        End Set
    End Property
    Private m_UserID As String
    Public Property ErrorMessage() As String
        Get
            Return m_ErrorMessage
        End Get
        Set(ByVal value As String)
            m_ErrorMessage = value
        End Set
    End Property
    Private m_ErrorMessage As String
    Public Property StackTrace() As String
        Get
            Return m_StackTrace
        End Get
        Set(ByVal value As String)
            m_StackTrace = value
        End Set
    End Property
    Private m_StackTrace As String
End Class
