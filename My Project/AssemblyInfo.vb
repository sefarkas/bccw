﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes
<Assembly: AssemblyTitle("BCCW")>
<Assembly: AssemblyDescription("BCC Warehouse Inventory Mangement (DS-21)")>
<Assembly: AssemblyCompany("DSNY")> 
<Assembly: AssemblyProduct("Inventory")>
<Assembly: AssemblyCopyright("Copyright © DSNY 2019")>
<Assembly: AssemblyTrademark("")> 


<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("2619ed95-30f5-4e8c-b48e-543aa771df98")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

'<Assembly: AssemblyVersion("1.2011.89.1208")> ' by Farkas 24Jun2019 per https://stackoverflow.com/questions/2895898/visual-studio-build-fails-unable-to-copy-exe-file-from-obj-debug-to-bin-debug
<Assembly: AssemblyVersion("3.0.0.0")>

<Assembly: CLSCompliant(True)>    ' by Farkas 8Feb10 per http://msdn.microsoft.com/query/dev10.query?appId=Dev10IDEF1&l=EN-US&k=k(MARKASSEMBLIESWITHCLSCOMPLIANT);k(VS.ERRORLIST);k(TargetFrameworkMoniker-%22.NETFRAMEWORK%2cVERSION%3dV4.0%22)&rd=true


