﻿Imports System.Data.SqlClient
Imports System.Threading.Thread
Imports System.Security.Principal
Imports System.IO
Imports Entities
Imports DataAccess
Imports DataAccess.clsQueriesSelect
Imports DataAccess.clsQueriesAction

Public Class InventoryBase
    Inherits System.Web.UI.Page
    Private strUserRole As String
    Private strDefaultShop As String
    Public Sub SetPageTitle(ByVal strTitle As String)
        CType(Master.FindControl("lblTitle"), Label).Text = strTitle
    End Sub
    Public Sub RedirectTo(ByVal strPage As String)
        Response.Redirect(strPage)
    End Sub

    Public Function ExecuteSP(ByVal ParamValues(,) As String, ByVal SPName As String) As Integer
        Dim objclsQueriesAction As New clsQueriesAction
        Return objclsQueriesAction.ExecuteSPParamValues(ParamValues, SPName)

    End Function

    Public Function ExecuteScalar(ByVal Query As String) As String
        Dim objclsQueriesAction As New clsQueriesAction
        Return objclsQueriesAction.ExecuteScalar(Query)
    End Function



    Public Sub PopulateSearchCriteria(ByVal pnlObj As Panel, ByVal strViewName As String, _
                                      Optional ByVal blnResetText As Boolean = True, _
                                      Optional ByVal PartNoForLBLlookUp As String = "")
        Dim c As Control
        Dim textbox As System.Web.UI.WebControls.TextBox
        Dim objLabel As System.Web.UI.WebControls.Label
        Dim strSQLQuery As String
        Const LBL_VALUEof As String = "lbl_valueOf_"

        For Each c In pnlObj.Controls
            If c.GetType Is GetType(Web.UI.WebControls.DropDownList) And Left(c.ID, 13) <> "cboDateSearch" And Left(c.ID, 13) <> "cboLocation" Then
                strSQLQuery = "select distinct " & Right(c.ID, Len(c.ID) - 3) & " from " & strViewName & _
                " where " & Right(c.ID, Len(c.ID) - 3) & " is not null order by " & Right(c.ID, Len(c.ID) - 3)
                PopulateDropDownList(c, strSQLQuery, blnResetText)
            ElseIf c.GetType Is GetType(Web.UI.WebControls.TextBox) Then
                textbox = CType(c, TextBox)
                If blnResetText Then textbox.Text = ""
            ElseIf c.GetType Is GetType(Web.UI.WebControls.Label) Then
                objLabel = CType(c, Label)
                If objLabel.ID Like LBL_VALUEof & "*" AndAlso Len(Trim(PartNoForLBLlookUp & "")) > 0 Then
                    strSQLQuery = "select distinct " & Replace(c.ID, LBL_VALUEof, "") & " from " & strViewName & _
                    " where " & Replace(c.ID, LBL_VALUEof, "") & " is not null and PartNo like '%' + " _
                    & Trim(PartNoForLBLlookUp) & " + '%' order by " & Replace(c.ID, LBL_VALUEof, "")
                    PopulateLBL(objLabel, strSQLQuery, False)
                End If
            End If
        Next
    End Sub

    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        If Not Me.DesignMode Then


            'Dim p As System.Security.Principal.WindowsPrincipal = TryCast(System.Threading.Thread.CurrentPrincipal, System.Security.Principal.WindowsPrincipal)
            Dim user As String = String.Empty

            If Context IsNot Nothing AndAlso Context.Session IsNot Nothing Then
                If Not IsNothing(Session("IsAuthenticated")) Then

                    If Not IsNothing(Session("User")) Then
                        user = Session("User")
                    Else
                        user = My.Resources.ErrorMessage.DEFAULT_USER
                    End If


                Else
                    FormsAuthentication.RedirectToLoginPage()
                End If
            Else
                FormsAuthentication.RedirectToLoginPage()
            End If

            If Not IsPostBack Then

                If Context IsNot Nothing AndAlso Context.Session IsNot Nothing Then
                    If IsNothing(Session("PreviousPage")) Then
                        Session.Remove("PreviousPage")

                    End If
                End If

                If Context IsNot Nothing AndAlso Context.Session IsNot Nothing Then
                    If Not IsNothing(Request.UrlReferrer) Then
                        Session.Add("PreviousPage", Request.UrlReferrer.ToString())
                    End If
                End If



            End If

            SetUserRole(user)
            SetDefaultShop(user)

        End If
    End Sub
    Public Sub PopulateComboBox(ByVal ComboBoxObj As DropDownList, ByVal strSQL As String, Optional ByVal blnResetText As Boolean = True)
        Dim objclsQueriesAction As New clsQueriesAction

        Dim Results As SqlDataReader = objclsQueriesAction.GetReader(strSQL)

        ComboBoxObj.Items.Clear()

        While Results.Read()
            If Results.FieldCount = 1 Then
                If Not Results.Item(0) Is System.DBNull.Value Then
                    ComboBoxObj.Items.Add((Results.Item(0)))
                End If
            End If
        End While
        ComboBoxObj.Items.Insert(0, "")

        Results.Close()

    End Sub
    Public Sub PopulateComboBox(ByVal ComboBoxObj As DropDownList, ByVal Results As SqlDataReader)
        ComboBoxObj.Items.Clear()

        While Results.Read()
            If Results.FieldCount = 1 Then
                If Not Results.Item(0) Is System.DBNull.Value Then
                    ComboBoxObj.Items.Add((Results.Item(0)))
                End If
            End If
        End While

        ComboBoxObj.Items.Insert(0, "")

        Results.Close()

    End Sub

    Public Sub PopulateComboBoxWithTwoFields(ByRef ComboBoxObj As DropDownList, ByVal Results As DataSet)


        ComboBoxObj.Items.Clear()

        ComboBoxObj.DataSource = Results

        ComboBoxObj.DataValueField = "Id"
        ComboBoxObj.DataTextField = "Name"

        'If Results.FieldCount = 2 Then
        '    If Not Results.Item(0) Is System.DBNull.Value Then
        '        'ComboBoxObj.Items.Insert(Results.Item(0), Results.Item(1))

        '        ComboBoxObj.DataValueField = Results.Item(0)
        '        ComboBoxObj.DataTextField = Results.Item(1)

        '    End If
        'End If
        'End While
        ComboBoxObj.DataBind()

        If ComboBoxObj.Items.Count > 0 Then
            ComboBoxObj.Items.Insert(0, "")
        End If
        'Results.Close()

    End Sub

    '*****************To populate textbox from sqlreader***************
    Public Sub Populatetextbox(ByVal objTextbox As TextBox, ByVal Results As SqlDataReader)
        objTextbox.Text = ""

        While Results.Read()
            If Results.FieldCount = 1 Then
                If Not Results.Item(0) Is System.DBNull.Value Then
                    objTextbox.Text = Results.Item(0)
                End If
            End If
        End While

        Results.Close()

    End Sub



    Public Sub PopulateDDL(ByVal ComboBoxObj As DropDownList, ByVal strSQL As String, Optional ByVal blnResetText As Boolean = True)
        Dim objclsQueriesAction As New clsQueriesAction
        Dim Results As SqlDataReader = objclsQueriesAction.GetReader(strSQL)

        ComboBoxObj.Items.Clear()
        ComboBoxObj.DataValueField = "Id"
        ComboBoxObj.DataTextField = "Name"
        ComboBoxObj.DataSource = Results
        ComboBoxObj.DataBind()
        ComboBoxObj.Items.Insert(0, "")

    End Sub
    Public Sub PopulateLBL(ByVal objLabel As Label, ByVal strSQL As String, _
                            Optional ByVal blnResetText As Boolean = True)
        Dim objclsQueriesAction As New clsQueriesAction
        Dim Results As SqlDataReader = objclsQueriesAction.GetReader(strSQL)
        Results.Read()
        objLabel.Text = Results.Item(0).ToString
        Results.Close()

    End Sub
    Public Sub LoadCatSubItem(ByRef ComboBoxObj1 As DropDownList, ByRef ComboBoxObj2 As DropDownList, ByRef ComboBoxObj3 As DropDownList, ByVal PageName As String, ByVal Mode As String)


        Dim strSubCategory As String
        Dim strCategory As String
        Dim strItemDesc As String
        strCategory = ComboBoxObj1.SelectedValue.Trim.ToString()
        strSubCategory = ComboBoxObj2.SelectedValue.Trim.ToString()
        strItemDesc = ComboBoxObj3.SelectedValue.Trim.ToString()

        If InStr(strItemDesc.Trim(), "'") <> 0 Then
            strItemDesc = Replace(strItemDesc.Trim(), "'", "''")
        Else
            strItemDesc = strItemDesc.Trim()
        End If
        If (Len(Trim(strCategory & "")) = 0 And Len(Trim(strSubCategory & "")) = 0 And Len(Trim(strItemDesc & "")) = 0) Then
            PopulateCategory(ComboBoxObj1, PageName, Mode, strSubCategory, strItemDesc)
            PopulateSubCategory(ComboBoxObj2, PageName, Mode, strCategory, strItemDesc)
            PopulateItemDesc(ComboBoxObj3, PageName, Mode, strCategory, strSubCategory)
        ElseIf (Len(Trim(strCategory & "")) > 0 And Len(Trim(strSubCategory & "")) = 0 And Len(Trim(strItemDesc & "")) = 0) Then
            PopulateSubCategory(ComboBoxObj2, PageName, Mode, strCategory, strItemDesc)
            'If ComboBoxObj2.Items.Count() > 0 Then
            '    ComboBoxObj2.SelectedIndex = 1
            'End If

            PopulateItemDesc(ComboBoxObj3, PageName, Mode, strCategory, strSubCategory)
            '  ComboBoxObj3.SelectedIndex = 1
        ElseIf (Len(Trim(strCategory & "")) = 0 And Len(Trim(strSubCategory & "")) > 0 And Len(Trim(strItemDesc & "")) = 0) Then
            PopulateCategory(ComboBoxObj1, PageName, Mode, strSubCategory, strItemDesc)
            If ComboBoxObj1.Items.Count() > 0 Then
                ComboBoxObj1.SelectedIndex = 1
            End If

            PopulateSubCategory(ComboBoxObj2, PageName, Mode, ComboBoxObj1.SelectedValue.Trim(), strItemDesc)
            ComboBoxObj2.Items.FindByText(strSubCategory).Selected = True

            PopulateItemDesc(ComboBoxObj3, PageName, Mode, strCategory, strSubCategory)
            ' ComboBoxObj3.SelectedIndex = 1
        ElseIf (Len(Trim(strCategory & "")) = 0 And Len(Trim(strSubCategory & "")) = 0 And Len(Trim(strItemDesc & "")) > 0) Then
            PopulateCategory(ComboBoxObj1, PageName, Mode, strSubCategory, strItemDesc)
            If ComboBoxObj1.Items.Count() > 0 Then
                ComboBoxObj1.SelectedIndex = 1
            End If
            PopulateSubCategory(ComboBoxObj2, PageName, Mode, strCategory, strItemDesc)
            If ComboBoxObj2.Items.Count() > 0 Then
                ComboBoxObj2.SelectedIndex = 1
            End If
        ElseIf ((strCategory.Trim() <> "" And strSubCategory.Trim() <> "" And strItemDesc.Trim() = "")) Then
            PopulateItemDesc(ComboBoxObj3, PageName, Mode, strCategory, strSubCategory)
            'omboBoxObj3.SelectedIndex = 1
        ElseIf (Len(Trim(strCategory & "")) > 0 And Len(Trim(strSubCategory & "")) = 0 And Len(Trim(strItemDesc & "")) > 0) Then
            PopulateSubCategory(ComboBoxObj2, PageName, Mode, strCategory, strItemDesc)
            If ComboBoxObj2.Items.Count() > 0 Then
                ComboBoxObj2.SelectedIndex = 1
            End If
        End If



    End Sub
    Public Sub LoadCatSubItem(ByRef ComboBoxObj1 As DropDownList, ByRef ComboBoxObj2 As DropDownList, ByVal PageName As String, ByVal Mode As String)


        Dim strSubCategory As String
        Dim strCategory As String

        strCategory = ComboBoxObj1.SelectedValue.Trim.ToString()
        strSubCategory = ComboBoxObj2.SelectedValue.Trim.ToString()
     
        If (Len(Trim(strCategory & "")) = 0 And Len(Trim(strSubCategory & "")) = 0) Then
            PopulateCategory(ComboBoxObj1, PageName, Mode, strSubCategory)
            PopulateSubCategory(ComboBoxObj2, PageName, Mode, strCategory)

        ElseIf (Len(Trim(strCategory & "")) > 0 And Len(Trim(strSubCategory & "")) = 0) Then
            PopulateSubCategory(ComboBoxObj2, PageName, Mode, strCategory)
            'ComboBoxObj2.SelectedIndex = 1

        ElseIf (Len(Trim(strCategory & "")) = 0 And Len(Trim(strSubCategory & "")) > 0) Then
            PopulateCategory(ComboBoxObj1, PageName, Mode, strSubCategory)
            ComboBoxObj1.SelectedIndex = 1

            PopulateSubCategory(ComboBoxObj2, PageName, Mode, ComboBoxObj1.SelectedValue.Trim())
            ComboBoxObj2.Items.FindByText(strSubCategory).Selected = True

        End If
      
    End Sub
    Public Sub PopulateCategory(ByRef ComboBoxObj1 As DropDownList, ByVal PageName As String, ByVal Mode As String, ByVal SubCategory As String)
        Dim ddlDAO As New DDLDAO
        Dim objSql As System.Data.SqlClient.SqlDataReader
        Dim ItemDesc As String = ""
        objSql = ddlDAO.GetCategory(PageName, Mode, SubCategory, ItemDesc)
        PopulateComboBox(ComboBoxObj1, objSql)
    End Sub
    Public Sub PopulateSubCategory(ByVal ComboBoxObj2 As DropDownList, ByVal PageName As String, ByVal Mode As String, ByVal Category As String)
        Dim ddlDAO As New DDLDAO
        Dim objSql As System.Data.SqlClient.SqlDataReader
        Dim ItemDesc As String = ""
        objSql = ddlDAO.GetSubCategory(PageName, Mode, Category, ItemDesc)
        PopulateComboBox(ComboBoxObj2, objSql)
    End Sub

    Public Sub PopulateCategory(ByRef ComboBoxObj1 As DropDownList, ByVal PageName As String, ByVal Mode As String, ByVal SubCategory As String, ByVal ItemDesc As String)
        Dim ddlDAO As New DDLDAO
        Dim objSql As System.Data.SqlClient.SqlDataReader
        objSql = ddlDAO.GetCategory(PageName, Mode, SubCategory, ItemDesc)
        PopulateComboBox(ComboBoxObj1, objSql)
    End Sub
    Public Sub PopulateSubCategory(ByVal ComboBoxObj2 As DropDownList, ByVal PageName As String, ByVal Mode As String, ByVal Category As String, ByVal ItemDesc As String)
        Dim ddlDAO As New DDLDAO
        Dim objSql As System.Data.SqlClient.SqlDataReader
        objSql = ddlDAO.GetSubCategory(PageName, Mode, Category, ItemDesc)
        PopulateComboBox(ComboBoxObj2, objSql)
    End Sub
    Public Sub PopulateItemDesc(ByVal ComboBoxObj3 As DropDownList, ByVal PageName As String, ByVal Mode As String, ByVal Category As String, ByVal SubCategory As String)
        Dim ddlDAO As New DDLDAO
        Dim objSql As System.Data.SqlClient.SqlDataReader
        objSql = ddlDAO.GetItemDesc(PageName, Mode, Category, SubCategory)
        PopulateComboBox(ComboBoxObj3, objSql)
    End Sub
    Public Function GetCurrentPageName() As String
        Dim sPath As String = System.Web.HttpContext.Current.Request.Url.AbsolutePath
        Dim oInfo As New System.IO.FileInfo(sPath)
        Dim sRet As String = oInfo.Name
        Return sRet
    End Function
    Public Sub PopulateSearchCriteriaSQL(ByVal pnlObj As Panel, ByVal strQuery As String, ByVal strKeyField As String, Optional ByVal blnResetText As Boolean = True)
        Dim c As Web.UI.Control
        Dim strSQLQuery As String
        Dim textbox As System.Web.UI.WebControls.TextBox

        For Each c In pnlObj.Controls
            If c.GetType Is GetType(Web.UI.WebControls.DropDownList) And Left(c.ID, 13) <> strKeyField Then
                strSQLQuery = "select distinct " & Right(c.ID, Len(c.ID) - 3) & " " & strQuery & _
                " and " & Right(c.ID, Len(c.ID) - 3) & " is not null order by " & Right(c.ID, Len(c.ID) - 3)
                PopulateDropDownList(c, strSQLQuery, blnResetText)
            ElseIf c.GetType Is GetType(Web.UI.WebControls.TextBox) Then
                textbox = CType(c, TextBox)
                If blnResetText Then textbox.Text = ""
            End If
        Next
    End Sub

    Public Sub PopulateDropDownList(ByVal DropDownListObj As DropDownList, ByVal strSQL As String, Optional ByVal blnResetText As Boolean = True,
                                    Optional ByVal valueField As String = Nothing, Optional ByVal textField As String = Nothing)
        Dim qa As New clsQueriesAction
        If Not valueField Is Nothing Then
            Dim ds As New DataSet

            qa.GetDataSet(strSQL, ds)
            DropDownListObj.DataSource = ds
            DropDownListObj.DataTextField = textField
            DropDownListObj.DataValueField = valueField
            DropDownListObj.DataBind()
        Else
            Dim Results As SqlDataReader = qa.GetReader(strSQL)

            DropDownListObj.Items.Clear()

            While Results.Read()
                If Results.FieldCount = 1 Then
                    DropDownListObj.Items.Add((Results.Item(0)))
                End If
            End While
            Results.Close()
        End If


        If DropDownListObj.Items.Count > 0 Then
            DropDownListObj.Items.Insert(0, "")
        End If
    End Sub
    Public Sub PopulateDataGridViewDataSet(ByRef dgvObj As GridView, ByRef dsObj As DataSet)
        dgvObj.DataSource = dsObj.Tables(0)
        dgvObj.DataBind()

    End Sub
    Public Sub PopulateDataGridViewDataReader(ByRef dgvObj As GridView, ByVal objRdr As SqlClient.SqlDataReader) 'ByRef dsObj As DataSet)
        Dim dt As New DataTable()
        Try
            dt.Load(objRdr)
            With dgvObj
                .DataSource = dt.DefaultView
                .DataBind()
            End With

        Catch ex As Exception
            'Diagnostics.Debug.Print(ex.Message)
            Throw
        Finally
            dt = Nothing
        End Try

    End Sub
    Public Function PopulateDataGridViewRetDataSet(ByRef dgvObj As GridView, ByVal strSQL As String, ByRef dsObj As DataSet) As DataSet
        Dim qa As New clsQueriesAction
        qa.ClearDataSet(strSQL, dsObj)
        'dgvObj.DataSource = dsObj.Tables(0)
        'dgvObj.DataBind()
        Return dsObj
    End Function
    Public Sub PopulateDataGridView(ByRef dgvObj As GridView, ByVal strSQL As String, ByRef dsObj As DataSet)
        Dim qa As New clsQueriesAction
        qa.ClearDataSet(strSQL, dsObj)
        dgvObj.DataSource = dsObj.Tables(0)
        dgvObj.DataBind()
        'dgvObj.DataSource = dsObj.Tables(0)
        'dgvObj.DataBind()
    End Sub
    Public Enum PartOrDesc
        PartNo = 1
        Description = 2
    End Enum
    Public Function GetSearchCriteria(ByVal pnlObj As Panel, ByVal strViewName As String, ByVal strColumns As String) As String
        Dim c As Web.UI.Control
        Dim dropdown As System.Web.UI.WebControls.DropDownList
        Dim textbox As System.Web.UI.WebControls.TextBox
        Dim strSQL As String

        strSQL = "select " & strColumns & " from " & strViewName & " where "
        For Each c In pnlObj.Controls
            If c.GetType Is GetType(Web.UI.WebControls.DropDownList) Then
                dropdown = CType(c, DropDownList)
                If dropdown.Text <> "" Then
                    If c.ID = "cboItemName" Then
                        strSQL += "Description like '%" & Replace(Replace(Replace(dropdown.SelectedItem.Text, "'", "''"), "[", "[[]"), "%", "[%]") & "%' and "
                    ElseIf c.ID = "cboCategory" Then
                        strSQL += "Category like '%" & Replace(Replace(Replace(dropdown.Text, "'", "''"), "[", "[[]"), "%", "[%]") & "%' and "
                    ElseIf c.ID = "cboSubCategory" Then
                        strSQL += "SubCategory like '%" & Replace(Replace(Replace(dropdown.Text, "'", "''"), "[", "[[]"), "%", "[%]") & "%' and "
                    ElseIf Not Right(c.ID, 4) = "Date" AndAlso Not c.ID.ToLower = "cbodatesearchdispdate" AndAlso Not c.ID = "cboCategory" AndAlso Not c.ID = "cboSubCategory" Then
                        strSQL += GetColumnName(c.ID) & "like '%" & Replace(Replace(Replace(dropdown.Text, "'", "''"), "[", "[[]"), "%", "[%]") & "%' and "
                    Else
                    End If
                End If
            End If
            If c.GetType Is GetType(Web.UI.WebControls.TextBox) Then
                textbox = CType(c, TextBox)
                If textbox.Text <> "" Then
                    If c.ID = "txtDescription" Then
                        strSQL += "Description like '%" & Replace(Replace(Replace(textbox.Text, "'", "''"), "[", "[[]"), "%", "[%]") & "%' and "
                    ElseIf c.ID = "txtStockNo" Then
                        strSQL += "StockNo like '%" & Replace(Replace(Replace(textbox.Text, "'", "''"), "[", "[[]"), "%", "[%]") & "%' and "
                    ElseIf Not (Right(c.ID, 4) = "Date") Then
                        strSQL += GetColumnName(c.ID) & " like '%" & textbox.Text & "%' and "
                    End If
                End If
            End If
        Next
        If Right(strSQL, 5) = " and " Then
            strSQL = Left(strSQL, Len(strSQL) - 5)
        ElseIf Right(strSQL, 7) = " where " Then
            strSQL = Left(strSQL, Len(strSQL) - 7)
        End If

        GetSearchCriteria = strSQL
    End Function
    Public Function GetInventorySearchCriteria(ByVal pnlObj As Panel, ByVal strViewName As String, ByVal strColumns As String) As String
        Dim c As Web.UI.Control
        Dim dropdown As System.Web.UI.WebControls.DropDownList
        Dim textbox As System.Web.UI.WebControls.TextBox
        Dim strSQL As String

        strSQL = "select " & strColumns & " from " & strViewName & " where "
        For Each c In pnlObj.Controls
            If c.GetType Is GetType(Web.UI.WebControls.DropDownList) Then
                dropdown = CType(c, DropDownList)
                If dropdown.Text <> "" Then
                    If c.ID = "cboItemName" Then
                        strSQL += "Description like '%" & Replace(Replace(Replace(dropdown.SelectedItem.Text, "'", "''"), "[", "[[]"), "%", "[%]") & "%' and "
                    ElseIf c.ID = "cboDescription" Then
                        strSQL += "Description like '%" & Replace(Replace(Replace(dropdown.SelectedItem.Text, "'", "''"), "[", "[[]"), "%", "[%]") & "%' and "
                    ElseIf c.ID = "cboCategory" Then
                        strSQL += "Category like '%" & Replace(Replace(Replace(dropdown.Text, "'", "''"), "[", "[[]"), "%", "[%]") & "%' and "
                    ElseIf c.ID = "cboSubCategory" Then
                        strSQL += "[Sub Category] like '%" & Replace(Replace(Replace(dropdown.Text, "'", "''"), "[", "[[]"), "%", "[%]") & "%' and "
                    ElseIf Not Right(c.ID, 4) = "Date" AndAlso Not c.ID.ToLower = "cbodatesearchdispdate" AndAlso Not c.ID = "cboCategory" AndAlso Not c.ID = "cboSubCategory" AndAlso Not c.ID = "cboDescription" Then
                        strSQL += GetColumnName(c.ID) & "='" & dropdown.Text & "' and "
                    Else
                    End If
                End If
            End If
            If c.GetType Is GetType(Web.UI.WebControls.TextBox) Then
                textbox = CType(c, TextBox)
                If textbox.Text <> "" Then
                    If c.ID = "txtDescription" Then
                        strSQL += "Description like '%" & Replace(Replace(Replace(textbox.Text, "'", "''"), "[", "[[]"), "%", "[%]") & "%' and "
                    ElseIf c.ID = "txtStockNo" Then
                        strSQL += "StockNo like '%" & Replace(Replace(Replace(textbox.Text, "'", "''"), "[", "[[]"), "%", "[%]") & "%' and "
                    ElseIf Not (Right(c.ID, 4) = "Date") Then
                        strSQL += GetColumnName(c.ID) & " like '%" & textbox.Text & "%' and "
                    End If
                End If
            End If
        Next
        If Right(strSQL, 5) = " and " Then
            strSQL = Left(strSQL, Len(strSQL) - 5)
        ElseIf Right(strSQL, 7) = " where " Then
            strSQL = Left(strSQL, Len(strSQL) - 7)
        End If

        GetInventorySearchCriteria = strSQL
    End Function
    Public Function GetSearchCriteria1(ByVal pnlObj As Panel, ByVal strViewName As String, ByVal strColumns As String) As String
        Dim c As Web.UI.Control
        Dim dropdown As System.Web.UI.WebControls.DropDownList
        Dim textbox As System.Web.UI.WebControls.TextBox
        Dim strSQL As String

        strSQL = "select " & strColumns & " from " & strViewName & " where "
        For Each c In pnlObj.Controls
            If c.GetType Is GetType(Web.UI.WebControls.DropDownList) Then
                dropdown = CType(c, DropDownList)
                If dropdown.Text <> "" Then
                    If c.ID = "cboItemName" Then
                        strSQL += "Item like '%" & Replace(Replace(Replace(dropdown.SelectedItem.Text, "'", "''"), "[", "[[]"), "%", "[%]") & "%' and "
                    ElseIf c.ID = "cboCategory" Then
                        strSQL += "Category like '%" & Replace(Replace(Replace(dropdown.Text, "'", "''"), "[", "[[]"), "%", "[%]") & "%' and "
                    ElseIf c.ID = "cboSubCategory" Then
                        strSQL += "SubCategory like '%" & Replace(Replace(Replace(dropdown.Text, "'", "''"), "[", "[[]"), "%", "[%]") & "%' and "
                    ElseIf Not Right(c.ID, 4) = "Date" AndAlso Not c.ID.ToLower = "cbodatesearchdispdate" AndAlso Not c.ID = "cboCategory" AndAlso Not c.ID = "cboSubCategory" Then
                        strSQL += GetColumnName(c.ID) & "='" & dropdown.Text & "' and "
                    Else
                    End If
                End If
            End If
            If c.GetType Is GetType(Web.UI.WebControls.TextBox) Then
                textbox = CType(c, TextBox)
                If textbox.Text <> "" Then
                    If c.ID = "txtDescription" Then
                        strSQL += "Description like '%" & Replace(Replace(Replace(textbox.Text, "'", "''"), "[", "[[]"), "%", "[%]") & "%' and "
                    ElseIf c.ID = "txtStockNo" Then
                        strSQL += "StockNo like '%" & Replace(Replace(Replace(textbox.Text, "'", "''"), "[", "[[]"), "%", "[%]") & "%' and "
                    ElseIf Not (Right(c.ID, 4) = "Date") Then
                        strSQL += GetColumnName(c.ID) & " like '%" & textbox.Text & "%' and "
                    End If
                End If
            End If
        Next
        If Right(strSQL, 5) = " and " Then
            strSQL = Left(strSQL, Len(strSQL) - 5)
        ElseIf Right(strSQL, 7) = " where " Then
            strSQL = Left(strSQL, Len(strSQL) - 7)
        End If

        GetSearchCriteria1 = strSQL
    End Function
    Public Function GetColumnName(ByVal strControlName As String) As String
        Dim strName, strTest As String
        Dim c, clast As Char
        clast = ""

        strName = Right(strControlName, Len(strControlName) - 3)

        For Each c In strName
            strTest = clast & c
            If Char.IsUpper(c) And strTest <> "PO" And strTest <> "SI" And strTest <> "WO" Then
                strName = Replace(strName, c, " " & c)
            End If
            clast = c
        Next

        GetColumnName = "[" & Trim(strName) & "]"
    End Function


    Public Sub InitializeDataGridView(ByVal dgvObj As GridView)

    End Sub
    Public Sub AddSqlForDateSearch(ByVal strColName As String, ByVal strType As String, _
    ByVal txtObj As Web.UI.WebControls.TextBox, ByVal txtToObj As Web.UI.WebControls.TextBox, _
    ByRef strCriteria As String)
        If InStr(strCriteria, "where") Then
            strCriteria += " and "
        Else
            strCriteria += " where "
        End If
        Select Case strType
            Case "Equal To"

                strCriteria += "[" & strColName & "]  = '" & txtObj.Text & "'"
            Case "After"
                strCriteria += " [" & strColName & "] > '" & txtObj.Text & "'"
            Case "Before"
                strCriteria += " [" & strColName & "] < '" & txtObj.Text & "'"
            Case "Between"
                If txtToObj.Text = "" Then 'Default to "After"
                    strCriteria += " [" & strColName & "] > '" & txtObj.Text & "'"
                Else
                    strCriteria += " [" & strColName & "] between '" & txtObj.Text & "' and '" & txtToObj.Text & "'"
                End If
        End Select
    End Sub
    Public Shared Sub AddSqlForDateSearchforAdj(ByVal strColName As String, ByVal strType As String, _
    ByRef txtObj As Web.UI.WebControls.TextBox, ByRef txtToObj As Web.UI.WebControls.TextBox, _
    ByRef strCriteria As String)
        If InStr(strCriteria, "where") Then
            strCriteria += " and "
        Else
            strCriteria += " where "
        End If
        Select Case strType
            Case "Equal To"

                strCriteria += "CONVERT(date,[" & strColName & "],101)  = '" & txtObj.Text & "'"
            Case "After"
                strCriteria += " CONVERT(date,[" & strColName & "],101) > '" & txtObj.Text & "'"
            Case "Before"
                strCriteria += " CONVERT(date,[" & strColName & "],101) < '" & txtObj.Text & "'"
            Case "Between"
                If txtToObj.Text = "" Then 'Default to "After"
                    strCriteria += " CONVERT(date,[" & strColName & "],101) > '" & txtObj.Text & "'"
                Else
                    strCriteria += " CONVERT(date,[" & strColName & "],101) between '" & txtObj.Text & "' and '" & txtToObj.Text & "'"
                End If
        End Select
    End Sub

    Public Sub AddSearchSQL(ByRef strSQL As String, ByVal strColName As String, ByVal strValue As String, ByVal strType As String)
        If InStr(strSQL, "where") Then
            strSQL += " and "
        Else
            strSQL += " where "
        End If
        Select Case strType
            Case "String"
                strSQL += strColName & "='" & strValue & "'"
            Case Else
                strSQL += strColName & "=" & strValue
        End Select
    End Sub

    'Public Function GetTotal(ByVal dtObj As DataTable, ByVal strColumnName As String) As Decimal
    '    Dim row As DataRow
    '    Dim Total As Decimal
    '    Total = 0

    '    For Each row In dtObj.Rows
    '        If row(strColumnName).ToString <> "" Then Total += row(strColumnName)
    '    Next

    '    GetTotal = Total
    'End Function

    Public Sub PopulateData(ByVal frmObj As Control, ByVal row As DataRow)
        Dim c As Control

        For Each c In frmObj.Controls
            If c.GetType Is GetType(Web.UI.WebControls.TextBox) Or c.GetType Is GetType(Web.UI.WebControls.DropDownList) Then
                If c.GetType Is GetType(Web.UI.WebControls.TextBox) Then
                    Dim tx As TextBox
                    tx = c
                    If InStr(tx.ID, "Total") = 0 Then tx.Text = row(Right(c.ID, Len(c.ID) - 3)).ToString()
                End If

            ElseIf c.GetType Is GetType(Web.UI.WebControls.Panel) Then
                If c.ID <> "pnlHeader" And (Left(c.ID, 5)) <> "Panel" Then PopulateData(c, row)
            End If
        Next
    End Sub

    Public Sub PopulateDataRecord(ByVal frmObj As Control, ByVal row As DataRow)
        Dim c As Control
        'Dim c As Web.UI.Control
        Dim dropdown As System.Web.UI.WebControls.DropDownList
        Dim textbox As System.Web.UI.WebControls.TextBox
        Dim Label As System.Web.UI.WebControls.Label

        For Each c In frmObj.Controls
            If c.GetType Is GetType(TextBox) Or c.GetType Is GetType(System.Web.UI.WebControls.DropDownList) Or c.GetType Is GetType(System.Web.UI.WebControls.Label) Then 'Or c.GetType Is GetType(System.Windows.Forms.MaskedTextBox) Then
                If InStr(c.ID, "Total") = 0 Then
                    If (c.GetType Is GetType(TextBox)) Then
                        textbox = CType(c, TextBox)
                        textbox.Text = row(Right(c.ID, Len(c.ID) - 3)).ToString()

                    ElseIf (c.GetType Is GetType(DropDownList)) Then
                        dropdown = CType(c, DropDownList)
                        dropdown.Text = row(Right(c.ID, Len(c.ID) - 3)).ToString()

                    ElseIf (c.GetType Is GetType(Label) And InStr(c.ID, "lbl") <> 0) Then
                        Label = CType(c, Label)
                        Label.Text = row(Right(c.ID, Len(c.ID) - 3)).ToString()

                    End If


                End If
            ElseIf c.GetType Is GetType(System.Web.UI.WebControls.Panel) Then
                If c.ID <> "pnlHeader" And (Left(c.ID, 5)) <> "Panel" Then PopulateDataRecord(c, row)
            End If
        Next
    End Sub
    'Public Function FormDataChanged(ByVal frmObj As Control, ByVal row As DataRow) As Boolean
    '    Dim blnDataChanged As Boolean
    '    Return blnDataChanged
    'End Function

    'Private Function SelectedValueChanged(ByVal c As DropDownList, ByVal strValue As String) As Boolean
    '    If c.SelectedValue.ToString() <> strValue Then Return True Else Return False
    'End Function

    Public Sub ClearData(ByVal frmObj As Control)
        Dim c As Web.UI.Control

        For Each c In frmObj.Controls
            If c.GetType Is GetType(Web.UI.WebControls.TextBox) Or c.GetType Is GetType(Web.UI.WebControls.DropDownList) Or c.GetType Is GetType(Web.UI.WebControls.TextBox) Then
                ' c.ResetText()
            ElseIf c.GetType Is GetType(Web.UI.WebControls.Panel) Then
                ClearData(c)
            End If
        Next
    End Sub

    'Public Function GetPrincipal() As WindowsPrincipal
    '    Dim MyPrincipal As WindowsPrincipal = CType(System.Threading.Thread.CurrentPrincipal, WindowsPrincipal)
    '    GetPrincipal = MyPrincipal
    'End Function

    Public Function GetUserName() As String
        If Not IsNothing(Session("User")) Then
            GetUserName = Session("User")
        Else
            GetUserName = "Guest"
        End If

    End Function

    Public Sub SetPolicy()
        AppDomain.CurrentDomain.SetPrincipalPolicy(PrincipalPolicy.WindowsPrincipal)
    End Sub

    'Public Function GetParameterName(ByVal strColumnName As String) As String
    '    Dim strName As String

    '    If strColumnName = "Shop" Then
    '        strName = "ShopCode"
    '    ElseIf strColumnName = "Item Comments" Then
    '        strName = "Comments"
    '    Else
    '        strName = Replace(strColumnName, " ", "")
    '    End If

    '    Return strName
    'End Function
    Public Function UpdateInvPart(ByVal PartDataChanged As Boolean, ByVal PartData(,) As String, ByVal InvDataChanged As Boolean) As Integer
        Dim objDataAccess As New InventoryDAO

        Return objDataAccess.UpdateInvPart(PartDataChanged, PartData, InvDataChanged)
    End Function



    Public Function RetrieveMfr(ByVal strSql As String)
        Dim objDataAccess As New InventoryDAO
        Return objDataAccess.RetrieveMfr(strSql)
    End Function
    Public Function Process(ByVal intTempID As Integer, ByVal strSPName As String) As Boolean
        Dim objDataAccess As New DataAccess.DataAccess
        Return objDataAccess.Process(intTempID, strSPName)
    End Function

    Public Sub CleanUpTempData(ByVal intHeaderID As Integer, ByVal strFunction As String)
        Dim objDataAccess As New DataAccess.DataAccess
        objDataAccess.CleanUpTempData(intHeaderID, strFunction)
    End Sub

    Public Sub SaveErrorLog(objErr As Entities.AppError)
        If objErr Is Nothing Then
            Throw New ArgumentNullException(NameOf(objErr))
        End If

        Dim objDataAccess As New DataAccess.DataAccess
        objDataAccess.SaveErrorLog(objErr)

    End Sub
    Public Sub SetUserRole(ByVal strLoginID As String)
        Dim strResult As String
        strResult = ExecuteScalar("select Role from vwAppRoles where LoginID = '" & strLoginID & "'")
        ' strResult = ExecuteScalar("select Role from vwAppRoles where LoginID = 'ELebron'")
        If strResult <> "" Then
            strUserRole = strResult
        Else
            strUserRole = "View"
        End If

        If IsNothing(Session("UserRole")) Then
            Session.Add("UserRole", strUserRole)
        Else
            Session("UserRole") = strUserRole
        End If

    End Sub

    Public Function GetUserRole() As String
        GetUserRole = strUserRole

    End Function

    Public Sub SetDefaultShop(ByVal strLoginID As String)
        strDefaultShop = ExecuteScalar("select ShopName from vwShopPersonnel where LoginID = '" & strLoginID & "' and isDefault = 1")

    End Sub

    Public Function GetDefaultShop() As String
        GetDefaultShop = strDefaultShop
    End Function

    Public Function HaveShopPermissions(ByVal strShop As String, ByVal strLoginID As String) As Boolean
        Dim blnHavePermissions As Boolean = False

        If ExecuteScalar("select count(ShopCode) from vwShopPersonnel where LoginID = '" & strLoginID & "' and ShopName = '" & strShop & "'") > 0 Then
            blnHavePermissions = True
        End If

        HaveShopPermissions = blnHavePermissions
    End Function
    Public Function GetFieldName(ByVal strControlName As String) As String
        Dim strName, strTest As String
        Dim c, clast As Char
        clast = ""

        strName = Right(strControlName, Len(strControlName) - 3)

        For Each c In strName
            strTest = clast & c
            If Char.IsUpper(c) And strTest <> "PO" And strTest <> "ID" And strTest <> "WO" Then
                strName = Replace(strName, c, " " & c)
            End If
            clast = c
        Next

        GetFieldName = Trim(strName)
    End Function

    Public Function DatesValid(ByVal txt1 As TextBox, ByVal txt2 As TextBox, ByVal NullOK As Boolean) As Boolean
        Dim blnValid = True

        If NullOK Then
            If txt1.Text <> "" And txt2.Text <> "" Then
                If CDate(txt1.Text) > CDate(txt2.Text) Then
                    blnValid = False
                End If
            End If
        Else
            If txt1.Text <> "" And txt2.Text <> "" Then
                If CDate(txt1.Text) > CDate(txt2.Text) Then
                    blnValid = False
                End If
            ElseIf txt1.Text = "" And txt2.Text <> "" Then
                blnValid = False
            End If
        End If
        Return blnValid
    End Function


    'Private Sub SetReadOnly(ByVal txtObj As TextBox, ByVal blnValue As Boolean)
    '    txtObj.ReadOnly = blnValue
    'End Sub

    Public Sub RedirectToPrevious()
        Response.Redirect(Session("PreviousPage"))
    End Sub
    Public Sub RedirectToPrevious(ByVal strrQuery As String)
        Response.Redirect(Session("PreviousPage") & "?" & strrQuery)
    End Sub

    'Public Sub ASPNET_MsgBox(ByVal Message As String)

    '    System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=""JavaScript"">" & vbCrLf)

    '    System.Web.HttpContext.Current.Response.Write("alert(""" & Message & """)" & vbCrLf)

    '    System.Web.HttpContext.Current.Response.Write("</SCRIPT>")

    'End Sub
    'Public Function GetCurrentFY() As Integer
    '    If Now.Month >= 7 Then
    '        Return (Now.Year + 1)
    '    Else
    '        Return Now.Year
    '    End If
    'End Function
    Public Property GridViewSortExpression() As String
        Get
            Return IIf(ViewState("SortExpression") = Nothing, String.Empty, ViewState("SortExpression"))
        End Get
        Set(ByVal value As String)
            ViewState("SortExpression") = value
        End Set
    End Property
    Public Property GridViewSortDirection() As String
        Get
            Return IIf(ViewState("SortDirection") = Nothing, "ASC", ViewState("SortDirection"))
        End Get
        Set(ByVal value As String)
            ViewState("SortDirection") = value
        End Set
    End Property
    Public Function GetSortDirection() As String
        Select Case GridViewSortDirection
            Case "ASC"
                GridViewSortDirection = "DESC"
            Case "DESC"
                GridViewSortDirection = "ASC"
        End Select
        Return GridViewSortDirection
    End Function
    Public Function SortDataTable(ByVal pdataTable As DataTable, ByVal isPageIndexChanging As Boolean) As DataView
        If Not pdataTable Is Nothing Then
            Dim pdataView As New DataView(pdataTable)
            If GridViewSortExpression <> String.Empty Then
                If isPageIndexChanging Then
                    pdataView.Sort = String.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection)
                Else
                    pdataView.Sort = String.Format("{0} {1}", GridViewSortExpression, GetSortDirection())
                End If
            End If
            Return pdataView
        Else
            Return New DataView()
        End If

    End Function


End Class
