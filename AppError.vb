﻿
Imports System.Collections

Imports System.Collections.Generic

<Serializable()> _
Public Class AppError

    Public Property ErrorID() As Long
        Get
            Return m_ErrorID
        End Get
        Set(ByVal value As Long)
            m_ErrorID = value
        End Set
    End Property
    Private m_ErrorID As Long
    Public Property Url() As String
        Get
            Return m_Url
        End Get
        Set(ByVal value As String)
            m_Url = value
        End Set
    End Property
    Private m_Url As String
    Public Property RequestType() As String
        Get
            Return m_RequestType
        End Get
        Set(ByVal value As String)
            m_RequestType = value
        End Set
    End Property
    Private m_RequestType As String
    Public Property UserID() As String
        Get
            Return m_UserID
        End Get
        Set(ByVal value As String)
            m_UserID = value
        End Set
    End Property
    Private m_UserID As String
    Public Property ErrorMessage() As String
        Get
            Return m_ErrorMessage
        End Get
        Set(ByVal value As String)
            m_ErrorMessage = value
        End Set
    End Property
    Private m_ErrorMessage As String
    Public Property StackTrace() As String
        Get
            Return m_StackTrace
        End Get
        Set(ByVal value As String)
            m_StackTrace = value
        End Set
    End Property
    Private m_StackTrace As String
End Class

Public Class AppErrConverter
    Public Shared Function ConvertToErrDTO(ByVal errsCH As IList(Of AppError)) As IList(Of AppErrorDTO)
        Dim errsDTO As IList(Of AppErrorDTO) = New List(Of AppErrorDTO)()

        For Each appErr As AppError In errsCH
            errsDTO.Add(ConvertToErrDTO(appErr))
        Next

        Return errsDTO
    End Function

    Public Shared Function ConvertToErrDTO(ByVal errCH As AppError) As AppErrorDTO
        Dim errDTO As New AppErrorDTO()

        errDTO.ErrorID = errCH.ErrorID
        errDTO.ErrorMessage = errCH.ErrorMessage
        errDTO.RequestType = errCH.RequestType
        errDTO.StackTrace = errCH.StackTrace
        errDTO.Url = errCH.Url
        errDTO.UserID = errCH.UserID

        Return errDTO

    End Function
End Class

Public Class ErrorUtility
    Inherits InventoryBase
    Public Sub LogError(ByRef objErr As Entities.AppError)
        If objErr Is Nothing Then
            Throw New ArgumentNullException(NameOf(objErr))
        End If

        SaveErrorLog(objErr)
    End Sub
End Class
