﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frm69_Errorlog.aspx.vb"
    Inherits="cErrorlog"   %>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>--%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%--<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">--%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Error Log Details</title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:CustomValidator ID="CustomValidator1" runat="server" Display="None"></asp:CustomValidator>
    <asp:Panel ID="pnBody" Style="z-index: 105; left: 45px; top: 130px; font-family: 'Microsoft Sans Serif';
        font-size: 8pt; position: relative;" runat="server" Width="950" Height="480">
        <table>
            <tr>
                <td>
                    <asp:Label ID="Label10" runat="server" Width="62" Height="13">Date:</asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="cboDateSearchAdjDate" runat="server" AutoPostBack="True">
                        <asp:ListItem Value="0">Equal To</asp:ListItem>
                        <asp:ListItem Value="1">Before</asp:ListItem>
                        <asp:ListItem Value="2">After</asp:ListItem>
                        <asp:ListItem Value="3">Between</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:TextBox ID="txtAdjDate" runat="server" Width="140" Height="21" TabIndex="5"></asp:TextBox>
                </td>
                <td>
                    <asp:ImageButton ID="imgBtnRceiveDateFrom" runat="server" SkinID="Calender" />
                    <cc1:CalendarExtender ID="calLotDate" runat="server" PopupButtonID="imgBtnRceiveDateFrom"
                        TargetControlID="txtAdjDate" PopupPosition="TopRight" />
                </td>
                <td>
                    <asp:Label ID="lblAdjDate2" runat="server" Width="28" Height="13">and</asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtToAdjDate" runat="server" Height="21" TabIndex="6"></asp:TextBox>
                </td>
                <td>
                    <asp:ImageButton ID="imgBtnRceiveDateTo" runat="server" SkinID="Calender" />
                    <cc1:CalendarExtender ID="calLotDate1" runat="server" PopupButtonID="imgBtnRceiveDateTo"
                        TargetControlID="txtToAdjDate" PopupPosition="TopRight" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:ImageButton ID="btnFind" runat="server" SkinID="findButton" />
                </td>
            </tr>
        </table>
        <asp:Panel ID="Panel3" runat="server" Width="870" Height="470" ScrollBars="Auto">
            <asp:GridView ID="dgvSearchResult" SkinID="gridviewSkin" runat="server" Width="870px"
                Height="470px">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:ImageButton ID="btnSelect" runat="server" Text="View" SkinID="View" CommandName="btnSelect"
                                CommandArgument='<%# Eval("ErrorID") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%-- <asp:TemplateField Visible="false">
                    <ItemTemplate>
                    <asp:Label ID="lblerrorid" runat="server" Text='<%# Eval("ErrorID") %>'></asp:Label>
                    </ItemTemplate>
                    </asp:TemplateField>--%>
                </Columns>
            </asp:GridView>
        </asp:Panel>
    </asp:Panel>
    </form>
</body>
</html>
