﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frm66_LocationMapping.aspx.vb" Inherits="frm66_LocationMapping" MasterPageFile="~/Master/Inventory.Master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style4
        {
            width: 6%;
            background-color:Gray; 
        }
        .style7
        {
            width: 51px;
             background-color:Gray; 
        }
        .style12
        {
            width: 190px;
            background-color:#d3d3d3;
            
        }
        .style14
        {
            width: 133px;
             background-color:#d3d3d3;
        }
        .style16
        {
            width: 19px;
             background-color:#d3d3d3;
        }
        .style17
        {
            width: 21px;
           background-color:#d3d3d3;
        }
        .style18
        {
            width: 2%;
        }
        .style19
        {
            width: 2%;
            background-color: Gray;
        }
        .style20
        {
            width: 21px;
        }
    </style>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
     <asp:Panel ID="PnlMessage" runat="server"  
            Style="z-index: 105; left: 37px; top: 1px; font-family: 'Microsoft Sans Serif';" 
            Width="917px" Height="410px"  >
            
        
   <table style="width: 490px;left: 37px; " align="center"   >
   <tr>
   <td  class="style19" >
   <asp:RadioButton ID="radBuilding" runat="server" GroupName="radLocation" 
           Checked="false" AutoPostBack="true" TabIndex="1"   /> 
     </td>
       
           <td class="style7">
               <asp:Label ID="lblBuilding" runat="server" style="margin-left: 0px" 
                   Text="Building" Width="66px" Font-Bold="True"></asp:Label>
           </td>
           <td class="style4">
               <asp:RadioButton ID="radAisle" runat="server" AutoPostBack="true" 
                   Checked="false" GroupName="radLocation" Width="10Px" TabIndex="2" />
           </td>
           <td class="style7">
               <asp:Label ID="lblAsile" runat="server" Text="Aisle" Width="30px" 
                   Font-Bold="True"></asp:Label>
           </td>
           <td class="style4">
               <asp:RadioButton ID="radTier" runat="server" AutoPostBack="true" 
                   Checked="false" GroupName="radLocation" Width="10Px" TabIndex="3" />
           </td>
           <td class="style7">
               <asp:Label ID="lblTier" runat="server" Text="Tier" Width="30px" 
                   Font-Bold="True"></asp:Label>
           </td>
           <td class="style4">
               <asp:RadioButton ID="radLevel" runat="server" AutoPostBack="true" 
                   Checked="false" GroupName="radLocation" Width="10Px" TabIndex="4" />
           </td>
           <td class="style7">
               <asp:Label ID="lblLevel" runat="server" Text="Level" Width="30px" 
                   Font-Bold="True"></asp:Label>
           </td>
           <tr>
               <td class="style18">
               </td>
           </tr>
           <tr>
               <td class="style18">
               </td>
           </tr>
       </tr>
    </table>
    <table align="center" style="left: 37px; ">
    <tr>
    <td class="style17" align="left">
    <asp:RadioButton ID="radAdd" Width="10Px" runat="server" Visible="false" 
            Checked="false" GroupName="radAddEdit"  AutoPostBack="true" TabIndex="5" />
    </td>
    <td class="style14">
    <asp:Label ID="lblAdd" runat="server" Visible="False" Font-Bold="True"></asp:Label>
    </td>
    <td class="style16">
    <asp:RadioButton ID="radEdit" Width="10Px" runat="server"  Checked="false" 
            Visible="false" GroupName="radAddEdit" AutoPostBack="true" TabIndex="6" />
    </td>
    <td class="style12">
    <asp:Label ID="lblEdit" runat="server" Visible="False" Font-Bold="True"></asp:Label>
    </td>
     <td class="style16">
     <asp:RadioButton ID="radDelete" Width="10Px" runat="server"  Checked="false" 
             Visible="false" GroupName="radAddEdit" AutoPostBack="true" TabIndex="7" />
    </td>
     <td class="style12">
    <asp:Label ID="lblDelete" runat="server" Visible="False" Font-Bold="True"></asp:Label>
    </td>
    </tr>
    </table>


    <table width="800px" align="center" style="left: 37px; ">
    <tr>
    <td>
    <asp:Label ID="lblBuildingdropdown" runat="server"  
            Visible="False" Font-Bold="True">
            Select Building:</asp:Label>
    </td>
    <td>
    <asp:DropDownList ID="ddlBuilding" runat="server" Visible="false" Width="180px" 
            Height="21px"  AutoPostBack="true" TabIndex="8"  ></asp:DropDownList>
    </td>
    <td>
     <asp:Label ID="lblAisleddl" runat="server"  Visible="False" 
            Font-Bold="True">
            <span style="color: Red; font-weight: bolder">*</span>Select Aisle:</asp:Label>
    </td>
    <td>
    <asp:DropDownList ID="ddlAisle" runat="server" Visible="false" Width="180px" 
            Height="21px" AutoPostBack="true" TabIndex="9"  ></asp:DropDownList>
    </td>
    </tr>
    <tr>
    <td>
     <asp:Label ID="lblTierddl" runat="server"  Visible="False" 
            Font-Bold="True">
            <span style="color: Red; font-weight: bolder">*</span>Select Tier:</asp:Label>
    </td>
    <td>
    <asp:DropDownList ID="ddlTier" runat="server" Visible="false" Width="180px" 
            Height="21px" AutoPostBack="true" TabIndex="10" ></asp:DropDownList>
    </td>
    <td>
     <asp:Label ID="lblLevelddl" runat="server"  Visible="False" 
            Font-Bold="True">
            <span style="color: Red; font-weight: bolder">*</span>Select Level:</asp:Label>
    </td>
    <td>
    <asp:DropDownList ID="ddlLevel" runat="server" Visible="false" Width="180px" 
            Height="21px" TabIndex="11"  ></asp:DropDownList>
    </td>
    </tr>
    <tr>
    <td>
    <asp:Label ID="lblLocation"  runat="server" Visible="False" 
            Font-Bold="True">
             <span style="color: Red; font-weight: bolder">*</span>Enter Here:</asp:Label>
    </td>
    <td>
    <asp:TextBox ID="txtLocation" runat="server" Visible="false" Width="175px" 
            MaxLength="50" TabIndex="12"></asp:TextBox>
    </td>
    <td>
   
   <asp:ImageButton ID="btnSave" Style="font-family: 'Trebuchet MS'; font-size: 9pt;"
                            runat="server" Text="Done" 
            ImageUrl="~/Images/save_static.png" TabIndex="13"></asp:ImageButton>
   <%-- <asp:Button ID="btnSave" Text="Save" runat="server" Visible="false"    />--%>
    </td>
     <td>
     <asp:ImageButton id="btnDelete" runat="server" Visible="false"  
             ImageUrl="~/Images/delete_static.png" TabIndex="14" />
   
     <%--<asp:Button ID="btnDelete" Text="Delete" runat="server" Visible="false"    />--%>
    </td>
    </tr>
    </table>
    <table style="left: 37px; ">
    <tr>
    <td>
      <asp:Label ID="lblEBuilding" Text="Building:" runat="server" Visible="False" 
            Font-Bold="True"></asp:Label>
    </td>
    <td>
    <asp:TextBox ID="txtEBuilding" runat="server" Visible="false" Width="180px"></asp:TextBox>
    </td>
    
    <td>
      <asp:Label ID="lblEAisle" Text="Aisle:" runat="server" Visible="False" 
            Font-Bold="True"></asp:Label>
    </td>
    <td>
    <asp:TextBox ID="txtEAisle" runat="server" Visible="false" Width="180px"></asp:TextBox>
    </td>
     <td class="style20">
      <asp:Label ID="lblETier" Text="Tier:" runat="server" Visible="False" 
             Font-Bold="True"></asp:Label>
    </td>
    <td>
    <asp:TextBox ID="txtETier" runat="server" Visible="false" Width="180px"></asp:TextBox>
    </td>
     <td>
      <asp:Label ID="lblELevel" Text="Level:" runat="server" Visible="False" 
             Font-Bold="True"></asp:Label>
    </td>
    <td>
    <asp:TextBox ID="txtELevel" runat="server" Visible="false" Width="180px"></asp:TextBox>
    </td>
    </tr>
    </table>

    <%-- <asp:GridView ID="gdvLocations" runat="server" SkinID="gridviewSkin"    
            width="800px"  Style="z-index: 135; left: 6px; top: 185px; font-family: 'Microsoft Sans Serif';
                font-size: 8pt; position: absolute;">
     <Columns>
                     <asp:CommandField ShowEditButton="True"></asp:CommandField>
                   <asp:TemplateField>
                   <ItemTemplate>
                  
                   </ItemTemplate>
                   </asp:TemplateField>  

     </Columns>
     
     </asp:GridView>
--%> 
 <asp:ImageButton ID="btnReset" runat="server" SkinID="resetButton" Style="position: absolute;
            top: 450px; left: 368px;" TabIndex="16" />
             <asp:ImageButton ID="btnCancel" Style="z-index: 102; left: 487px; top: 450px; font-family: 'Trebuchet MS';
                        font-size: 9pt; position: absolute;" runat="server" 
             Text="Cancel" ImageUrl="~/Images/cancel_static.png"
                        CausesValidation="False" TabIndex="17"></asp:ImageButton>
 <asp:CustomValidator ID="cutValidator" runat="server" Display="None"></asp:CustomValidator>
 </asp:Panel>
    </div>
</asp:Content>