<%@ Page Language="VB" CodeBehind="frm06_AddMfrPartNo.aspx.vb" AutoEventWireup="false"
    Inherits="frm06_AddMfrPartNo" MasterPageFile="~/Master/Inventory.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="pnlDetail" Style="z-index: 105; left: 40px; top: 3px; font-family: 'Microsoft Sans Serif';
        font-size: 8pt; position: relative; height: 200px;" runat="server" Width="469"
        BorderStyle="None">
        <asp:TextBox ID="txtMfrPartNo" Style="z-index: 102; left: 92px; top: 54px; font-family: 'Verdana';
            font-size: 8pt; position: absolute; width: 203px;" runat="server" 
            Height="21" TabIndex="43"></asp:TextBox>
        <asp:Label ID="Label9" Style="z-index: 103; left: 14px; top: 57px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="76" Height="13">Mfr Part No:</asp:Label>
        <asp:Label ID="Label6" Style="z-index: 104; left: 60px; top: 25px; font-family: 'Verdana';
            font-size: 8pt; position: absolute; height: 14px;" runat="server" 
            Width="30">Mfr:</asp:Label>
        <asp:DropDownList ID="cboMfrName" Style="z-index: 105; left: 92px; top: 22px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;width: 202px;" 
            runat="server">
        </asp:DropDownList>
        <asp:ImageButton ID="btnCancel" Style="z-index: 102; left: 200px; top: 83px; font-family: 'Trebuchet MS';
            font-size: 9pt; position: absolute;" runat="server" Text="Cancel" 
            ImageUrl="~/Images/cancel_static.png">
        </asp:ImageButton>
        <asp:ImageButton ID="btnDone" Style="z-index: 104; left: 91px; top: 83px; font-family: 'Trebuchet MS';
            font-size: 9pt; position: absolute;" runat="server" Text="Done" 
            ImageUrl="~/Images/save_static.png">
        </asp:ImageButton>
        <asp:CustomValidator ID="cutValidator" runat="server" Display="None"></asp:CustomValidator>
    </asp:Panel>
</asp:Content>
