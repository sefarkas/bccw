<%@ Page language="VB" Codebehind="frm55_SearchMfrs.aspx.vb" AutoEventWireup="false" Inherits="frm55_SearchMfrs" MasterPageFile="~/Master/Inventory.Master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel id="pnBody" style="z-index:105; left: 45px; top: 130px; font-family: 'Microsoft Sans Serif'; font-size:8pt; position:relative; width: 875px;" 
    runat="server" Height="500" DefaultButton="btnFind" >

<asp:Panel id="pnlSearchCriteria" SkinID="panelSearch"
            style="z-index:104; left:5px; top:10px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:absolute; width: 871px; height: 61px;" 
            runat="server" ><asp:Label id="Label9" 
        style="z-index:105; left:359px; top:11px; font-family:'Verdana'; font-size:8pt; position:absolute;" 
        runat="server" Width="19px" Height="13px" Visible="false">or</asp:Label>
<asp:DropDownList id="cboMfrName" 
        style="z-index:106; left:72px; top:8px; font-family:'Verdana'; font-size:8pt; position:absolute;" 
        runat="server" Width="280" Height="21" TabIndex="1"></asp:DropDownList>
<asp:Label id="Label6" style="z-index:107; left:7px; top:11px; font-family:'Verdana'; font-size:8pt; position:absolute; font-weight:bold;" runat="server" Width="67" Height="13">Mfr Name:</asp:Label>
<asp:TextBox id="txtMfrName" 
        style="z-index:108; left:386px; top:8px; font-family:'Verdana'; font-size:8pt; position:absolute;" 
        runat="server" Width="280" Height="21" TabIndex="2" Visible="false"></asp:TextBox>
 <asp:ImageButton ID="btnFind" runat="server"  skinid="findButton" 
            style="position:absolute; top: 43px; left: 5px; right: 852px;" 
        TabIndex="3" />
        <asp:ImageButton ID="btnCancel" runat="server" skinid="resetButton" 
            style="position:absolute; top: 43px; left: 59px; " TabIndex="4" />
        <asp:ImageButton ID="btnAddNew" runat="server" skinid="AddNewButton" 
            style="position:absolute; top: 43px; left: 163px; right: 694px;" 
        TabIndex="5" />
</asp:Panel>
<asp:Panel id="Panel3" 
    style="z-index:114; left:0px; top:79px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:absolute; width: 874px; height: 400px;" 
    runat="server" ScrollBars="Auto">
    <asp:GridView id="dgvSearchResult" SkinID="gridviewSkin"
        style="z-index:135; left:16px; top:0px; font-family:'Microsoft Sans Serif'; font-size:8pt; position:absolute;" 
        runat="server" Width="600px" AllowPaging="True"   
        OnSorting="dgvSearchResult_Sorting" AllowSorting ="True" 
           OnPageIndexChanging="dgvSearchResult_PageIndexChanging" TabIndex="6">
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton ID="btnSelect" runat="server" Text="Edit" CommandName="btnSelect" SkinID="EditButton" TabIndex="7" />
                </ItemTemplate>
                <ItemStyle Width="50px" />
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Panel>
        <asp:CustomValidator ID="cutValidator" runat="server" Display="None"></asp:CustomValidator>
       
</asp:Panel>
</asp:Content>