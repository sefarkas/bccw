﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Master/Inventory.Master" CodeBehind="Login.aspx.vb" Inherits="Login1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

  <table width="832px" border="0" cellspacing="0" cellpadding="2" align="center">

        <tr>
            <td colspan="3" align="center">
                <table class="loginBox" cellpadding="14" align="center">
                    <tr>
                        <td>
                            PLEASE SIGN IN BELOW WITH
                            <br />
                            YOUR ID AND PASSWORD
                        <br />
                        <br />                        

                        </td>

                    </tr>
                    <tr class="loginBoxInternal" align="left">
                        <td>
                            <asp:Label ID="lMessage" runat="server" Font-Size="Small"></asp:Label>
                            <br />
                            Sign in ID*<br />
                            <asp:TextBox ID="txtUserName" runat="server" Width="275"></asp:TextBox><br />
                             
                            <br />
                            Password*<br />
                            <asp:TextBox ID="txtPassword" runat="server" Width="275" TextMode="Password"></asp:TextBox><br />
                            <br />
                            <asp:ImageButton ID="btnLogin" runat="server" ImageUrl="../Images/login.png"
                                OnClick="btnLogin_Click" />
                            &nbsp;&nbsp;
                            <br />
                            <br />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Version:
                            <asp:Label ID="lblVersion" runat="server"></asp:Label>
                            <span style="FONT-FAMILY: Arial; FONT-SIZE: 10pt"><br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
                    </tr>
                </table>
                
                <table class="loginBoxLower" align="center">
                    <tr>
                        <td>
                            Need Help? Please contact IT Service Desk at 917.237.5200 or email us at itServiceDesk@dsny.nyc.gov
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
