<%@ Page Language="VB" CodeBehind="frm4_AddInvAdj.aspx.vb" AutoEventWireup="false"
    Inherits="frm4_AddInvAdj" MasterPageFile="~/Master/Inventory.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style2
        {
            width: 113px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<%--    <script type="text/javascript">
        // test auto-ready logic - call corner before DOM is ready
        $('#ContentPlaceHolder1_pnlDetail').corner();


        $(document).ready(function () {
            $('#<%=txtQtyAdj.ClientID%>').focusout(function () {

                $('#<%=txtValueAdj.ClientID%>').val($('#<%=txtUnitCost.ClientID%>').val() * $('#<%=txtQtyAdj.ClientID%>').val())

                $('#<%=txtValueAdj.ClientID%>').val(parseFloat($('#<%=txtValueAdj.ClientID%>').val()).toFixed(2)) 
              
            });
        }); 
      
        
    </script>--%>
    <asp:Panel ID="pnBody" Style="z-index: 105; left: 45px; top: 130px; font-family: 'Microsoft Sans Serif';
        font-size: 8pt; position: relative;" runat="server" Width="950" Height="370" DefaultButton="btnDone">
        <asp:Panel ID="pnlDetail" 
            
            Style="z-index: 102; left: 0px; top:0px; font-family: 'Microsoft Sans Serif';
            font-size: 8pt; position: absolute; height: 213px; width: 871px; top: 0px;" runat="server"
            SkinID="panelSearch">
            <table cellpadding="3" cellspacing="3" border="0px">
                <tr>
                    <td align="left">
                        <asp:Label ID="Label1" runat="server"  Font-Bold="true"  >
                         <span style="color: Red; font-weight: bolder">*</span>Category</asp:Label>
                        :</td>
                    <td>
                        <asp:DropDownList ID="ddlCategory" AutoPostBack="true" runat="server" Width="150"
                            Height="21" TabIndex="1">
                        </asp:DropDownList>
                    </td>
                    <td align="left">
                        <asp:Label ID="Label12" runat="server" Font-Bold="true" >
                         <span style="color: Red; font-weight: bolder">*</span>SubCategory:</asp:Label>
                    </td>
                    <td class="style2">
                        <asp:DropDownList ID="ddlSubCategory" AutoPostBack="true" runat="server" Width="158px"
                            Height="22px" TabIndex="2">
                        </asp:DropDownList>
                    </td>
                    <td align="left">
                        <asp:Label ID="lblUnitOfIssue" Text="Unit Of Issue:" Font-Bold="true"   runat="server">
                        </asp:Label>
                    </td>
                    <td>
                        <%--                    <asp:TextBox  ID="txtUnitOfissue" runat="server"  Width="90px" Height="18px"></asp:textbox>--%>
                        <asp:TextBox ID="txtUnitOfissue" runat="server" ReadOnly="true" SkinID="txtReadonly"
                            Width="90px" Height="18px" TabIndex="4"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="Label11" runat="server" Font-Bold="true" >
                         <span style="color: Red; font-weight: bolder">*</span>Item Desc:</asp:Label>
                        </td>
                    <td colspan="3">
                        <asp:DropDownList ID="ddlItem" AutoPostBack="true" runat="server" Width="403px" 
                            Height="22px" TabIndex="3">
                        </asp:DropDownList>
                    </td>
                    <td align="left">
                        <asp:Label ID="Label9" Text="Unit Cost:" Font-Bold="true"  runat="server"></asp:Label>
                    </td>
                    <td>
                        <%--      <asp:Label ID="txtUnitCost" Height="18px" runat="server" Width="90px" BorderStyle="None"></asp:Label>--%>
                        <asp:TextBox ID="txtUnitCost" runat="server" ReadOnly="true" SkinID="txtReadonly"
                            Width="90px" Height="18px" TabIndex="5"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="lblQtyAdj"  Font-Bold="true" runat="server" >
                          <span style="color: Red; font-weight: bolder">*</span>Qty Adj:</asp:Label>
                        </td>
                    <td>
                        <asp:TextBox ID="txtQtyAdj" runat="server" Width="142px" 
                            height="18px" TabIndex="8" MaxLength="4"></asp:TextBox>
                    </td>
                    <td align="left">
                        <asp:Label ID="lblCQOH" runat="server" Font-Bold="true">CQOH:</asp:Label>
                    </td>
                    <td class="style2">
                        <asp:TextBox ID="txtCQOH" runat="server"  Width="150px" SkinID="txtReadonly" 
                            ReadOnly="True" height="18px" TabIndex="9"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Label ID="Label13" runat="server" Font-Bold="true">Value Adj: </asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtValueAdj" runat="server" Height="18px" ReadOnly="true" 
                            SkinID="txtReadonly" TabIndex="7" Width="90px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="lblAdjRea" runat="server" Font-Bold="true">
                          <span style="color: Red; font-weight: bolder">*</span>Adj Reas:</asp:Label>
                    </td>
                    <td colspan="3">
                        <asp:DropDownList ID="cboAdjReas" runat="server" Height="25px" TabIndex="6" 
                            Width="403px">
                        </asp:DropDownList>
                    </td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:Label ID="Label8" Text="Comments:" Font-Bold="true" runat="server"></asp:Label>
                        </td>
                    <td colspan="3">
                        <asp:TextBox ID="txtComments" runat="server" Width="397px" TextMode="MultiLine" 
                            Height="31px" TabIndex="10"></asp:TextBox>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td colspan="6" align="left">
                        <asp:ImageButton ID="btnDone" runat="server" 
                            ImageUrl="~/Images/save_static.png" TabIndex="11">
                        </asp:ImageButton>
                        <asp:ImageButton ID="btnCancel" runat="server" 
                            ImageUrl="~/Images/cancel_static.png" TabIndex="12">
                        </asp:ImageButton>
                    </td>
                </tr>
            </table>
            <asp:CustomValidator ID="cutValidator" runat="server" Display="None"></asp:CustomValidator>
        </asp:Panel>
    </asp:Panel>
</asp:Content>
