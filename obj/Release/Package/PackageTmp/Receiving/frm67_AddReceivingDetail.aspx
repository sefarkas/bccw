﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frm67_AddReceivingDetail.aspx.vb"
    Inherits="frmAddReceivingDetail" MasterPageFile="~/Master/Inventory.Master" %>
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style4
        {
            width: 82px;
        }
        .style5
        {
            width: 97px;
        }
        .style8
        {
            width: 241px;
        }
        .style10
        {
            width: 57px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
<%--    <script type="text/javascript">
        // test auto-ready logic - call corner before DOM is ready
        $('#ContentPlaceHolder1_pnlDetail').corner();
    </script>--%>
    <asp:Panel ID="pnBody" Style="z-index: 105; left: 45px; top: 10px; font-family: 'Microsoft Sans Serif';
        font-size: 8pt; position: relative;" runat="server" Width="950" Height="370" DefaultButton="btnSave">
        <asp:Panel ID="pnlDetail" Style="z-index: 102; left: 0px; font-family: 'Microsoft Sans Serif';
            font-size: 8pt; position: absolute; height: 199px; width: 780; top: 0px;" runat="server"
            SkinID="panelSearch">
            <table border="0" style="width: 872px">
                <tr>
                    <td class="style4">
                        <asp:Label ID="Label10"  Font-Bold="true" runat="server">
                         <span style="color: Red; font-weight: bolder">*</span>Category:</asp:Label>
                    </td>
                    <td width="100px">
                        <asp:DropDownList ID="cboCategory" runat="server" Width="243px" 
                            AutoPostBack="true" Height="21px" TabIndex="1">
                        </asp:DropDownList>
                        <%--     <asp:RequiredFieldValidator ID="ReqCategory" runat="server" SkinID="RequiredValidator"
                        ValidationGroup="BtnSave" ControlToValidate="cboCategory"></asp:RequiredFieldValidator>--%>
                    </td>
                    <td class="style5">
                        <asp:Label ID="Label2" Font-Bold="true" runat="server">
                        <span style="color: Red; font-weight: bolder">*</span>Sub Category:</asp:Label>
                    
                    </td>
                    <td>
                        <asp:DropDownList ID="cboSubCategory" runat="server" Width="233px" 
                            AutoPostBack="true" Height="22px" TabIndex="2">
                        </asp:DropDownList>
                        <%--<asp:RequiredFieldValidator ID="ReqSubCategory" runat="server" SkinID="RequiredValidator"
                        ValidationGroup="BtnSave" ControlToValidate="cboSubCategory"></asp:RequiredFieldValidator>--%>
                    </td>
                </tr>
                </table>
                <table>
                <tr>
                    <td class="style4">
                        <asp:Label ID="Label5" Font-Bold="true" runat="server">
                        <span style="color: Red; font-weight: bolder">*</span>Item Desc:</asp:Label>
                    </td>
                    <td colspan="3">
                        <asp:DropDownList ID="cboDescription" Width="581px" runat="server" 
                            AutoPostBack="true" Height="22px" TabIndex="3">
                        </asp:DropDownList>
                        <%--   <asp:RequiredFieldValidator ID="ReqItemName" runat="server" SkinID="RequiredValidator"
                        ValidationGroup="BtnSave" ControlToValidate="txtDescription"></asp:RequiredFieldValidator>--%>
                    </td>
                </tr>
                 </table>
                  <table border="0" style="width: 872px">
                <tr>
                 <td class="style4">
                        <asp:Label ID="Label7" Font-Bold="true"  runat="server">Comments:</asp:Label>
                    </td>
                    <td class="style8">
                        <asp:TextBox ID="txtComments" runat="server" MaxLength="75" Width="235px"  
                            height="18px" TabIndex="4"  ></asp:TextBox>
                        <%--      <asp:RequiredFieldValidator ID="ReqDescription" runat="server" SkinID="RequiredValidator"
                        ValidationGroup="BtnSave" ControlToValidate="cboDescription"></asp:RequiredFieldValidator>--%>
                    </td>
                    
                    <td class="style10">
                        <asp:Label ID="Label3" Font-Bold="true" runat="server">Unit Size:</asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtUnitSize" ReadOnly="true" SkinID="txtReadonly" 
                            runat="server" height="18px" width="224px" TabIndex="5"></asp:TextBox>
                    </td>
                </tr>
               
                <tr>
                   <td class="style4">
                        <asp:Label ID="Label9" Font-Bold="true" runat="server">Qty On Hand:</asp:Label>
                    </td>
                    <td >
                        <asp:TextBox ID="txtQty" ReadOnly="true" runat="server" SkinID="txtReadonly" 
                            height="18px" Width="235px" TabIndex="6"></asp:TextBox>
                        <%--     <asp:RequiredFieldValidator ID="ReqQty" runat="server" SkinID="RequiredValidator"
                        ValidationGroup="BtnSave" ControlToValidate="txtUnitOfRec"></asp:RequiredFieldValidator>--%>
                    </td>
                    <td class="style10">
                        <asp:Label ID="Label1" Font-Bold="true" runat="server" Width="70px">Stock No:</asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtPartNo" ReadOnly="true" SkinID="txtReadonly" runat="server" 
                            height="18px" width="224px" TabIndex="7"></asp:TextBox>
                        <asp:HiddenField ID="hdnPartNo" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="style4">
                        <asp:Label ID="Label8" Font-Bold="true" runat="server">Date:</asp:Label>
                    </td>
                    <td class="style8">
                        <asp:TextBox ID="txtDateRec" runat="server" height="18px" width="127px" 
                            TabIndex="8"></asp:TextBox>
                                    &nbsp;<asp:ImageButton ID="DateButton" runat="server" SkinID="Calender" Height="16px" 
                            TabIndex="6" />
                        <cc1:CalendarExtender ID="calLotDate" runat="server" PopupButtonID="DateButton" PopupPosition="TopRight"
                            TargetControlID="txtDateRec" />
                    </td>
                    <td class="style10">
                        <asp:Label ID="Label4" Font-Bold="true" runat="server" Width="100px">
                        <span style="color: Red; font-weight: bolder">*</span>Qty Receive:</asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtQtyRec" runat="server" MaxLength="4" height="18px"  CausesValidation="true"
                            width="224px" TabIndex="9"></asp:TextBox>
                            <%--  <asp:RegularExpressionValidator ID="regQty" runat="server"    
                    ControlToValidate="txtQtyRec"  ErrorMessage="Qty should be Whole Numeric Number"   ForeColor="Red"  
                    ValidationExpression="\d{4}" Display="None"   ></asp:RegularExpressionValidator>--%>
                            <%--<asp:RequiredFieldValidator ID="ReqQtyRec" runat="server" SkinID="RequiredValidator"
                        ValidationGroup="BtnSave" ControlToValidate="txtQtyRec"></asp:RequiredFieldValidator>--%>
                      <%--  <asp:RegularExpressionValidator ID="regQty" runat="server" ErrorMessage="Qty should be Whole Numeric Number"  
                        ControlToValidate="txtQtyRec"  ValidationExpression="\d{4}" ValidationGroup="BtnSave"  ForeColor="Red" ></asp:RegularExpressionValidator>--%>
                    </td>
                </tr>
                <tr>
                    <td class="style4">
                        <asp:Label ID="Label14" Font-Bold="true"  runat="server">Unit Price:</asp:Label>
                    </td>
                    <td class="style8">
                        <asp:TextBox ID="txtUnitCost" 
                            runat="server" height="18px" width="235px" TabIndex="10"></asp:TextBox>
                        <%--    <asp:RequiredFieldValidator ID="ReqUnitPrice" runat="server" SkinID="RequiredValidator"
                        ValidationGroup="BtnSave" ControlToValidate="txtUnitCost"></asp:RequiredFieldValidator>--%>
                    </td>
                    <td class="style10">
                        <asp:Label ID="Label6" Font-Bold="true" runat="server">
                        <span style="color: Red; font-weight: bolder">*</span>PO No:</asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtPONo" runat="server" MaxLength="29" height="18px" 
                            width="224px" TabIndex="11"></asp:TextBox>
                        <%--     <asp:RequiredFieldValidator ID="ReqPONO" runat="server" SkinID="RequiredValidator"
                        ValidationGroup="BtnSave" ControlToValidate="txtPONo"></asp:RequiredFieldValidator>--%>
                    </td>
                </tr>
                <tr style="top:20px;">
                    <td colspan="6" align="left" >
                        <asp:ImageButton ID="btnSave" runat="server" ValidationGroup="BtnSave" SkinID="Save"
                            CausesValidation="true" TabIndex="12" />
                        <asp:CustomValidator ID="cutValidator" runat="server" Display="None"></asp:CustomValidator>
                        <asp:ImageButton ID="btnClose" runat="server" SkinID="CancelButton" 
                            TabIndex="13" />
                        <asp:ImageButton ID="btnAddNew" runat="server" ImageUrl="~/Images/add_static.png"
                            Style="font-family: 'Trebuchet MS'; font-size: 9pt;" Text="Add New" 
                            ToolTip="Parts Management" TabIndex="14" />
                        <asp:ImageButton ID="btnReset" runat="server" Style="font-family: 'Trebuchet MS';
                            font-size: 9pt; height: 20px;" SkinID="resetButton" TabIndex="15" />
                             <asp:HiddenField ID="hdnRecTransId" runat="server" />
                              <asp:HiddenField ID="hdnShopName" runat="server" />
                               <asp:HiddenField ID="hdnPartNoEdit" runat="server" />

                    </td>
                </tr>
            </table>
        </asp:Panel>
    </asp:Panel>
</asp:Content>
