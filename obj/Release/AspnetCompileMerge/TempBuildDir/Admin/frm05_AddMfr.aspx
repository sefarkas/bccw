<%@ Page Language="VB" CodeBehind="frm05_AddMfr.aspx.vb" AutoEventWireup="false"
    Inherits="frm05_AddMfr" MasterPageFile="~/Master/Inventory.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="pnlDetail" Style="z-index: 105; left: 40px; top: 3px; font-family: 'Microsoft Sans Serif';
        font-size: 8pt; position: relative; height: 262px;" runat="server" Width="469" DefaultButton="btnDone"
        BorderStyle="None">
        <asp:ImageButton ID="btnCancel" Style="z-index: 102; left: 210px; top: 43px; font-family: 'Trebuchet MS';
            font-size: 9pt; position: absolute; bottom: 299px;" runat="server" Text="Cancel" 
            ImageUrl="~/Images/cancel_static.png" TabIndex="3">
        </asp:ImageButton>
        <asp:ImageButton ID="btnDone" Style="z-index: 104; left: 101px; top: 43px; font-family: 'Trebuchet MS';
            font-size: 9pt; position: absolute;" runat="server" Text="Done" 
            ImageUrl="~/Images/save_static.png" TabIndex="2">
        </asp:ImageButton>
        <asp:TextBox ID="txtMfrName" Style="z-index: 104; left: 84px; top: 13px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="260" 
            Height="21" TabIndex="1"></asp:TextBox>
        <asp:Label ID="Label9" Style="z-index: 105; left: 6px; top: 16px; font-family: 'Verdana';
            font-size: 8pt; position: absolute; font-weight:bold; width: 76px;" 
            runat="server" Height="13">
            <span style="color: Red; font-weight: bolder">*</span>Mfr Name:</asp:Label>
        <asp:CustomValidator ID="cutValidator" runat="server" Display="None"></asp:CustomValidator>
    </asp:Panel>
</asp:Content>
