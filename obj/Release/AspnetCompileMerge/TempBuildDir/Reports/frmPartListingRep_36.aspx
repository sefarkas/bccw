<%@ Page Language="VB" CodeBehind="frmPartListingRep_36.aspx.vb" AutoEventWireup="false"
    Inherits="frmPartListingRep" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Part Listing</title>
    <meta name="GENERATOR" content="form.suite4.net">
    <meta name="CODE_LANGUAGE" content="VB">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <style type="text/css">
        #frmPartListingRep
        {
            height: 154px;
        }
    </style>
</head>
<body ms_positioning="GridLayout">
    <form id="frmPartListingRep" method="post" runat="server">
    <asp:Panel ID="pnlRepCriteria" Style="z-index: 101; left: 10px; top: 12px; font-family: 'Microsoft Sans Serif';
        font-size: 8pt; position: absolute;" runat="server" Width="415" Height="123">
        <asp:CheckBox ID="chkBasicInfo" Style="z-index: 102; left: 23px; top: 93px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="148" 
            Height="17" Text="Show Basic Info Only">
        </asp:CheckBox>
        <asp:CheckBox ID="chkGroup" Style="z-index: 103; left: 181px; top: 93px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="142" 
            Height="17" Text="Group by Item Type">
        </asp:CheckBox>
        <%--<asp:DropDownList ID="cboSubItemType" Style="z-index: 104; left: 103px; top: 62px;
            font-family: 'Verdana'; font-size: 8pt; position: absolute;" runat="server" Width="143"
            Height="21">
        </asp:DropDownList>--%>
        <fieldset id="grpFilterOn" runat="server" style="position: absolute; left: 21px;
            top: 8px; width: 144px; height: 75">
            <legend style="z-index: 105; color: black; font-family: 'Verdana'; font-size: 8pt;">
                Filter On:</legend>
            <asp:RadioButton ID="rbtItemType" Style="z-index: 106; left: 14px; top: 40px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" GroupName="FilterCriteria" Width="84" Height="17" Text="Item Type">
            </asp:RadioButton>
            <asp:RadioButton ID="rbtCategory" Style="z-index: 107; left: 14px; top: 17px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" GroupName="FilterCriteria"  runat="server" Width="78" Height="17" Text="Category">
            </asp:RadioButton>
        </fieldset>
        <%--<asp:Label ID="Label7" Style="z-index: 108; left: 32px; top: 38px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="71" Height="13">Item Type:</asp:Label>--%>
<%--        <asp:DropDownList ID="cboShopName" Style="z-index: 109; left: 103px; top: 8px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="143" Height="21" AutoPostBack="true"  EnableViewState="true" >
        </asp:DropDownList>--%>
 <%--       <asp:DropDownList ID="cboItemType" Style="z-index: 110; left: 103px; top: 35px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="143" Height="21" EnableViewState="true"  >
        </asp:DropDownList>--%>
        <%--<asp:Label ID="Label3" Style="z-index: 111; left: 6px; top: 65px; font-family: 'Verdana';
            font-size: 8pt; position: absolute; height: 13px;" runat="server" 
            Width="97">Sub Item Type:</asp:Label>--%>
        <%--<asp:Label ID="Label2" Style="z-index: 112; left: 14px; top: 11px; font-family: 'Verdana';
            font-size: 8pt; position: absolute;" runat="server" Width="89" Height="13">Select a shop:</asp:Label>--%>
        <asp:Button ID="btnPreview" runat="server" Text="Preview" Style="z-index: 134; left: 89px;
            top: 127px; position: absolute; width: 62px;" runat="server" Height="21" />
        <asp:Button ID="btnClose" runat="server" Text="Close" Style="z-index: 134; left: 239px;
            top: 127px; position: absolute; width: 62px;" runat="server" Height="21" />
    </asp:Panel>
    </form>
</body>
</html>
