﻿<%@ Page Language="vb" AutoEventWireup="false" Theme="InvSkin" CodeBehind="frm64_ItemLocationSearch.aspx.vb"
    Inherits="frm64_ItemLocationSearch" MasterPageFile="~/Master/Inventory.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style2
        {
            width: 44px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<%--     <script type="text/javascript">
         // test auto-ready logic - call corner before DOM is ready
         $('#ContentPlaceHolder1_PnlHeader').corner();
    </script>--%>
   
    <asp:Panel ID="pnBody" runat="server" Style="z-index: 105; left: 37px; top: 1px; font-family: 'Microsoft Sans Serif';
        font-size: 8pt; position: relative; Width:890px; Height: 430px"
        HorizontalAlign="Center">

        <asp:Panel ID="PnlHeader" runat="server"  Width="871px" Height="120px">
                <table cellpadding=".75" cellspacing="1.5" border="0px" style="width: 99%">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblCategory" runat="server" Text="Category:" Font-Bold="True"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlCategory" runat="server" Width="200px" AutoPostBack="true"
                                OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged" TabIndex="1">
                            </asp:DropDownList>
                        </td>
                        <td style="width: 120px">
                        </td>
                        <td align="left">
                            <asp:Label ID="lblBuilding" runat="server" Text="Building:" Font-Bold="True"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlBuilding" runat="server" AutoPostBack="true" 
                                OnSelectedIndexChanged="ddlBuilding_SelectedIndexChanged" Height="22px" 
                                Width="179px" TabIndex="7">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblSubCategory" runat="server" Text="Sub Category:" 
                                Font-Bold="True"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlSubCategory" runat="server" Width="200px" AutoPostBack="true"
                                Height="22px" TabIndex="2">
                            </asp:DropDownList>
                        </td>
                        <td>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblAisle" runat="server" Text="Aisle:" Font-Bold="True"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlAisle" runat="server" AutoPostBack="true" Width="179px"
                                Height="22px" TabIndex="8">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">
                            <asp:CheckBox ID="chkItem" runat="server" Text="Items without Location:" 
                                Font-Bold="true" TabIndex="3" />
                        </td>
                        <td>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblTier" runat="server" Text="Tier:" Font-Bold="True"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlTier" runat="server" AutoPostBack="true" Width="179px" 
                                Height="22px" TabIndex="9">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%--   <asp:Button ID="btnGo" Text="Go" runat="server" Width="55px" 
                            Style="position: relative; top: 30px; left: 150px;"/>--%>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblLevel" runat="server" Text="Level:" Font-Bold="True"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlLevel" runat="server" AutoPostBack="true" Width="179px"
                                Height="22px" TabIndex="10">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="left">
                            <asp:ImageButton ID="ImgGo" ImageUrl="~\images\go_static.png" runat="server" 
                                TabIndex="4" />
                        </td>
                        <td>
                        </td>
                        <td colspan="2" align="left">
                            <asp:ImageButton ID="btnfind" ImageUrl="~\images\go_static.png" runat="server" 
                                TabIndex="11" />
                            <%--<asp:Button ID="BtnAddNewLocation" runat="server" Text="Add New Location" />--%>
                            <asp:ImageButton ID="BtnAddNewLocation" runat="server" 
                                ImageUrl="~\images\add_loc_static.png" TabIndex="12" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        <asp:Panel ID="Panel3" Style="z-index: 105; left: 0px; top: 180px; font-family: 'Microsoft Sans Serif';
            font-size: 8pt; position: absolute; width: 870px; height: 340px;" runat="server"
            ScrollBars="Auto">
               <table border="0px">
                   <tr>
                       <td colspan="3">
                           &nbsp;
                       </td>
                   </tr>

                   <tr>
                       <td colspan="3">
                           &nbsp;
                       </td>
                   </tr>
                   <tr>
                       <td colspan="3">
                           &nbsp;
                       </td>
                   </tr>
                   <tr>
                       <td colspan="3">
                           &nbsp;
                       </td>
                   </tr>
                   <tr>
                       <td colspan="3">
                           &nbsp;
                       </td>
                   </tr>
                   <tr>
                       <td colspan="3">
                            <asp:Label ID="lblMsg" runat="server" Font-Bold="True" ForeColor="Red" Style="position: absolute;
                                top: 0px; left: 256px; width: 306px;" Visible="False"></asp:Label>
                       </td>
                   </tr>
                    <tr>
                        <td>&nbsp;
                        </td>
                        <td class="style2">&nbsp;
                        </td>
                        <td>
                            Click the [+] to display item(s).<br />
                            Click the
                            <asp:ImageButton ID="ImgBtnDelete" runat="server" ImageUrl="~\images\btn_Del.JPG" />
                            icon in RHS to Un-assign Item(s) from Location.
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td style="margin-left: 10px; align-items:flex-start">
                            <asp:Panel ID="pnlGdvItem" runat="server" ScrollBars="Vertical" Height="230px" Width="325px"
                                Style="margin-right: 30px;">
                                <asp:GridView ID="gdvItem" runat="server" AutoGenerateColumns="False" SkinID="gridviewSkin1"
                                     Width="325px" Height="210px" DataKeyNames="Item"   OnRowCommand="gdvItem_RowCommand"
                                    AllowSorting="true" AllowPaging="true"  OnSorting="gdvItem_Sorting" 
                                    OnPageIndexChanging="gdvItem_PageIndexChanging" TabIndex="5">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-Width="30px">
                                            <ItemTemplate>
                                                <%--<asp:LinkButton ID="imgBtnRefresh" CommandName="btn" Width="25px" ForeColor="Black"
                                                    runat="server" CommandArgument='<%#  Container.DataItemIndex + "," +Eval("Id").ToString() %>' />--%>
                                                        <asp:LinkButton ID="imgBtnRefresh" CommandName="btn" Width="25px" ForeColor="Black"
                                                    runat="server" CommandArgument='<%# Eval("Id").ToString() %>' />
                                            </ItemTemplate>
                                            <ItemStyle Width="30px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="" ControlStyle-Width="10Px">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkItem" Checked="false" runat="server"  TabIndex="6"/>
                                                &nbsp;
                                            </ItemTemplate>
                                            <ControlStyle Width="10px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lblItemid" runat="server" Text='<%# Eval("Id") %>' />
                                                <asp:Label ID="lblLMId" runat="server" Text='<%# Eval("Location") %>' />
                                                <%-- <asp:Label ID="lblLMId" runat="server" Text='<%# Eval("Location") %>' />--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Item" HeaderText="Item" SortExpression="Item" />
                                        <%-- <asp:BoundField DataField="Location" />--%>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                </td></tr>
                                                <tr>
                                                    <td colspan="100%">
                                                        <div id="div<%#  Container.DataItemIndex  %>">
                                                            <asp:GridView ID="gvChildOne" runat="server" Width="100%" AutoGenerateColumns="false"
                                                                Visible="false">
                                                                <Columns>
                                                                    <asp:BoundField DataField="Location" HeaderText="Location">
                                                                        <%--  <HeaderStyle CssClass="dashHeaderTxt" BackColor="Gray" ForeColor="White" Font-Underline="False"
                                                                                                        Wrap="False" Width="150px" />
                                                                                                    <ItemStyle CssClass="dashLineCenter" Width="150px" />--%>
                                                                    </asp:BoundField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </asp:Panel>
                        </td>
                        <td style="margin-left: 10px; margin-right: 50px;" class="style2">
                            <%--<asp:Button ID="btnAddtoLocation" Text="Assign To Location" runat="server" 
                    Width="121px"  />--%>
                            <asp:ImageButton ID="btnAddtoLocation" runat="server" SkinID="AssignToLocation" 
                                TabIndex="15" />
                        </td>
                        <td style="margin-left: 10px;">
                            <asp:Panel ID="pnlLocation" runat="server" ScrollBars="Vertical" Height="210px" Width="300px">
                                <asp:GridView ID="gdvLocation" runat="server" AutoGenerateColumns="False" SkinID="gridviewSkin1"
                                    BorderWidth="0px" OnRowCommand="gdvLocation_RowCommand" 
                                    AllowSorting="true" AllowPaging="true" Width="300px" 
                                    OnSorting="gdvLocation_Sorting" 
                                    OnPageIndexChanging="gdvLocation_PageIndexChanging" TabIndex="13">
                                    <Columns>
                                        <asp:TemplateField ControlStyle-Width="35px">
                                            <ItemTemplate>
                                               <%-- <asp:LinkButton ID="imgBtnDelete" runat="server" CommandArgument="<%#  Container.DataItemIndex  %>"
                                                    CommandName="btn" ForeColor="Black" />--%>
                                                    <asp:LinkButton ID="imgBtnDelete" runat="server" CommandArgument='<%# Eval("ILMId").ToString()  %>'
                                                    CommandName="btn" ForeColor="Black" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="   " >
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkItem" TabIndex="14" runat="server" Checked="false" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLAid" runat="server" Text='<%# Eval("LAid") %>' />
                                                <asp:Label ID="lblLBid" runat="server" Text='<%# Eval("LBid") %>' />
                                                <asp:Label ID="lblLTid" runat="server" Text='<%# Eval("LTid") %>' />
                                                <asp:Label ID="lblLLid" runat="server" Text='<%# Eval("LLid") %>' />
                                                <%--                            <asp:Label ID="lblItemId" runat="server" Text='<%# Eval("itemId") %>' />--%>
                                                <asp:Label ID="lblLMId" runat="server" Text='<%# Eval("ILMId") %>' />
                                                <asp:Label ID="lblLMidSave" runat="server" Text='<%# Eval("LMid") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--          <asp:BoundField DataField="LAid" Visible="false"></asp:BoundField>
                            <asp:BoundField DataField="LBid" Visible="false"></asp:BoundField>
                            <asp:BoundField DataField="LTid" Visible="false"></asp:BoundField>
                            <asp:BoundField DataField="LLid" Visible="false"></asp:BoundField>--%>
                                        <asp:BoundField DataField="Location1" HeaderText="Location" SortExpression="Location1"  ControlStyle-Width="220px" />
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <tr>
                                                    <td colspan="100%">
                                                        <div id="div<%#  Container.DataItemIndex  %>">
                                                            <asp:GridView ID="gvChild" runat="server" AutoGenerateColumns="false" OnRowCommand="gvChild_RowCommand"
                                                                Visible="false" Width="100%">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-Width="30px">
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="ImgBtnDelete" runat="server" CommandArgument='<%#  Eval("id")  %>'
                                                                                CommandName="DeleteItem" ImageUrl="~\images\btn_Del.JPG" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="ItemType" HeaderText="Item Name" />
                                                                    <asp:TemplateField Visible="false">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblItemid" runat="server" Text='<%# Eval("ItemId") %>' />
                                                                            <asp:Label ID="lblid" runat="server" Text='<%# Eval("id") %>' />
                                                                            <asp:Label ID="lblLocationMappingId" runat="server" Text='<%# Eval("LocationMappingId") %>' />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
        </asp:Panel>
 
    </asp:Panel>
</asp:Content>
