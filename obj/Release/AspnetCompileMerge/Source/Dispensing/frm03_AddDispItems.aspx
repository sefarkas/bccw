<%@ Page Language="VB" CodeBehind="frm03_AddDispItems.aspx.vb" AutoEventWireup="false"
    Inherits="frmAddDispItems_3" MasterPageFile="~/Master/Inventory.Master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style24
        {
            width: 206px;
        }
        .style25
        {
            width: 97px;
        }
        .style28
        {
            width: 207px;
        }
        .style32
        {
            width: 84px;
        }
        .style33
        {
            width: 85px;
        }
        .style34
        {
            width: 79px;
        }
        .style35
        {
            width: 212px;
        }
        .style36
        {
            width: 98px;
        }
        .style37
        {
            width: 88px;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<%--    <script type="text/javascript">
        // test auto-ready logic - call corner before DOM is ready
        $('#ContentPlaceHolder1_pnlDetail').corner();
        //$('#ContentPlaceHolder1_Panel1').corner();
    </script>--%>
    <asp:Panel ID="Panel1" Style="z-index: 105; left: 45px; top: 10px; font-family: 'Microsoft Sans Serif';
        font-size: 8pt; position: relative; height: 468px; width: 950px;" runat="server" DefaultButton="btnDone"
        BorderStyle="None">
        <asp:Panel ID="pnlDetail" SkinID="panelSearch" Style="font-family: 'Microsoft Sans Serif';
            font-size: 8pt;  " runat="server" Width="871px"
            Height="155px">
            <table border="0px" >
            <tr>
            <td  align="left" class="style33"  >
                        <asp:Label ID="CategoryLabel" Style="font-family: 'Verdana'; font-size: 8pt; width: 31px;"
                            runat="server" Font-Bold="true">
                        <span style="color: Red; font-weight: bolder">*</span>Category:</asp:Label>
                    </td>
                    <td   >
                    
                        <asp:DropDownList ID="cboCategory" runat="server" Height="21px" Width="205px"  
                            AutoPostBack="True" style="margin-left: 2px" TabIndex="1" >
                        </asp:DropDownList>
                    </td>
                    <td   >
                        <asp:Label ID="SubCategoryLabel" Style="font-family: 'Verdana'; font-size: 8pt; width: 31px;"
                            runat="server" Font-Bold="true">
                        <span style="color: Red; font-weight: bolder">*</span>Sub Category:</asp:Label>
                    </td>
                    <td  >
                      
                        <asp:DropDownList ID="cboSubCategory" runat="server"  Width="205px" 
                            AutoPostBack="True" Height="22px" TabIndex="2">
                        </asp:DropDownList>
                    </td>
                     <td class="style33"     >
                    <asp:Label ID="lblPreloc"  Style="font-family: 'Verdana'; font-size: 8pt; " 
                     Text="Dispense To:" runat="server" Font-Bold="True"></asp:Label>
                        
                    </td>
                    <td   >
                    
                        <asp:DropDownList ID="PrefLocDropDownList" runat="server" Height="22px" 
                            Width="137px" style="margin-left: 6px" TabIndex="4">
                        </asp:DropDownList>
                    </td>
            </tr>
            </table>
          
          
            <table border="0px">
              <tr>
             <td align="left" class="style32" >
                        <asp:Label ID="ItemNameLabel" Style="font-family: 'Verdana'; font-size: 8pt; width: 26px;"
                            runat="server" Font-Bold="true">
                        <span style="color: Red; font-weight: bolder">*</span>Item Desc :</asp:Label>
                        </td>
                    <td >
                      
                        <asp:DropDownList ID="cboItemName" runat="server" AutoPostBack="true"  
                            Width="517px" Height="22px" style="margin-left: 3px" TabIndex="3">
                        </asp:DropDownList>
                    </td>
                     <td class="style33" align="left">
                       <asp:Label ID="Label1"  runat="server" Font-Bold="True">Date:</asp:Label>

                    </td>
                    <td >
                       
                        <asp:TextBox ID="txtDate" runat="server"  
                            style="margin-left: 5px; width: 82px;" height="20px" TabIndex="5" 
                            ></asp:TextBox>
                        <asp:ImageButton ID="DateButton" runat="server" SkinID="Calender" Height="16px" 
                            TabIndex="6" />
                        <cc1:CalendarExtender ID="calLotDate" runat="server" PopupButtonID="DateButton" PopupPosition="TopRight"
                            TargetControlID="txtDate" />
                        
                        <asp:RegularExpressionValidator ID="StartDateRegExp" runat="server" Display="None"
                            ErrorMessage="Please enter valid date." ControlToValidate="txtDate"
                            ValidationExpression="((^(10|12|0?[13578])([/])(3[01]|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(11|0?[469])([/])(30|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(2[0-8]|1[0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(29)([/])([2468][048]00)$)|(^(0?2)([/])(29)([/])([3579][26]00)$)|(^(0?2)([/])(29)([/])([1][89][0][48])$)|(^(0?2)([/])(29)([/])([2-9][0-9][0][48])$)|(^(0?2)([/])(29)([/])([1][89][2468][048])$)|(^(0?2)([/])(29)([/])([2-9][0-9][2468][048])$)|(^(0?2)([/])(29)([/])([1][89][13579][26])$)|(^(0?2)([/])(29)([/])([2-9][0-9][13579][26])$))">
                        </asp:RegularExpressionValidator>
                    </td>
            </tr>
            </table>
            <table border="0px">
            <tr>
            <td class="style33" >
                        <asp:Label ID="QtyLabel" Style="font-family: 'Verdana'; font-size: 8pt; width: 31px;"
                            runat="server" Font-Bold="true">
                        Qty On Hand:</asp:Label>
                    </td>
                    <td  align="left" class="style24">
                    <asp:TextBox ID="QuantityTextBox" runat="server"  ReadOnly="true"
                                        Width="195px" Height="20px" BackColor="White" SkinID="txtReadonly" 
                            style="margin-left: 1px" TabIndex="7" ></asp:TextBox>
                    </td>
                     <td class="style25" align="left">
                       <asp:Label ID="Label2" Style="font-family: 'Verdana'; font-size: 8pt; width: 31px;"
                            runat="server" Font-Bold="true">   &nbsp;&nbsp;Stock No:</asp:Label>
                                   
                                </td>
                                <td class="style28" align="left">
                                    <asp:TextBox ID="StockNoTextBox"  SkinID="txtReadonly"  runat="server" 
                                        ReadOnly="true"   Width="135px" Height="20px" TabIndex="8"></asp:TextBox>
                                </td>
                                <td class="style37">
                                 <asp:Label ID="Label3" Style="font-family: 'Verdana'; font-size: 8pt; width: 31px;"
                            runat="server" Font-Bold="true">     Unit Size:</asp:Label>
                                  
                                </td>
                                <td>
                                  <asp:TextBox ID="UnitSizeTextBox" runat="server" SkinID="txtReadonly"  
                                        ReadOnly="true" Width="79px" Height="20px" style="margin-left: 3px" 
                                        TabIndex="9"></asp:TextBox>
                               
                                </td>
            </tr>
           
            </table>
            <table border="0">
           
                     
                         <asp:Label ID="Label5" Style="font-family: 'Verdana'; font-size: 8pt; width: 31px;"
                            runat="server" Font-Bold="true" Visible="false">     Item Description:</asp:Label>
                     
                       
                        <asp:TextBox ID="txtDescription2" Rows="3" TextMode="MultiLine" runat="server" TabIndex="6"
                            ReadOnly="true"  SkinID="txtReadonly"  Visible="false"  Width="500px"></asp:TextBox>
                      
                        
               
                
                            <tr>

                             <td align="left" class="style34" >
                       
                         <asp:Label ID="Label4" Style="font-family: 'Verdana'; font-size: 8pt; width: 31px;"
                            runat="server" Font-Bold="true">     Slip No:</asp:Label>
                    </td>
                    <td align="left" class="style35">
                        &nbsp;
                        <asp:TextBox ID="txtSlipNo" runat="server" Width="196px" 
                            style="margin-left: 0px" height="20px" TabIndex="10"></asp:TextBox>
                    </td>
                              <td align="left"  style=" font-weight:bold;" class="style36">

                         <span style="color: Red; font-weight: bolder">* </span>Qty Dispensed:
                    </td>
                    <td align="left">
                     
                        <asp:TextBox ID="txtQtyDisp" runat="server" 
                            MaxLength="4" Width="135px" height="20px" TabIndex="11" ></asp:TextBox>
                    </td>
                            </tr>
            
            </table>
            
                
                    <asp:HiddenField ID="DispTransIDHidden" runat="server" />
                                    <asp:HiddenField ID="DispItemIDHidden" runat="server" />
                    
                       <%-- <asp:Label ID="LocationLabel" Style="font-family: 'Verdana'; font-size: 8pt; width: 31px;"
                            runat="server"><span style="color:Red; font-weight:bolder">*</span>LOCATION:</asp:Label>--%>
                   
                    
                      <%--  <asp:DropDownList ID="cboLocation" runat="server" Width="130" Visible="false">
                        </asp:DropDownList>--%>
                 
             
               <table style=" top:30px;">
                <tr>
                    <td colspan="4" style=" top:5px;">
                        <asp:ImageButton ID="btnDone" Style="font-family: 'Trebuchet MS'; font-size: 9pt;"
                            runat="server" Text="Done" ImageUrl="~/Images/save_static.png" 
                            TabIndex="12"></asp:ImageButton>
                        <asp:ImageButton ID="btnCancel" Style="font-family: 'Trebuchet MS'; font-size: 9pt;
                            height: 20px;" runat="server" Text="Cancel" 
                            ImageUrl="~/Images/cancel_static.png" TabIndex="13">
                        </asp:ImageButton>
                        <asp:ImageButton ID="btnReset" runat="server" Style="font-family: 'Trebuchet MS';
                            font-size: 9pt; " SkinID="resetButton" Width="100px" Height="20px" 
                            TabIndex="14" />
                        <asp:ImageButton ID="btnAddNew" Style="font-family: 'Trebuchet MS'; font-size: 9pt; left:15px"
                            runat="server" ImageUrl="~/Images/newslip_static.png" 
                            ToolTip="Click here for New Requisition / Slip" TabIndex="15"></asp:ImageButton>
                    </td>
                </tr>
            </table>
            <asp:CustomValidator ID="cutValidator" runat="server" Display="None"></asp:CustomValidator>
        </asp:Panel>
    
    
                <asp:Panel ID="Panel3" Style="z-index: 105;  top: 10px; font-family: 'Microsoft Sans Serif';
        font-size: 8pt; position: relative; height: 300px; width: 871px;" runat="server"
                   ScrollBars="Vertical"   >
                    <asp:GridView ID="DispenseGridView" SkinID="gridviewSkin"  runat="server" 
                        Width="871px" AutoGenerateColumns="false" TabIndex="16">
                        <Columns>
                            <asp:BoundField DataField="Category" HeaderText="Category" />
                            <asp:BoundField DataField="SubCategory" HeaderText="Sub Category" />
                            <asp:BoundField DataField="Description1" HeaderText="Description" />
                            <asp:BoundField DataField="PartNo" HeaderText="Part No" />
                            <asp:BoundField DataField="StockNo" HeaderText="Stock No" />
                            <asp:BoundField DataField="QtyDisp" HeaderText="Qty Dispensed" />
                            <asp:BoundField DataField="UnitofRec" HeaderText="Unit Size" />
                            <asp:BoundField DataField="CQOH" HeaderText="CQOH" />
                        </Columns>
                    </asp:GridView>
                </asp:Panel>
         </asp:Panel>   
</asp:Content>
