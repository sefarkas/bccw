<%@ Page Language="VB" Theme="InvSkin" CodeBehind="frm53_SearchDispensing.aspx.vb"
    AutoEventWireup="false" Inherits="frm53_SearchDispensing" MasterPageFile="~/Master/Inventory.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style2
        {
            width: 82px;
        }
        .style3
        {
            width: 513px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<%--    <script type="text/javascript">
        // test auto-ready logic - call corner before DOM is ready
        $('#ContentPlaceHolder1_pnlSearchCriteria').corner();

        function StopAction() {
            alert('This is under construction');
            return false;
        }
    </script>--%>
    <asp:Panel ID="pnBody" Style="z-index: 105; left: 45px; top: 150px; font-family: 'Microsoft Sans Serif';
        font-size: 8pt; position: relative;" runat="server" Width="950" Height="550"
        DefaultButton="btnFind">
        <asp:Panel ID="Panel3" Style="z-index: 105; left: 0px; top: 135px; font-family: 'Microsoft Sans Serif';
            font-size: 8pt; position: absolute; width: 870px; height: 390px;" runat="server"
            ScrollBars="Auto">
            <asp:GridView ID="dgvSearchResult" SkinID="gridviewSkin" Style="z-index: 135; left: 6px;
                top: 0px; font-family: 'Microsoft Sans Serif'; font-size: 8pt; position: absolute; width: 880px;"
                runat="server" AutoGenerateColumns="false"    HeaderStyle-Height="15px"
                CellSpacing="0" AllowSorting="true"  AllowPaging="true" TabIndex="15" >                      
  
                <Columns>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="btnSelect" runat="server"  TabIndex="16" Text="Edit" ImageUrl="~/Images/edit_static.png"
                                CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.DispItemID", "")%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="StockNo" HeaderText="Stock No"  SortExpression="StockNo" />
                    <asp:BoundField DataField="Category" HeaderText="Category"  SortExpression="Category"/>
                    <asp:BoundField DataField="SubCategory" HeaderText="Sub Category"  SortExpression="SubCategory"/>
                    <asp:BoundField DataField="Description" HeaderText="Description"  SortExpression="Description"/>
                    <asp:BoundField DataField="PRELoc" HeaderText="Location Dispensed To"  SortExpression="PRELoc"/>
                    <asp:BoundField DataField="QtyDisp" HeaderText="Qty Disp"  SortExpression="QtyDisp"/>
                    <asp:BoundField DataField="UnitofRec" HeaderText="Unit Size"  SortExpression="UnitofRec"/>
                    <asp:BoundField DataField="AddDate" HeaderText="Date Disp"  SortExpression="AddDate" />
                    <asp:BoundField DataField="SlipNo" HeaderText="Slip No" ItemStyle-Width="10%"   SortExpression="SlipNo" />
                </Columns>
            </asp:GridView>
        </asp:Panel>
        <asp:Panel ID="pnlSearchCriteria" Style="z-index: 108; left: 0px; top: 2px; font-family: 'Microsoft Sans Serif';
            font-size: 8pt; position: absolute; height: 130px; width: 870px;" runat="server"
            SkinID="panelSearch">
            <table border="0px" style="width: 89%" >
                <tr>
                    <td>
                        <asp:Label ID="lblCategory" runat="server" Font-Bold="True" Width="65px">Category:</asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="cboCategory" runat="server" Width="205px" 
                            AutoPostBack="True" Height="21px" style="margin-left: 5px" TabIndex="1">
                        </asp:DropDownList>
                    </td>
                    <td >
                        <asp:Label ID="lblSubCategory" Font-Bold="true" runat="server" Width="80px">Sub Category:</asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="cboSubCategory" runat="server" Width="205px" 
                            AutoPostBack="True" Height="22px" TabIndex="2">
                        </asp:DropDownList>
                    </td>
                    <td class="style2">
                        <asp:Label ID="Label4" runat="server" Width="84px" Height="13px" Font-Bold="True"
                            style="margin-left: 0px">Dispense To:</asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="cboPreloc" runat="server" Height="22px" Width="130px" 
                            TabIndex="4">
                        </asp:DropDownList>
                    </td>
                </tr>
                </table>
            <table border="0px" style="width: 89%" >
                <tr>
                    <td>
                        <asp:Label ID="Label1" Font-Bold="True" runat="server" Width="65px">Item Desc:</asp:Label>
                    </td>
                    <td style="width: 49%" >
                        <asp:DropDownList ID="cboItemName" runat="server" Width="498px" 
                            AutoPostBack="true" Height="22px" style="margin-left: 5px" TabIndex="3">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:Label ID="Label6" runat="server"  Font-Bold="True" Width="85px" 
                            style="margin-left: 0px" Height="16px">Stock No:</asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtStockNo" runat="server" Height="17px" Width="124px" 
                            style="margin-left: 0px" TabIndex="5"></asp:TextBox>
                    </td>
                </tr>
                </table> 
            <table border="0px"  >
                <tr>
                    <td>
                        <asp:Label ID="Label9" runat="server" Font-Bold="True" Height="16px" 
                            style="margin-left: 0px" Width="65px">Slip No:</asp:Label>
                    </td>
                    <td >
                        <asp:TextBox ID="txtSlipno" runat="server" style="margin-left: 5px" 
                            Width="197px" height="17px" TabIndex="6"></asp:TextBox>
                    </td>
                    <td >
                        <asp:Label ID="Label15" runat="server" Font-Bold="true" style="margin-left: 5px" >Disp Date:</asp:Label>
                    </td>
                    <td class="style3">
                        <asp:DropDownList ID="cboDateSearchDispDate" runat="server" Width="100" 
                            Height="22px" style="margin-left: 20px"
                            AutoPostBack="True" TabIndex="7">
                            <asp:ListItem Value=""></asp:ListItem>
                            <asp:ListItem Value="0">Equal To</asp:ListItem>
                            <asp:ListItem Value="1">Before</asp:ListItem>
                            <asp:ListItem Value="2">After</asp:ListItem>
                            <asp:ListItem Value="3">Between</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtStartDate" Width="70px" runat="server" height="17px" 
                            TabIndex="8"></asp:TextBox>
                        <asp:ImageButton ID="StartDateImageButton" runat="server" SkinID="Calender" 
                            TabIndex="9" />
                        <cc1:CalendarExtender ID="calLotDate" runat="server" PopupButtonID="StartDateImageButton"
                            TargetControlID="txtStartDate" PopupPosition="TopRight"  />
                        <asp:RegularExpressionValidator ID="StartDateRegExp" runat="server" Display="None"
                            ErrorMessage="Invalid  Date." ControlToValidate="txtStartDate" ValidationExpression="((^(10|12|0?[13578])([/])(3[01]|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(11|0?[469])([/])(30|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(2[0-8]|1[0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(29)([/])([2468][048]00)$)|(^(0?2)([/])(29)([/])([3579][26]00)$)|(^(0?2)([/])(29)([/])([1][89][0][48])$)|(^(0?2)([/])(29)([/])([2-9][0-9][0][48])$)|(^(0?2)([/])(29)([/])([1][89][2468][048])$)|(^(0?2)([/])(29)([/])([2-9][0-9][2468][048])$)|(^(0?2)([/])(29)([/])([1][89][13579][26])$)|(^(0?2)([/])(29)([/])([2-9][0-9][13579][26])$))">
                        </asp:RegularExpressionValidator>
                        <asp:Label ID="lblDispDate" runat="server" Font-Bold="true" Visible="false">and</asp:Label>
                        <asp:TextBox ID="txtToDate" Width="70px" runat="server" Visible="false" 
                            height="17px" TabIndex="10"></asp:TextBox>
                        <asp:ImageButton ID="ToDateImageButton" runat="server" SkinID="Calender" 
                            Visible="false" TabIndex="11" />
                        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" PopupButtonID="ToDateImageButton"
                            TargetControlID="txtToDate" PopupPosition="TopRight" />
                        <asp:RegularExpressionValidator ID="ToDateRegExp" runat="server" Display="None" ErrorMessage="Invalid To Date."
                            ControlToValidate="txtToDate" ValidationExpression="((^(10|12|0?[13578])([/])(3[01]|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(11|0?[469])([/])(30|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(2[0-8]|1[0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(29)([/])([2468][048]00)$)|(^(0?2)([/])(29)([/])([3579][26]00)$)|(^(0?2)([/])(29)([/])([1][89][0][48])$)|(^(0?2)([/])(29)([/])([2-9][0-9][0][48])$)|(^(0?2)([/])(29)([/])([1][89][2468][048])$)|(^(0?2)([/])(29)([/])([2-9][0-9][2468][048])$)|(^(0?2)([/])(29)([/])([1][89][13579][26])$)|(^(0?2)([/])(29)([/])([2-9][0-9][13579][26])$))">
                        </asp:RegularExpressionValidator>
                    </td>
                </tr>
            </table>
            <asp:CustomValidator ID="cutValidator" runat="server" Display="None"></asp:CustomValidator>
            <asp:ImageButton ID="btnReset" runat="server" SkinID="resetButton" Style="position: absolute;
                top: 95px; left: 58px; right: 878px;" TabIndex="13" />
            <asp:ImageButton ID="btnFind" runat="server" SkinID="findButton" Style="position: absolute;
                top: 95px; left: 4px; right: 932px;" TabIndex="12" />
            <asp:ImageButton ID="btnAddNew" Style="z-index: 102; left: 163px; top: 95px; font-family: 'Trebuchet MS';
                font-size: 9pt; position: absolute;" runat="server" ImageUrl="~/Images/issue_static.png"
                ToolTip="Click to Dispense Item(s)" TabIndex="14"></asp:ImageButton>
        </asp:Panel>

    </asp:Panel>
</asp:Content>
