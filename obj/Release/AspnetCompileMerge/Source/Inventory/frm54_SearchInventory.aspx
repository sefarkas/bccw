<%@ Page Language="VB" Theme="InvSkin" CodeBehind="frm54_SearchInventory.aspx.vb"
    AutoEventWireup="false" Inherits="frm54_SearchInventory" MasterPageFile="~/Master/Inventory.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
<%--    <script type="text/javascript">
        // test auto-ready logic - call corner before DOM is ready
        $('#ContentPlaceHolder1_pnlSearchCriteria').corner();
    </script>--%>
    <asp:Panel ID="pnBody" Style="z-index: 105; left: 45px; top: 130px; font-family: 'Microsoft Sans Serif';
        font-size: 8pt; position: relative; height: 500px;" runat="server" Width="890" DefaultButton="btnFind">
        
        
        <asp:Panel ID="pnlSearchCriteria" SkinID="panelSearch" Style="z-index: 105; left: 0px;
            top: 3px; font-family: 'Microsoft Sans Serif'; font-size: 8pt; position: absolute;"
            runat="server" Width="870" Height="90">
            <asp:ImageButton ID="btnFind" runat="server" SkinID="findButton" Style="position: absolute;
            top: 70px; left: 3px;" TabIndex="4" />
            <asp:ImageButton ID="btnMfr" Style="z-index: 103; left: 364px; top: 70px; font-family: 'Trebuchet MS';
            font-size: 9pt; position: absolute;" runat="server" Text="Manufacturers" 
            ImageUrl="~/Images/Manufacturer_static.png" 
            ToolTip="Click here to add new manufacturers." TabIndex="8">
        </asp:ImageButton>
            <asp:ImageButton ID="btnAddNew" Style="z-index: 102; left: 261px; top: 70px; font-family: 'Trebuchet MS';
            font-size: 9pt; position: absolute;" runat="server" Text="Add New" 
            SkinID="ImgbtnAddNewPart" ToolTip="Parts Management" TabIndex="7">
        </asp:ImageButton>
             <asp:ImageButton ID="btnReset" runat="server" SkinID="resetButton" Style="position: absolute;
            top: 70px; left: 55px;right: 801px;" TabIndex="5" />
        <asp:ImageButton ID="btnAdmin" runat="server" SkinID="Admin" Style="position: absolute;
            top: 70px; left: 157px;" ImageUrl="~/Images/Admin_static.png" 
            ToolTip="Click here to create new category and sub category." TabIndex="6" />
            <asp:CheckBox ID="chkNotActive" Style="z-index: 106; left: 767px; top: 437px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="142" 
                Height="17" Visible="False">
            </asp:CheckBox>
            <asp:CheckBox ID="chkOrderAsNeeded" Style="z-index: 107; left: 628px; top: 427px;
                font-family: 'Verdana'; font-size: 8pt; position: absolute;" 
                runat="server" Width="124"
                Height="17" Visible="False"></asp:CheckBox>
            <asp:Label ID="Label2" Style="z-index: 108; left: 2px; top: 11px; font-family: 'Verdana';
                font-size: 8pt; position: absolute; font-weight:bold;"   runat="server" Width="65" Height="13">Category:</asp:Label>
            <asp:DropDownList ID="cboCategory" Style="z-index: 109; left: 71px; top: 8px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="140" 
                Height="21" AutoPostBack="True" TabIndex="1">
            </asp:DropDownList>
            <asp:Label ID="Label10" Style="z-index: 110; left: 218px; top: 11px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;font-weight:bold;" runat="server" Width="91" Height="13">Sub Category:</asp:Label>
            <asp:DropDownList ID="cboSubCategory" Style="z-index: 111; left: 309px; top: 8px;
                font-family: 'Verdana'; font-size: 8pt; position: absolute;" 
                runat="server" Width="140" AutoPostBack="True" Height="19px" TabIndex="2"
                >
            </asp:DropDownList>
            <asp:RadioButton id="ALLNotDead" Text="All (not dead)" runat="server"  
                Style="z-index: 111; left: 490px; top: 8px;
                font-family: 'Verdana'; font-size: 8pt; position: absolute; width: 111px;" 
                Checked="True" GroupName="gpDeadItem"/>
            <asp:RadioButton id="ALLDead" Text="All (with dead)" runat="server"  
                Style="z-index: 111; left: 610px; top: 8px;
                font-family: 'Verdana'; font-size: 8pt; position: absolute; width: 103px;" 
                GroupName="gpDeadItem"/>
                  <asp:RadioButton id="Dead" Text="Dead" runat="server"  Style="z-index: 111; left: 740px; top: 8px;
                font-family: 'Verdana'; font-size: 8pt; position: absolute; width: 51px;" 
                GroupName="gpDeadItem"/>
            <asp:Label ID="Label13" Style="z-index: 112; left: 2px; top: 38px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;font-weight:bold;" runat="server" Width="71" Height="13">Item Desc:</asp:Label>
            <asp:DropDownList ID="cboShopName" Style="z-index: 114; left: 73px; top: 381px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="140" 
                Height="21" AutoPostBack="True" Visible="False">
            </asp:DropDownList>
            <asp:Label ID="Label12" Style="z-index: 115; left: 454px; top: 361px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="71px" 
                Height="13px" Visible="False">Item Type:</asp:Label>
            <asp:DropDownList ID="cboItemType" Style="z-index: 116; left: 525px; top: 358px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="140" 
                Height="21" AutoPostBack="True" Visible="False">
            </asp:DropDownList>
            <asp:Label ID="Label11" Style="z-index: 117; left: 672px; top: 361px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="97px" 
                Height="13px" Visible="False">Sub Item Type:</asp:Label>
            <asp:DropDownList ID="cboSubItemType" Style="z-index: 118; left: 770px; top: 358px;
                font-family: 'Verdana'; font-size: 8pt; position: absolute;" 
                runat="server" Width="140"
                Height="21" Visible="False">
            </asp:DropDownList>
            <asp:Label ID="Label4" Style="z-index: 119; left: 30px; top: 374px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="41px" 
                Height="13px" Visible="False">Shop:</asp:Label>
            <asp:DropDownList ID="cboPartNo" Style="z-index: 120; left: 311px; top: 381px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="140" 
                Height="21" Visible="False">
            </asp:DropDownList>
            <asp:Label ID="Label3" Style="z-index: 121; left: 254px; top: 384px; font-family: 'Verdana';
                font-size: 8pt; position: absolute;" runat="server" Width="54px" 
                Height="13px" Visible="False">Part No:</asp:Label>
            <asp:Panel ID="pnlMfrInfo" Style="z-index: 122; left: 483px; top: 373px; font-family: 'Microsoft Sans Serif';
                font-size: 8pt; position: absolute;" runat="server" Width="437" 
                Height="35" Visible="False">
                <asp:DropDownList ID="cboMfrPartNo" Style="z-index: 123; left: 286px; top: 8px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute;" runat="server" Width="140" Height="21">
                </asp:DropDownList>
                <asp:DropDownList ID="cboMfrName" Style="z-index: 124; left: 42px; top: 8px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute;" runat="server" Width="140" Height="21" AutoPostBack="True"
                    DataTextField="MfrName" DataValueField="MfrID">
                </asp:DropDownList>
                <asp:Label ID="Label9" Style="z-index: 125; left: 210px; top: 11px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute;" runat="server" Width="76px" 
                    Height="13px" Visible="False">Mfr Part No:</asp:Label>
                <asp:Label ID="Label6" Style="z-index: 126; left: 11px; top: 11px; font-family: 'Verdana';
                    font-size: 8pt; position: absolute;" runat="server" Width="30" Height="13">Mfr:</asp:Label>
            </asp:Panel>
            <asp:DropDownList ID="cboDescription" runat="server" Height="19px" Style="z-index: 111; left: 71px; top: 38px;
                font-family: 'Verdana'; font-size: 8pt; position: absolute;" 
                Width="380px"  AutoPostBack="true" TabIndex="3">
            </asp:DropDownList>
            
        </asp:Panel>
        <asp:Panel ID="Panel3" Style="z-index: 133; left: 0px; top: 110px; font-family: 'Microsoft Sans Serif';
            font-size: 8pt; position: absolute;" runat="server" Width="870" Height="370"
            ScrollBars="Auto">
            <asp:GridView ID="dgvSearchResult" SkinID="gridviewSkin" runat="server"
                Style="z-index: 135; left: 6px; top: 6px; font-family: 'Microsoft Sans Serif';
                font-size: 8pt; position: absolute;"  AllowSorting="true"  
                AllowPaging="true" TabIndex="9" >
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:ImageButton ID="btnSelect" runat="server" TabIndex="10" Text="Edit" ImageUrl="~/Images/edit_static.png"
                                CommandName="Edit"  />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%--<asp:TemplateField Visible="false">
                    <ItemTemplate>
                    <asp:Label ID="lblItem" runat="server" Text='<%# Eval("Description") %>'></asp:Label>
                    </ItemTemplate>
                    </asp:TemplateField>--%>
                </Columns>
            </asp:GridView>
        </asp:Panel>
        <asp:CustomValidator ID="cutValidator" runat="server" Display="None"></asp:CustomValidator>
        
       
    </asp:Panel>
</asp:Content>
