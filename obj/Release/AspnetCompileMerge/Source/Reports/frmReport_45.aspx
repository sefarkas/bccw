﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmReport_45.aspx.vb"
    Inherits="frmReport_45" MasterPageFile="~/Master/Inventory.Master" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<%--    <script type="text/javascript">
        // test auto-ready logic - call corner before DOM is ready
        $('#ContentPlaceHolder1_pnlSearchCriteria').corner();
    </script>--%>
    <asp:Panel ID="Panel1" runat="server" Style="z-index: 105; left: 15px; top: 1px; font-family: 'Microsoft Sans Serif';
        font-size: 8pt; position: relative;" Width="876" Height="500"  >
        <asp:Panel ID="pnBody" runat="server" SkinID="panelBody" 
            HorizontalAlign="Center" Width="876px" >
            <div>
                <%--    <input type="button" value="Back" onclick="goBack()" />--%>
                <rsweb:ReportViewer ID="ReportViewer1"  runat="server" Width="850px" Height="480px" >
                </rsweb:ReportViewer>
            </div>
            <script type="text/javascript">
                function goBack() {
                    window.history.back()
                }
            </script>
        </asp:Panel>
    </asp:Panel>
</asp:Content>
