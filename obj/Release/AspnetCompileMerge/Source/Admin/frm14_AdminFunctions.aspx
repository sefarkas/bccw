<%@ Page Language="VB" CodeBehind="frm14_AdminFunctions.aspx.vb" AutoEventWireup="false"
    Inherits="frm14_AdminFunctions" MasterPageFile="~/Master/Inventory.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<%--    <script type="text/javascript">
        // test auto-ready logic - call corner before DOM is ready
        $('#ContentPlaceHolder1_pnlDetail').corner();
    </script>--%>
    <asp:Panel ID="pnBody" Style="z-index: 105; left: 45px; top: 130px; font-family: 'Microsoft Sans Serif';
        font-size: 8pt; position: relative;" runat="server" Width="950" Height="350" DefaultButton="btnOK">
     <%--   <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>--%>
                <asp:Panel ID="pnlDetail" Style="z-index: 105; left: 0px; top: 1px; font-family: 'Microsoft Sans Serif';
                    font-size: 8pt; position: relative; height: 125px;" 
            SkinID="panelSearch" runat="server"
                    Width="469" BorderStyle="None">
                    <asp:DropDownList ID="cboRefField" Style="z-index: 103; left: 121px; top: 41px; font-family: 'Verdana';
                        font-size: 8pt; position: absolute;" runat="server" Width="250px" 
                        Height="21" TabIndex="3">
                    </asp:DropDownList>
                    <asp:RadioButton ID="rbtAddSubCategory" Style="z-index: 104; left: 156px; top: 11px;
                        font-family: 'Verdana'; font-size: 8pt; position: absolute; width: 120px;" runat="server"
                        Height="17" Text="Sub Category" AutoPostBack="True" GroupName="grpItem" 
                        Font-Bold="True" TabIndex="2"></asp:RadioButton>
                    <asp:ImageButton ID="btnOK" SkinID="SaveButton" Style="z-index: 106; left: 101px;
                        top: 100px; font-family: 'Trebuchet MS'; font-size: 8pt; position: absolute;"
                        runat="server" Text="Ok" TabIndex="5"></asp:ImageButton>
                    <asp:ImageButton ID="btnCancel" SkinID="CancelButton" Style="z-index: 107; left: 203px;
                        top: 101px; font-family: 'Trebuchet MS'; font-size: 8pt; position: absolute;"
                        runat="server" Text="Cancel" TabIndex="6"></asp:ImageButton>
                    <asp:RadioButton ID="rbtAddCategory" Style="z-index: 108; left: 67px; top: 11px;
                        font-family: 'Verdana'; font-size: 8pt; position: absolute; width: 82px;" runat="server"
                        Height="17" Text="Category" AutoPostBack="True" GroupName="grpItem" 
                        Checked="True" Font-Bold="True" TabIndex="1">
                    </asp:RadioButton>
                    <asp:Label ID="Label2" 
                        Style="z-index: 110; left: 20px; top: 44px; font-family: 'Verdana';
                        font-size: 8pt; position: absolute; width: 90px; font-weight:bold; height: 13px;" 
                        runat="server">
                        <span style="color: Red; font-weight: bolder">*</span>Category:</asp:Label>
                    <asp:Label ID="Label10" Style="z-index: 111; left: 19px; top: 77px; font-family: 'Verdana';
                        font-size: 8pt; position: absolute;font-weight:bold; width: 100px;" 
                        runat="server" Height="13">
                        <span style="color: Red; font-weight: bolder">*</span>Sub Category:</asp:Label>
                    <asp:Panel ID="Panel1" Style="z-index: 112; left: 17px; top: 33px; font-family: 'Microsoft Sans Serif';
                        font-size: 8pt; position: absolute; width: 436px; height: 68px;" 
                        runat="server">
                    </asp:Panel>
                    <asp:TextBox ID="txtFieldToAdd" Style="z-index: 112; left: 121px; top: 72px; font-family: 'Verdana';
                        font-size: 8pt; position: absolute; width: 244px; height: 16px;" runat="server" 
                        TabIndex="4"></asp:TextBox>
                    <asp:CustomValidator ID="cutValidator" runat="server" Display="None"></asp:CustomValidator>
                </asp:Panel>
     <%--       </ContentTemplate>
        </asp:UpdatePanel>--%>
    </asp:Panel>
</asp:Content>
