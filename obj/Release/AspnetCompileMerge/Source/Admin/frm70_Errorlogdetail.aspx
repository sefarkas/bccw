﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frm70_Errorlogdetail.aspx.vb"
    Inherits=".frm70_Errorlogdetail" %>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">--%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <style type="text/css">
        .style3
        {
            width: 195px;
        }
        .style4
        {
            width: 51px;
        }
        .style5
        {
            width: 534px;
        }
        .style7
        {
            width: 792px;
        }
        .style9
        {
            width: 89px;
        }
        .style12
        {
            width: 147px;
        }
    </style>
    <%--</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">--%>
    <asp:Panel ID="pnBody" Style="z-index: 105; left: 45px; top: 130px; font-family: 'Microsoft Sans Serif';
        font-size: 8pt; position: relative; width: 872px;" runat="server" Height="390">
        <asp:Panel ID="Panel1" Style="z-index: 105; left: 0px; top: 1px; font-family: 'Microsoft Sans Serif';
            font-size: 8pt; position: relative; width: 872px;" runat="server" Height="300"
            SkinID="panelSearch">
            <table>
                <tr>
                    <td class="style9">
                        <asp:Label ID="lblUser" runat="server" Font-Size="12px" Text="User Id :"></asp:Label>
                    </td>
                    <td class="style3">
                        <asp:Label ID="txtUser" runat="server"></asp:Label>
                    </td>
                    <td class="style4">
                        <asp:Label ID="lblDate" runat="server" Font-Size="12px" Text="Date :"></asp:Label>
                    </td>
                    <td class="style5">
                        <asp:Label ID="txtDate" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td class="style12">
                        <asp:Label ID="lblurl" runat="server" Font-Size="12px" Text="URL :"></asp:Label>
                    </td>
                    <td class="style7">
                        <asp:Label ID="txturl" runat="server" Width="250px"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style12">
                        <asp:Label ID="lblErrormsg" runat="server" Font-Size="12px" Text="Error Message :"></asp:Label>
                    </td>
                    <td class="style7">
                        <asp:Label ID="txtErrormsg" runat="server" Width="763px" Height="28px"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style12">
                        <asp:Label ID="lblStacktrace" runat="server" Font-Size="12px" Text="Stack Trace :"></asp:Label>
                    </td>
                    <td class="style7">
                        <asp:Label ID="txtStacktrace" runat="server" Width="761px" Height="218px"></asp:Label>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:ImageButton ID="btnCancel" runat="server" ImageUrl="~/Images/cancel_static.png"
            Height="20px" Style="margin-left: 139px; margin-top: 9px" Width="100px"></asp:ImageButton>
    </asp:Panel>
    <%--    </asp:Content>--%>
    </form>
</body>
</html>
